del /Q /A Collider.suo

del /Q /S SpriteEffectsPipeline\*.cachefile
del /Q /S SpriteEffectsPipeline\*.user
del /Q /S SpriteEffectsPipeline\bin\*.*
del /Q /S SpriteEffectsPipeline\obj\*.*
rd /Q /S SpriteEffectsPipeline\bin
rd /Q /S SpriteEffectsPipeline\obj

del /Q /S Collider\*.cachefile
del /Q /S Collider\*.user
del /Q /S Collider\bin\*.*
del /Q /S Collider\obj\*.*
rd /Q /S Collider\bin
rd /Q /S Collider\obj

del /Q /S Collider\Content\bin\*.*
del /Q /S Collider\Content\obj\*.*
rd /Q /S Collider\Content\bin
rd /Q /S Collider\Content\obj
