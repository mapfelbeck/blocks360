#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Collider
{
    public enum ShapeType
    {
        Circle,
        Rectangle,
        Unknown,
    }

    abstract public class ColliderObject : IQuadTreeItem<ColliderObject>
    {
        protected AABB aabb;
        public AABB AABB
        {
            get { return aabb; }
        }

        UpdateItemDelegate<ColliderObject> updateDelegate;
        public UpdateItemDelegate<ColliderObject> UpdateDelegate
        {
            get { return updateDelegate; }
            set { updateDelegate = value; }
        }
        DeleteItemDelegate<ColliderObject> deleteDelegate;
        public DeleteItemDelegate<ColliderObject> DeleteDelegate
        {
            get { return deleteDelegate; }
            set { deleteDelegate = value; }
        }

        protected ShapeType type;
        public ShapeType Type
        {
            get { return type; }
        }

        protected bool isStatic = false;
        public bool IsStatic
        {
            get { return isStatic; }
        }

        protected Vector2 position = Vector2.Zero;
        public Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
                UpdateAABB();
            }
        }

        protected Vector2 velocity = Vector2.Zero;
        public Vector2 Velocity
        {
            get { return velocity; }
            set
            {
                if ((value.X == Single.NaN) || (value.Y == Single.NaN))
                {
                    throw new ArgumentException("Velocity was NaN");
                }
                velocity = value;
            }
        }

        protected float rotation = 0f;
        public float Rotation
        {
            get { return rotation; }
            set
            {
                rotation = value;
                UpdateAABB();
            }
        }

        protected float radius = 1f;
        public float Radius
        {
            get { return radius; }
            set
            {
                radius = value;
                UpdateAABB();
            }
        }

        protected float mass = 1f;
        public float Mass
        {
            get { return mass; }
        }

        protected bool collidedThisFrame = false;
        public bool CollidedThisFrame
        {
            get { return collidedThisFrame; }
            set { collidedThisFrame = value; }
        }

        public ColliderObject()
        {
            type = ShapeType.Unknown;
        }

        public virtual void Update(float elapsedTime)
        {
            collidedThisFrame = false;
            
            UpdateAABB();
            if (updateDelegate != null)
            {
                updateDelegate(this);
            }
        }

        public virtual void UpdateAABB()
        {
            if (aabb != null)
            {
                aabb.Set(Position - new Vector2(radius), Position + new Vector2(radius));
            }
            else
            {
                aabb = new AABB(Position - new Vector2(radius), Position + new Vector2(radius));
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch) { }

        public virtual bool CircleCollision(ColliderObject otherObject, Vector2 movement, ref CollisionResult result)
        {
            throw new NotImplementedException();
        }

        public virtual bool RectangleCollision(ColliderObject otherObject, ref CollisionResult result)
        {
            throw new NotImplementedException();
        }
    }
}