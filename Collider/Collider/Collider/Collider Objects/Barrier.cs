﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;

namespace Collider
{
    public class Barrier : ColliderObject
    {
        Rectangle rectangle;

        const float massRadiusRatio = 0.5f;
        const float dragPerSecond = 0.15f;
        const float velocityMassRatioToRotationScalar = 0.0017f;

        public Texture2D texture;
        public Color color;

        public Barrier(Texture2D theTexture, Rectangle rectangle, Color color):base()
        {
            type = ShapeType.Rectangle;
            this.rectangle = rectangle;
            this.color = color;
            texture = theTexture;
            position = new Vector2(rectangle.Center.X, rectangle.Center.Y);
            velocity = Vector2.One * 200f;
            radius = Math.Max(rectangle.Height / 2, rectangle.Width / 2);
            mass = rectangle.Height * rectangle.Width;
            isStatic = true;
            UpdateAABB();
        }

        public override void Update(float elapsedTime)
        {
            base.Update(elapsedTime);

            float velocityMassRatio = (Velocity.LengthSquared() / Mass);
            rotation += velocityMassRatio * velocityMassRatioToRotationScalar *
                elapsedTime;

            Velocity -= Velocity * (elapsedTime * dragPerSecond);

        }

        public override void UpdateAABB()
        {
            aabb = new AABB(rectangle);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Draw(texture, rectangle, color);
        }
    }
}