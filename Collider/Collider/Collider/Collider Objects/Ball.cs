﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;

namespace Collider
{
    public class Ball : ColliderObject
    {
        const float massRadiusRatio = 0.5f;
        const float dragPerSecond = 0.15f;
        const float velocityMassRatioToRotationScalar = 0.0017f;

        public Texture2D texture;
        public Vector2 center;
        public Color color;

        public Ball(Texture2D theTexture, Vector2 theLocation, Vector2 theVelocity, Color color):base()
        {
            type = ShapeType.Circle;
            this.color = color;
            texture = theTexture;
            center = new Vector2(texture.Height / 2);
            radius = center.X;
            position = theLocation;
            velocity = theVelocity;
            mass = this.radius * massRadiusRatio;
            UpdateAABB();
        }

        public override void Update(float elapsedTime)
        {
            float velocityMassRatio = (Velocity.LengthSquared() / Mass);
            rotation += velocityMassRatio * velocityMassRatioToRotationScalar *
                elapsedTime;

            Velocity -= Velocity * (elapsedTime * dragPerSecond);

            base.Update(elapsedTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.Draw(texture, Position, null, color, Rotation,
                    center, Vector2.One, SpriteEffects.None, 0f);
        }

        public override bool CircleCollision(ColliderObject checkActor, Vector2 movement,
            ref CollisionResult collisionResult)
        {
            bool result = false;
            float movementLength = movement.Length();

            // calculate the target vector
            float combinedRadius = checkActor.Radius + Radius;
            Vector2 checkVector = checkActor.Position - Position;
            float checkVectorLength = checkVector.Length();
            if (checkVectorLength <= 0f)
            {
                return result;
            }

            float distanceBetween = MathHelper.Max(checkVectorLength -
                (checkActor.Radius + Radius), 0);

            // check if they could possibly touch no matter the direction
            if (movementLength < distanceBetween)
            {
                return result;
            }

            // determine how much of the movement is bringing the two together
            float movementTowards = Vector2.Dot(movement, checkVector);

            // check to see if the movement is away from each other
            if (movementTowards < 0f)
            {
                return result;
            }

            if (movementTowards < distanceBetween)
            {
                return result;
            }

            result = true;
            collisionResult.Distance = distanceBetween;
            collisionResult.Normal = Vector2.Normalize(checkVector);
            collisionResult.CollisionObject = checkActor;

            return result;
        }

        /*
         CollisionMath.CircleRectangleCollide(
         this[i].Position,
         this[i].Radius,
         this.barriers[b], ref result);*/
        /*public static bool CircleRectangleCollide(Vector2 center, float radius,
            Rectangle rectangle, ref CircleLineCollisionResult result)*/
        public override bool RectangleCollision(ColliderObject checkActor, ref CollisionResult collisionResult)
        {
            bool result = false;
            float xVal = position.X;
            Rectangle rectangle = checkActor.AABB.Rectangle;

            if (xVal < rectangle.Left) xVal = rectangle.Left;
            if (xVal > rectangle.Right) xVal = rectangle.Right;

            float yVal = position.Y;
            if (yVal < rectangle.Top) yVal = rectangle.Top;
            if (yVal > rectangle.Bottom) yVal = rectangle.Bottom;

            Vector2 direction = new Vector2(position.X - xVal, position.Y - yVal);
            float distance = direction.Length();

            if ((distance > 0) && (distance < radius))
            {
                result = true;
                collisionResult.Distance = radius - distance;
                collisionResult.Normal = Vector2.Normalize(direction);
            }
            else
            {
                result = false;
            }
            if (checkActor.IsStatic)
            {
                collisionResult.CollisionObject = null;
            }
            else
            {
                collisionResult.CollisionObject = checkActor;
            }

            return result;
        }
    }
}