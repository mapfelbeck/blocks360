﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Collider
{
    class PlayerShot : Ball
    {
        int collisionCount;
        public int CollisionCount
        {
            get { return collisionCount; }
            set { collisionCount = value; }
        }
        const int collisionCountMax = 3;
        const float fadeTimeMax = 4f;
        float fadeTime;

        const float lifeTimeMax = 15f;
        float lifeTime;

        public PlayerShot(Texture2D theTexture, Vector2 theLocation, Vector2 theVelocity, Color color)
            : base(theTexture, theLocation, theVelocity, color)
        {
            fadeTime = fadeTimeMax;
            lifeTime = 0f;
        }

        public override void Update(float elapsedTime)
        {
            lifeTime += elapsedTime;

            if (lifeTime >= lifeTimeMax)
            {
                DeleteDelegate(this);
            }
            else
            {
                if (CollidedThisFrame)
                {
                    collisionCount++;
                }
                if (CollisionCount >= collisionCountMax)
                {
                    fadeTime = Math.Max(fadeTime - elapsedTime, 0f);
                    color.A = (byte)((fadeTime / fadeTimeMax) * 128 + 128);
                    if (color.A <= 128)
                    {
                        DeleteDelegate(this);
                    }
                }
            }

            base.Update(elapsedTime);
        }
    }
}