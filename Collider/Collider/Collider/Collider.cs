#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
#endregion

namespace Collider
{
    public struct CollisionResult
    {
        public float Distance;
        public Vector2 Normal;
        public ColliderObject CollisionObject;

        public static int Compare(CollisionResult a, CollisionResult b)
        {
            return a.Distance.CompareTo(b.Distance);
        }
    }

    public class Collider : QuadTree<ColliderObject>
    {
        public QuadTree<ColliderObject> Collection
        {
            get { return this as QuadTree<ColliderObject>; }
        }

        private Rectangle dimensions;
        public Rectangle Dimensions
        {
            get
            {
                return dimensions;
            }
            set
            {
                dimensions = value;
            }
        }

        /// <summary>
        /// Cached list of collision results, for more optimal collision detection.
        /// </summary>
        List<CollisionResult> collisionResults = new List<CollisionResult>();

        public Collider(Rectangle colliderRectangle)
            :base(new AABB(colliderRectangle)){
                dimensions = colliderRectangle;
        }

        /// <summary>
        /// Update the collision system.
        /// </summary>
        /// <param name="elapsedTime">The amount of elapsed time, in seconds.</param>
        public void Update(float elapsedTime)
        {
            // move each object
            for (int i = 0; i < Count; ++i)
            {
                if (!this[i].IsStatic)
                {
                    // determine how far they are going to move
                    Vector2 movement = this[i].Velocity * elapsedTime;
                    if (this[i].CollidedThisFrame == false)
                    {
                        movement = MoveAndCollide(this[i], movement);
                    }
                    this[i].Position += movement;
                }
            }
            
            Collection.ApplyPendingRemovals();
        }

        private Vector2 MoveAndCollide(ColliderObject colliderObject,
            Vector2 movement)
        {
            if (colliderObject == null)
            {
                throw new ArgumentNullException("colliderObject");
            }

            if (movement.LengthSquared() <= 0f)
            {
                return movement;
            }

            Collide(colliderObject, movement);

            if (this.collisionResults.Count > 0)
            {
                this.collisionResults.Sort(CollisionResult.Compare);
                foreach (CollisionResult collision in this.collisionResults)
                {
                    if (collision.CollisionObject != null)
                    {
                        colliderObject.CollidedThisFrame =
                            collision.CollisionObject.CollidedThisFrame = true;
                        AdjustVelocities(colliderObject, collision.CollisionObject);
                        return Vector2.Zero;
                    }
                    else
                    {
                        colliderObject.CollidedThisFrame = true;
                        float vn = Vector2.Dot(colliderObject.Velocity,
                            collision.Normal);
                        colliderObject.Velocity -= (2.0f * vn) *
                            collision.Normal;
                        colliderObject.Position += collision.Normal *
                            collision.Distance;
                        return Vector2.Zero;
                    }
                }
            }

            return movement;
        }


        /// <summary>
        /// Determine all collisions that will happen as the given gameplayObject moves.
        /// </summary>
        /// <param name="colliderObject">The ColliderObject that is moving.</param>
        /// <param name="movement">The gameplayObject's movement vector.</param>
        /// <remarks>The results are stored in the cached list.</remarks>
        public void Collide(ColliderObject colliderObject, Vector2 movement)
        {
            this.collisionResults.Clear();

            if (colliderObject == null)
            {
                throw new ArgumentNullException("colliderObject");
            }

            // determine the movement direction and scalar
            float movementLength = movement.Length();
            if (movementLength <= 0f)
            {
                return;
            }

            // check each collisionObject
            //switch collision types here
            foreach (ColliderObject checkActor in CollidesWith(colliderObject))
            {
                if (colliderObject == checkActor)
                {
                    continue;
                }

                bool collided = false;
                CollisionResult result = new CollisionResult();

                switch (checkActor.Type)
                {
                    case ShapeType.Circle:
                        collided = colliderObject.CircleCollision(checkActor, movement, ref result);
                        break;
                    case ShapeType.Rectangle:
                        collided = colliderObject.RectangleCollision(checkActor, ref result);
                        break;
                    case ShapeType.Unknown:
                        throw new Exception("Unknown object type");
                        //break;
                    default:
                        break;
                }
                if (collided)
                {
                    collisionResults.Add(result);
                }
            }
        }

        private IEnumerable<ColliderObject> CollidesWith(ColliderObject colliderObject)
        {
            return Query(colliderObject.AABB);
        }

        /// <summary>
        /// Adjust the velocities of the two collisionManager as if they have collided,
        /// distributing their velocities according to their masses.
        /// </summary>
        /// <param name="actor1">The first gameplayObject.</param>
        /// <param name="actor2">The second gameplayObject.</param>
        private void AdjustVelocities(ColliderObject actor1,
            ColliderObject actor2)
        {
            // don't adjust velocities if at least one has negative mass
            if ((actor1.Mass <= 0f) || (actor2.Mass <= 0f))
            {
                return;
            }

            // determine the vectors normal and tangent to the collision
            Vector2 collisionNormal = actor2.Position - actor1.Position;
            if (collisionNormal.LengthSquared() > 0f)
            {
                collisionNormal.Normalize();
            }
            else
            {
                return;
            }

            Vector2 collisionTangent = new Vector2(
                -collisionNormal.Y, collisionNormal.X);

            // determine the velocity components along the normal and tangent vectors
            float velocityNormal1 = Vector2.Dot(actor1.Velocity, collisionNormal);
            float velocityTangent1 = Vector2.Dot(actor1.Velocity, collisionTangent);
            float velocityNormal2 = Vector2.Dot(actor2.Velocity, collisionNormal);
            float velocityTangent2 = Vector2.Dot(actor2.Velocity, collisionTangent);

            // determine the new velocities along the normal
            float velocityNormal1New = ((velocityNormal1 * (actor1.Mass - actor2.Mass))
                + (2f * actor2.Mass * velocityNormal2)) / (actor1.Mass + actor2.Mass);
            float velocityNormal2New = ((velocityNormal2 * (actor2.Mass - actor1.Mass))
                + (2f * actor1.Mass * velocityNormal1)) / (actor1.Mass + actor2.Mass);

            // determine the new total velocities
            actor1.Velocity = (velocityNormal1New * collisionNormal) +
                (velocityTangent1 * collisionTangent);
            actor2.Velocity = (velocityNormal2New * collisionNormal) +
                (velocityTangent2 * collisionTangent);
        }
    }
}