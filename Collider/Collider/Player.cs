﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Collider
{
    public class Player
    {
        public struct KeyMap
        {
            public Keys ShootKey;
            public Keys UpKey;
            public Keys DownKey;

            public KeyMap(Keys Shoot, Keys Up, Keys Down)
            {
                ShootKey = Shoot;
                UpKey = Up;
                DownKey = Down;
            }
        }

        string playerName;
        public string PlayerName
        {
            get { return playerName; }
        }

        PlayerIndex index;
        public PlayerIndex Index
        {
            get { return index; }
        }

        public virtual PlayerType Type
        {
            get { return PlayerType.Human; }
        }

        float rotation;
        public float Rotation
        {
            get { return rotation; }
        }

        SpriteFont font;
        public SpriteFont Font
        {
            get { return font; }
        }

        Texture2D scoreTexture;
        public Texture2D ScoreTexture
        {
            get { return scoreTexture; }
            set { scoreTexture = value; }
        }

        const int winScore = 3;
        int playerScore;
        public int PlayerScore
        {
            get { return playerScore; }
            set { playerScore = value; }
        }

        public bool PlayerWon
        {
            get { return playerScore >= winScore; }
        }

        Vector2 scoreLocation;
        public Vector2 ScoreLocation
        {
            get { return scoreLocation; }
            set { scoreLocation = value; }
        }

        Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        Texture2D texture;
        public Texture2D Texture
        {
            get { return texture; }
            set
            {
                texture = value;
                center.X = texture.Width / 2;
                center.Y = texture.Height / 2;
            }
        }

        Color playerColor;
        public Color PlayerColor
        {
            get { return playerColor; }
            set { playerColor = value; }
        }

        Vector2 center;
        public Vector2 Center
        {
            get { return center; }
        }

        KeyboardState oldKeyState;

        protected float upperBound;
        protected float lowerBound;

        Vector2 playerShootDirection;
        public Vector2 PlayerShootDirection
        {
            get { return playerShootDirection; }
        }

        protected bool playerShot;
        public bool PlayerShot
        {
            get { return playerShot; }
        }

        protected float moveAmountMax;
        protected float moveAmount;

        KeyMap controls;

        Rectangle bounds;

        public Player(PlayerIndex whichPlayer, Color playerColor)
        {
            index = whichPlayer;

            center = Vector2.Zero;
            moveAmountMax = 250f;
            this.playerColor = playerColor;

            switch (index)
            {
                case PlayerIndex.One:
                    rotation = 0;
                    controls = new KeyMap(Keys.Space, Keys.Up, Keys.Down);
                    playerShootDirection = Vector2.UnitX;
                    playerName = "Player 1";
                    break;
                case PlayerIndex.Two:
                    rotation = MathHelper.Pi;
                    controls = new KeyMap(Keys.O, Keys.P, Keys.L);
                    playerShootDirection = -Vector2.UnitX;
                    playerName = "Player 2";
                    break;
                default:
                    throw new ArgumentException("Only player one and two supported");
            }

            playerShot = false;
        }

        public virtual void HandleInput(InputState input)
        {
            int playerIndex = (int)index;
            KeyboardState newKeyState = input.CurrentKeyboardStates[playerIndex];

            moveAmount = 0f;
            playerShot = false;

            if (KeyPressed(oldKeyState, newKeyState, controls.ShootKey))
            {
                playerShot = true;
            }

            if (KeyHeld(oldKeyState, newKeyState, controls.UpKey))
            {
                moveAmount -= moveAmountMax;
            }

            if (KeyHeld(oldKeyState, newKeyState, controls.DownKey))
            {
                moveAmount += moveAmountMax;
            }

            oldKeyState = newKeyState;
        }

        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/ScoringFont");
            texture = content.Load<Texture2D>("Triangle");
            center = new Vector2(texture.Width / 2, texture.Height / 2);

            upperBound = bounds.Top + center.Y;
            lowerBound = bounds.Bottom - center.Y;

            switch (index)
            {
                case PlayerIndex.One:
                    position.X = bounds.Left + center.X;
                    break;
                case PlayerIndex.Two:
                    position.X = bounds.Right - center.X;
                    break;
                default:
                    break;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Vector2 stringSize = font.MeasureString(playerName);
            spriteBatch.Draw(Texture, Position, null, PlayerColor, Rotation, Center, 1f, SpriteEffects.None, 0f);
            spriteBatch.DrawString(font, PlayerName, scoreLocation + Vector2.One, Color.Black, 0f, stringSize / 2, 1f, SpriteEffects.None, 0);
            spriteBatch.DrawString(font, PlayerName, scoreLocation, Color.White, 0f, stringSize / 2, 1f, SpriteEffects.None, 0);

            Rectangle scoreIconRect = new Rectangle((int)(scoreLocation.X - stringSize.X / 2), (int)(scoreLocation.Y + stringSize.Y / 2), 20, 20);
            for (int i = 0; i < playerScore; i++)
            {
                spriteBatch.Draw(scoreTexture, scoreIconRect, playerColor);
                scoreIconRect.X = scoreIconRect.X + 25;
            }
        }

        public virtual void Update(float elapsedTime)
        {
            position.Y = MathHelper.Clamp(position.Y + moveAmount * elapsedTime, upperBound, lowerBound);
        }

        private bool KeyPressed(KeyboardState oldState, KeyboardState newState, Keys key)
        {
            return oldState.IsKeyUp(key) && newState.IsKeyDown(key);
        }

        private bool KeyHeld(KeyboardState oldState, KeyboardState newState, Keys key)
        {
            return oldState.IsKeyDown(key) && newState.IsKeyDown(key);
        }

        public void SetBounds(Rectangle bounds)
        {
            this.bounds = bounds;
            upperBound = bounds.Top;
            lowerBound = bounds.Bottom;
        }
    }
}