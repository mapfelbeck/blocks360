﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Collider
{
    class OptionMenuEntry : MenuEntry
    {
        public OptionMenuEntry(string text)
            : base(text)
        {
            selectCue = "SliderMove";
        }

        public OptionMenuEntry(string text, MenuEntryAlignment align) :
            base(text, align)
        {
            selectCue = "SliderMove";
        }

        protected internal override void OnIncrementEntry(PlayerIndex playerIndex)
        {
            base.OnIncrementEntry(playerIndex);
        }

        protected internal override void OnDecrementEntry(PlayerIndex playerIndex)
        {
            base.OnDecrementEntry(playerIndex);
        }
    }
}