﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Collider
{
    class SpriteMenuEntry : MenuEntry
    {
        Texture2D menuTexture;
        public Texture2D MenuTexture
        {
            get { return menuTexture; }
            set { menuTexture = value; }
        }

        public SpriteMenuEntry(string text)
            : base(text, MenuEntryAlignment.Left)
        {
        }

        public SpriteMenuEntry(string text, MenuEntryAlignment align)
            : base(text, align)
        {
        }

        public override void Draw(MenuScreen screen, Vector2 position, Vector2 offset, bool isSelected, GameTime gameTime)
        {
            if (menuTexture == null)
            {
                throw new NullReferenceException("SpriteMenuEntry has null texture");
            }
            // Draw the selected entry in yellow, otherwise white.
            //Color color = isSelected ? Color.Yellow : Color.White;
            Color color = Color.White;

            // Draw text, centered on the middle of each line.
            ScreenManager screenManager = screen.ScreenManager;
            SpriteBatch spriteBatch = screenManager.SpriteBatch;
            SpriteFont font = screenManager.Font;

            switch (alignment)
            {
                case MenuEntryAlignment.Center:
                    Vector2 transform = new Vector2();
                    //transform.X = ((GraphicsConfig.GraphicsDevice.Viewport.Width) / 2);
                    //FIXME
                    transform.X = (800 / 2);
                    transform.X -= menuTexture.Width / 2;
                    position.X += transform.X - position.X;
                    position += offset;
                    break;
                case MenuEntryAlignment.Left:
                    position += offset;
                    break;
                default:
                    break;
            }

            // Pulsate the size of the selected menu entry.
            //double time = gameTime.TotalGameTime.TotalSeconds;

            float scale = 1 + 0.1f * selectionFade;

            // Modify the alpha to fade text out during transitions.
            color = new Color(color.R, color.G, color.B, screen.TransitionAlpha);

            Vector2 origin = new Vector2(0, menuTexture.Height/2);

            spriteBatch.Draw(menuTexture, position, null, color, 0f, origin, scale, SpriteEffects.None, 0f);
            /*spriteBatch.DrawString(font, text, new Vector2(position.X - 1f, position.Y - 1f), Color.Black, 0,
                                   origin, scale, SpriteEffects.None, 0);*/
        }
        
        public override int GetHeight(MenuScreen screen)
        {
            if (menuTexture != null)
            {
                return menuTexture.Height;
            }
            else
            {
                return 0;
            }
        }
    }
}