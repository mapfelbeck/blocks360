﻿using System;

namespace Collider
{
    class GamePadMessageBoxScreen:MessageBoxScreen
    {
        public GamePadMessageBoxScreen(string message)
            : this(message, true)
        { }

        public GamePadMessageBoxScreen(string message, bool includeUsageText)
            : base(message, "A button: Ok", "B button: Cancel")
        {
        }
    }
}
