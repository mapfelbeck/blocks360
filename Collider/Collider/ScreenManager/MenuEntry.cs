#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Collider
{
    public enum MenuEntryAlignment
    {
        Left,
        Center,
    };

    /// <summary>
    /// Helper class represents a single entry in a MenuScreen. By default this
    /// just draws the entry text string, but it can be customized to display menu
    /// entries in different ways. This also provides an event that will be raised
    /// when the menu entry is selected.
    /// </summary>
    class MenuEntry
    {
        /// <summary>
        /// The text rendered for this entry.
        /// </summary>
        protected string text;

        protected string selectCue;

        protected MenuEntryAlignment alignment;

        /// <summary>
        /// Tracks a fading selection effect on the entry.
        /// </summary>
        /// <remarks>
        /// The entries transition out of the selection effect when they are deselected.
        /// </remarks>
        protected float selectionFade;

        /// <summary>
        /// Gets or sets the text of this menu entry.
        /// </summary>
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        string detailText;
        public string DetailText
        {
            get { return detailText; }
            set { detailText = value; }
        }

        /// <summary>
        /// Event raised when the menu entry is selected.
        /// </summary>
        public event EventHandler<PlayerIndexEventArgs> Selected;
        public event EventHandler<PlayerIndexEventArgs> IncrementEntry;
        public event EventHandler<PlayerIndexEventArgs> DecrementEntry;


        /// <summary>
        /// Method for raising the Selected event.
        /// </summary>
        protected internal virtual void OnSelectEntry(PlayerIndex playerIndex)
        {
            if (Selected != null)
            {
                Selected(this, new PlayerIndexEventArgs(playerIndex));
            }
        }

        protected internal virtual void OnIncrementEntry(PlayerIndex playerIndex)
        {
            if (IncrementEntry != null)
            {
                IncrementEntry(this, new PlayerIndexEventArgs(playerIndex));
            }
        }

        protected internal virtual void OnDecrementEntry(PlayerIndex playerIndex)
        {
            if (DecrementEntry != null)
            {
                DecrementEntry(this, new PlayerIndexEventArgs(playerIndex));
            }
        }

        /// <summary>
        /// Constructs a new menu entry with the specified text.
        /// </summary>
        public MenuEntry(string text)
        {
            this.text = text;
            alignment = MenuEntryAlignment.Left;
            selectCue = "MenuAdvance";
        }

        /// <summary>
        /// Constructs a new menu entry with the specified text.
        /// </summary>
        public MenuEntry(string text, MenuEntryAlignment align)
        {
            this.text = text;
            alignment = align;
            selectCue = "MenuAdvance";
        }

        /// <summary>
        /// Updates the menu entry.
        /// </summary>
        public virtual void Update(MenuScreen screen, bool isSelected,
                                                      GameTime gameTime)
        {
            // When the menu selection changes, entries gradually fade between
            // their selected and deselected appearance, rather than instantly
            // popping to the new state.
            float fadeSpeed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (isSelected)
                selectionFade = Math.Min(selectionFade + fadeSpeed*4, 1);
            else
                selectionFade = Math.Max(selectionFade - fadeSpeed*3, 0);
        }


        /// <summary>
        /// Draws the menu entry. This can be overridden to customize the appearance.
        /// </summary>
        public virtual void Draw(MenuScreen screen, Vector2 position, Vector2 offset,
                                 bool isSelected, GameTime gameTime)
        {
            Color drawColor;
            Color shadowColor = Color.Black;
            if (isSelected)
            {
                drawColor = Color.Yellow;
            }
            else
            {
                drawColor = Color.White;
            }

            // Draw text, centered on the middle of each line.
            ScreenManager screenManager = screen.ScreenManager;
            SpriteBatch spriteBatch = screenManager.SpriteBatch;
            SpriteFont font = screenManager.Font;

            switch (alignment)
            {
                case MenuEntryAlignment.Center:
                    Vector2 transform = new Vector2();
                    //transform.X = ((GraphicsConfig.GraphicsDevice.Viewport.Width) / 2);
                    //FIXME
                    transform.X = (800 / 2);
                    transform.X -= font.MeasureString(text).X / 2;
                    position.X += transform.X - position.X;
                    position += offset;
                    break;
                case MenuEntryAlignment.Left:
                    position += offset;
                    break;
                default:
                    break;
            }
            
            // Pulsate the size of the selected menu entry.
            //double time = gameTime.TotalGameTime.TotalSeconds;
            
            float scale = 1 + 0.1f * selectionFade;

            // Modify the alpha to fade text out during transitions.
            drawColor = new Color(drawColor.R, drawColor.G, drawColor.B, screen.TransitionAlpha);
            shadowColor.A = (byte)(Math.Pow((double)screen.TransitionAlpha / (double)255, 2) * 255);

            Vector2 origin = new Vector2(0, font.LineSpacing / 2);

            spriteBatch.DrawString(font, text, new Vector2(position.X - 1f, position.Y - 1f), shadowColor, 0,
                                   origin, scale, SpriteEffects.None, 0);
            spriteBatch.DrawString(font, text, position, drawColor, 0,
                                   origin, scale, SpriteEffects.None, 0);
        }


        /// <summary>
        /// Queries how much space this menu entry requires.
        /// </summary>
        public virtual int GetHeight(MenuScreen screen)
        {
            return screen.ScreenManager.Font.LineSpacing;
        }
    }
}
