#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Collider
{
    /// <summary>
    /// Base class for screens that contain a menu of options. The user can
    /// move up and down to select an entry, or cancel to back out of the screen.
    /// </summary>
    abstract class MenuScreen : GameScreen
    {
        List<MenuEntry> menuEntries = new List<MenuEntry>();
        int selectedEntry = 0;
        string menuTitle;
        public string MenuTitle
        {
            get { return menuTitle; }
        }

        string menuSubTitle;
        public string MenuSubTitle
        {
            get { return menuSubTitle; }
            set { menuSubTitle = value; }
        }

        protected string backCue;

        protected Vector2 titlePositionAdjust;
        protected Vector2 subTitlePositionAdjust;
        protected Vector2 menuPositionAdjust;
        protected Vector2 detailPositionAdjust;
        protected float menuEntrySpacingAdjust;

        /// <summary>
        /// Gets the list of menu entries, so derived classes can add
        /// or change the menu contents.
        /// </summary>
        protected IList<MenuEntry> MenuEntries
        {
            get { return menuEntries; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public MenuScreen(string menuTitle):base()
        {
            this.menuTitle = menuTitle;

            menuSubTitle = "";

            backCue = "MenuBack";

            titlePositionAdjust = Vector2.Zero;
            subTitlePositionAdjust = Vector2.Zero;
            menuPositionAdjust = Vector2.Zero;
            detailPositionAdjust = Vector2.Zero;
            menuEntrySpacingAdjust = 0f;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        /// <summary>
        /// Responds to user input, changing the selected entry and accepting
        /// or cancelling the menu.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            // Move to the previous menu entry?
            if (input.IsMenuUp(ControllingPlayer))
            {
                selectedEntry--;

                if (selectedEntry < 0)
                    selectedEntry = menuEntries.Count - 1;
            }

            // Move to the next menu entry?
            if (input.IsMenuDown(ControllingPlayer))
            {
                selectedEntry++;

                if (selectedEntry >= menuEntries.Count)
                    selectedEntry = 0;
            }

            // Accept or cancel the menu? We pass in our ControllingPlayer, which may
            // either be null (to accept input from any player) or a specific index.
            // If we pass a null controlling player, the InputState helper returns to
            // us which player actually provided the input. We pass that through to
            // OnSelectEntry and OnCancel, so they can tell which player triggered them.
            PlayerIndex playerIndex;

            if (input.IsMenuSelect(ControllingPlayer, out playerIndex))
            {
                OnSelectEntry(selectedEntry, playerIndex);
            }
            else if (input.IsMenuCancel(ControllingPlayer, out playerIndex))
            {
                OnCancel(playerIndex);
            }
            else if (input.IsMenuIncrement(ControllingPlayer, out playerIndex))
            {
                OnIncrementEntry(selectedEntry, playerIndex);
            }
            else if (input.IsMenuDecrement(ControllingPlayer, out playerIndex))
            {
                OnDecrementEntry(selectedEntry, playerIndex);
            }
        }


        /// <summary>
        /// Handler for when the user has chosen a menu entry.
        /// </summary>
        protected virtual void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[selectedEntry].OnSelectEntry(playerIndex);
        }

        protected virtual void OnIncrementEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[selectedEntry].OnIncrementEntry(playerIndex);
        }

        protected virtual void OnDecrementEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[selectedEntry].OnDecrementEntry(playerIndex);
        }


        /// <summary>
        /// Handler for when the user has cancelled the menu.
        /// </summary>
        protected virtual void OnCancel(PlayerIndex playerIndex)
        {
            //EventManager.Trigger(this, "MenuBack");
            ExitScreen();
        }


        /// <summary>
        /// Helper overload makes it easy to use OnCancel as a MenuEntry event handler.
        /// </summary>
        protected void OnCancel(object sender, PlayerIndexEventArgs e)
        {
            OnCancel(e.PlayerIndex);
        }

        /// <summary>
        /// Updates the menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            // Update each nested MenuEntry object.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                bool isSelected = IsActive && (i == selectedEntry);

                menuEntries[i].Update(this, isSelected, gameTime);
            }
        }


        /// <summary>
        /// Draws the menu.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            spriteBatch.Begin();

            // Draw the menu title.
            //Vector2 titlePosition = new Vector2(GraphicsConfig.GraphicsDevice.Viewport.Width / 2, GraphicsConfig.GraphicsDevice.Viewport.Height / 8) + titlePositionAdjust;
            //FIXME
            Vector2 titlePosition = new Vector2(800 / 2, 600 / 8) + titlePositionAdjust;
            Vector2 titleOrigin = font.MeasureString(menuTitle) / 2;
            //Color titleColor = new Color(192, 192, 192, TransitionAlpha);
            Color titleColor = new Color(255, 255, 255, TransitionAlpha);
            float titleScale = 1.25f;

            //titlePosition.Y -= transitionOffset * 100;
            Vector2 titleTransform = new Vector2(0, transitionOffset * 100);

            spriteBatch.DrawString(font, menuTitle, titlePosition - titleTransform, titleColor, 0,
                                   titleOrigin, titleScale, SpriteEffects.None, 0);

            //Vector2 subTitlePosition = new Vector2(GraphicsConfig.GraphicsDevice.Viewport.Width / 2, titlePosition.Y + (titleOrigin.Y * 2f)) + subTitlePositionAdjust;
            //FIXME
            Vector2 subTitlePosition = new Vector2(600 / 2, titlePosition.Y + (titleOrigin.Y*2f)) + subTitlePositionAdjust;
            Vector2 subTitleOrigin = font.MeasureString(menuSubTitle) / 2;
            float subTitleScale = .75f;
            
            spriteBatch.DrawString(font, menuSubTitle, subTitlePosition - titleTransform, titleColor, 0,
                                   subTitleOrigin, subTitleScale, SpriteEffects.None, 0);

            //setup menu positions
            /*Vector2 menuPosition = new Vector2(GraphicsConfig.SafeArea.X*1.25f,
                subTitlePosition.Y + font.MeasureString(menuTitle).Y) + menuPositionAdjust;*/
            //FIXME
            Vector2 menuPosition = new Vector2(100,
                subTitlePosition.Y + font.MeasureString(menuTitle).Y) + menuPositionAdjust;

            Vector2 menuPositionOffset = new Vector2();
            if (ScreenState == ScreenState.TransitionOn)
            {
                menuPositionOffset.X = -transitionOffset * 256;
            }
            else
            {
                menuPositionOffset.X = transitionOffset * 512;
            }

            // Draw each menu entry in turn.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                bool isSelected = IsActive && (i == selectedEntry);

                menuEntry.Draw(this, menuPosition, menuPositionOffset, isSelected, gameTime);

                menuPosition.Y += (menuEntry.GetHeight(this)+menuEntrySpacingAdjust);
            }

            //draw detail text if it exists
            if (menuEntries[selectedEntry].DetailText != null)
            {
                //Vector2 detailTextPosition = new Vector2(GraphicsConfig.GraphicsDevice.Viewport.Width / 2, menuPosition.Y) + detailPositionAdjust;
                //FIXME
                Vector2 detailTextPosition = new Vector2(800 / 2, menuPosition.Y) + detailPositionAdjust;
                Vector2 detailTextOrigin = font.MeasureString(menuEntries[selectedEntry].DetailText) / 2;
                //Color detailTextColor = new Color(192, 192, 192, TransitionAlpha);
                Color detailTextColor = new Color(255, 255, 255, TransitionAlpha);
                float detailTextScale = .75f;

                detailTextPosition.Y += transitionOffset * 100;

                spriteBatch.DrawString(font, menuEntries[selectedEntry].DetailText,
                    detailTextPosition, detailTextColor, 0, detailTextOrigin,
                    detailTextScale, SpriteEffects.None, 0);
            }

            spriteBatch.End();
        }

        protected void DrawShadowedString(SpriteBatch spriteBatch, SpriteFont font, string text, Vector2 position, Color color)
        {
            spriteBatch.DrawString(font, text, new Vector2(position.X - 1f, position.Y - 1f), Color.Black);
            spriteBatch.DrawString(font, text, position, color);
        }
    }
}
