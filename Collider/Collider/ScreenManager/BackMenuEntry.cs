﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Collider
{
    class BackMenuEntry : MenuEntry
    {
        public BackMenuEntry(string text):base(text)
        {
            selectCue = "MenuBack";
        }

        public BackMenuEntry(string text, MenuEntryAlignment align):
            base(text,align)
        {
            selectCue = "MenuBack";
        }
    }
}