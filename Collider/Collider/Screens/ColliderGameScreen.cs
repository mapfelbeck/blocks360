﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Collider
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class ColliderGameScreen : GameScreen
    {
        ContentManager content;
        KeyboardState oldKeyState;

        Texture2D shotTexture;
        Random random;

        Point screenSize;

        QuadTreeVisualizer<ColliderObject> visualizer;
        Collider gameCollider;

        List<Player> players;

        List<GoalZone> goalZones;
        Ball gameBall;

        Rectangle boundsRect;

        const float shotSpeed = 400f;

        public ColliderGameScreen(List<Player> players, Collider collider, List<GoalZone> zones, Ball gameBall)
        {
            goalZones = zones;
            screenSize = new Point(collider.Dimensions.Width, collider.Dimensions.Height);
            boundsRect = new Rectangle(0, 0, screenSize.X, screenSize.Y);
            gameCollider = collider;
            this.gameBall = gameBall;
            random = new Random();
            this.players = players;
            Initialize();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected void Initialize()
        {
            visualizer = new QuadTreeVisualizer<ColliderObject>(gameCollider.Collection);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            visualizer.LoadContent(content);

            foreach (Player player in players)
            {
                player.LoadContent(content);
                player.ScoreTexture = gameBall.texture;
                switch (player.Index)
                {
                    case PlayerIndex.One:
                        player.ScoreLocation = new Vector2(boundsRect.Width / 10 + player.Font.MeasureString(player.PlayerName).X / 2, boundsRect.Height / 10);
                        break;
                    case PlayerIndex.Two:
                        player.ScoreLocation = new Vector2(boundsRect.Width*.9f - player.Font.MeasureString(player.PlayerName).X/2, boundsRect.Height / 10);
                        break;
                    default:
                        break;
                }
            }

            shotTexture = TextureMaker.CircleTexture(ScreenManager.GraphicsDevice, 10, 1, 1, 1, Color.White, Color.Black);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        public override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public override void HandleInput(InputState input)
        {
            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState newKeyState = input.CurrentKeyboardStates[playerIndex];

            if (input.IsPauseGame(null))
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }

            foreach (Player player in players)
            {
                player.HandleInput(input);
            }

            oldKeyState = newKeyState;
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (!otherScreenHasFocus)
            {
                foreach (Player player in players)
                {
                    player.Update(elapsedTime);

                    if (player.PlayerWon)
                    {
                        MessageBoxScreen gameOver = ScreenManager.GetMessageBox(string.Format("{0} wins!",player.PlayerName));

                        gameOver.Accepted += ConfirmRestart;
                        gameOver.Cancelled += LoadMainMenu;

                        ScreenManager.AddScreen(gameOver, null);
                    }

                    if (player.PlayerShot)
                    {
                        PlayerShot newCircle;
                        Vector2 location = new Vector2(player.Position.X + player.Center.X,
                                                        player.Position.Y);
                        Vector2 velocity = shotSpeed * player.PlayerShootDirection;
                        newCircle = new PlayerShot(shotTexture, location, velocity, player.PlayerColor);
                        gameCollider.Add(newCircle);
                    }
                }

                foreach (ColliderObject collisionObject in gameCollider.Collection.Contents)
                {
                    collisionObject.Update(elapsedTime);
                }
                gameCollider.Update(elapsedTime);

                CheckRoundReset();
            }
        }

        protected void ConfirmRestart(object sender, PlayerIndexEventArgs e)
        {
            BackgroundScreen background = ScreenManager.GetScreens()[0] as BackgroundScreen;

            if (background == null)
            {
                throw new NullReferenceException("null background screen, unpossible!");
            }

            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                                new BackgroundScreen(background.BackGroundName),
                                ColliderGameFactory.BuildGame(players[0].Type, players[1].Type, ScreenManager.GraphicsDevice));
        }

        protected void LoadMainMenu(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                               new BackgroundScreen("Menu Backgrounds/Blank"),
                               new MainMenuScreen());
        }

        private void CheckRoundReset()
        {
            foreach (GoalZone zone in goalZones)
            {
                if (zone.InZone(gameBall))
                {
                    gameBall.Position = new Vector2(boundsRect.Center.X, boundsRect.Center.Y);
                    gameBall.Velocity = Vector2.Zero;
                    foreach (ColliderObject collider in gameCollider.Collection.Contents)
                    {
                        if (collider is PlayerShot)
                        {
                            gameCollider.QueuePendingRemoval(collider);
                        }
                    }
                    gameCollider.ApplyPendingRemovals();

                    zone.Owner.PlayerScore = zone.Owner.PlayerScore + 1;
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch; 

            //visualizer.Draw(spriteBatch);

            spriteBatch.Begin();
            foreach (ColliderObject colliderObject in gameCollider.Collection.Contents)
            {
                colliderObject.Draw(spriteBatch);
            }

            foreach (Player player in players)
            {
                player.Draw(spriteBatch);
            }

            foreach (GoalZone zone in goalZones)
            {
                zone.Draw(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}