#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
#endregion

namespace Collider
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class MainMenuScreen : MenuScreen
    {
        MenuEntry playMenuEntry;
        BackMenuEntry exitMenuEntry;

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public MainMenuScreen()
            : base("Collider Game")
        {
            playMenuEntry = new MenuEntry("Play", MenuEntryAlignment.Center);
            exitMenuEntry = new BackMenuEntry("Exit", MenuEntryAlignment.Center);

            // Hook up menu event handlers.
            playMenuEntry.Selected += PlayGameSelected;
            exitMenuEntry.Selected += OnCancel;

            playMenuEntry.DetailText = "Play the game.";
            exitMenuEntry.DetailText = "Leave the game.";

            // Add entries to the menu.
            MenuEntries.Add(playMenuEntry);
            MenuEntries.Add(exitMenuEntry);
        }

        public override void LoadContent()
        {
            base.LoadContent();
            ContentManager content = ScreenManager.Game.Content;

            //menuPositionAdjust = new Vector2 (0, GraphicsConfig.GraphicsDevice.Viewport.Height / 4);
            //detailPositionAdjust = new Vector2(0, GraphicsConfig.GraphicsDevice.Viewport.Height / 16);
            //FIXME
            menuPositionAdjust = new Vector2 (0, 600 / 4);
            detailPositionAdjust = new Vector2(0, 600 / 16);
            menuEntrySpacingAdjust = -6f;
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            const string message = "Are you sure you want to exit?";

            MessageBoxScreen confirmExitMessageBox = ScreenManager.GetMessageBox(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmExitMessageBox, playerIndex);
        }

        void PlayGameSelected(object sender, PlayerIndexEventArgs args)
        {
            LoadingScreen.Load(ScreenManager, false, args.PlayerIndex,
                                new BackgroundScreen("Menu Backgrounds/Blank"),
                                ColliderGameFactory.BuildGame(PlayerType.Human,PlayerType.AI,ScreenManager.GraphicsDevice));
        }

        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to exit" message box.
        /// </summary>
        void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }
    }
}