﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Collider
{
    public class GoalZone
    {
        Texture2D texture;
        Rectangle rectangle;
        Player owner;
        public Player Owner
        {
            get { return owner; }
        }

        public GoalZone(Texture2D texture, Rectangle rectangle, Player owner)
        {
            this.texture = texture;
            this.rectangle = rectangle;
            this.owner = owner;
        }

        public bool InZone(ColliderObject colliderObject)
        {
            return rectangle.Contains(new Point((int)colliderObject.Position.X, (int)colliderObject.Position.Y));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rectangle, owner.PlayerColor);
        }
    }
}
