﻿#region using statements
using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
#endregion

namespace Collider
{
    public class QuadTreeNode<T> where T : IQuadTreeItem<T>
    {
        enum NodeLocation
        {
            Self,
            Parent,
            Child,
        };

        AABB bounds;
        public AABB Bounds
        {
            get { return bounds; }
        }
        QuadTreeNode<T> parent;
        List<QuadTreeNode<T>> nodes;
        public List<QuadTreeNode<T>> Nodes
        {
            get { return nodes; }
        }
        QuadTreeNode<T> UL
        {
            get { return IsLeaf ? null : nodes[0]; }
        }
        QuadTreeNode<T> UR
        {
            get { return IsLeaf ? null : nodes[1]; }
        }
        QuadTreeNode<T> LL
        {
            get { return IsLeaf ? null : nodes[2]; }
        }
        QuadTreeNode<T> LR
        {
            get { return IsLeaf ? null : nodes[3]; }
        }
        List<T> contents;
        public List<T> Contents
        {
            get { return contents; }
        }

        public List<T> AllContents
        {
            get
            {
                List<T> result = new List<T>();
                AllContentsHelper(ref result);
                return result;
            }
        }
        private void AllContentsHelper(ref List<T> result)
        {
            result.AddRange(contents);

            foreach (QuadTreeNode<T> node in nodes)
            {
                node.AllContentsHelper(ref result);
            }
        }
        public List<T> SubTreeContents
        {
            get
            {
                List<T> result = new List<T>();
                foreach (QuadTreeNode<T> node in nodes)
                    node.AllContentsHelper(ref result);
                return result;
            }
        }
        public int Count
        {
            get
            {
                int count = contents.Count;
                count += SubTreeCount;
                return count;
            }
        }

        public int SubTreeCount
        {
            get
            {
                int count = 0;
                foreach (QuadTreeNode<T> node in nodes)
                {
                    count += node.Count;
                }
                return count;
            }
        }

        public int LeafCount
        {
            get
            {
                int count = 1;
                foreach (QuadTreeNode<T> node in nodes)
                {
                    count += node.LeafCount;
                }
                return count;
            }
        }
        
        public bool IsEmpty
        {
            get { return Count == 0; }
        }

        bool isLeaf;
        public bool IsLeaf
        {
            get { return isLeaf; }
        }

        bool isSplitable;

        public bool IsRoot
        {
            get { return parent == null; }
        }

        float splitLimit;

        public QuadTreeNode(QuadTreeNode<T> parent, AABB bounds, float splitLimit)
        {
            if (!bounds.Valid)
            {
                throw new InvalidOperationException("Bad bounds AABB");
            }
            this.parent = parent;
            this.bounds = bounds;
            contents = new List<T>();
            nodes = new List<QuadTreeNode<T>>();
            isLeaf = true;
            this.splitLimit = splitLimit;
            isSplitable = bounds.Height >= splitLimit && bounds.Width >= splitLimit;
            /*if (!isSplitable)
            {
                Console.WriteLine("Creating unsplitable node with size {0} X {1}", bounds.Width, bounds.Height);
            }
            else
            {
                Console.WriteLine("Creating splitable node with size {0} X {1}", bounds.Width, bounds.Height);
            }*/
        }

        private bool PushItemDown(ref T item, bool removeFromCurrent)
        {
            if (removeFromCurrent)
            {
                contents.Remove(item);
            }

            if (isLeaf && isSplitable)
            {
                CreateSubNodes();
            }

            foreach (QuadTreeNode<T> node in nodes)
            {
                if (node.Bounds.Contains(item.AABB))
                {
                    return node.Add(ref item);
                }
            }

            return false;
        }

        private bool PushItemUp(ref T item, bool removeFromCurrent)
        {
            if (removeFromCurrent)
            {
                contents.Remove(item);
            }

            return parent.Add(ref item);
        }

        public bool Add(ref T item)
        {
            NodeLocation location = ItemLocation(ref item);

            if (location == NodeLocation.Parent && !IsRoot)
            {
                return PushItemUp(ref item, false);
            }
            else if (location == NodeLocation.Child && isSplitable)
            {
                PushItemDown(ref item, false);
            }
            else
            {
                item.UpdateDelegate = this.UpdatePosition;
                item.DeleteDelegate = this.Remove;
                contents.Add(item);
                if (SubTreeCount == 0)
                {
                    DeleteSubNodes();
                }
                return true;
            }

            return false;
        }

        void UpdatePosition(T item)
        {
            NodeLocation location = ItemLocation(ref item);

            if (location == NodeLocation.Parent && !IsRoot)
            {
                PushItemUp(ref item, true);
            }
            else if (location == NodeLocation.Child && isSplitable)
            {
                PushItemDown(ref item, true);
            }
        }

        private NodeLocation ItemLocation(ref T item)
        {
            NodeLocation result = NodeLocation.Self;

            float halfWidthLocation = bounds.X + bounds.Width / 2;
            float halfHeightLocation = bounds.Y + bounds.Height / 2;

            if (!bounds.Contains(item.AABB))
            {
                result = NodeLocation.Parent;
            }
            else if (item.AABB.ContainsX(ref halfWidthLocation) || item.AABB.ContainsY(ref halfHeightLocation))
            {
                result = NodeLocation.Self;
            }
            else
            {
                result = NodeLocation.Child;
            }
            return result;
        }

        private void CreateSubNodes()
        {
            if (nodes.Count > 0 || !isSplitable)
            {
                return;
            }
            isLeaf = false;

            float halfWidth = bounds.Width / 2;
            float halfHeight = bounds.Height / 2;

            nodes.Add(new QuadTreeNode<T>(this, new AABB(bounds.X, bounds.Y, halfHeight, halfWidth), splitLimit));
            nodes.Add(new QuadTreeNode<T>(this, new AABB(bounds.X + halfWidth, bounds.Y, halfHeight, halfWidth), splitLimit));

            nodes.Add(new QuadTreeNode<T>(this, new AABB(bounds.X, bounds.Y + halfHeight, halfHeight, halfWidth), splitLimit));
            nodes.Add(new QuadTreeNode<T>(this, new AABB(bounds.X + halfWidth, bounds.Y + halfHeight, halfHeight, halfWidth), splitLimit));
            
        }

        public void DeleteSubNodes()
        {
            foreach (QuadTreeNode<T> node in nodes)
            {
                node.DeleteSubNodes();
            }
            nodes.Clear();
            isLeaf = true;
        }

        public void DeleteData()
        {
            contents.Clear();
        }

        public List<T> Query(AABB queryAABB)
        {
            List<T> result = new List<T>();

            QueryHelper(ref queryAABB, ref result);

            return result;
        }

        public void Query(ref AABB queryAABB, ref List<T> result)
        {
            QueryHelper(ref queryAABB, ref result);
        }

        public void QueryHelper(ref AABB queryAABB, ref List<T> result)
        {
            foreach (T item in contents)
            {
                if (queryAABB.Intersects(item.AABB))
                {
                    result.Add(item);
                }
            }

            foreach (QuadTreeNode<T> node in nodes)
            {
                // Case 1: search area completely contained by sub-quad
                // if a node completely contains the query area, go down that branch
                // and skip the remaining nodes (break this loop)
                if (node.bounds.Contains(ref queryAABB))
                {
                    node.QueryHelper(ref queryAABB, ref result);
                    break;
                }

                // Case 2: Sub-quad completely contained by search area 
                // if the query area completely contains a sub-quad,
                // just add all the contents of that quad and it's children 
                // to the result set. You need to continue the loop to test 
                // the other quads
                if (queryAABB.Contains(ref node.bounds))
                {
                    result.AddRange(node.AllContents);
                    continue;
                }

                // Case 3: search area intersects with sub-quad
                // traverse into this quad, continue the loop to search other
                // quads
                if (node.bounds.Intersects(ref queryAABB))
                {
                    node.QueryHelper(ref queryAABB, ref result);
                }
            }
        }

        public void Remove(T item)
        {
            if (!contents.Remove(item))
            {
                foreach (QuadTreeNode<T> node in nodes)
                {
                    if (node.bounds.Contains(item.AABB))
                    {
                        node.Remove(item);
                    }
                }
            }

            if (SubTreeCount == 0)
            {
                DeleteSubNodes();
            }
        }

        public void Prune()
        {
            if (Count == 0)
            {
                DeleteSubNodes();
            }
            else
            {
                foreach (QuadTreeNode<T> node in nodes)
                {
                    node.Prune();
                }
            }
        }
    }
}