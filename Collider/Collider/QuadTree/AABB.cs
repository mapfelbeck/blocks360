﻿#region using statements
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
#endregion

namespace Collider
{
    /// <summary>
    /// class represents a 2D axis-aligned bounding box, for collision detection, etc.
    /// </summary>
    public class AABB
    {
        public Vector2 UL;
        public Vector2 LR;
        public float X
        {
            get { return UL.X; }
        }
        public float Y
        {
            get { return UL.Y; }
        }
        public float Height
        {
            get { return LR.Y - UL.Y; }
        }
        public float Width
        {
            get { return LR.X - UL.X; }
        }

        public bool Valid;

        public Rectangle Rectangle
        {
            get { return new Rectangle((int)X, (int)Y, (int)Width, (int)Height); }
        }

        public AABB()
        {
            UL = LR = Vector2.Zero;
            Valid = false;
        }

        /// <summary>
        /// create a boundingbox with the given min and max points.
        /// </summary>
        /// <param name="minPt">min point</param>
        /// <param name="maxPt">max point</param>
        public AABB(Vector2 UpperLeft, Vector2 LowerRight)
        {
            UL = UpperLeft;
            LR = LowerRight;
            Valid = true;
        }

        public void Set(Vector2 UpperLeft, Vector2 LowerRight)
        {
            UL = UpperLeft;
            LR = LowerRight;
            Valid = true;
        }

        /// <summary>
        /// create a boundingbox with the given dimensions
        /// </summary>
        /// <param name="x">x location of the upper left corner</param>
        /// <param name="y">y location of the upper right corner</param>
        /// <param name="height">AABB height</param>
        /// <param name="width">AABB width</param>
        public AABB(float x, float y, float height, float width)
        {
            UL = new Vector2(x, y);
            LR = new Vector2(x + width, y + height);
            Valid = true;
        }

        public AABB(Rectangle rectangle)
        {
            UL = new Vector2(rectangle.X, rectangle.Y);
            LR = new Vector2(rectangle.Right, rectangle.Bottom);
            Valid = true;
        }

        /// <summary>
        /// Resets a bounding box to invalid.
        /// </summary>
        public void Clear()
        {
            UL.X = LR.X = UL.Y = LR.Y = 0;
            Valid = false;
        }

        public void ExpandToInclude(ref Vector2 pt)
        {
            if (Valid)
            {
                if (pt.X < UL.X) { UL.X = pt.X; }
                else if (pt.X > LR.X) { LR.X = pt.X; }

                if (pt.Y < UL.Y) { UL.Y = pt.Y; }
                else if (pt.Y > LR.Y) { LR.Y = pt.Y; }
            }
            else
            {
                UL = LR = pt;
                Valid = true;
            }
        }

        /// <summary>
        /// Does this AABB completely contain the passed in point?
        /// </summary>
        public bool Contains(Vector2 pt)
        {
            if (!Valid) { return false; }

            return ((pt.X >= UL.X) && (pt.X <= LR.X) && (pt.Y >= UL.Y) && (pt.Y <= LR.Y));
        }

        /// <summary>
        /// Does this AABB completely contain the passed in AABB?
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        public bool Contains(ref AABB box)
        {
            return (UL.Y <= box.UL.Y && UL.X <= box.UL.X &&
                    LR.Y >= box.LR.Y && LR.X >= box.LR.X);
        }

        /// <summary>
        /// Does this AABB completely contain the passed in AABB?
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        public bool Contains(AABB box)
        {
            return (UL.Y <= box.UL.Y && UL.X <= box.UL.X &&
                    LR.Y >= box.LR.Y && LR.X >= box.LR.X);
        }

        public bool ContainsX(float X)
        {
            return (UL.X <= X && LR.X >= X);
        }

        public bool ContainsX(ref float X)
        {
            return (UL.X <= X && LR.X >= X);
        }

        public bool ContainsY(float Y)
        {
            return (UL.Y <= Y && LR.Y >= Y);
        }

        public bool ContainsY(ref float Y)
        {
            return (UL.Y <= Y && LR.Y >= Y);
        }

        public bool Intersects(ref AABB box)
        {
            return ((UL.X <= box.LR.X) && (LR.X >= box.UL.X)) &&
                ((UL.Y <= box.LR.Y) && (LR.Y >= box.UL.Y));
        }

        public bool Intersects(AABB box)
        {
            return ((UL.X <= box.LR.X) && (LR.X >= box.UL.X)) &&
                ((UL.Y <= box.LR.Y) && (LR.Y >= box.UL.Y));
        }

        public bool Within(AABB box, float overlap)
        {
            bool overlapX =
                ((UL.X + overlap <= box.LR.X + overlap) && (LR.X + overlap >= box.UL.X + overlap));
            bool overlapY =
                ((UL.Y + overlap <= box.LR.Y + overlap) && (LR.Y + overlap >= box.UL.Y + overlap));

            return (overlapX && overlapY);
        }

    }
}
