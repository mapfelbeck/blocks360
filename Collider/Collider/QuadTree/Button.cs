﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;

namespace Collider
{
    public class QuadTreeButtonButton : IQuadTreeItem<QuadTreeButtonButton>
    {
        AABB aabb;
        public AABB AABB
        {
            get { return aabb; }
        }

        UpdateItemDelegate<QuadTreeButtonButton> treePositionUpdate;
        public UpdateItemDelegate<QuadTreeButtonButton> UpdateDelegate
        {
            get { return treePositionUpdate; }
            set { treePositionUpdate = value; }
        }
        DeleteItemDelegate<QuadTreeButtonButton> treeDelete;
        public DeleteItemDelegate<QuadTreeButtonButton> DeleteDelegate
        {
            get { return treeDelete; }
            set { treeDelete = value; }
        }

        public Texture2D texture;
        public Vector2 center;
        Vector2 position;
        public Vector2 Position{
            get{return position;}
            set{position = value;
                UpdateAABB();
            }
        }
        public float Rotation;
        public Vector2 Velocity;

        public QuadTreeButtonButton(Texture2D theTexture, Vector2 theLocation, Vector2 theVelocity)
        {
            texture = theTexture;
            center = new Vector2(texture.Height / 2);
            Rotation = 0f;
            Position = theLocation;
            Velocity = theVelocity;
            UpdateAABB();
        }

        public void Update(float elapsedTime)
        {
            position += Velocity * elapsedTime;
            if (position.X < 0 || position.X > 800)
            {
                Velocity.X = -Velocity.X;
            }
            if (position.Y < 0 || position.Y > 600)
            {
                Velocity.Y = -Velocity.Y;
            }

            UpdateAABB();
            if (treePositionUpdate != null)
            {
                treePositionUpdate(this);
            }
        }

        public void UpdateAABB()
        {
            if (aabb != null)
            {
                aabb.Set(Position - center, Position + center);
            }
            else
            {
                aabb = new AABB(Position - center, Position + center);
            }
        }
    }
}