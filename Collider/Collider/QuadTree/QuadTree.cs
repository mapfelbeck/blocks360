﻿#region using statements
using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
#endregion

namespace Collider
{
    public class QuadTree<T> where T : IQuadTreeItem<T>
    {
        QuadTreeNode<T> root;
        public QuadTreeNode<T> Root
        {
            get { return root; }
        }

        public int LeafCount
        {
            get
            {
                if (root == null)
                {
                    return 0;
                }
                else
                {
                    return root.LeafCount;
                }
            }
        }

        private List<T> pendingRemovals;

        List<T> contentsCache;
        bool contentsCacheValid;
        public List<T> Contents
        {
            get
            {
                if (root == null)
                {
                    return null;
                }
                else
                {
                    if (!contentsCacheValid)
                    {
                        contentsCache = root.AllContents;
                        contentsCacheValid = true;
                    }
                    return contentsCache;
                }
            }
        }

        public T this[int index]
        {
            get
            {
                return Contents[index];
            }
        }

        int maxDepth = 4;

        public QuadTree(AABB bounds)
        {
            float splitLimit = (float)(Math.Min((double)bounds.Width, (double)bounds.Height) / Math.Pow(2.0, (double)maxDepth));
            root = new QuadTreeNode<T>(null, bounds, splitLimit);
            contentsCache = new List<T>();
            contentsCacheValid = false;
            pendingRemovals = new List<T>();
        }

        public QuadTree(AABB bounds, int maxDepth)
        {
            this.maxDepth = maxDepth;
            float splitLimit = (float)(Math.Min((double)bounds.Width, (double)bounds.Height) / Math.Pow(2.0, (double)maxDepth));
            root = new QuadTreeNode<T>(null, bounds, splitLimit);
            contentsCache = new List<T>();
            contentsCacheValid = false;
            pendingRemovals = new List<T>();
        }

        public int Count
        {
            get {
                if (!contentsCacheValid)
                {
                    contentsCache = root.AllContents;
                    contentsCacheValid = true;
                }
                return contentsCache.Count;
            }
        }

        public bool Collides(T item){
            bool result = false;

            if (!root.Bounds.Contains(item.AABB))
            {
                result = true;
            }
            else if (root.Query(item.AABB).Count > 0)
            {
                result = true;
            }

            return result;
        }

        public virtual void Add(T item)
        {
            root.Add(ref item);
            contentsCacheValid = false;
        }

        public List<T> Query(AABB aabb)
        {
            if (root != null)
            {
                return root.Query(aabb);
            }
            return null;
        }

        public void Query(ref AABB aabb, ref List<T> queryResults)
        {
            if (root != null)
            {
                root.Query(ref aabb, ref queryResults);
            }
        }

        public void Remove(T item)
        {
            if (item.DeleteDelegate != null)
            {
                item.DeleteDelegate(item);
            }
            else
            {
                root.Remove(item);
            }
            PruneTree();
            contentsCache.Clear();
            contentsCacheValid = false;
        }

        public void Clear()
        {
            root.DeleteSubNodes();
        }

        public void PruneTree()
        {
            if (root != null)
            {
                root.Prune();
            }
        }

        public void QueuePendingRemoval(T item)
        {
            pendingRemovals.Add(item);
        }

        /// <summary>
        /// Remove all of the "garbage" objects from this collection.
        /// </summary>
        public void ApplyPendingRemovals()
        {
            for (int i = 0; i < pendingRemovals.Count; i++)
            {
                Remove(pendingRemovals[i]);
            }
            pendingRemovals.Clear();
            contentsCacheValid = false;
        }
    }
}