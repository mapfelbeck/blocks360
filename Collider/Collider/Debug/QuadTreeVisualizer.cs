﻿#region using statements
using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
#endregion

namespace Collider
{
    public class QuadTreeVisualizer<T> where T:IQuadTreeItem<T>
    {
        Color[] NodeColors = { Color.CornflowerBlue, 
                                 Color.Red, 
                                 Color.Green, 
                                 Color.Blue, 
                                 Color.Yellow,
                                 Color.Purple,
                                 Color.White,
                                 Color.Black,};

        int numColors;
        Texture2D texture;
        QuadTree<T> tree;

        public QuadTreeVisualizer(QuadTree<T> tree)
        {
            this.tree = tree;
            numColors = NodeColors.Length;
        }

        public void LoadContent(ContentManager content){

            texture = content.Load<Texture2D>("blank");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int colorIndex = 0;

            spriteBatch.Begin();
            DrawLayers(spriteBatch, tree.Root, colorIndex);
            spriteBatch.End();
        }

        private void DrawLayers(SpriteBatch spriteBatch, QuadTreeNode<T> node, int colorIndex)
        {
            if (node.Count <= 0)
            {
                return;
            }

            spriteBatch.Draw(texture, node.Bounds.Rectangle, NodeColors[colorIndex]);

            colorIndex = (colorIndex + 1) % numColors;
            foreach (QuadTreeNode<T> subNode in node.Nodes)
            {
                DrawLayers(spriteBatch, subNode, colorIndex);
            }
        }

    }
}