﻿using System;
using Microsoft.Xna.Framework;

namespace Collider
{
    class GarbageCollectionCounter : GameComponent
    {
        WeakReference gcTracker;
        float gcFreq;

        public GarbageCollectionCounter(Game game)
            : base(game)
        {
            gcTracker = new WeakReference(new object());
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            gcFreq += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (!gcTracker.IsAlive)
            {
                Console.WriteLine("GC: " + gcFreq.ToString());
                gcFreq = 0f;
                gcTracker = new WeakReference(new object());
            }
        }
    }
}
