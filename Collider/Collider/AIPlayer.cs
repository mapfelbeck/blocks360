﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Collider
{
    class AIPlayer : Player
    {
        Rectangle goal;
        Vector2 goalCenter;
        Ball ball;

        const float shootDistanceMax = 2f;
        const float shootRate = 1f;
        float shotTimer;

        public override PlayerType Type
        {
            get { return PlayerType.AI; }
        }

        public AIPlayer(PlayerIndex whichPlayer, Color playerColor, Rectangle goal, Ball ball)
            : base(whichPlayer, playerColor)
        {
            this.goal = goal;
            this.ball = ball;
            goalCenter = new Vector2(goal.Center.X, goal.Center.Y);
        }

        public override void Update(float elapsedTime)
        {
            playerShot = false;
            shotTimer += elapsedTime;
            float difference = ball.Position.Y - Position.Y;
            if (Math.Abs(difference) <= shootDistanceMax && shotTimer >= shootRate)
            {
                playerShot = true;
                shotTimer = 0f;
            }

            float actualMoveAmount = MathHelper.Clamp(difference, -moveAmountMax * elapsedTime, moveAmountMax * elapsedTime);
            moveAmount = actualMoveAmount / elapsedTime;
            base.Update(elapsedTime);
        }

        public override void HandleInput(InputState input)
        {
        }
    }
}
