using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Collider
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class ColliderGame : Microsoft.Xna.Framework.Game
    {
        ScreenManager screenManager;
        FrameRateCounter fps;
        GarbageCollectionCounter gc;
        GraphicsDeviceManager graphics;

        public ColliderGame()
        {
            graphics = new GraphicsDeviceManager(this);
            
            //this.IsFixedTimeStep = false;
            //graphics.SynchronizeWithVerticalRetrace = false;

            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";

            screenManager = new ScreenManager(this);
            Components.Add(screenManager);

#if DEBUG
            fps = new FrameRateCounter(this);
            this.Components.Add(fps);

            gc = new GarbageCollectionCounter(this);
            this.Components.Add(gc);
#endif

            screenManager.AddScreen(new BackgroundScreen("Menu Backgrounds/Blank"), null);
            screenManager.AddScreen(new MainMenuScreen(), null);
        }
    }
}