﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Collider
{
    public enum PlayerType
    {
        Human,
        AI,
    }

    public static class ColliderGameFactory
    {
        public static ColliderGameScreen BuildGame(PlayerType p1type, PlayerType p2type, GraphicsDevice graphicsDevice)
        {
            int width = 800;
            int height = 600;
            int widthOverTen = width / 10;
            int heightOverTen = height / 10;
            Rectangle colliderBounds = new Rectangle(0, 0, width, height);
            //create the collider
            Collider collider = new Collider(colliderBounds);
            
            //create the barriers
            //ends
            Texture2D endTexture = TextureMaker.RectangleTexture(graphicsDevice,
                widthOverTen, height, 2, 1, 1, Color.White, Color.Black);
            Barrier leftSide = new Barrier(endTexture, new Rectangle(0, 0, widthOverTen, height), Color.Yellow);
            Barrier rightSide = new Barrier(endTexture, new Rectangle(width-widthOverTen, 0, widthOverTen, height), Color.Yellow);
            //top and bottom
            Texture2D middleTexture = TextureMaker.RectangleTexture(graphicsDevice,
                width - 2 * widthOverTen, heightOverTen, 2, 1, 1, Color.White, Color.Black);
            Barrier topSide = new Barrier(middleTexture, new Rectangle(widthOverTen, 0, width - 2 * widthOverTen, heightOverTen), Color.Yellow);
            Barrier bottomSide = new Barrier(middleTexture, new Rectangle(widthOverTen, height-heightOverTen, width - 2 * widthOverTen, heightOverTen), Color.Yellow);
            //corners
            Texture2D cornerTexture = TextureMaker.RectangleTexture(graphicsDevice,
                widthOverTen, 2 * heightOverTen, 2, 1, 1, Color.White, Color.Black);
            Barrier upperLeft = new Barrier(cornerTexture, new Rectangle(widthOverTen, heightOverTen, widthOverTen, 2 * heightOverTen), Color.Yellow);
            Barrier upperRight = new Barrier(cornerTexture, new Rectangle(width - 2 * widthOverTen, heightOverTen, widthOverTen, 2 * heightOverTen), Color.Yellow);
            Barrier lowerLeft = new Barrier(cornerTexture, new Rectangle(widthOverTen, height - 3 * heightOverTen, widthOverTen, 2 * heightOverTen), Color.Yellow);
            Barrier lowerRight = new Barrier(cornerTexture, new Rectangle(width - 2 * widthOverTen, height - 3 * heightOverTen, widthOverTen, 2 * heightOverTen), Color.Yellow);
            
            collider.Add(leftSide);
            collider.Add(rightSide);
            collider.Add(topSide);
            collider.Add(bottomSide);
            collider.Add(upperLeft);
            collider.Add(upperRight);
            collider.Add(lowerLeft);
            collider.Add(lowerRight);

            //create the ball
            Texture2D ballTexture = TextureMaker.CircleTexture(graphicsDevice, 20, 2, 1, 1, Color.White, Color.Black);
            Ball gameBall = new Ball(ballTexture, new Vector2(width/2, height/2), Vector2.Zero, Color.Green);
            collider.Add(gameBall);

            //create players
            List<Player> players = new List<Player>();
            Color p1Color = Color.Blue;
            Color p2Color = Color.Red;
            Rectangle p1GoalRectangle = new Rectangle(width - 2 * widthOverTen, 3 * heightOverTen, widthOverTen, 4 * heightOverTen);
            Rectangle p2GoalRectangle = new Rectangle(widthOverTen, 3 * heightOverTen, widthOverTen, 4 * heightOverTen);
            Rectangle playerBounds = new Rectangle(widthOverTen * 2, heightOverTen, width - 4 * widthOverTen, height - 2 * heightOverTen);
            Player p1 = CreatePlayer(PlayerIndex.One, p1Color, p1type, p1GoalRectangle, gameBall);
            p1.SetBounds(playerBounds);
            p1.Position = new Vector2(16, height / 3);
            Player p2 = CreatePlayer(PlayerIndex.Two, p2Color, p2type, p1GoalRectangle, gameBall);
            p2.SetBounds(playerBounds);
            p2.Position = new Vector2(width - (2 * widthOverTen + 16), 2 * height / 3);
            players.Add(p1);
            players.Add(p2);

            //create the goal zones
            List<GoalZone> zones = new List<GoalZone>();
            Color whiteHalfTrans = Color.White;
            whiteHalfTrans.A = 128;
            Texture2D goalTexture = TextureMaker.RectangleTexture(graphicsDevice,
                widthOverTen, 4 * heightOverTen, 2, 1, 1, whiteHalfTrans, Color.White);
            GoalZone p1Zone = new GoalZone(goalTexture, p1GoalRectangle, p1);
            GoalZone p2Zone = new GoalZone(goalTexture, p2GoalRectangle, p2);
            zones.Add(p1Zone);
            zones.Add(p2Zone);

            ColliderGameScreen newGame = new ColliderGameScreen(players, collider, zones, gameBall);
            return newGame;
        }

        private static Player CreatePlayer(PlayerIndex index, Color color, 
            PlayerType type, Rectangle goal, Ball ball)
        {
            Player createdPlayer = null;
            switch (type)
            {
                case PlayerType.Human:
                    createdPlayer = new Player(index, color);
                    break;
                case PlayerType.AI:
                    createdPlayer = new AIPlayer(index, color, goal, ball);
                    break;
                default:
                    break;
            }
            return createdPlayer;
        }
    }
}