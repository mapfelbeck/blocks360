﻿#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using Utilities;

#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class BackgroundBlocks
    {
        bool physicsDisposed;
        protected Physics.World backgroundWorld;
        public Physics.World BackGroundWorld
        {
            get { return backgroundWorld; }
        }

        GameConfig gameConfig;

        BasicEffect blockEffect;
        protected List<GamePiece> backgroundPieces;
        protected TiledTexture tiledTexture;

        protected Vector2 groundLocation = new Vector2(0, -4);

        float queueTimer;
        const float queueTimerMax = 2f;
        const float maxTimeBetweenSpawn = 5f;
        Queue<GamePiece> pieceQueue;

        float blockSize;
        const byte blockAlpha = 92;

        //last X location a block spawned at
        float lastX;
        const float xVarMax = 45f;
        const float spawnY = 40f;

        Gravity gravity;
        float backgroundDamping;

        public BackgroundBlocks(GameConfig config)
        {
            gameConfig = config;

            switch (config.gameDifficulty)
            {
                case GameDifficulty.Easy:
                    gameConfig.blockSize = BlockSize.BackgroundLarge;
                    backgroundDamping = .9f;
                    gravity = Gravity.Low;
                    break;
                case GameDifficulty.Medium:
                    gameConfig.blockSize = BlockSize.BackGroundMedium;
                    backgroundDamping = .8f;
                    gravity = Gravity.Low;
                    break;
                case GameDifficulty.Hard:
                    gameConfig.blockSize = BlockSize.BackgroundSmall;
                    backgroundDamping = .7f;
                    gravity = Gravity.Low;
                    break;
                default:
                    gameConfig.blockSize = BlockSize.BackGroundMedium;
                    backgroundDamping = .9f;
                    gravity = Gravity.Low;
                    break;
            }
            gameConfig.shape = ShapeType.Single;
            gameConfig.blockType = BlockType.Background;
            blockSize = GlobalConfig.EnumCoefficient(gameConfig.blockSize);
            RegisterEvents();
            
            queueTimer = 0;
            pieceQueue = new Queue<GamePiece>();

            backgroundPieces = new List<GamePiece>();
            backgroundWorld = Physics.WorldFactory.BuildWorld();
            backgroundWorld.SetBodyDampening(backgroundDamping);
            //backgroundWorld.PhysicsSimIter = 2;
            backgroundWorld.externalAccumulator = backgroundAccumulator;

            Physics.ClosedShape backgroundGroundShape;
            foreach (StaticBodyConfig newBody in buildbackgroundGround())
            {
                backgroundGroundShape = new Physics.ClosedShape(newBody.Vertexes);

                Physics.Body physicsBody = new Physics.Body(backgroundGroundShape, float.PositiveInfinity, newBody.Location + groundLocation, 0f, Vector2.One, false);
                backgroundWorld.addBody(physicsBody);
            }
        }

        public void LoadContent(ContentManager content)
        {
            Texture2D rawTexture = content.Load<Texture2D>(
                gameConfig.backgroundTextureName);
            tiledTexture = new TiledTexture(
                rawTexture, gameConfig.backgroundTextureSize, gameConfig.backgroundTextureSize);

            // visual effect
            blockEffect = new BasicEffect(GraphicsConfig.GraphicsDevice, null);
            blockEffect.VertexColorEnabled = true;
            blockEffect.LightingEnabled = false;
            blockEffect.TextureEnabled = GlobalConfig.DrawMethod == DrawStyle.Textured;
            blockEffect.Texture = tiledTexture.BaseTexture;
            blockEffect.View = GraphicsConfig.View;
            blockEffect.Projection = GraphicsConfig.Projection;
            blockEffect.World = GraphicsConfig.World;
        }

        public void Update(float elapsedTime)
        {
            backgroundWorld.Update(elapsedTime);

            queueTimer += elapsedTime;
            if (queueTimer > queueTimerMax && pieceQueue.Count > 0)
            {
                queueTimer = 0;
                GamePiece newPiece = pieceQueue.Dequeue();

                backgroundPieces.Add(newPiece);
                foreach (GameBlock theBlock in newPiece.Blocks)
                {
                    backgroundWorld.addBody(theBlock);
                }
                lastX = newPiece.GamePieceCenter().X;
            }
            else if (queueTimer > maxTimeBetweenSpawn)
            {
                queueTimer = 0;
                GamePiece newPiece = GamePieceFactory.CreateGamePiece(GetSpawnLocation(), tiledTexture, gameConfig, new BackgroundColorGenerator(null, blockAlpha));
                backgroundPieces.Add(newPiece);
                foreach (GameBlock theBlock in newPiece.Blocks)
                {
                    backgroundWorld.addBody(theBlock);
                }
            }
            
            for (int i = 0; i < backgroundPieces.Count; i++)
            {
                backgroundPieces[i].Update(elapsedTime);

                if (backgroundPieces[i].Blocks.Count == 0)
                {
                    backgroundPieces.Remove(backgroundPieces[i]);
                    i--;
                }
            }
        }

        public void Draw(DrawBatch drawBatch)
        {
            drawBatch.Begin(PrimitiveType.TriangleList, blockEffect);
            foreach (GamePiece piece in backgroundPieces)
            {
                piece.UpdateDrawVerts();
                drawBatch.Draw(piece.DrawVerts);
            }
            drawBatch.End();
        }

        private void backgroundAccumulator(float elapsedTime)
        {
            Vector2 gravityVector = new Vector2(0f, GlobalConfig.EnumCoefficient(gravity));

            foreach (GamePiece piece in backgroundPieces)
            {
                piece.ApplyForce(gravityVector);
                piece.GamePieceAccumulator(elapsedTime);
            }
        }

        private List<StaticBodyConfig> buildbackgroundGround()
        {
            List<StaticBodyConfig> groundBodies = new List<StaticBodyConfig>();

            float width = 30f;
            float height = 3f;

            Vector2 location;
            if (GraphicsConfig.IsWideScreen)
            {
                location = new Vector2(0f, -10f);
            }
            else
            {
                location = new Vector2(0f, -13f);
            }

            groundBodies.Add(new StaticBodyConfig(width, height, location));

            return groundBodies;
        }

        private void backgroundSpawnCallback(object sender, string theEvent, params object[] args)
        {
            Combo combo = args[0] as Combo;
            if (combo != null)
            {
                GamePiece newPiece = GamePieceFactory.CreateGamePiece(GetSpawnLocation(), tiledTexture, gameConfig, new BackgroundColorGenerator(combo.ComboColor, blockAlpha));
                pieceQueue.Enqueue(newPiece);
            }
        }

        private void RegisterEvents()
        {
            EventManager.Register(backgroundSpawnCallback, "Combo");
        }

        private void UnRegisterEvents()
        {
            EventManager.UnRegister(backgroundSpawnCallback, "Combo");
        }

        private Vector2 GetSpawnLocation()
        {
            Vector2 spawnLocation = new Vector2(0f, spawnY);
            float newX;
            int count = 1;
            /*this do while is here to make sure we don't spawn 2
             * blocks too close to each other, this helps make sure
             * we get a decent spread in the background block locations */
            do
            {
                newX = (float)RandomMath.Random.NextDouble() * xVarMax - (xVarMax / 2f);
                count++;
            } while (InRange(newX, lastX - blockSize * 2, lastX + blockSize * 2));
            spawnLocation.X = newX;
            lastX = newX;

            return spawnLocation;
        }

        private bool InRange(float val, float min, float max)
        {
            return val >= min && val <= max;
        }

        public void Dispose()
        {
            UnRegisterEvents();
            if (!physicsDisposed)
            {
                physicsDisposed = true;
                backgroundWorld.Dispose();
            }
        }
    }
}