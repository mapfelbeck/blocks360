using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Utilities;
using Physics;
using System.Collections;

namespace BalloonBlocks
{
    /// <summary>
    /// a GamePiece manages and draws the GameBlocks that it contains as well as the
    /// ExternalSprings that connect them
    /// </summary>
    public class GamePiece
    {
        protected List<GameBlock> blocks;
        public List<GameBlock> Blocks { get { return blocks; } }

        List<Physics.ExternalSpring> attatchSprings;

        protected VertexPositionColorTexture[] drawVerts = null;
        public VertexPositionColorTexture[] DrawVerts
        {
            get { return drawVerts; }
        }

        protected VertexPositionColorTexture[] frozenVerts = null;
        public VertexPositionColorTexture[] FrozenVerts
        {
            get { return frozenVerts; }
        }

        bool isControlledPiece;
        public bool IsControlledPiece
        {
            get { return isControlledPiece; }
            set { isControlledPiece = value; }
        }

        bool showColorPulse;
        public bool ShowColorPulse
        {
            get { return showColorPulse; }
            set { showColorPulse = value; }
        }

        protected int pieceNumber;
        public int PieceNumber
        {
            get { return pieceNumber; }
            set { pieceNumber = value; }
        }

        bool hasEverCollided;
        public bool HasEverCollided { get { return hasEverCollided; } }
        bool collidedThisFrame;
        public bool CollidedThisFrame { get { return collidedThisFrame; } }

        float pushForceX;
        float pushForceY;
        float torqueForce;

        bool inFailLocation;
        public bool InFailLocation
        {
            get { return inFailLocation; }
            set { inFailLocation = value; }
        }

        float remainingLifeTime;
        public float RemainingLifeTime
        {
            get { return remainingLifeTime; }
            set { remainingLifeTime = value; }
        }

        ControlMode controlMode;
        public ControlMode ControlMode
        {
            get { return controlMode; }
            set { controlMode = value; }
        }

        float autoDampRate;
        public float AutoDampRate
        {
            get { return autoDampRate; }
            set { autoDampRate = value; }
        }

        int originalBlockCount;
        float inputScalar;

        float rotationLastFrame;
        float rotationSpeed;

        public GamePiece(List<GameBlock> theBlocks, List<Physics.ExternalSpring> theSprings,
            VertexPositionColorTexture[] theVertexes, float gravityScalar)
        {
            showColorPulse = true;
            blocks = theBlocks;
            originalBlockCount = blocks.Count;
            attatchSprings = theSprings;
            drawVerts = theVertexes;
            frozenVerts = new VertexPositionColorTexture[drawVerts.Length];
            
            SetControlForces(gravityScalar);

            foreach (GameBlock block in blocks)
            {
                block.DeleteCallback += RemoveBlockFromGamePiece;
            }

            controlMode = ControlMode.Unassisted;

            inputScalar = (float)Math.Sqrt(2.0);
            //unpossible
            rotationLastFrame = 999f;
        }

        public void SetControlForces(float gravityScalar)
        {
            pushForceX = -gravityScalar * GameConstants.PushForceXCoefficient;
            pushForceY = -gravityScalar * GameConstants.PushForceYCoefficient;
            torqueForce = -gravityScalar * GameConstants.TorqueForceCoefficient;
        }

        public void Update(float elapsedTime)
        {
            float rotationThisFrame = GamePieceRotation();

            if (rotationLastFrame == 999f)
            {
                rotationLastFrame = rotationThisFrame;
            }
            rotationSpeed = (rotationThisFrame - rotationLastFrame) / elapsedTime;

            collidedThisFrame = false;
            foreach (GameBlock block in blocks)
            {
                if (block.HasEverCollided)
                {
                    if (!hasEverCollided)
                    {
                        EventManager.Trigger(this, "BlockHit");
                    }
                    hasEverCollided = true;
                    foreach (GameBlock otherBlock in blocks)
                    {
                        otherBlock.HasEverCollided = true;
                    }
                }
                if (block.CollidedThisFrame)
                {
                    collidedThisFrame = true;
                }
            }
            if (hasEverCollided && !collidedThisFrame)
            {
                //Console.WriteLine("collisionless frame");
                foreach (GameBlock block in blocks)
                {
                    block.CollisionlessFrame();
                }
            }
            //FIXME: why is this here?
            else if(hasEverCollided)
            {
                foreach (GameBlock block in blocks)
                {
                    block.CollidedThisFrame = false;
                }
                //Console.WriteLine("collision this frame, piece {0}",pieceNumber);
            }
            UpdateDrawVerts();
            UpdateFrozenVerts();

            rotationLastFrame = rotationThisFrame;
        }

        public void GamePieceAccumulator(float elapsedTime)
        {
            Vector2 Force = Vector2.Zero;

            for (int i = 0; i < attatchSprings.Count; i++)
            {
                VectorTools.calculateSpringForce(
                    ref attatchSprings[i].bodyA.getPointMass(attatchSprings[i].pointMassA).Position, ref attatchSprings[i].bodyA.getPointMass(attatchSprings[i].pointMassA).Velocity,
                    ref attatchSprings[i].bodyB.getPointMass(attatchSprings[i].pointMassB).Position, ref attatchSprings[i].bodyB.getPointMass(attatchSprings[i].pointMassB).Velocity,
                    attatchSprings[i].springD, attatchSprings[i].springK, attatchSprings[i].damping, ref Force);

                attatchSprings[i].bodyA.getPointMass(attatchSprings[i].pointMassA).Force += Force;
                attatchSprings[i].bodyB.getPointMass(attatchSprings[i].pointMassB).Force -= Force;
            }
        }

        public void ApplyForce(Vector2 force)
        {
            Vector2 position;
            foreach (GameBlock block in blocks)
            {
                position = block.DerivedPosition;
                block.addGlobalForce(ref position, ref force);
            }
        }

        /// <summary>
        /// Listens for pad input
        /// </summary>
        /// <param name="gamePadState"></param>
        /// <param name="keyboardState"></param>
        public void HandleInput(GamePadState gamePadState, KeyboardState keyboardState)
        {
            if (originalBlockCount != blocks.Count)
            {
                return;
            }

            Vector2 center = GamePieceCenter();

            float rotationAmount = gamePadState.Triggers.Left - gamePadState.Triggers.Right - gamePadState.ThumbSticks.Right.X;
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                rotationAmount += 1f;
            }
            if (keyboardState.IsKeyDown(Keys.Right))
            {
                rotationAmount -= 1f;
            }

            rotationAmount = MathHelper.Clamp(rotationAmount * inputScalar, -1f, 1f);

            if (rotationAmount == 0f && ControlMode == ControlMode.Easy)
            {
                rotationAmount = -MathHelper.Clamp(rotationSpeed, -autoDampRate, autoDampRate);
            }

            if (rotationAmount != 0f)
            {
                foreach (GameBlock body in blocks)
                {
                    body.rotateAmount = rotationAmount;
                    body.rotateForce = torqueForce;
                    body.rotateCenter = center;
                }
            }

            Vector2 nudge = Vector2.Zero;
            float nudgeY = 0f;
            if (controlMode == ControlMode.Easy)
            {
                //range from (-1,-.5,1) instead of (-1,0,1)
                nudgeY = .5f;
                float thumbInputY = gamePadState.ThumbSticks.Left.Y;
                if (thumbInputY > 0f)
                {
                    nudgeY += (MathHelper.Clamp(thumbInputY * inputScalar, -1f, 1f) / 2f);
                }
                else
                {
                    nudgeY += (MathHelper.Clamp(thumbInputY * inputScalar, -1f, 1f) * 1.5f);
                }
            }
            else
            {
                nudgeY = MathHelper.Clamp(gamePadState.ThumbSticks.Left.Y * inputScalar, -1f, 1f);
            }
            float nudgeX = MathHelper.Clamp(gamePadState.ThumbSticks.Left.X * inputScalar, -1f, 1f);
            if (keyboardState.IsKeyDown(Keys.D))
            {
                nudgeX += 1f;
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                nudgeX -= 1f;
            }
            if (keyboardState.IsKeyDown(Keys.W))
            {
                nudgeY += 1f;
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                nudgeY -= 1f;
            }
            
            nudge.X = nudgeX * pushForceX;
            nudge.Y = nudgeY * pushForceY;

            foreach (GameBlock body in blocks)
            {
                body.moveForce = nudge;
            }
        }

        /// <summary>
        /// Finds the center of mass of the GamePiece
        /// </summary>
        /// <returns></returns>
        public Vector2 GamePieceCenter()
        {
            Vector2 center = Vector2.Zero;

            foreach (Physics.SpringBody body in blocks)
            {
                center += body.DerivedPosition;
            }

            center = center / blocks.Count;
            return center;
        }

        public float GamePieceRotation()
        {
            float rotation = 0f;

            foreach (Physics.SpringBody body in blocks)
            {
                rotation += body.DerivedAngle;
            }

            rotation = rotation / blocks.Count;
            return rotation;
        }

        internal virtual void UpdateDrawVerts()
        {
            // update vert buffer.
            int vertIndex = 0;
            for (int i = 0; i < blocks.Count; i++)
            {
                drawVerts[vertIndex].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(0).Position);
                drawVerts[vertIndex + 1].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(1).Position);
                drawVerts[vertIndex + 2].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(2).Position);
                drawVerts[vertIndex + 3].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(2).Position);
                drawVerts[vertIndex + 4].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(3).Position);
                drawVerts[vertIndex + 5].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(0).Position);

                drawVerts[vertIndex].TextureCoordinate = blocks[i].DrawTile.LowerLeft;
                drawVerts[vertIndex + 1].TextureCoordinate = blocks[i].DrawTile.UpperLeft;
                drawVerts[vertIndex + 2].TextureCoordinate = blocks[i].DrawTile.UpperRight;

                drawVerts[vertIndex + 3].TextureCoordinate = blocks[i].DrawTile.UpperRight;
                drawVerts[vertIndex + 4].TextureCoordinate = blocks[i].DrawTile.LowerRight;
                drawVerts[vertIndex + 5].TextureCoordinate = blocks[i].DrawTile.LowerLeft;

                drawVerts[vertIndex].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 1].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 2].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 3].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 4].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 5].Color = blocks[i].BlockColor;

                if (IsControlledPiece && showColorPulse && remainingLifeTime < GameConstants.BlockSpawnWarningTime)
                {
                    Vector3 blockColor = blocks[i].BlockColor.ToVector3();
                    Vector3 pulseColor;
                    if (InFailLocation)
                    {
                        pulseColor = Color.Red.ToVector3();
                    }
                    else
                    {
                        pulseColor = Color.White.ToVector3();
                    }

                    float t = (float)Math.Cos(2 * remainingLifeTime);
                    t = .25f + .5f * t;

                    Color newColor = new Color(Vector3.Lerp(blockColor, pulseColor, t));
                    drawVerts[vertIndex].Color = newColor;
                    drawVerts[vertIndex + 1].Color = newColor;
                    drawVerts[vertIndex + 2].Color = newColor;
                    drawVerts[vertIndex + 3].Color = newColor;
                    drawVerts[vertIndex + 4].Color = newColor;
                    drawVerts[vertIndex + 5].Color = newColor;
                }
                
                if (GlobalConfig.ShimmerDisplayOption || blocks[i].Popping)
                {
                    Vector2[] shape = blocks[i].GlobalShape;
                    float pm0dist = (blocks[i].getPointMass(0).Position - shape[0]).Length() * 2f;
                    float pm1dist = (blocks[i].getPointMass(1).Position - shape[1]).Length() * 2f;
                    float pm2dist = (blocks[i].getPointMass(2).Position - shape[2]).Length() * 2f;
                    float pm3dist = (blocks[i].getPointMass(3).Position - shape[3]).Length() * 2f;

                    MathHelper.Clamp(pm0dist, 0, 1);
                    MathHelper.Clamp(pm1dist, 0, 1);
                    MathHelper.Clamp(pm2dist, 0, 1);
                    MathHelper.Clamp(pm3dist, 0, 1);                    

                    if (GlobalConfig.ShimmerDisplayOption && !isControlledPiece)
                    {
                        Vector3 blockColor = blocks[i].BlockColor.ToVector3();
                        Vector3 whiteColor = Color.White.ToVector3();

                        drawVerts[vertIndex + 0].Color = new Color(Vector3.Lerp(blockColor, whiteColor, pm0dist));
                        drawVerts[vertIndex + 1].Color = new Color(Vector3.Lerp(blockColor, whiteColor, pm1dist));
                        drawVerts[vertIndex + 2].Color = new Color(Vector3.Lerp(blockColor, whiteColor, pm2dist));

                        drawVerts[vertIndex + 3].Color = new Color(Vector3.Lerp(blockColor, whiteColor, pm2dist));
                        drawVerts[vertIndex + 4].Color = new Color(Vector3.Lerp(blockColor, whiteColor, pm3dist));
                        drawVerts[vertIndex + 5].Color = new Color(Vector3.Lerp(blockColor, whiteColor, pm0dist));
                    }
                    if (blocks[i].Popping)
                    {
                        drawVerts[vertIndex + 0].Color.A = (byte)MathHelper.Lerp(blocks[i].BlockColor.A, 0, pm0dist);
                        drawVerts[vertIndex + 1].Color.A = (byte)MathHelper.Lerp(blocks[i].BlockColor.A, 0, pm1dist);
                        drawVerts[vertIndex + 2].Color.A = (byte)MathHelper.Lerp(blocks[i].BlockColor.A, 0, pm2dist);

                        drawVerts[vertIndex + 3].Color.A = (byte)MathHelper.Lerp(blocks[i].BlockColor.A, 0, pm2dist);
                        drawVerts[vertIndex + 4].Color.A = (byte)MathHelper.Lerp(blocks[i].BlockColor.A, 0, pm3dist);
                        drawVerts[vertIndex + 5].Color.A = (byte)MathHelper.Lerp(blocks[i].BlockColor.A, 0, pm0dist);
                    }
                }
                vertIndex += 6;
            }
        }

        internal void UpdateFrozenVerts()
        {
            // update vert buffer.
            int vertIndex = 0;
            for (int i = 0; i < blocks.Count; i++)
            {
                frozenVerts[vertIndex].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(0).Position);
                frozenVerts[vertIndex + 1].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(1).Position);
                frozenVerts[vertIndex + 2].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(2).Position);
                frozenVerts[vertIndex + 3].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(2).Position);
                frozenVerts[vertIndex + 4].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(3).Position);
                frozenVerts[vertIndex + 5].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(0).Position);

                frozenVerts[vertIndex].TextureCoordinate = blocks[i].DrawTile.LowerLeft;
                frozenVerts[vertIndex + 1].TextureCoordinate = blocks[i].DrawTile.UpperLeft;
                frozenVerts[vertIndex + 2].TextureCoordinate = blocks[i].DrawTile.UpperRight;

                frozenVerts[vertIndex + 3].TextureCoordinate = blocks[i].DrawTile.UpperRight;
                frozenVerts[vertIndex + 4].TextureCoordinate = blocks[i].DrawTile.LowerRight;
                frozenVerts[vertIndex + 5].TextureCoordinate = blocks[i].DrawTile.LowerLeft;

                Color color = Color.LightBlue;
                if (!blocks[i].IsAsleep)
                {
                    color.A = 0;
                }
                else
                {
                    float trans = Math.Min(1f, blocks[i].TimeFrozen * .25f);
                    trans = trans * trans;
                    color.A = Math.Max((byte)64, (byte)(255 * trans));
                }
                frozenVerts[vertIndex].Color = color;
                frozenVerts[vertIndex + 1].Color = color;
                frozenVerts[vertIndex + 2].Color = color;
                frozenVerts[vertIndex + 3].Color = color;
                frozenVerts[vertIndex + 4].Color = color;
                frozenVerts[vertIndex + 5].Color = color;

                vertIndex += 6;
            }
        }

        /// <summary>
        /// Removes the specified Body from the GamePiece along with any 
        /// springs attached to it
        /// </summary>
        /// <param name="body"></param>
        internal void RemoveBlockFromGamePiece(Body body)
        {
            GameBlock block = body as GameBlock;

            if (block!=null && blocks.Contains(block))
            {
                RemoveSpringsAttachedTo(block);
            }
            drawVerts = new VertexPositionColorTexture[drawVerts.Length-6];
            frozenVerts = new VertexPositionColorTexture[drawVerts.Length];
            blocks.Remove(block);
        }

        private void RemoveSpringsAttachedTo(GameBlock block)
        {
            for (int i = 0; i < attatchSprings.Count; i++)
            {
                if (attatchSprings[i].bodyA == block || attatchSprings[i].bodyB == block)
                {
                    attatchSprings.RemoveAt(i);
                    i--;
                }
            }
        }
    }
}
