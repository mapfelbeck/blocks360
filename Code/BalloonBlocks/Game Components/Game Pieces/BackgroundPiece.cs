﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

namespace BalloonBlocks
{
    public class BackgroundPiece : GamePiece
    {
        public BackgroundPiece(List<GameBlock> theBlocks, List<Physics.ExternalSpring> theSprings,
            VertexPositionColorTexture[] theVertexes, float graviy) :
            base(theBlocks, theSprings, theVertexes, graviy)
        {
        }

        internal override void UpdateDrawVerts()
        {
            // update vert buffer.
            int vertIndex = 0;
            for (int i = 0; i < blocks.Count; i++)
            {
                drawVerts[vertIndex].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(0).Position);
                drawVerts[vertIndex + 1].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(1).Position);
                drawVerts[vertIndex + 2].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(2).Position);
                drawVerts[vertIndex + 3].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(2).Position);
                drawVerts[vertIndex + 4].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(3).Position);
                drawVerts[vertIndex + 5].Position = VectorTools.vec3FromVec2(
                    ref blocks[i].getPointMass(0).Position);

                drawVerts[vertIndex].TextureCoordinate = blocks[i].DrawTile.LowerLeft;
                drawVerts[vertIndex + 1].TextureCoordinate = blocks[i].DrawTile.UpperLeft;
                drawVerts[vertIndex + 2].TextureCoordinate = blocks[i].DrawTile.UpperRight;

                drawVerts[vertIndex + 3].TextureCoordinate = blocks[i].DrawTile.UpperRight;
                drawVerts[vertIndex + 4].TextureCoordinate = blocks[i].DrawTile.LowerRight;
                drawVerts[vertIndex + 5].TextureCoordinate = blocks[i].DrawTile.LowerLeft;

                drawVerts[vertIndex].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 1].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 2].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 3].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 4].Color = blocks[i].BlockColor;
                drawVerts[vertIndex + 5].Color = blocks[i].BlockColor;

                vertIndex += 6;
            }
        }
    }
}