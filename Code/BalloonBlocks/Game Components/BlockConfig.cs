﻿namespace BalloonBlocks
{
    public class BlockConfig
    {
        //time before effect begins
        public float timeTillDamping;
        //damping rate
        public float dampingRate;
        //max physics damping
        public float dampingMax;

        //time until deflating begins
        public float timeTillDeflate;
        //how much pressure does a deflating block loose per second
        public float deflateRate;

        //time until freeze
        public float timeTillFreeze;
        //how long to wait before attempts to freze the block
        public float freezeWaitTimerLength;
        //don't freeze if the block is moving faster than this rate
        public float freezeVelocityThreshhold;
        //don't freeze if the block is distroted by more than this
        public float freezeDistortionThreshhold;
    }
}