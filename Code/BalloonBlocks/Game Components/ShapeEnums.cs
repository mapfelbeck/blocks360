﻿namespace BalloonBlocks
{
    public enum ShapeType
    {
        Single,
        Double,
        Triomino,
        Tetromino,
        Pentomino,
        Square,
    };

    public enum DoubleShape
    {
        Vertical,
        Horizontal,
    };

    public enum TriominoShape
    {
        I,
        L,
    };

    public enum TetrominoShape
    {
        O,
        I,
        L,
        ReverseL,
        N,
        ReverseN,
        T,
    };

    public enum PentominoShape
    {
        F,
        ReverseF,
        I,
        L,
        ReverseL,
        N,
        ReverseN,
        P,
        ReverseP,
        T,
        U,
        V,
        W,
        X,
        Y,
        ReverseY,
        Z,
        ReverseZ,
    };
}
