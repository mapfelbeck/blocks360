﻿using System;
using Microsoft.Xna.Framework;

namespace BalloonBlocks
{
    public class DeflatingGameBlock : DampingGameBlock
    {
        public bool Deflated
        {
            get { return GasPressure <= 0; }
        }

        public DeflatingGameBlock(Physics.ClosedShape s, float massPerPoint,
            float shapeSpringK, float shapeSpringDamp, float edgeSpringK, float edgeSpringDamp,
            Vector2 pos, float angle, Vector2 scale, BlockConfig blockConfig)
            : base(s, massPerPoint, shapeSpringK, shapeSpringDamp,
            edgeSpringK, edgeSpringDamp, pos, angle, scale, blockConfig)
        {
        }
        
        /// <summary>
        /// Causes the block to lose pressure after it reaches a certain age
        /// </summary>
        /// <param name="elapsedTime">frame time</param>
        public override void Update(float elapsedTime)
        {
            base.Update(elapsedTime);

            if (!popping && lifeTime > config.timeTillDamping && !IsAsleep)
            {
                GasPressure = Math.Max(
                    GasPressure - elapsedTime * GameConstants.GasPressure * config.deflateRate, 0f);
            }
        }
    }
}
