﻿using Microsoft.Xna.Framework;

namespace BalloonBlocks
{
    /// <summary>
    /// Represents one square in the physics world
    /// </summary>
    public class BackgroundBlock : GameBlock
    {
        public BackgroundBlock(Physics.ClosedShape s, float massPerPoint,
            float shapeSpringK, float shapeSpringDamp, float edgeSpringK, float edgeSpringDamp,
            Vector2 pos, float angle, Vector2 scale, BlockConfig blockConfig)
            :
            base(s, massPerPoint, shapeSpringK, shapeSpringDamp, edgeSpringK, edgeSpringDamp,
            pos, angle, scale, blockConfig)
        {
            deleteRange = -30f;
        }

        internal override void CollideWith(Physics.Body otherBlock)
        {
        }
    }
}