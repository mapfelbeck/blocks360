﻿using System;
using Microsoft.Xna.Framework;
using Physics;

namespace BalloonBlocks
{
    /// <summary>
    /// GameBlock variant that looses presure as it ages and eventually locks in place
    /// </summary>
    public class FreezingGameBlock : DeflatingGameBlock
    {
        bool freezeAble;
        public override bool FreezeAble{
            get { return freezeAble; }
            set { freezeAble = value; }
        }

        float freezeWaitTimer;

        public bool IsFrozen
        {
            get { return getPointMass(0).Mass == float.PositiveInfinity; }
        }
        
        public FreezingGameBlock(Physics.ClosedShape s, float massPerPoint,
            float shapeSpringK, float shapeSpringDamp, float edgeSpringK, float edgeSpringDamp,
            Vector2 pos, float angle, Vector2 scale, BlockConfig blockConfig)
            :base(s, massPerPoint, shapeSpringK, shapeSpringDamp,
            edgeSpringK, edgeSpringDamp, pos, angle, scale, blockConfig)
        {
            freezeAble = true;
            freezeWaitTimer = 0;
        }

        /// <summary>
        /// Causes the block to loose pressure after it reaches a certain age, after
        /// it reaches 0 pressure it freezes in place
        /// </summary>
        /// <param name="elapsedTime"></param>
        public override void Update(float elapsedTime)
        {
            base.Update(elapsedTime);

            if (!popping && Deflated && !IsAsleep && lifeTime > config.timeTillFreeze && freezeAble)
            {
                freezeWaitTimer += elapsedTime;
                if (freezeWaitTimer > config.freezeWaitTimerLength)
                {
                    if (CanFreeze())
                    {
                        FreezeBlock();
                    }
                    else
                    {
                        freezeWaitTimer = 0f;
                    }
                }
            }
            else if (popping && IsFrozen)
            {
                UnFreezeBlock();
            }
            if (IsAsleep)
            {
                timeFrozen += elapsedTime;
            }
        }

        private bool UnFreezeBlock()
        {
            if (!IsAsleep) { return false; }

            timeFrozen = 0f;
            IsAsleep = false;
            foreach (Physics.PointMass mass in pointMasses)
            {
                mass.Mass = GameConstants.MassPerPoint;
                mass.Force = Vector2.Zero;
                mass.Velocity = Vector2.Zero;
            }
            return true;
        }

        private void FreezeBlock()
        {
            IsAsleep = true;
            for (int i = 0; i < PointMassCount; i++)
            {
                PointMass mass = getPointMass(i);

                ///Sub-Pixel Distance Hack
                ///In the freeze game type it's possile for pieces of the
                ///same color to lock in place less than a pixel away,
                ///looking like they're touching when in the physcics sim
                ///they aren't, leading to match-3 combos that look good
                ///but are invalid, this hack moves the pointmasses out a tiny
                ///bit when they lock, largly eliminating the problem
                Vector2 pointMassDirectionFromDerived;
                pointMassDirectionFromDerived = mass.Position - DerivedPosition;
                pointMassDirectionFromDerived.Normalize();

                mass.Position += pointMassDirectionFromDerived * .1f;
                ///End Hack

                mass.Mass = float.PositiveInfinity;
            }
        }

        private bool CanFreeze()
        {
            bool result = true;
            //cant't freeze if the block isn't touching anything
            if(!CollidedThisFrame){
                result = false;
            }
            //can't freeze if the block is moving too fast
            else if (DerivedVelocity.LengthSquared() > config.freezeVelocityThreshhold)
            {
                result = false;
            }
            else
            {
                // this checks to see how distorted the block is, if the block is too
                // smashed out of shape we wait an freeze it later
                // this keeps blocks from freezing in ugly, twisted shapes
                float accumulatedDistortion = 0;
                Vector2 distortion = Vector2.Zero;
                for (int i = 0; i < PointMassCount; i++)
                {
                    distortion = getPointMass(i).Position - GlobalShape[i];
                    accumulatedDistortion += distortion.Length();
                }
                if (accumulatedDistortion >= config.freezeDistortionThreshhold)
                {
                    result = false;
                }
            }
            return result;
        }

        public override void CollisionlessFrame()
        {
            base.CollisionlessFrame();

            if (IsFrozen)
            {
                if (UnFreezeBlock())
                {
                    freezeWaitTimer -= config.freezeWaitTimerLength;
                }
            }
        }

        internal void UnFreezeFor(float seconds)
        {
            UnFreezeBlock();
            freezeWaitTimer -= seconds;
        }

        public override void resetExternalForces()
        {
            base.resetExternalForces();
            //CollidedThisFrame = false;
        }
    }
}