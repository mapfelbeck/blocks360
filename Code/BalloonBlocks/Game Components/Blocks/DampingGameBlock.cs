﻿using System;
using Microsoft.Xna.Framework;

namespace BalloonBlocks
{
    /// <summary>
    /// GameBlock variant that increases physics damping as it ages
    /// </summary>
    public class DampingGameBlock : GameBlock
    {
        public DampingGameBlock(Physics.ClosedShape s, float massPerPoint,
            float shapeSpringK, float shapeSpringDamp, float edgeSpringK, float edgeSpringDamp,
            Vector2 pos, float angle, Vector2 scale, BlockConfig blockConfig)
            : base(s, massPerPoint, shapeSpringK, shapeSpringDamp,
            edgeSpringK, edgeSpringDamp, pos, angle, scale, blockConfig)
        {
        }

        /// <summary>
        /// Causes the block to loose pressure after it reaches a certain age, after
        /// it reaches 0 pressure it freezes in place
        /// </summary>
        /// <param name="elapsedTime"></param>
        public override void Update(float elapsedTime)
        {
            base.Update(elapsedTime);
            if (lifeTime > config.timeTillDamping)
            {
                float dampingStep = elapsedTime * config.dampingRate;
                VelocityDamping = Math.Min(VelocityDamping + dampingStep, config.dampingMax);
            }

        }
    }
}