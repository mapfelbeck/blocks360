using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

namespace BalloonBlocks
{
    /// <summary>
    /// Represents one square in the physics world
    /// </summary>
    public class GameBlock : Physics.PressureBody
    {
        public virtual bool FreezeAble
        {
            get { return false; }
            set { ;}
        }

        protected BlockConfig config;

        Tile drawTile;
        public Tile DrawTile
        {
            get { return drawTile; }
            set { drawTile = value; }
        }

        public Color NormalColor = Color.White;

        public Color BlockColor
        {
            get
            {
                return NormalColor;
            }
            set
            {
                NormalColor = value;
            }
        }

        protected bool popping = false;
        public bool Popping
        {
            get { return popping; }
            set
            {
                if (value == true)
                {
                    popping = value;
                }
            }
        }

        bool hasEverCollided;
        public bool HasEverCollided
        {
            get { return hasEverCollided; }
            set { hasEverCollided = value; }
        }

        bool collidedThisFrame;
        public bool CollidedThisFrame
        {
            get { return collidedThisFrame; }
            set { collidedThisFrame = value; }
        }

        private float collideTime = 0f;

        public Vector2 moveForce;
        public float rotateAmount;
        public float rotateForce;
        public Vector2 rotateCenter;

        protected float lifeTime;

        //delete if the block falls below this treshhold
        protected float deleteRange;

        protected float timeFrozen;
        public float TimeFrozen
        {
            get { return timeFrozen; }
        }

        bool poppable;
        public bool Poppable
        {
            get { return poppable; }
            set { poppable = value; }
        }

        List<GameBlock> CollisionList;

        public GameBlock(Physics.ClosedShape s, float massPerPoint,
            float shapeSpringK, float shapeSpringDamp, float edgeSpringK, float edgeSpringDamp,
            Vector2 pos, float angle, Vector2 scale, BlockConfig blockConfig)
            :
            base(s, massPerPoint, GameConstants.GasPressure, shapeSpringK, shapeSpringDamp, edgeSpringK, edgeSpringDamp,
            pos, angle, scale, false)
        {
            config = blockConfig;
            CollisionCallback = CollideWith;
            CollisionList = new List<GameBlock>();
            moveForce = Vector2.Zero;
            rotateAmount = 0f;
            lifeTime = 0f;
            deleteRange = -80f;
            timeFrozen = 0f;
            poppable = true;
        }

        /// <summary>
        /// When the block collides with other blocks of it's color, add the other block to
        /// it's list of collisions
        /// </summary>
        /// <param name="otherBlock"></param>
        internal virtual void CollideWith(Physics.Body otherBlock)
        {
            if (GroupNumber != otherBlock.GroupNumber)
            {
                hasEverCollided = true;
            }

            if (GroupNumber != otherBlock.GroupNumber && !otherBlock.IsStatic ||
                hasEverCollided && !otherBlock.IsStatic)
            {
                collidedThisFrame = true;
            }

            GameBlock other = otherBlock as GameBlock;
            if (other != null && NormalColor == other.NormalColor)
            {
                if (!CollisionList.Contains(other))
                {
                    CollisionList.Add(other);
                }
            }
        }

        public override void resetExternalForces()
        {
            base.resetExternalForces();

            moveForce = Vector2.Zero;
            rotateAmount = 0f;
            rotateCenter = Vector2.Zero;
        }

        /// <summary>
        /// check to see if the block has fallen off the edge of the screen, has
        /// collided with enough other blocks it's color or has popped
        /// </summary>
        /// <param name="elapsedTime"></param>
        public override void Update(float elapsedTime)
        {
            base.Update(elapsedTime);

            if (DerivedPosition.Y < deleteRange)
            {
                //delete because we're out of bounds
                deleteThis = true;
            }

            if (hasEverCollided)
            {
                lifeTime += elapsedTime;
            }

            //if a block has collided with 2 or more others of it's color we know
            //that at least 3 of the same color are touching, so mark this block and
            //every block in it's collision list as collided so they inflate and pop
            if (CollisionList.Count >= 2 && Poppable)
            {
                popping = true;

                for (int i = 0; i < CollisionList.Count; i++)
                {
                    CollisionList[i].popping = true;
                }
                //EventManager.Trigger(this, "Inflate");
            }

            CollisionList.Clear();

            if (popping)
            {
                collideTime += elapsedTime;
                GasPressure += elapsedTime * GameConstants.GasPressure;
                if (collideTime > GameConstants.BlockCollideTime)
                {
                    deleteThis = true;
                    EventManager.Trigger(this, "BubblePop");
                }
            }
        }

        /// <summary>
        /// accumulates gravity and any input forces on the block
        /// </summary>
        /// <param name="elapsedTime"></param>
        public override void accumulateExternalForces(float elapsedTime)
        {
            base.accumulateExternalForces(elapsedTime);

            if (!IsAsleep)
            {
                Vector2 nCenter = new Vector2();
                Vector2 origin = new Vector2();
                Vector2 rotationForce = new Vector2();

                // gravity, and control input
                for (int i = 0; i < pointMasses.Count; i++)
                {
                    pointMasses[i].Force += moveForce;

                    if (rotateAmount != 0f)
                    {
                        nCenter = pointMasses[i].Position + rotateCenter;
                        origin = pointMasses[i].Position - rotateCenter;

                        rotationForce.X =
                            origin.X * (float)Math.Cos(rotateAmount) - origin.Y * (float)Math.Sin(rotateAmount);
                        rotationForce.Y =
                            origin.X * (float)Math.Sin(rotateAmount) + origin.Y * (float)Math.Cos(rotateAmount);
                        pointMasses[i].Force += rotationForce * rotateForce;
                    }
                }
            }
        }
        public virtual void CollisionlessFrame() { }
    }
}