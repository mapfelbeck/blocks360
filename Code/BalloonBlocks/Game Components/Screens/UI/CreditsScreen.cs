﻿#region File Description
//-----------------------------------------------------------------------------
// BackgroundScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace BalloonBlocks
{
    public class CreditsScreen : SplashScreen
    {
        SpriteFont creditsFont;

        float fontHeight;
        float entrySeperation;

        Vector2 scrollPosition;
        float scrollRate;

        string[] entries = {"Credits:",
                            "",
                            "Gameplay Design",
                            "Michael Apfelbeck",
                            "creativecoggames.com",
                            "",
                            "Graphic Design",
                            "Joseph Gray",
                            "grauwald.com",
                            "",
                            "Programming",
                            "Michael Apfelbeck",
                            "creativecoggames.com",
                            "",
                            "Music Tracks",
                            "Kevin MacLeod",
                            "incompetech.com",
                            "",
                            "QA",
                            "Michael Apfelbeck",
                            "Joseph Burchett"};
        /*
        string[] entries = {"Credits:",
                            "",
                            "Gameplay Design",
                            "Michael Apfelbeck",
                            "",
                            "Programming",
                            "Michael Apfelbeck",
                            "",
                            "QA",
                            "Michael Apfelbeck"};
        */
        /// <summary>
        /// Constructor.
        /// </summary>
        public CreditsScreen()
            : base("Menu Backgrounds/Blank", true, false, 100f, false)
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        public override void LoadContent()
        {
            base.LoadContent();

            creditsFont = Content.Load<SpriteFont>("Fonts/MenuFont");
            fontHeight = creditsFont.MeasureString("H").X;
            entrySeperation = fontHeight;

            scrollPosition = new Vector2(
                (float)GraphicsConfig.GraphicsDevice.Viewport.Width / 2,
                (float)GraphicsConfig.GraphicsDevice.Viewport.Height);
            scrollRate = 50f;
        }

        /// <summary>
        /// Updates the background screen. Unlike most screens, this should not
        /// transition off even if it has been covered by another screen: it is
        /// supposed to be covered, after all! This overload forces the
        /// coveredByOtherScreen parameter to false in order to stop the base
        /// Update method wanting to transition off.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);
            scrollPosition.Y -= (float)gameTime.ElapsedGameTime.TotalSeconds * scrollRate;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);

            Vector2 position = scrollPosition;
            Vector2 entryOrigin;

            spriteBatch.Begin(SpriteBlendMode.AlphaBlend);
            foreach (string entry in entries)
            {
                entryOrigin = creditsFont.MeasureString(entry) / 2;
                spriteBatch.DrawString(creditsFont, entry, position - entryOrigin + Vector2.One,
                    Color.Black);
                spriteBatch.DrawString(creditsFont, entry, position - entryOrigin,
                    Color.White);
                position.Y += fontHeight + entrySeperation;
            }
            spriteBatch.End();

            if (position.Y < 0f)
            {
                exit = true;
            }
        }

        public override void TransitionOff(PlayerIndex? playerIndex)
        {
            LoadingScreen.Load(ScreenManager, false, playerIndex.Value,
                               new BackgroundScreen("Menu Backgrounds/Main"),
                               new MainMenuScreen());
        }
    }
}