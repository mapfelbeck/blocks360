﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace BalloonBlocks
{
    public class RandomizedBackgroundScreen : BackgroundScreen
    {
        string[] BaseLayerImages = { "Randomized Backgrounds/Base/Base1", 
                                       "Randomized Backgrounds/Base/Base2", 
                                       "Randomized Backgrounds/Base/Base3", 
                                       "Randomized Backgrounds/Base/Base4" };
        
        string[] FillLayerImages = { "Randomized Backgrounds/Fill/Fill1", 
                                       "Randomized Backgrounds/Fill/Fill2", 
                                       "Randomized Backgrounds/Fill/Fill3", 
                                       "Randomized Backgrounds/Fill/Fill4" };

        string[] OverlayLayerImages = { "Randomized Backgrounds/Overlay/Overlay1", 
                                       "Randomized Backgrounds/Overlay/Overlay2", 
                                       "Randomized Backgrounds/Overlay/Overlay3", 
                                       "Randomized Backgrounds/Overlay/Overlay4" };

        Texture2D baseLayer;
        Texture2D fillLayer;
        Texture2D overlayLayer;

        Color fillColor;
        byte fillAlpha;
        Color overlayColor;
        byte overlayAlpha;

        Rectangle sourceRect;

        public RandomizedBackgroundScreen():base("")
        {
        }

        public override void LoadContent()
        {
            base.LoadContent();

            baseLayer = Content.Load<Texture2D>(BaseLayerImages[RandomMath.Random.Next(BaseLayerImages.Length)]);
            fillLayer = Content.Load<Texture2D>(FillLayerImages[RandomMath.Random.Next(FillLayerImages.Length)]);
            overlayLayer = Content.Load<Texture2D>(OverlayLayerImages[RandomMath.Random.Next(OverlayLayerImages.Length)]);

            fillAlpha = 128;
            overlayAlpha = 32;

            fillColor = Color.White;
            fillColor.A = fillAlpha;

            overlayColor = Color.White;
            overlayColor.A = overlayAlpha;

            sourceRect = SourceRectangle(baseLayer);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);
            byte fade = TransitionAlpha;

            spriteBatch.Begin(SpriteBlendMode.AlphaBlend);

            spriteBatch.Draw(baseLayer, fullscreen, sourceRect, Color.White);
            spriteBatch.Draw(fillLayer, fullscreen, sourceRect, fillColor);
            spriteBatch.Draw(overlayLayer, fullscreen, sourceRect, overlayColor);

            spriteBatch.End();
        }
    }
}