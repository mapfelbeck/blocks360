﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;
using Utilities;
#endregion

namespace BalloonBlocks
{
    public class IntroScreen : SplashScreen
    {
        const string pressStartPrompt = "Press [A] or [START] to start.";
        const string dummyString = "Press A or S to start.";
        Vector2 drawPosition;
        Vector2 drawOrigin;

        public IntroScreen()
            : base("Splash Screens/Title Screen", GameConstants.IntroSkippable, GameConstants.IntroTimesOut,GameConstants.IntroDisplayTime, true)
        {
        }

        public override void LoadContent()
        {
            base.LoadContent();
            Vector2 textSize = font.MeasureString(dummyString);
            drawOrigin = textSize / 2;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            drawPosition = new Vector2(GraphicsConfig.SafeArea.Center.X,
                                        (GraphicsConfig.SafeArea.Center.Y + GraphicsConfig.SafeArea.Bottom) / 2);

            ScreenManager.SpriteBatch.Begin();
            DrawShadowedString(ScreenManager.SpriteBatch, pressStartPrompt, drawPosition,
                drawOrigin, 1f, Vector2.Zero);
            ScreenManager.SpriteBatch.End();
        }

        public override void TransitionOff(PlayerIndex? playerIndex)
        {
            if (playerIndex.HasValue)
            {
                LoadingScreen.Load(ScreenManager, false, playerIndex.Value,
                                   new BackgroundScreen("Menu Backgrounds/Main"),
                                   new MainMenuScreen());
            }
            else
            {
                LoadingScreen.Load(ScreenManager, false, null,
                                   new BackgroundScreen("Menu Backgrounds/Main"),
                                   new MainMenuScreen());
            }
        }

        protected override bool SkipButtonPressed(InputState input, out PlayerIndex playerIndex)
        {
            return input.IsAnyKeyPressed(ControllingPlayer, out playerIndex)||
                    input.IsNewButtonPress(Buttons.A, ControllingPlayer, out playerIndex)||
                    input.IsNewButtonPress(Buttons.Start, ControllingPlayer, out playerIndex);
        }
    }
}