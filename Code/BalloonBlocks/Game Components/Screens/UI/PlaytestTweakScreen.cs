﻿#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways. Interfaces with the GlobalConfig.
    /// </summary>
    class PlayTestTweakScreen : PressureBodyMenuScreen
    {
        const float repeatRate = .15f;

        MenuEntry autoDampMenuEntry;
        MenuEntry autoDampRateMenuEntry;
        MenuEntry speedIncreaseMenuEntry;

        static bool autoDampEnabled = GlobalConfig.AutoDampRotation;
        static int autoDampRate = GlobalConfig.AutoDampRate;

        static bool increasingSpeed = GlobalConfig.IncreasingSpeed;

        public PlayTestTweakScreen()
            : base("Playtest Tweakables")
        {
            // Create our menu entries.
            autoDampMenuEntry = CreateOptionMenuEntry(string.Empty);
            autoDampRateMenuEntry = CreateOptionMenuEntry(string.Empty);
            speedIncreaseMenuEntry = CreateOptionMenuEntry(string.Empty);

            autoDampRateMenuEntry.HoldToRepeat = true;
            autoDampRateMenuEntry.RepeatRate = repeatRate;

            MenuTextChanged = true;

            MenuEntry exitMenuEntry = CreateBackMenuEntry("Back");

            autoDampMenuEntry.Selected += ToggleAutoDampMenuEntrySelected;
            autoDampMenuEntry.IncrementEntry += ToggleAutoDampMenuEntrySelected;
            autoDampMenuEntry.DecrementEntry += ToggleAutoDampMenuEntrySelected;

            autoDampRateMenuEntry.Selected += AutoDampRateUp;
            autoDampRateMenuEntry.IncrementEntry += AutoDampRateUp;
            autoDampRateMenuEntry.DecrementEntry += AutoDampRateDown;

            speedIncreaseMenuEntry.Selected += ToggleSpeedIncrease;
            speedIncreaseMenuEntry.IncrementEntry += ToggleSpeedIncrease;
            speedIncreaseMenuEntry.DecrementEntry += ToggleSpeedIncrease;

            autoDampMenuEntry.NormalDetailText = "Stop game pieces from rotating when you're\nnot holding down the rotate buttons";
            autoDampRateMenuEntry.NormalDetailText = "How quickly rotation stops";
            speedIncreaseMenuEntry.NormalDetailText = "Increase speed as the Game progresses";

            exitMenuEntry.Selected += OnCancel;

            AddMenuEntry(autoDampMenuEntry);
            AddMenuEntry(autoDampRateMenuEntry);
            AddMenuEntry(speedIncreaseMenuEntry);
            AddMenuEntry(exitMenuEntry);
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            EventManager.Trigger(this, "SaveSettings");
            LoadingScreen.Load(ScreenManager, false, null,
                               new BackgroundScreen("Menu Backgrounds/Main"),
                               new MainMenuScreen());
        }

        protected override void SetMenuEntryText()
        {
            autoDampMenuEntry.Text = "Auto Dampen Rotation: " + (autoDampEnabled ? "on" : "off");
            autoDampRateMenuEntry.Text = "Auto Dampen Rate: " + ((int)autoDampRate).ToString();
            speedIncreaseMenuEntry.Text = "Game speed increases: " + (increasingSpeed ? "on" : "off");

            base.SetMenuEntryText();
        }

        void ToggleSpeedIncrease(object sender, PlayerIndexEventArgs e)
        {
            increasingSpeed = !increasingSpeed;

            GlobalConfig.IncreasingSpeed = increasingSpeed;

            MenuTextChanged = true;
        }

        void ToggleAutoDampMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            autoDampEnabled = !autoDampEnabled;

            GlobalConfig.AutoDampRotation = autoDampEnabled;

            MenuTextChanged = true;
        }

        void AutoDampRateUp(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.AutoDampRateUp();

            autoDampRate = GlobalConfig.AutoDampRate;

            MenuTextChanged = true;
        }

        void AutoDampRateDown(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.AutoDampRateDown();

            autoDampRate = GlobalConfig.AutoDampRate;

            MenuTextChanged = true;
        }
    }
}