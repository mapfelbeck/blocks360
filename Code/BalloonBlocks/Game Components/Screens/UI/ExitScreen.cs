﻿
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;
using Utilities;
#endregion

namespace BalloonBlocks
{
    public class ExitScreen : SplashScreen
    {
        string[] ExitMessage = { "Like it? Love it? Hate it ?!",
                                   "Feedback@CreativeCogGames.com"};
        
        string[] UpsellMessage = { "Press [A] to buy Ballon Blocks"};

        string[] displayedMessage;

        public ExitScreen()
            : base(
            "Splash Screens/Exit Screen",
            GameConstants.ExitSkippable 
#if XBOX360
            && !Guide.IsTrialMode
#endif
            , GameConstants.ExitTimesOut, GameConstants.ExitDisplayTime, false)
        {
#if XBOX360
            if (Guide.IsTrialMode)
            {
                displayedMessage = UpsellMessage;
            }
            else
            {
#endif
                displayedMessage = ExitMessage; 
#if XBOX360
            }
#endif
        }

        public override void TransitionOff(PlayerIndex? playerIndex)
        {
            ScreenManager.Game.Exit();
        }

        public override void LoadContent()
        {
            base.LoadContent();

        }

#if XBOX360
        public override void HandleInput(InputState input)
        {
            base.HandleInput(input);
            
            PlayerIndex playerIndex;

            if (Guide.IsTrialMode && 
                (input.IsNewButtonPress(Buttons.A, ControllingPlayer, out playerIndex) || 
                    input.IsNewKeyPress(Keys.Space, ControllingPlayer, out playerIndex)))
            {
                if (PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value) &&
                    !Guide.IsVisible)
                {
                    SellStuff();
                }
                else if (!PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value))
                {
                    PlayerManager.ShowSignIn(SellStuff);
                }
            }
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (!Guide.IsTrialMode)
            {
                displayedMessage = ExitMessage;
                skippable = true;
            }
            else
            {
                displayedMessage = UpsellMessage;
            }
        }
#endif

        protected void SellStuff()
        {
            if (PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value) &&
                !Guide.IsVisible)
            {
                Guide.ShowMarketplace(ControllingPlayer.Value);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Rectangle safeArea = GraphicsConfig.SafeArea;
            Vector2 shadowScale = Vector2.One *2;
            Vector2 messageSize = Vector2.Zero;
            Vector2 screenCenter = new Vector2(safeArea.Center.X, safeArea.Center.Y);
            float messageScale = 0;
            foreach (string line in displayedMessage)
            {
                messageSize.X = Math.Max(messageSize.X, font.MeasureString(line).X);
                messageSize.Y += font.MeasureString(line).Y;
            }
            messageScale = (float)safeArea.Width / messageSize.X;

            Vector2 location = screenCenter;
            location.Y -= (messageSize.Y / 2) * messageScale;

            spriteBatch.Begin();
            foreach (string line in displayedMessage)
            {
                Vector2 origin = font.MeasureString(line) / 2;
                DrawShadowedString(spriteBatch, line, location, origin, messageScale, shadowScale * messageScale);
                location.Y += (messageSize.Y / 2) * messageScale;
            }
            spriteBatch.End();
        }
    }
}