﻿#region File Description
//-----------------------------------------------------------------------------
// OptionsMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System;
using System.Globalization;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways. Interfaces with the GlobalConfig.
    /// </summary>
    class HighScoreScreen : PressureBodyMenuScreen
    {
        MenuEntry typeMenuEntry;
        MenuEntry difficultyMenuEntry;

        GameType type = GameType.Normal;
        GameDifficulty difficulty = GameDifficulty.Easy;

        float scoreEntryHeight;
        /// <summary>
        /// Constructor.
        /// </summary>
        public HighScoreScreen()
            : base("High Scores")
        {
            typeMenuEntry = CreateMenuEntry(string.Empty);
            difficultyMenuEntry = CreateMenuEntry(string.Empty);
            MenuEntry backMenuEntry = CreateBackMenuEntry("Back");

            //backMenuEntry.DetailText = "Go back to the main menu.";

            MenuTextChanged = true;

            typeMenuEntry.Selected += NextGameType;
            typeMenuEntry.IncrementEntry += NextGameType;
            typeMenuEntry.DecrementEntry += PrevGameType;

            difficultyMenuEntry.Selected += NextGameDifficulty;
            difficultyMenuEntry.IncrementEntry += NextGameDifficulty;
            difficultyMenuEntry.DecrementEntry += PrevGameDifficulty;

            backMenuEntry.Selected += OnCancel;

            AddMenuEntry(typeMenuEntry);
            AddMenuEntry(difficultyMenuEntry);
            AddMenuEntry(backMenuEntry);
        }

        protected override void SetMenuEntryText()
        {
            typeMenuEntry.Text = type.ToString();
            difficultyMenuEntry.Text = difficulty.ToString();
            base.SetMenuEntryText();
        }

        void NextGameType(object sender, PlayerIndexEventArgs e)
        {
            type++;
            if (type > GameType.Survival)
            {
                type = GameType.Normal;
            }
            difficulty = GameDifficulty.Easy;
            MenuTextChanged = true;
        }

        void PrevGameType(object sender, PlayerIndexEventArgs e)
        {
            type--;
            if (type < GameType.Normal)
            {
                type = GameType.Survival;
            }
            MenuTextChanged = true;
        }

        void NextGameDifficulty(object sender, PlayerIndexEventArgs e)
        {
            difficulty++;
            if (difficulty > GameDifficulty.Hard)
            {
                difficulty = GameDifficulty.Easy;
            }
            MenuTextChanged = true;
        }

        void PrevGameDifficulty(object sender, PlayerIndexEventArgs e)
        {
            difficulty--;
            if (difficulty < GameDifficulty.Easy)
            {
                difficulty = GameDifficulty.Hard;
            }
            MenuTextChanged = true;
        }

        public override void LoadContent()
        {
            base.LoadContent();
            scoreEntryHeight = ScreenManager.Font.MeasureString("H").Y;
        }

        protected override void OnCancel(PlayerIndex playerIndex)
        {
            //EventManager.Trigger(this, "MenuBack");
            LoadingScreen.Load(ScreenManager, false, null,
                               new BackgroundScreen("Menu Backgrounds/Main"),
                               new MainMenuScreen());
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            //Vector2 position = new Vector2(225f, 100f);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            Color drawColor = new Color(255, 255, 255, TransitionAlpha);
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            //float menuRightSide = MenuWidth() * 1.25f + GraphicsConfig.SafeArea.X * 1.5f;
            float menuRightSide = GraphicsConfig.SafeArea.X * 3f;

            string header = ScoreHeader();
            Vector2 headerPosition = new Vector2(menuRightSide,
                new Vector2(GraphicsConfig.GraphicsDevice.Viewport.Width / 2, GraphicsConfig.GraphicsDevice.Viewport.Height / 8).Y + font.MeasureString(MenuTitle).Y * .4f);

            if (ScreenState == ScreenState.TransitionOn)
            {
                headerPosition.X += transitionOffset * 256;
            }
            else
            {
                headerPosition.X += transitionOffset * 512;
            }

            Vector2 scorePosition = new Vector2(menuRightSide,
                new Vector2(GraphicsConfig.GraphicsDevice.Viewport.Width / 2, GraphicsConfig.GraphicsDevice.Viewport.Height / 8).Y + font.MeasureString(MenuTitle).Y);

            if (ScreenState == ScreenState.TransitionOn)
            {
                scorePosition.X += transitionOffset * 256;
            }
            else
            {
                scorePosition.X += transitionOffset * 512;
            }

            List<HighScoreEntry> list = HighScoreTable.GetScores(type, difficulty);
            TimeSpan time;

            spriteBatch.Begin();

            DrawShadowedString(spriteBatch, font, header, headerPosition, drawColor);

            string entry;
            for (int i = 0; i < 10 && i < list.Count; i++)
            {
                time = TimeSpan.FromSeconds(list[i].PlayTime);
                entry = ScoreEntry(list, ref time, i);
                DrawShadowedString(spriteBatch, font, entry, scorePosition, drawColor);
                scorePosition.Y += scoreEntryHeight * .9f;
            }
            spriteBatch.End();
        }

        private float MenuWidth()
        {
            float maxX = 0f;

            foreach (MenuEntry entry in MenuEntries)
            {
                maxX = Math.Max(maxX, ScreenManager.Font.MeasureString(entry.Text).X);
            }

            return maxX;
        }

        private string ScoreEntry(List<HighScoreEntry> list, ref TimeSpan time, int listNumber)
        {
            string entry = "";
            
            switch (type)
            {
                case GameType.Normal:
                    entry = "  Player  Score  Time";
                    entry = string.Format(
                        "{0,-2} {1,-6} {2,-6} {3:00}:{4:00}:{5:00}",
                        listNumber + 1, list[listNumber].Name, list[listNumber].Score.ToString("#,#", CultureInfo.CurrentCulture), time.Hours, time.Minutes, time.Seconds);
                    break;
                case GameType.Clear:
                    entry = string.Format(
                        "{0,-2} {1,-8} {2,-5} {3,-6} {4:00}:{5:00}:{6:00}",
                        listNumber + 1, list[listNumber].Name, list[listNumber].BlocksRemaining, list[listNumber].Score.ToString("#,#", CultureInfo.CurrentCulture), time.Hours, time.Minutes, time.Seconds);
                    break;
                case GameType.Survival:
                    entry = string.Format(
                        "{0,-2} {1,-7} {2:00}:{3:00}:{4:00} {5,-6}",
                        listNumber + 1, list[listNumber].Name, time.Hours, time.Minutes, time.Seconds, list[listNumber].Score.ToString("#,#", CultureInfo.CurrentCulture));
                    break;
                default:
                    break;
            }
            return entry;
        }

        string ScoreHeader()
        {
            string header = "";
            switch (type)
            {
                case GameType.Normal:
                    header = "  Player  Score  Time";
                    break;
                case GameType.Clear:
                    header = "  Player  Blocks  Score    Time";
                    break;
                case GameType.Survival:
                    header = "  Player    Time    Score";
                    break;
                default:
                    break;
            }
            return header;
        }
    }
}