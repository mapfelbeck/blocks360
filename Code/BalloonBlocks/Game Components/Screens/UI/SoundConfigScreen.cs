﻿#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    ///  Interfaces with the GraphicsConfig.
    /// </summary>
    class SoundConfigScreen : PressureBodyMenuScreen
    {
        const float repeatRate = .15f;

        MenuEntry soundEffectsMenuEntry;
        MenuEntry soundVolumeMenuEntry;
        MenuEntry musicMenuEntry;
        MenuEntry musicVolumeMenuEntry;

        static bool soundEffectsEnabled = GlobalConfig.SoundEnabled;
        static bool musicEnabled = GlobalConfig.MusicEnabled;

        static int soundVolume = GlobalConfig.SoundVolume;
        static int musicVolume = GlobalConfig.MusicVolume;

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public SoundConfigScreen()
            : base("Sound Options")
        {
            // Create our menu entries.
            soundEffectsMenuEntry = CreateOptionMenuEntry(string.Empty);
            soundVolumeMenuEntry = CreateOptionMenuEntry(string.Empty);
            musicMenuEntry = CreateOptionMenuEntry(string.Empty);
            musicVolumeMenuEntry = CreateOptionMenuEntry(string.Empty);

            soundVolumeMenuEntry.HoldToRepeat = true;
            soundVolumeMenuEntry.RepeatRate = repeatRate;

            musicVolumeMenuEntry.HoldToRepeat = true;
            musicVolumeMenuEntry.RepeatRate = repeatRate;

            MenuTextChanged = true;

            MenuEntry exitMenuEntry = CreateBackMenuEntry("Back");

            // Hook up menu event handlers.
            soundEffectsMenuEntry.Selected += ToggleSoundMenuEntrySelected;
            soundEffectsMenuEntry.IncrementEntry += ToggleSoundMenuEntrySelected;
            soundEffectsMenuEntry.DecrementEntry += ToggleSoundMenuEntrySelected;

            musicMenuEntry.Selected += ToggleMusicMenuEntrySelected;
            musicMenuEntry.IncrementEntry += ToggleMusicMenuEntrySelected;
            musicMenuEntry.DecrementEntry += ToggleMusicMenuEntrySelected;

            exitMenuEntry.Selected += OnCancel;

            soundVolumeMenuEntry.Selected += SoundVolumeUp;
            soundVolumeMenuEntry.IncrementEntry += SoundVolumeUp;
            soundVolumeMenuEntry.DecrementEntry += SoundVolumeDown;

            musicVolumeMenuEntry.Selected += MusicVolumeUp;
            musicVolumeMenuEntry.IncrementEntry += MusicVolumeUp;
            musicVolumeMenuEntry.DecrementEntry += MusicVolumeDown;

            soundEffectsMenuEntry.NormalDetailText = "Toggle sounds on/off.";
            soundVolumeMenuEntry.NormalDetailText = "Change sound volume.";
            musicMenuEntry.NormalDetailText = "Toggle music on/off.";
            musicVolumeMenuEntry.NormalDetailText = "Change music volume.";
            exitMenuEntry.NormalDetailText = "Go back to the options menu.";

            AddMenuEntry(soundEffectsMenuEntry);
            AddMenuEntry(soundVolumeMenuEntry);
            AddMenuEntry(musicMenuEntry);
            AddMenuEntry(musicVolumeMenuEntry);

            AddMenuEntry(exitMenuEntry);
        }

        public override void LoadContent()
        {
            menuEntrySpacingAdjust = -6f;
            base.LoadContent();
        }

        /// <summary>
        /// Fills in the latest values for the options screen menu text.
        /// </summary>
        protected override void SetMenuEntryText()
        {
            soundEffectsMenuEntry.Text = "Sound Effects: " + (soundEffectsEnabled ? "on" : "off");
            soundVolumeMenuEntry.Text = "Sound Volume: " + ((int)soundVolume).ToString();
            musicMenuEntry.Text = "Music: " + (musicEnabled ? "on" : "off");
            musicVolumeMenuEntry.Text = "Music Volume: " + ((int)musicVolume).ToString();
            base.SetMenuEntryText();
        }

        /// <summary>
        /// Event handler for when the Frobnicate menu entry is selected.
        /// </summary>
        void ToggleSoundMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.CycleSoundEnabled();
            soundEffectsEnabled = GlobalConfig.SoundEnabled;

            MenuTextChanged = true;
        }

        void SoundVolumeUp(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.SoundVolumeUp();

            soundVolume = GlobalConfig.SoundVolume;

            MenuTextChanged = true;
        }

        void SoundVolumeDown(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.SoundVolumeDown();

            soundVolume = GlobalConfig.SoundVolume;

            MenuTextChanged = true;
        }

        void MusicVolumeUp(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.MusicVolumeUp();

            musicVolume = GlobalConfig.MusicVolume;

            MenuTextChanged = true;
        }

        void MusicVolumeDown(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.MusicVolumeDown();

            musicVolume = GlobalConfig.MusicVolume;

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the Frobnicate menu entry is selected.
        /// </summary>
        void ToggleMusicMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.CycleMusicEnabled();
            musicEnabled = GlobalConfig.MusicEnabled;

            MenuTextChanged = true;
        }
    }
}