﻿#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class MultiPlayerMenuScreen : PressureBodyMenuScreen
    {
        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public MultiPlayerMenuScreen()
            : base("Coming Soon!")
        {
            // Create our menu entries.
            MenuEntry exitMenuEntry = CreateBackMenuEntry("Back");

            // Hook up menu event handlers.
            exitMenuEntry.Selected += OnCancel;

            // Add entries to the menu.
            AddMenuEntry(exitMenuEntry);
        }

        public override void LoadContent()
        {
            menuEntrySpacingAdjust = -6f;
            base.LoadContent();
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            LoadingScreen.Load(ScreenManager, false, null,
                               new BackgroundScreen("Menu Backgrounds/Main"),
                               new MainMenuScreen());
        }
    }
}