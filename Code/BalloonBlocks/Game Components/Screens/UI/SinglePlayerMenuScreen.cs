﻿#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using System;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class SinglePlayerModeScreen : PressureBodyMenuScreen
    {
        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public SinglePlayerModeScreen()
            : base("Game Type")
        {
            MenuSubTitle = "Please select a game type.";

            MenuEntry regularGameMenuEntry = CreateMenuEntry("Normal Game");
            MenuEntry clearGameMenuEntry = CreateMenuEntry("Clear Game");
            MenuEntry survivalGameMenuEntry = CreateMenuEntry("Survival Game");
            MenuEntry customPlayMenuEntry = CreateMenuEntry("Custom Game", Guide.IsTrialMode);
            MenuEntry backMenuEntry = CreateBackMenuEntry("Back");

            // Hook up menu event handlers.
            regularGameMenuEntry.Selected += NormalGameMenuEntrySelected;
            clearGameMenuEntry.Selected += ClearGameMenuEntrySelected;
            survivalGameMenuEntry.Selected += SurvivalGameMenuEntrySelected;
            customPlayMenuEntry.Selected += CustomGameMenuEntrySelected;
            backMenuEntry.Selected += OnCancel;

            regularGameMenuEntry.NormalDetailText = "Play a normal mode game.";
            clearGameMenuEntry.NormalDetailText = "Play a clear mode game.";
            survivalGameMenuEntry.NormalDetailText = "Play a survival mode game.";
            customPlayMenuEntry.NormalDetailText = "Play a custom game.";
            customPlayMenuEntry.TrialDetailText = "Buy Balloon Blocks to unlock the Custom Game Creation menu.";
            backMenuEntry.NormalDetailText = "Go back to the main menu.";

            // Add entries to the menu.
            AddMenuEntry(regularGameMenuEntry);
            AddMenuEntry(clearGameMenuEntry);
            AddMenuEntry(survivalGameMenuEntry);
            AddMenuEntry(customPlayMenuEntry);
            AddMenuEntry(backMenuEntry);
        }

        public override void LoadContent()
        {
            menuEntrySpacingAdjust = -6f;
            base.LoadContent();
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            LoadingScreen.Load(ScreenManager, false, null,
                               new BackgroundScreen("Menu Backgrounds/Main"),
                               new MainMenuScreen());
        }

        /// <summary>
        /// Event handler for when the Easy Game menu entry is selected.
        /// </summary>
        void NormalGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new DifficultyScreen(GameType.Normal), e.PlayerIndex);
        }

        /// <summary>
        /// Event handler for when the Medium Game menu entry is selected.
        /// </summary>
        void ClearGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new DifficultyScreen(GameType.Clear), e.PlayerIndex);
        }

        /// <summary>
        /// Event handler for when the Hard Game menu entry is selected.
        /// </summary>
        void SurvivalGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new DifficultyScreen(GameType.Survival), e.PlayerIndex);
        }

        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// </summary>
        void CustomGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new CustomGameCreateScreen(), e.PlayerIndex);
        }
    }
}