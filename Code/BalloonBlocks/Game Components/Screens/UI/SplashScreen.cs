﻿#region File Description
//-----------------------------------------------------------------------------
// MenuScreen.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;
using Utilities;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// Base class for screens that contain a menu of options. The user can
    /// move up and down to select an entry, or cancel to back out of the screen.
    /// </summary>
    public class SplashScreen : BackgroundScreen
    {
        float lifeSpan;
        float maxLifeSpan;
        protected bool skippable;
        bool timesOut;
        bool checkSignIn;
        protected bool exit;

        protected TextElement glyphText;
        protected SpriteFont font;

        /// <summary>
        /// Constructor.
        /// </summary>
        public SplashScreen(string background, bool isSkippable, bool timesOut, float lifeTime, bool checkSignIn)
            : base(background)
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            this.checkSignIn = checkSignIn;
            this.timesOut = timesOut;
            skippable = isSkippable;
            maxLifeSpan = lifeTime;
        }

        public override void LoadContent()
        {
            base.LoadContent();

            font = Content.Load<SpriteFont>("Fonts/SplashScreenFont");
            glyphText = new TextElement("Fonts/SplashScreenFont");
            glyphText.LoadContent(Content, GraphicsConfig.GraphicsDevice);
            glyphText.ButtonScale = .5f;
        }

        /// <summary>
        /// Updates the menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

#if XBOX360
            if (!Guide.IsVisible)
            {
#endif
                lifeSpan += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if ((lifeSpan > maxLifeSpan && timesOut) || exit)
                {
                    TransitionOff(ControllingPlayer);
                }
#if XBOX360
            }
#endif
        }
        public override void HandleInput(InputState input)
        {
            base.HandleInput(input);

            PlayerIndex playerIndex;
            
            if (skippable && SkipButtonPressed(input, out playerIndex))
            {
#if XBOX360
                if (!PlayerManager.IsControllerSignedIn(playerIndex))
                {
                    PlayerManager.ShowSignIn(null);
                }
#endif
                TransitionOff(playerIndex);
            }
        }

        protected virtual bool SkipButtonPressed(InputState input, out PlayerIndex playerIndex)
        {
            return input.IsAnyKeyPressed(ControllingPlayer, out playerIndex)
                    || input.IsAnyButtonPressed(ControllingPlayer, out playerIndex);
        }

        public virtual void TransitionOff(PlayerIndex? playerIndex) { }
        
        protected void DrawShadowedString(SpriteBatch spriteBatch, string text, Vector2 position, Vector2 origin, float scale, Vector2 ShadowSize)
        {
            glyphText.Scale = new Vector2(scale);
            origin *= scale;
            glyphText.Text = text;

            glyphText.Color = Color.Black;
            glyphText.Position = position + Vector2.One - origin;
            glyphText.Draw(spriteBatch);

            glyphText.Color = Color.White;
            glyphText.Position = position - origin;
            glyphText.Draw(spriteBatch);
        }
    }
}