#region File Description
//-----------------------------------------------------------------------------
// OptionsMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways. Interfaces with the GlobalConfig.
    /// </summary>
    class OptionsMenuScreen : PressureBodyMenuScreen
    {
        const float repeatRate = .15f;

        MenuEntry graphicsConfigMenuEntry;
        MenuEntry soundConfigMenuEntry;
        MenuEntry rumbleMenuEntry;
        MenuEntry controlModeMenuEntry;
        MenuEntry autoDampRateMenuEntry;
        MenuEntry drawMethodMenuEntry;
        MenuEntry shimmerDisplayMenuEntry;
        MenuEntry fpsDisplayMenuEntry;
        MenuEntry safeAreaDisplayMenuEntry;
        MenuEntry perfDataMenuEntry;

        static DrawStyle currentDrawStyle = GlobalConfig.DrawMethod;
        static bool shimmerOption = GlobalConfig.ShimmerDisplayOption;
        static bool rumbleActive = GlobalConfig.RumbleActive;
        static ControlMode controlMode = GlobalConfig.ControlMode;

        static int autoDampRate = GlobalConfig.AutoDampRate;

        bool fpsDisplay = GlobalConfig.ShowFPSHUD;
        bool safeAreaDisplay = GlobalConfig.ShowSafeArea;
        bool showPerfData = GlobalConfig.ShowPerfData;

        /// <summary>
        /// Constructor.
        /// </summary>
        public OptionsMenuScreen()
            : base("Options")
        {
            // Create our menu entries.
            graphicsConfigMenuEntry = CreateMenuEntry("Graphics Options");
            soundConfigMenuEntry = CreateMenuEntry("Sound Options");
            rumbleMenuEntry = CreateOptionMenuEntry(string.Empty);
            controlModeMenuEntry = CreateOptionMenuEntry(string.Empty);
            autoDampRateMenuEntry = CreateOptionMenuEntry(string.Empty);
            drawMethodMenuEntry = CreateOptionMenuEntry(string.Empty);
            fpsDisplayMenuEntry = CreateOptionMenuEntry(string.Empty);
            safeAreaDisplayMenuEntry = CreateOptionMenuEntry(string.Empty);
            shimmerDisplayMenuEntry = CreateOptionMenuEntry(string.Empty);
            perfDataMenuEntry = CreateOptionMenuEntry(string.Empty);

            autoDampRateMenuEntry.HoldToRepeat = true;
            autoDampRateMenuEntry.RepeatRate = repeatRate;

            SetMenuEntryText();

            MenuEntry backMenuEntry = CreateBackMenuEntry("Back");

            // Hook up menu event handlers.
            graphicsConfigMenuEntry.Selected += GraphicsMenuEntrySelected;
            soundConfigMenuEntry.Selected += SoundMenuEntrySelected;
            rumbleMenuEntry.Selected += RumbleMenuEntrySelected;
            rumbleMenuEntry.IncrementEntry += RumbleMenuEntrySelected;
            rumbleMenuEntry.DecrementEntry += RumbleMenuEntrySelected;

            controlModeMenuEntry.Selected += ToggleControlModeMenuEntrySelected;
            controlModeMenuEntry.IncrementEntry += ToggleControlModeMenuEntrySelected;
            controlModeMenuEntry.DecrementEntry += ToggleControlModeMenuEntrySelected;

            autoDampRateMenuEntry.Selected += AutoDampRateUp;
            autoDampRateMenuEntry.IncrementEntry += AutoDampRateUp;
            autoDampRateMenuEntry.DecrementEntry += AutoDampRateDown;

            drawMethodMenuEntry.Selected += DrawMethodMenuEntrySelected;
            fpsDisplayMenuEntry.Selected += FPSDisplayMenuEntrySelected;
            safeAreaDisplayMenuEntry.Selected += SafeAreaMenuEntrySelected;
            shimmerDisplayMenuEntry.Selected += ShimmerDisplayMenuEntrySelected;
            perfDataMenuEntry.Selected += PerfDataMenuEntrySelected;
            backMenuEntry.Selected += OnCancel;

            graphicsConfigMenuEntry.NormalDetailText = "Change Graphics options.";
            soundConfigMenuEntry.NormalDetailText = "Change Audio options.";
            rumbleMenuEntry.NormalDetailText = "Turn gamepad rumble on or off.";
            controlModeMenuEntry.NormalDetailText = "Easy mode stops game pieces from rotating when you're\nnot holding down the rotate buttons and\nslows how fast game pieces fall";
            autoDampRateMenuEntry.NormalDetailText = "How quickly rotation stops";

            drawMethodMenuEntry.NormalDetailText = "Change how the game graphics appear.";
            fpsDisplayMenuEntry.NormalDetailText = "Toggle frames per second display.";
            safeAreaDisplayMenuEntry.NormalDetailText = "Toggle safe area overlay.";
            shimmerDisplayMenuEntry.NormalDetailText = "Toggle whether blocks change color as they distort.";
            perfDataMenuEntry.NormalDetailText = "Show update time in gameplay.";

            backMenuEntry.NormalDetailText = "Go back to the main menu.";

            // Add entries to the menu.
            AddMenuEntry(graphicsConfigMenuEntry);
            AddMenuEntry(soundConfigMenuEntry);
            AddMenuEntry(rumbleMenuEntry);
            AddMenuEntry(controlModeMenuEntry);
#if DEBUG
            AddMenuEntry(autoDampRateMenuEntry);
            AddMenuEntry(drawMethodMenuEntry);
            AddMenuEntry(fpsDisplayMenuEntry);
            AddMenuEntry(safeAreaDisplayMenuEntry);
            AddMenuEntry(shimmerDisplayMenuEntry);
            AddMenuEntry(perfDataMenuEntry);
#endif
            AddMenuEntry(backMenuEntry);
        }

        public override void LoadContent()
        {
            menuEntrySpacingAdjust = -6f;
            base.LoadContent();
        }


        /// <summary>
        /// Fills in the latest values for the options screen menu text.
        /// </summary>
        protected override void SetMenuEntryText()
        {
            drawMethodMenuEntry.Text = "Graphics Style: " + currentDrawStyle;
            fpsDisplayMenuEntry.Text = "FPS Display: " + (fpsDisplay ? "on" : "off");
            rumbleMenuEntry.Text = "Gamepad Rumble: " + (rumbleActive ? "on" : "off");
            controlModeMenuEntry.Text = "Control Style: " + controlMode.ToString();
            autoDampRateMenuEntry.Text = "Auto Stop Rate: " + ((int)autoDampRate).ToString();

            safeAreaDisplayMenuEntry.Text = "Safe Area Display: " + (safeAreaDisplay ? "on" : "off");
            shimmerDisplayMenuEntry.Text = "Draw Option Shimmer: " + (shimmerOption ? "on" : "off");
            perfDataMenuEntry.Text = "Display perf data: " + (showPerfData ? "on" : "off");
            base.SetMenuEntryText();
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            EventManager.Trigger(this, "SaveSettings");
            LoadingScreen.Load(ScreenManager, false, null,
                               new BackgroundScreen("Menu Backgrounds/Main"),
                               new MainMenuScreen());
        }

        /// <summary>
        /// Event handler for when the draw style menu entry is selected.
        /// </summary>
        void RumbleMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            rumbleActive = !rumbleActive;
            GlobalConfig.RumbleActive = rumbleActive;

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the draw style menu entry is selected.
        /// </summary>
        void GraphicsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new GraphicsConfigScreen(), e.PlayerIndex);
        }

        /// <summary>
        /// Event handler for when the draw style menu entry is selected.
        /// </summary>
        void SoundMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new SoundConfigScreen(), e.PlayerIndex);
        }

        void ToggleControlModeMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.CycleControlMode();

            controlMode = GlobalConfig.ControlMode;

            MenuTextChanged = true;
        }

        void AutoDampRateUp(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.AutoDampRateUp();

            autoDampRate = GlobalConfig.AutoDampRate;

            MenuTextChanged = true;
        }

        void AutoDampRateDown(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.AutoDampRateDown();

            autoDampRate = GlobalConfig.AutoDampRate;

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the draw style menu entry is selected.
        /// </summary>
        void DrawMethodMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.CycleDrawStyle();
            currentDrawStyle = GlobalConfig.DrawMethod;

            MenuTextChanged = true;
        }

        void PerfDataMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            showPerfData = !showPerfData;
            GlobalConfig.ShowPerfData = showPerfData;

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the Frobnicate menu entry is selected.
        /// </summary>
        void ShimmerDisplayMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.CycleShimmerDisplayOption();
            shimmerOption = GlobalConfig.ShimmerDisplayOption;

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the Frobnicate menu entry is selected.
        /// </summary>
        void FPSDisplayMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.CycleShowFPSHUD();
            fpsDisplay = GlobalConfig.ShowFPSHUD;
            EventManager.Trigger(this, "EnableDebugDisplay", fpsDisplay);

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the Frobnicate menu entry is selected.
        /// </summary>
        void SafeAreaMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            GlobalConfig.CycleShowSafeArea();
            safeAreaDisplay = GlobalConfig.ShowSafeArea;
            EventManager.Trigger(this, "EnableSafeArea", safeAreaDisplay);

            MenuTextChanged = true;
        }
    }
}