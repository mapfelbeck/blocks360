#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using Utilities;
using System.Diagnostics;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class MainMenuScreen : PressureBodyMenuScreen
    {
        MenuEntry purchaseMenuEntry;
        MenuEntry exitMenuEntry;

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public MainMenuScreen()
            : base("")
        {
            MusicPlayer.PlayTitleTrack();

            // Create our menu entries.
            purchaseMenuEntry = CreateMenuEntry("Buy Balloon Blocks!", MenuEntryAlignment.Center);
            MenuEntry quickPlayMenuEntry = CreateMenuEntry("Quick Play", MenuEntryAlignment.Center);
            MenuEntry singlePlayerMenuEntry = CreateMenuEntry("Single Player Game", MenuEntryAlignment.Center);
            MenuEntry howToPlayMenuEntry = CreateMenuEntry("How To Play", MenuEntryAlignment.Center);
            MenuEntry perfTestMenuEntry = CreateMenuEntry("Perf Test", MenuEntryAlignment.Center);
            MenuEntry loadTestMenuEntry = CreateMenuEntry("Load Test", MenuEntryAlignment.Center);
            MenuEntry optionsMenuEntry = CreateMenuEntry("Options", MenuEntryAlignment.Center);
            MenuEntry scoreMenuEntry = CreateMenuEntry("High Scores", MenuEntryAlignment.Center);
            MenuEntry creditsMenuEntry = CreateMenuEntry("Credits", MenuEntryAlignment.Center);
            exitMenuEntry = CreateBackMenuEntry(Resources.Exit, MenuEntryAlignment.Center);

            // Hook up menu event handlers.
            purchaseMenuEntry.Selected += PurchaseMeuEntrySelected;
            quickPlayMenuEntry.Selected += QuickPlayMenuEntrySelected;
            singlePlayerMenuEntry.Selected += SinglePlayerMenuEntrySelected;
            howToPlayMenuEntry.Selected += TutorialMenuEntrySelected;
            perfTestMenuEntry.Selected += PerfTestMenuEntrySelected;
            loadTestMenuEntry.Selected += LoadTestMenuEntrySelected;
            optionsMenuEntry.Selected += OptionsMenuEntrySelected;
            scoreMenuEntry.Selected += ScoreMenuEntrySelected;
            creditsMenuEntry.Selected += CreditsMenuEntrySelected;
            exitMenuEntry.Selected += OnCancel;

            quickPlayMenuEntry.NormalDetailText = "Jump straight into a Normal Medium difficulty game";
            singlePlayerMenuEntry.NormalDetailText = "Choose one of several game modes to play";
            howToPlayMenuEntry.NormalDetailText = "Learn how to play Balloon Blocks";
            purchaseMenuEntry.NormalDetailText = "Buy Ballon Blocks!";
            perfTestMenuEntry.NormalDetailText = "Bring the game engine to it's knees.";
            loadTestMenuEntry.NormalDetailText = "Stress the game engine.";
            optionsMenuEntry.NormalDetailText = "Change game options";
            scoreMenuEntry.NormalDetailText = "View the high score lists";
            creditsMenuEntry.NormalDetailText = "View the credits";
            exitMenuEntry.NormalDetailText = "Leave the game.";

#if XBOX360
            // Add entries to the menu.
            if (Guide.IsTrialMode)
            {
                AddMenuEntry(purchaseMenuEntry);
                //AddMenuEntry(purchaseMenuEntry);
            }
#endif
            AddMenuEntry(quickPlayMenuEntry);
            AddMenuEntry(singlePlayerMenuEntry);
            AddMenuEntry(howToPlayMenuEntry);
#if DEBUG
            AddMenuEntry(perfTestMenuEntry);
            AddMenuEntry(loadTestMenuEntry);
#endif
            AddMenuEntry(optionsMenuEntry);
            AddMenuEntry(scoreMenuEntry);
            AddMenuEntry(creditsMenuEntry);
            AddMenuEntry(exitMenuEntry);
        }

        public override void LoadContent()
        {
            base.LoadContent();
            ContentManager content = ScreenManager.Game.Content;

            //GraphicsConfig.GraphicsDevice.DeviceReset += new System.EventHandler(DeviceReset);

            menuPositionAdjust = new Vector2(0, GraphicsConfig.GraphicsDevice.Viewport.Height / 4);
            detailPositionAdjust = new Vector2(0, GraphicsConfig.GraphicsDevice.Viewport.Height / 16);

            menuEntrySpacingAdjust = -6f;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (!Guide.IsTrialMode && MenuEntries.Contains(purchaseMenuEntry))
            {
                RemoveMenuEntry(purchaseMenuEntry);
            }
        }

        protected override void DeviceReset(object sender, EventArgs e)
        {
            base.DeviceReset(sender, e);

            menuPositionAdjust = new Vector2(0, GraphicsConfig.GraphicsDevice.Viewport.Height / 4);
            detailPositionAdjust = new Vector2(0, GraphicsConfig.GraphicsDevice.Viewport.Height / 16);
        }

        /// <summary>
        /// Event handler for when the Play Game menu entry is selected.
        /// Go straight into and easy mode game.
        /// </summary>
        void QuickPlayMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                                new BackgroundScreen("Gameplay Backgrounds/Medium"),
                                BlockGameFactory.CreateGame(GameType.Normal, GameDifficulty.Medium));
        }

        void PurchaseMeuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ControllingPlayer = e.PlayerIndex;
            if (PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value) &&
                !Guide.IsVisible)
            {
                SellStuff();
            }
            else if (!PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value))
            {
                PlayerManager.ShowSignIn(SellStuff);
            }
        }

        protected void SellStuff()
        {
            if (PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value) &&
                !Guide.IsVisible)
            {
                Guide.ShowMarketplace(ControllingPlayer.Value);
            }
        }

        void TutorialMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                                new BackgroundScreen("Gameplay Backgrounds/Custom"),
                                BlockGameFactory.CreateTutorialGame());
        }

        /// <summary>
        /// Event handler for when the Quick Play menu entry is selected.
        /// </summary>
        void SinglePlayerMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                               new BackgroundScreen("Menu Backgrounds/Blank"),
                               new SinglePlayerModeScreen());
        }

        /// <summary>
        /// Event handler for when the Quick Play menu entry is selected.
        /// </summary>
        void PerfTestMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                                new BackgroundScreen("Gameplay Backgrounds/Easy"),
                                BlockGameFactory.CreatePerfTestGame());

            //ScreenManager.AddScreen(new MultiPlayerMenuScreen(), e.PlayerIndex);
        }
        /// <summary>
        /// Event handler for when the Quick Play menu entry is selected.
        /// </summary>
        void LoadTestMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                                new BackgroundScreen("Gameplay Backgrounds/Easy"),
                                BlockGameFactory.CreateLoadTestGame());

            //ScreenManager.AddScreen(new MultiPlayerMenuScreen(), e.PlayerIndex);
        }

        /// <summary>
        /// Event handler for when the Options menu entry is selected.
        /// </summary>
        void OptionsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                               new BackgroundScreen("Menu Backgrounds/Blank"),
                               new OptionsMenuScreen());
        }

        void ScoreMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                               new BackgroundScreen("Menu Backgrounds/Blank"),
                               new HighScoreScreen());
        }

        void CreditsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                               new CreditsScreen());
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        protected override void OnCancel(PlayerIndex playerIndex)
        {
            const string message = "Exit the game?";

            MessageBoxScreen confirmExitMessageBox = ScreenManager.GetMessageBox(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmExitMessageBox, playerIndex);
        }

        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to exit" message box.
        /// </summary>
        void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            if (!GameConstants.skipExitScreen)
            {
                LoadingScreen.Load(ScreenManager, false, e.PlayerIndex,
                                   new ExitScreen(),
                                   BlockGameFactory.CreateSplashScreenGame());
            }
            else
            {
                ScreenManager.Game.Exit();
            }
        }
    }
}