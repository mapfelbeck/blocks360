﻿#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Utilities;
using System;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class DifficultyScreen : PressureBodyMenuScreen
    {
        GameType type;
        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public DifficultyScreen(GameType type)
            : base("Normal Mode")
        {
            this.type = type;

            MenuSubTitle = "Please select a difficulty level.";
            // Create our menu entries.
            MenuEntry easyGameMenuEntry = CreateMenuEntry("Easy", Guide.IsTrialMode && TrialTracker.IsPlayed(type,GameDifficulty.Easy));
            MenuEntry mediumGameMenuEntry = CreateMenuEntry("Medium", Guide.IsTrialMode && TrialTracker.IsPlayed(type, GameDifficulty.Medium));
            MenuEntry hardGameMenuEntry = CreateMenuEntry("Hard", Guide.IsTrialMode && TrialTracker.IsPlayed(type, GameDifficulty.Hard));
            MenuEntry backMenuEntry = CreateBackMenuEntry("Back");

            // Hook up menu event handlers.
            easyGameMenuEntry.Selected += EasyGameMenuEntrySelected;
            mediumGameMenuEntry.Selected += MediumGameMenuEntrySelected;
            hardGameMenuEntry.Selected += HardGameMenuEntrySelected;
            backMenuEntry.Selected += OnCancel;

            easyGameMenuEntry.NormalDetailText = string.Format("Play an Easy {0} game.", type.ToString());
            mediumGameMenuEntry.NormalDetailText = string.Format("Play a Medium {0} game.", type.ToString());
            hardGameMenuEntry.NormalDetailText = string.Format("Play a Hard {0} game.", type.ToString());

            easyGameMenuEntry.TrialDetailText = string.Format("Buy Balloon Blocks to play an Easy {0} game again.", type.ToString());
            mediumGameMenuEntry.TrialDetailText = string.Format("Buy Balloon Blocks to play a Medium {0} game again.", type.ToString());
            hardGameMenuEntry.TrialDetailText = string.Format("Buy Balloon Blocks to play a Hard {0} game again.", type.ToString());
            
            backMenuEntry.NormalDetailText = "Go back to the main menu.";

            // Add entries to the menu.
            AddMenuEntry(easyGameMenuEntry);
            AddMenuEntry(mediumGameMenuEntry);
            AddMenuEntry(hardGameMenuEntry);
            AddMenuEntry(backMenuEntry);
        }

        public override void LoadContent()
        {
            menuEntrySpacingAdjust = -6f;
            base.LoadContent();
        }

        /// <summary>
        /// Event handler for when the Easy Game menu entry is selected.
        /// </summary>
        void EasyGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                                new BackgroundScreen("Gameplay Backgrounds/Easy"),
                                BlockGameFactory.CreateGame(type, GameDifficulty.Easy));
        }
        /// <summary>
        /// Event handler for when the Medium Game menu entry is selected.
        /// </summary>
        void MediumGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                                new BackgroundScreen("Gameplay Backgrounds/Medium"),
                                BlockGameFactory.CreateGame(type, GameDifficulty.Medium));
        }

        /// <summary>
        /// Event handler for when the Hard Game menu entry is selected.
        /// </summary>
        void HardGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                                new BackgroundScreen("Gameplay Backgrounds/Hard"),
                                BlockGameFactory.CreateGame(type, GameDifficulty.Hard));
        }
    }
}