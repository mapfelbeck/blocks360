﻿#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using System.Collections.Generic;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    ///  Interfaces with the GraphicsConfig.
    /// </summary>
    class GraphicsConfigScreen : PressureBodyMenuScreen
    {
        MenuEntry aspectMenuEntry;
        MenuEntry resolutionMenuEntry;
        MenuEntry fullScreenMenuEntry;
        MenuEntry applyMenuEntry;

        bool isFullScreen = GraphicsConfig.IsFullscreen;
        bool isWideScreen = GraphicsConfig.IsWideScreen;
        GraphicsResolution resolution = GraphicsConfig.Resolution;
        GraphicsAspectRatio aspect = GraphicsConfig.AspectRatioEnum;
        List<GraphicsResolution> resolutions;
        int resolutionIndex;
        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public GraphicsConfigScreen()
            : base("Graphics Options")
        {
            resolutions = GraphicsConfig.GetResolutions(aspect);
            resolutionIndex = resolutions.IndexOf(resolution);

            // Create our menu entries.
            aspectMenuEntry = CreateOptionMenuEntry(string.Empty);
            resolutionMenuEntry = CreateOptionMenuEntry(string.Empty);
            fullScreenMenuEntry = CreateOptionMenuEntry(string.Empty);
            applyMenuEntry = CreateOptionMenuEntry("Apply Changes!");

            MenuTextChanged = true;

            MenuEntry exitMenuEntry = CreateBackMenuEntry("Back");

            // Hook up menu event handlers.
            aspectMenuEntry.Selected += AspectRatioMenuEntrySelected;
            aspectMenuEntry.IncrementEntry += AspectRatioMenuEntrySelected;
            aspectMenuEntry.DecrementEntry += AspectRatioMenuEntrySelected;

            resolutionMenuEntry.Selected += ResolutionMenuEntryUp;
            resolutionMenuEntry.IncrementEntry += ResolutionMenuEntryUp;
            resolutionMenuEntry.DecrementEntry += ResolutionMenuEntryDown;

            fullScreenMenuEntry.Selected += FullScreenMenuEntrySelected;
            fullScreenMenuEntry.IncrementEntry += FullScreenMenuEntrySelected;
            fullScreenMenuEntry.DecrementEntry += FullScreenMenuEntrySelected;
            
            applyMenuEntry.Selected += OnApply;
            exitMenuEntry.Selected += OnCancel;

            aspectMenuEntry.NormalDetailText = "Toggle between standard and widescreen mode.";
            resolutionMenuEntry.NormalDetailText = "Change resolution.";
            fullScreenMenuEntry.NormalDetailText = "Toggle between fullscreen and windowed mode.";
            applyMenuEntry.NormalDetailText = "Apply changes.";
            exitMenuEntry.NormalDetailText = "Go back to the options menu.";

            // Add entries to the menu.
            AddMenuEntry(aspectMenuEntry);
#if WINDOWS
            AddMenuEntry(resolutionMenuEntry);
            AddMenuEntry(fullScreenMenuEntry);
#endif
            AddMenuEntry(applyMenuEntry);
            AddMenuEntry(exitMenuEntry);
        }

        public override void LoadContent()
        {
            menuEntrySpacingAdjust = -6f;
            base.LoadContent();
        }

        /// <summary>
        /// Fills in the latest values for the options screen menu text.
        /// </summary>
        protected override void SetMenuEntryText()
        {
            aspectMenuEntry.Text = 
                "Aspect Ratio: " + (isWideScreen?"WideScreen":"Standard");
            resolutionMenuEntry.Text = 
                "Resolution: " + resolution.Width + "X" + resolution.Height;
            fullScreenMenuEntry.Text = 
                "Fullscreen: " + (isFullScreen ? "on" : "off");
            base.SetMenuEntryText();
        }

        /// <summary>
        /// Event handler for when the Aspect Ratio menu entry is selected.
        /// </summary>
        void AspectRatioMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            aspect++;

            if (aspect > GraphicsAspectRatio.Standard)
            {
                aspect = GraphicsAspectRatio.WideScreen;
            }

            isWideScreen = aspect == GraphicsAspectRatio.WideScreen;
            resolutions = GraphicsConfig.GetResolutions(aspect);
            resolutionIndex = 0;
            resolution = resolutions[resolutionIndex];

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the Resolution menu entry is selected.
        /// </summary>
        void ResolutionMenuEntryUp(object sender, PlayerIndexEventArgs e)
        {
            resolutionIndex++;
            if (resolutionIndex >= resolutions.Count)
            {
                resolutionIndex = 0;
            }

            resolution = resolutions[resolutionIndex];

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the Resolution menu entry is selected.
        /// </summary>
        void ResolutionMenuEntryDown(object sender, PlayerIndexEventArgs e)
        {
            resolutionIndex--;
            if (resolutionIndex < 0)
            {
                resolutionIndex = resolutions.Count - 1;
            }

            resolution = resolutions[resolutionIndex];

            MenuTextChanged = true;
        }

        /// <summary>
        /// Event handler for when the Fullscreen menu entry is selected.
        /// </summary>
        void FullScreenMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            isFullScreen = !isFullScreen;

            MenuTextChanged = true;
        }

        /// <summary>
        /// When the user cancels the main menu, ask if they want to exit the sample.
        /// </summary>
        void OnApply(object sender, PlayerIndexEventArgs e)
        {
            //const string message = "Keep these setting?";

            //MessageBoxScreen keepSettingsMessageBox = new MessageBoxScreen(message);
            GraphicsConfig.IsFullscreen = isFullScreen;
            GraphicsConfig.AspectRatioEnum = aspect;
            GraphicsConfig.ResolutionIndex = resolutionIndex;
            GraphicsConfig.ApplyChanges();
            //keepSettingsMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            //ScreenManager.AddScreen(keepSettingsMessageBox, e.PlayerIndex);
        }
    }
}