﻿#region File Description
//-----------------------------------------------------------------------------
// MainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The main menu screen is the first thing displayed when the game starts up.
    /// </summary>
    class CustomGameCreateScreen : PressureBodyMenuScreen
    {
        MenuEntry gamePieceShapeMenuEntry;
        MenuEntry gamePieceSizeMenuEntry;
        MenuEntry blockTypeMenuEntry;
        MenuEntry gameSpeedMenuEntry;
        MenuEntry gameStartMenuEntry;
        MenuEntry backMenuEntry;

        GameType gameType = CustomGameConfig.gameType;
        GameSpeed gameSpeed = CustomGameConfig.gameSpeed;
        ShapeType pieceShape = CustomGameConfig.pieceShape;
        BlockSize blockSize = CustomGameConfig.blockSize;
        BlockType blockType = CustomGameConfig.blockType;

        Gravity gravity;
        Friction friction;

        /// <summary>
        /// Constructor fills in the menu contents.
        /// </summary>
        public CustomGameCreateScreen()
            : base("Create Custom Game")
        {
            // Create our menu entries.
            gamePieceShapeMenuEntry = CreateOptionMenuEntry(string.Empty);
            gamePieceSizeMenuEntry = CreateOptionMenuEntry(string.Empty);
            blockTypeMenuEntry = CreateOptionMenuEntry(string.Empty);
            gameSpeedMenuEntry = CreateOptionMenuEntry(string.Empty);
            gameStartMenuEntry = CreateMenuEntry("Start Game!");
            backMenuEntry = CreateBackMenuEntry("Back");

            // Hook up menu event handlers.
            gamePieceShapeMenuEntry.Selected += IncrementGamePieceShapeMenuEntry;
            gamePieceShapeMenuEntry.IncrementEntry += IncrementGamePieceShapeMenuEntry;
            gamePieceShapeMenuEntry.DecrementEntry += DecrementGamePieceShapeMenuEntry;

            gamePieceSizeMenuEntry.Selected += IncrementGamePieceSizeMenuEntry;
            gamePieceSizeMenuEntry.IncrementEntry += IncrementGamePieceSizeMenuEntry;
            gamePieceSizeMenuEntry.DecrementEntry += DecrementGamePieceSizeMenuEntry;

            blockTypeMenuEntry.Selected += IncrementBlockTypeMenuEntry;
            blockTypeMenuEntry.IncrementEntry += IncrementBlockTypeMenuEntry;
            blockTypeMenuEntry.DecrementEntry += DecrementBlockTypeMenuEntry;

            gameSpeedMenuEntry.Selected += IncrementGameSpeedMenuEntry;
            gameSpeedMenuEntry.IncrementEntry += IncrementGameSpeedMenuEntry;
            gameSpeedMenuEntry.DecrementEntry += DecrementGameSpeedMenuEntry;
            
            gameStartMenuEntry.Selected += GameStartMenuEntrySelected;
            backMenuEntry.Selected += OnCancel;

            gamePieceShapeMenuEntry.NormalDetailText = "Changes the shape of the game pieces.";
            gamePieceSizeMenuEntry.NormalDetailText = "Changes the size of the game pieces.";
            blockTypeMenuEntry.NormalDetailText = "Changes how the game pieces behave.";
            gameSpeedMenuEntry.NormalDetailText = "Changes the game Speed.";
            gameStartMenuEntry.NormalDetailText = "Start the game with these settings.";
            backMenuEntry.NormalDetailText = "Go back to the main menu.";

            // Add entries to the menu.
            AddMenuEntry(gamePieceShapeMenuEntry);
            AddMenuEntry(gamePieceSizeMenuEntry);
            AddMenuEntry(blockTypeMenuEntry);
            AddMenuEntry(gameSpeedMenuEntry);
            AddMenuEntry(gameStartMenuEntry);
            AddMenuEntry(backMenuEntry);
        }

        public override void LoadContent()
        {
            base.LoadContent();
            menuEntrySpacingAdjust = -6f;
            GlobalConfig.SpeedToGravityFriction(gameSpeed, out gravity, out friction);
            MenuTextChanged = true;
        }


        protected override void SetMenuEntryText()
        {
            gamePieceShapeMenuEntry.Text = "Game Piece Type: " + pieceShape;
            gamePieceSizeMenuEntry.Text = "Game Piece Size: " + blockSize;
            blockTypeMenuEntry.Text = "Block Type: " + blockType;
            gameSpeedMenuEntry.Text = "Game Speed: " + gameSpeed;
            base.SetMenuEntryText();
        }

        void IncrementGameSpeedMenuEntry(object sender, PlayerIndexEventArgs e)
        {
            CustomGameConfig.gameSpeed++;
            if (CustomGameConfig.gameSpeed > GameSpeed.VeryFast)
            {
                CustomGameConfig.gameSpeed = GameSpeed.VerySlow;
            }
            GlobalConfig.SpeedToGravityFriction(CustomGameConfig.gameSpeed, out gravity, out friction);

            gameSpeed = CustomGameConfig.gameSpeed;

            MenuTextChanged = true;
        }

        void DecrementGameSpeedMenuEntry(object sender, PlayerIndexEventArgs e)
        {
            CustomGameConfig.gameSpeed--;
            if (CustomGameConfig.gameSpeed < GameSpeed.VerySlow)
            {
                CustomGameConfig.gameSpeed = GameSpeed.VeryFast;
            }
            GlobalConfig.SpeedToGravityFriction(CustomGameConfig.gameSpeed, out gravity, out friction);

            gameSpeed = CustomGameConfig.gameSpeed;

            MenuTextChanged = true;
        }

        void IncrementGamePieceShapeMenuEntry(object sender, PlayerIndexEventArgs e)
        {
            CustomGameConfig.pieceShape++;
            if (CustomGameConfig.pieceShape > ShapeType.Square)
            {
                CustomGameConfig.pieceShape = ShapeType.Double;
            }

            pieceShape = CustomGameConfig.pieceShape;

            MenuTextChanged = true;
        }

        void DecrementGamePieceShapeMenuEntry(object sender, PlayerIndexEventArgs e)
        {
            CustomGameConfig.pieceShape--;
            if (CustomGameConfig.pieceShape < ShapeType.Double)
            {
                CustomGameConfig.pieceShape = ShapeType.Square;
            }

            pieceShape = CustomGameConfig.pieceShape;

            MenuTextChanged = true;
        }

        void IncrementGamePieceSizeMenuEntry(object sender, PlayerIndexEventArgs e)
        {
            CustomGameConfig.blockSize++;
            if (CustomGameConfig.blockSize > BlockSize.Large)
            {
                CustomGameConfig.blockSize = BlockSize.Small;
            }

            blockSize = CustomGameConfig.blockSize;

            MenuTextChanged = true;
        }

        void DecrementGamePieceSizeMenuEntry(object sender, PlayerIndexEventArgs e)
        {
            CustomGameConfig.blockSize--;
            if (CustomGameConfig.blockSize < BlockSize.Small)
            {
                CustomGameConfig.blockSize = BlockSize.Large;
            }

            blockSize = CustomGameConfig.blockSize;

            MenuTextChanged = true;
        }

        void IncrementBlockTypeMenuEntry(object sender, PlayerIndexEventArgs e)
        {
            CustomGameConfig.blockType++;
            if (CustomGameConfig.blockType > BlockType.Freeze)
            {
                CustomGameConfig.blockType = BlockType.Normal;
            }

            blockType = CustomGameConfig.blockType;

            MenuTextChanged = true;
        }

        void DecrementBlockTypeMenuEntry(object sender, PlayerIndexEventArgs e)
        {
            CustomGameConfig.blockType--;
            if (CustomGameConfig.blockType < BlockType.Normal)
            {
                CustomGameConfig.blockType = BlockType.Freeze;
            }

            blockType = CustomGameConfig.blockType;

            MenuTextChanged = true;
        }

        void GameStartMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                                new BackgroundScreen("Gameplay Backgrounds/Custom"),
                                BlockGameFactory.CreateGame(
                                pieceShape,blockSize,friction, gravity, blockType,false));
        }
    }
}