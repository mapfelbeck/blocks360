﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Physics;
using Utilities;
#endregion

namespace BalloonBlocks
{
    class MenuPressureBody : PressureBody
    {
        Vector2 menuPosition;
        /// <summary>
        /// Desired spot in global space for this body
        /// </summary>
        public Vector2 MenuPosition
        {
            get { return menuPosition; }
            set { menuPosition = value; }
        }

        public MenuPressureBody(ClosedShape s, float massPerPoint, float gasPressure, float shapeSpringK, float shapeSpringDamp, float edgeSpringK, float edgeSpringDamp, Vector2 pos, float angleInRadians, Vector2 scale, bool kinematic)
            : base(s, massPerPoint, gasPressure, shapeSpringK, shapeSpringDamp, edgeSpringK, edgeSpringDamp, pos, angleInRadians, scale, kinematic)
        {
        }

        public override void accumulateExternalForces(float elapsedTime)
        {
            base.accumulateExternalForces(elapsedTime);

            Vector2 position = DerivedPosition;

            Vector2 direction = menuPosition - position;
            direction *= Math.Abs(GameConstants.GravityConstant);

            addGlobalForce(ref position, ref direction);
        }
    }
}