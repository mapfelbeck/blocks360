﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Physics;
using Utilities;
#endregion

namespace BalloonBlocks
{
    /*
     * Building a pressure body menu
     * reqirements:
     * graphics device must be initialized
     * menu entries from MenuScreen base class must be populated
     * ButtonTextureGenerator must be initialized
     * menu physics world must be initialized
     * 
     * clear stuff out:
     * clear ButtonEntry list in ButtonTextureGenerator
     * clear ButtonTextureGenerator texture
     * clear menu physics world
     * 
     * build:
     * ButtonEntry list in ButtonTextureGenerator is populated with strings and text colors
     * render button texture sheet
     * load button texture sheet onto graphics device
     * set texture cooridinates for menu entries
     * build menu pressure bodies inside menu entries
     */
    class PressureBodyMenuScreen : MenuScreen
    {
        World menuWorld;
        ButtonTextureGenerator buttonGenerator;

        DrawBatch drawBatch;
        BasicEffect menuEffect;

        bool rebuildPressureBodyMenu;
        bool resetMatrices;

        Vector2 buttonScale;
        float buttonSpacing;

        public PressureBodyMenuScreen(string menuTitle)
            : base(menuTitle)
        {
            rebuildPressureBodyMenu = true;
            menuWorld = WorldFactory.BuildWorld();
            menuWorld.SetBodyDampening(.95f);
            //menuWorld.PhysicsSimIter = 2;

            buttonScale = new Vector2(1.5f);
            buttonSpacing = .4f;
        }

        public override void LoadContent()
        {
            base.LoadContent();

            int horizontalBordersize = 10;
            int verticalBordersize = 0;
            float[] horizontalProportions = { 1, 6, 1 };
            float[] verticalProportions = { 1, 6, 1 };

            Texture2D buttonTextureBase = Content.Load<Texture2D>("Game UI/Button Base");

            SpriteBatchNineSliceTexture nineSlice = new SpriteBatchNineSliceTexture(
                buttonTextureBase, horizontalProportions, verticalProportions);
            
            buttonGenerator = new ButtonTextureGenerator(GraphicsConfig.GraphicsDevice,
                null, ScreenManager.MenuEntryFont, nineSlice, horizontalBordersize, verticalBordersize);

            CreateButtons();
    
            //buttonGenerator.RenderTexture();

            menuEffect = new BasicEffect(GraphicsConfig.GraphicsDevice, null);
            drawBatch = new DrawBatch(GraphicsConfig.GraphicsDevice);

            SetupShader();
            resetMatrices = true;
            rebuildPressureBodyMenu = true;
        }

        private void CreateButtons()
        {
            buttonGenerator.ButtonEntries.Clear();
            for (int i = 0; i < MenuEntries.Count; i++)
            {
                buttonGenerator.ButtonEntries.Add(
                    new ButtonEntry(MenuEntries[i].Text, MenuEntries[i].TextColor));
            }
        }

        protected override Vector2 DetailTextPosition()
        {
            Vector2 subTitleScreenCoordinates = SubTitlePosition();
            float menuHeight = -MenuEntries.Count * 
                (1.85f * buttonScale.Y + buttonSpacing);

            Vector2 zeroZeroWorld = Vector2.Zero;
            Vector2 menuHeightWorld = new Vector2(0, menuHeight);
            Vector2 zeroZeroScreen = GraphicsConfig.WorldToScreenCoordinates(
                zeroZeroWorld);
            Vector2 menuHeightScreen = GraphicsConfig.WorldToScreenCoordinates(
                menuHeightWorld);

            float menuHeightYScreen = menuHeightScreen.Y - zeroZeroScreen.Y;

            Vector2 position = new Vector2(subTitleScreenCoordinates.X,
                subTitleScreenCoordinates.Y + menuHeightYScreen);

            return position;
        }

        protected override void SetMenuEntryText()
        {
            base.SetMenuEntryText();
            if (buttonGenerator != null)
            {
                CreateButtons();
                buttonGenerator.RenderTexture();
                if (menuEffect != null)
                {
                    menuEffect.Texture = buttonGenerator.ButtonTexture;
                }
                SetDrawTiles();
            }
        }

        private void SetDrawTiles()
        {
            PressureBodyMenuEntry pressureEntry;
            for (int i = 0; i < MenuEntries.Count; i++)
            {
                pressureEntry = MenuEntries[i] as PressureBodyMenuEntry;
                if (pressureEntry != null)
                {
                    pressureEntry.DrawTile = buttonGenerator.ButtonTiles[i];
                }
            }
        }

        protected override void DeviceReset(object sender, EventArgs e)
        {
            base.DeviceReset(sender, e);

            rebuildPressureBodyMenu = true;
            resetMatrices = true;
        }

        private void SetupShader()
        {
            SetupShaderMatices();

            menuEffect.VertexColorEnabled = true;
            menuEffect.LightingEnabled = false;
            menuEffect.TextureEnabled = GlobalConfig.DrawMethod == DrawStyle.Textured;
            menuEffect.Texture = buttonGenerator.ButtonTexture;
        }

        private void SetupShaderMatices()
        {
            menuEffect.View = GraphicsConfig.View;
            menuEffect.Projection = GraphicsConfig.Projection;
            menuEffect.World = GraphicsConfig.World;
        }

        public override void HandleInput(InputState input)
        {
            base.HandleInput(input);
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            float elepsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (resetMatrices)
            {
                SetupShaderMatices();
                resetMatrices = false;
                SetMenuEntryText();
            }

            if (rebuildPressureBodyMenu)
            {
                BuildPressureBodyMenu();
                rebuildPressureBodyMenu = false;
            }

            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                menuWorld.Update(elepsedTime);
            }
        }

        private void BuildPressureBodyMenu()
        {
            menuWorld.Clear();
            CreateButtons();
            buttonGenerator.RenderTexture();
            if (menuEffect != null)
            {
                menuEffect.Texture = buttonGenerator.ButtonTexture;
            }
            SetDrawTiles();

            //get the screen cooridinates of the normal menu top
            Vector2 menuPositionScreenCoordinates = MenuPosition();
            //translate into world coordinates
            Vector2 entryPosition = GraphicsConfig.ScreenToWorldCoordinates(
                menuPositionScreenCoordinates);
            //bump down a bit because the top menu entry is going to expand
            entryPosition.Y -= buttonScale.Y / 2f;

            entryPosition.X = 0f;

            PressureBodyMenuEntry pressureEntry;
            for (int i = 0; i < MenuEntries.Count; i++)
            {
                pressureEntry = MenuEntries[i] as PressureBodyMenuEntry;
                if (pressureEntry != null)
                {
                    menuWorld.removeBody(pressureEntry.PhysicsBody);
                    pressureEntry.BuildBody(entryPosition, buttonScale);
                    menuWorld.addBody(pressureEntry.PhysicsBody);

                    entryPosition.Y -= buttonScale.Y + buttonSpacing;
                }
            }
        }

        protected override void DrawMenuEntries(GameTime gameTime, ref Vector2 menuPosition, ref Vector2 menuPositionOffset)
        {
            //base.DrawMenuEntries(gameTime, ref menuPosition, ref menuPositionOffset);
            if (this.IsActive)
            {
                if (GlobalConfig.DrawMethod == DrawStyle.Debug)
                {
                    menuWorld.debugDrawAllBodies(GraphicsConfig.GraphicsDevice, menuEffect, true);
                }
                else
                {
                    drawBatch.Begin(PrimitiveType.TriangleList, menuEffect);
                    PressureBodyMenuEntry pressureEntry;
                    for (int i = 0; i < MenuEntries.Count; i++)
                    {
                        pressureEntry = MenuEntries[i] as PressureBodyMenuEntry;
                        if (pressureEntry != null)
                        {
                            drawBatch.Draw(pressureEntry.DrawVertexes);
                        }
                    }
                    drawBatch.End();
                }
            }
        }

        protected override void AddMenuEntry(MenuEntry entry)
        {
            base.AddMenuEntry(entry);
            rebuildPressureBodyMenu = true;
        }

        protected override void RemoveMenuEntry(MenuEntry entry)
        {
            base.RemoveMenuEntry(entry);
            rebuildPressureBodyMenu = true;
        }

        protected override MenuEntry CreateMenuEntry(string text, MenuEntryAlignment align, bool isTrial, MenuEntryType type)
        {
            PressureBodyMenuEntry entry;
            entry = new PressureBodyMenuEntry(text, align, isTrial, type);
            return entry;
        }

        public override void TrialModeSwitched(bool isTrial)
        {
            base.TrialModeSwitched(isTrial);
            rebuildPressureBodyMenu = true;
        }
    }
}