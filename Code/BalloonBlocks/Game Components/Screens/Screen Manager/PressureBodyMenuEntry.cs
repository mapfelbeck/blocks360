﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Physics;
using Utilities;
#endregion

namespace BalloonBlocks
{
    class PressureBodyMenuEntry : MenuEntry
    {
        MenuPressureBody physicsBody;
        public MenuPressureBody PhysicsBody
        {
            get { return physicsBody; }
        }

        Tile drawTile;
        public Tile DrawTile
        {
            get { return drawTile; }
            set { drawTile = value; }
        }

        VertexPositionColorTexture[] drawVertexes;
        public VertexPositionColorTexture[] DrawVertexes
        {
            get { return drawVertexes; }
        }

        public PressureBodyMenuEntry(string text, bool isTrialEntry) :
            this(text, MenuEntryAlignment.Left, isTrialEntry, MenuEntryType.Normal) { }

        public PressureBodyMenuEntry(string text) :
            this(text, MenuEntryAlignment.Left, false, MenuEntryType.Normal) { }

        public PressureBodyMenuEntry(string text, MenuEntryAlignment align, bool isTrialEntry) :
            this(text, align, isTrialEntry, MenuEntryType.Normal) { }

        public PressureBodyMenuEntry(string text, MenuEntryAlignment align) :
            this(text, align, false, MenuEntryType.Normal) { }

        public PressureBodyMenuEntry(string text, MenuEntryAlignment align, MenuEntryType type) :
            this(text, align, false, type) { }

        public PressureBodyMenuEntry(string text, MenuEntryAlignment align, bool isTrial, MenuEntryType type) :
            base(text, align, isTrial, type)
        {
        }

        internal void BuildBody(Vector2 position, Vector2 scale)
        {
            float buttonAspectRatio =
                (float)drawTile.SourceRect.Width / (float)drawTile.SourceRect.Height;

            float height = 1f;
            float length = height * buttonAspectRatio;

            int numberSegments = Math.Max(2, (int)Math.Ceiling((double)buttonAspectRatio));
            int numberMasses = 2 + 2 * numberSegments;
            int numberVertexes = numberSegments * 6;

            /*Console.WriteLine("{0} segments, {1} masses, {2} vertexes",
                numberSegments, numberMasses, numberVertexes);*/

            drawVertexes = null;
            drawVertexes = new VertexPositionColorTexture[numberVertexes];

            physicsBody = null;
            Physics.ClosedShape newShape = new Physics.ClosedShape();

            float segmentLength = length / numberSegments;

            newShape.begin();
            for (int i = 0; i < numberMasses / 2; i++)
            {
                newShape.addVertex(new Vector2(position.X + i * segmentLength, position.Y));
            }
            for (int i = 0; i < numberMasses / 2; i++)
            {
                newShape.addVertex(new Vector2(position.X + length - i * segmentLength, position.Y - height));
            }
            newShape.finish();

            switch (alignment)
            {
                case MenuEntryAlignment.Center:
                    break;
                case MenuEntryAlignment.Left:
                    Vector2 leftSideScreenCoordinate = new Vector2(
                        GraphicsConfig.SafeArea.X * 1.25f, 0);
                    Vector2 leftSideWorldCoordinate =
                        GraphicsConfig.ScreenToWorldCoordinates(leftSideScreenCoordinate);
                    position.X = leftSideWorldCoordinate.X + (length / 2) * scale.X;
                    break;
                default:
                    break;
            }

            physicsBody = new MenuPressureBody(newShape, GameConstants.MassPerPoint, 0f,
                        GameConstants.ShapeSpringK, GameConstants.ShapeSpringDamp,
                        GameConstants.EdgeSpringK, GameConstants.EdgeSpringDamp,
                        position, 0, scale, false);

            for (int i = 1; i < numberSegments; i++)
            {
                physicsBody.addInternalSpring(i, (numberMasses-i)-1, GameConstants.InternalSpringK, GameConstants.InternalSpringDamp);
            }

            physicsBody.MenuPosition = position;
        }

        public override void Update(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            base.Update(screen, isSelected, gameTime);

            if (physicsBody != null && screen.IsActive)
            {
                float targetGasPressure = 0f;
                float delateScaler = 0f;
                if (CurrentlySelected)
                {
                    targetGasPressure = GameConstants.GasPressure * 3;
                    delateScaler = 2.5f;
                }
                else
                {
                    targetGasPressure = GameConstants.GasPressure;
                    delateScaler = 5f;
                }

                float pressureDifference = targetGasPressure - physicsBody.GasPressure;
                float pressureDelta = pressureDifference * (float)gameTime.ElapsedGameTime.TotalSeconds * delateScaler;
                physicsBody.GasPressure = physicsBody.GasPressure + pressureDelta;

                UpdateVertexes();
            }
        }



        protected override void SetColors()
        {
#if XBOX360
            if (!IsTrial)
            {
#endif
            normalColor = NonTrialNormalColor;
            selectedColor = NonTrialNormalColor;
#if XBOX360
            }
            else
            {
                normalColor = TrialSelectedColor;
                selectedColor = TrialSelectedColor;
            }
#endif
        } 

        protected void UpdateVertexes()
        {
            if (drawTile != null && drawVertexes != null)
            {
                int numberSegments = (PhysicsBody.PointMassCount - 2) / 2;
                int vertsPerSegment = 6;
 
                for (int i = 0; i < numberSegments; i++)
                {
                    /*
                     * upperLeft = i
                     * upperRight = i+1
                     * lowerRight = PhysicsBody.PointMassCount - (i+1)
                     * lowerLeft = PhysicsBody.PointMassCount - (i+2)
                     */
                    // find position for each corner of the segment
                    int upperLeft = i;
                    int upperRight = i + 1;
                    int lowerRight = PhysicsBody.PointMassCount - (i + 2);
                    int lowerLeft = PhysicsBody.PointMassCount - (i + 1);

                    drawVertexes[i * vertsPerSegment].Position =
                        VectorTools.vec3FromVec2(
                        ref physicsBody.getPointMass(upperLeft).Position);
                    drawVertexes[i * vertsPerSegment + 1].Position =
                        VectorTools.vec3FromVec2(
                        ref physicsBody.getPointMass(upperRight).Position);
                    drawVertexes[i * vertsPerSegment + 2].Position =
                        VectorTools.vec3FromVec2(
                        ref physicsBody.getPointMass(lowerRight).Position);

                    drawVertexes[i * vertsPerSegment + 3].Position = 
                        VectorTools.vec3FromVec2(
                        ref physicsBody.getPointMass(lowerRight).Position);
                    drawVertexes[i * vertsPerSegment + 4].Position = 
                        VectorTools.vec3FromVec2(
                        ref physicsBody.getPointMass(lowerLeft).Position);
                    drawVertexes[i * vertsPerSegment + 5].Position = 
                        VectorTools.vec3FromVec2(
                        ref physicsBody.getPointMass(upperLeft).Position);
                    
                    // find texture coordinates
                    // lerp between tile texture coordinates to get
                    // segment coordinates

                    Vector2 upperLeftCooridinate = 
                        Vector2.Lerp(drawTile.UpperLeft, drawTile.UpperRight, i / (float)numberSegments);
                    Vector2 upperRightCooridinate = 
                        Vector2.Lerp(drawTile.UpperLeft, drawTile.UpperRight, (i + 1) / (float)numberSegments);
                    Vector2 lowerRightCooridinate = 
                        Vector2.Lerp(drawTile.LowerLeft, drawTile.LowerRight, (i + 1) / (float)numberSegments);
                    Vector2 lowerLeftCooridinate =
                        Vector2.Lerp(drawTile.LowerLeft, drawTile.LowerRight, i / (float)numberSegments);

                    drawVertexes[i * vertsPerSegment].TextureCoordinate = upperLeftCooridinate;
                    drawVertexes[i * vertsPerSegment + 1].TextureCoordinate = upperRightCooridinate;
                    drawVertexes[i * vertsPerSegment + 2].TextureCoordinate = lowerRightCooridinate;

                    drawVertexes[i * vertsPerSegment + 3].TextureCoordinate = lowerRightCooridinate;
                    drawVertexes[i * vertsPerSegment + 4].TextureCoordinate = lowerLeftCooridinate;
                    drawVertexes[i * vertsPerSegment + 5].TextureCoordinate = upperLeftCooridinate;

                    // set color
                    Color color = Color.White;
                    if (CurrentlySelected)
                    {
                        color = Color.White;
                    }
                    else
                    {
                        color.A = 128;
                    }
                    drawVertexes[i * vertsPerSegment].Color = color;
                    drawVertexes[i * vertsPerSegment + 1].Color = color;
                    drawVertexes[i * vertsPerSegment + 2].Color = color;

                    drawVertexes[i * vertsPerSegment + 3].Color = color;
                    drawVertexes[i * vertsPerSegment + 4].Color = color;
                    drawVertexes[i * vertsPerSegment + 5].Color = color;
                }
            }
        }
    }
}