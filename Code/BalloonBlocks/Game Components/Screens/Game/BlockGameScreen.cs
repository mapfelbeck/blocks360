#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Utilities;
using Physics;
#endregion

namespace BalloonBlocks
{
    public class BlockGameScreen : GameScreen
    {
        protected bool forceSpawn;
        float timeSinceSpawn;
        bool scoreAdded;
        bool saveAttempted;
        bool normalExit;
        bool freezePromptShown;

        protected int gamePieceCounter;

        public bool IsUnfreezeGame
        {
            get { return gameConfig.blockType == BlockType.Freeze; }
        }

        public bool PlayerCanUnfreeze
        {
            get { return unFreezeDisplay != null && unFreezeDisplay.UnFreezeReady; }
        }

        bool CanForceDrop
        {
            get { return currentPiece != null && 
                currentPiece.GamePieceCenter().Y < ground.FailTop&&
                timeSinceSpawn >= GameConstants.ForcedSpawnCoolOff;
            }
        }

        protected int groupCounter;

        protected World gameWorld;

        public int NumberBlockGameBodies
        {
            get
            {
                if (gameWorld != null)
                {
                    return gameWorld.NumberBodies;
                }
                else
                {
                    return 0;
                }
            }
        }
        
        string[] FreezePromptGamepad = { "Press [A] to unfreeze all the frozen blocks." };
        string[] FreezePromptKeyboard = { "Press Spacebar to unfreeze all the frozen blocks." };


        Texture2D rawTexture;
        Texture2D rawFrozenTexture;

        BasicEffect blockEffect;
        BasicEffect frozenEffect;
        BasicEffect outlineEffect;
        bool resetMatrices;

        VertexPositionColorTexture[] outlineVerts = null;

        List<Body> staticBodies;
        bool drawGround;

        GameGround ground;

        protected List<GamePiece> gamePieces;
        protected GamePiece currentPiece;
        protected GamePiece nextPiece;

        //this is the abs. max amount of time between piece spawns
        float dropTimer;
        float forceDropInterval;
        //this is the normal amount of time between piece spawns
        float dropAfterCollideInterval;

        protected TiledTexture tiledGamepieceTexture;
        protected TiledTexture tiledFrozenGamepieceTexture;

        protected GameConfig gameConfig;

        Timer updateTimer;
        float updateTime;

        SpriteFont font;
        Vector2 location;
        SpriteBatch spriteBatch;
        DrawBatch drawBatch;

        protected HintPrompt hints;
        protected ScoringDisplay scoring;
        protected NextPieceDisplay nextPieceDisplay;
        protected DropIndicator dropIndicator;
        protected BackgroundBlocks backgroundBlocks;
        protected UnfreezeDisplay unFreezeDisplay;

        BubblePopParticleSystem bubblePopParticleSystem;
        ComboParticleSystem comboParticleSystem;
        MessageParticleSystem messageParticleSystem;

        //world location
        protected Vector2 spawnLocation = new Vector2(0, 10);
        //screen location
        protected Vector2 nextPieceLocation = new Vector2(0, 0);

        protected string playerName;

        protected float highestFillPiece;

        protected float startFriction;
        protected float endFriction;
        protected float gravityScalar;
        protected int comboCounter;


        public BlockGameScreen(GameConfig config, GameGround groundBodies,
            bool drawGround)
            : base()
        {
            gameConfig = config;
            ground = groundBodies;
            this.drawGround = drawGround;
            forceSpawn = false;
            gamePieces = new List<GamePiece>();
            staticBodies = new List<Physics.Body>();

            updateTimer = new Timer();
            SetupPlugins();
            highestFillPiece = float.NegativeInfinity;
        }

        protected virtual void SetupPlugins()
        {
            Vector2 hintLocation = new Vector2(GraphicsConfig.SafeArea.Left + GraphicsConfig.SafeArea.Width / 2,
                                        GraphicsConfig.SafeArea.Top * 2);

            dropIndicator = new DropIndicator();
            dropIndicator.DrawOrder = 1;
            AddPlugin(dropIndicator);

            nextPieceLocation = new Vector2((float)GraphicsConfig.SafeArea.X,
                                            (float)GraphicsConfig.SafeArea.Y);
            float nextPieceDisplaySize = (float)Math.Min((float)GraphicsConfig.Resolution.Width / 7.5,
                                                    (float)GraphicsConfig.Resolution.Height / 7.5);
            nextPieceDisplay = new NextPieceDisplay(gameConfig.shape,
                                nextPieceLocation, nextPieceDisplaySize);

            nextPieceDisplay.DrawOrder = 1;
            AddPlugin(nextPieceDisplay);

            hints = new HintPrompt(hintLocation);
            hints.DrawOrder = 2;
            AddPlugin(hints);

            if (gameConfig.blockType == BlockType.Freeze)
            {
                Vector2 unFreezeLocation = new Vector2((GraphicsConfig.SafeArea.Left + GraphicsConfig.SafeArea.Right) / 2, GraphicsConfig.SafeArea.Top * 1.5f);
                Vector2 unFreezeSize = new Vector2(GraphicsConfig.Fullscreen.Width * .3f, (GraphicsConfig.Fullscreen.Width * .3f) / 12);

                float rechargeTime = 0f;
                switch (gameConfig.gameDifficulty)
                {
                    case GameDifficulty.Easy:
                        rechargeTime = 2 * 60f;
                        break;
                    case GameDifficulty.Medium:
                        rechargeTime = 3 * 60f;
                        break;
                    case GameDifficulty.Hard:
                        rechargeTime = 4 * 60f;
                        break;
                    case GameDifficulty.Custom:
                        rechargeTime = 2 * 60f;
                        break;
                    default:
                        rechargeTime = 5 * 60f;
                        break;
                }

                unFreezeDisplay = new UnfreezeDisplay(unFreezeLocation, unFreezeSize, rechargeTime);

                unFreezeDisplay.DrawOrder = 1;
                AddPlugin(unFreezeDisplay);
            }

            SetupScoring();
            if (scoring != null)
            {
                scoring.DrawOrder = 1;
                AddPlugin(scoring);
            }

            bubblePopParticleSystem = new BubblePopParticleSystem(10);
            bubblePopParticleSystem.DrawOrder = 3;
            AddPlugin(bubblePopParticleSystem);

            comboParticleSystem = new ComboParticleSystem(10);
            comboParticleSystem.DrawOrder = 3;
            AddPlugin(comboParticleSystem);

            messageParticleSystem = new MessageParticleSystem(3);
            messageParticleSystem.DrawOrder = 3;
            AddPlugin(messageParticleSystem);
        }

        protected virtual void Init()
        {
            backgroundBlocks = new BackgroundBlocks(gameConfig);

            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            RegisterEvents();

            currentPiece = null;

            if (gameWorld == null)
            {
                gravityScalar = 1f;
#if XBOX360
                gameWorld = Physics.WorldFactory.BuildThreadedWorld();
#else
                gameWorld = Physics.WorldFactory.BuildWorld();
#endif
                startFriction = GlobalConfig.EnumCoefficient(gameConfig.friction);
                endFriction = MathHelper.Max(0f, startFriction - GameConstants.MaxFrictionDecrease);
                gameWorld.SetBodyDampening(startFriction);
                gameWorld.PhysicsSimIter = 2;
                gameWorld.externalAccumulator = blockGameAccumulator;
            }

            dropAfterCollideInterval = gameConfig.dropAfterCollideTime;
            forceDropInterval = gameConfig.forceDropTime;
            dropTimer = 0f;

            ground.CreatAndAddStaticBodiesToWorld(gameWorld, groupCounter);
            groupCounter++;
        }

        protected virtual void SetupScoring()
        {
            scoring = new ScoringDisplay(gameConfig.gameDifficulty);
        }

        public override void RegisterEvents()
        {
            if (GlobalConfig.IncreasingSpeed)
            {
                EventManager.Register(speedIncreaseCallback, "Combo");
            }
            
            if (gameConfig.blockType == BlockType.Freeze)
            {
                SetupUnfreeze();
            }
        }

        public override void UnRegisterEvents()
        {
            EventManager.UnRegister(speedIncreaseCallback, "Combo");

            if (gameConfig.blockType == BlockType.Freeze)
            {
                EventManager.UnRegister(enableUnfreezeCallback, "EnableUnfreeze");
                EventManager.UnRegister(unfreezeCallback, "Unfreeze");
            }
        }

        private void speedIncreaseCallback(object sender, string theEvent, params object[] args)
        {
            comboCounter++;

            float rate = Math.Min(1f, (float)comboCounter / (float)GameConstants.CombosToMaxSpeed);
            float newFriction = MathHelper.Lerp(startFriction,endFriction,rate);
            gravityScalar = MathHelper.Lerp(1f, GameConstants.MaxGravityIncrease, rate);
            
            gameWorld.SetBodyDampening(newFriction);
        }

        private void enableUnfreezeCallback(object sender, string theEvent, params object[] args)
        {
            if (IsUnfreezeGame)
            {
                Message message = new Message();
                message.text = "Unfreeze power recharged!";
                message.color = Color.White;
                message.location = new Vector2(
                    GraphicsConfig.GraphicsDevice.Viewport.Width / 2,
                    GraphicsConfig.GraphicsDevice.Viewport.Height / 2);

                EventManager.Trigger(this, "Message", message);
            }
        }

        private void SetupUnfreeze()
        {
            EventManager.Register(enableUnfreezeCallback, "EnableUnfreeze");
            EventManager.Register(unfreezeCallback, "Unfreeze");
        }

        /// <summary>
        /// Load your graphics content.  If loadAllContent is true, you should
        /// load content from both ResourceManagementMode pools.  Otherwise, just
        /// load ResourceManagementMode.Manual content.
        /// </summary>
        /// <param name="loadAllContent">Which type of content to load.</param>
        public override void LoadContent()
        {
            base.LoadContent();
            Init();

            outlineVerts = new VertexPositionColorTexture[8];

            font = Content.Load<SpriteFont>("Fonts/FrameRateFont");
            location = new Vector2(GraphicsConfig.SafeArea.X * 2.5f, GraphicsConfig.SafeArea.Y * 1.5f);

#if XBOX360
            if (ControllingPlayer != null && PlayerManager.GetPlayer(ControllingPlayer.Value) != null)
            {
                playerName = PlayerManager.GetPlayer(ControllingPlayer.Value).Gamertag;
            }
            else 
#endif
            if (ControllingPlayer != null)
            {
                playerName = string.Format("Player {0}", ControllingPlayer.Value.ToString());
            }
            else
            {
                playerName = "Player";
            }

            backgroundBlocks.LoadContent(Content);
            ground.LoadContent(Content);

            if (nextPieceDisplay != null)
            {
                dropIndicator.PlaceRelativeTo(nextPieceDisplay.Location, nextPieceDisplay.Size);
            }

            spriteBatch = new SpriteBatch(GraphicsConfig.GraphicsDevice);
            drawBatch = new DrawBatch(GraphicsConfig.GraphicsDevice);

            rawTexture = Content.Load<Texture2D>(
                gameConfig.blockTextureName);
            rawFrozenTexture = Content.Load<Texture2D>(
                "block textures/frosted greyscale");

            tiledGamepieceTexture = new TiledTexture(
                rawTexture, gameConfig.blockTextureSize, gameConfig.blockTextureSize);
            tiledFrozenGamepieceTexture = new TiledTexture(
                rawFrozenTexture, gameConfig.blockTextureSize, gameConfig.blockTextureSize);

            // visual effect
            blockEffect = new BasicEffect(GraphicsConfig.GraphicsDevice, null);
            frozenEffect = new BasicEffect(GraphicsConfig.GraphicsDevice, null);
            outlineEffect = new BasicEffect(GraphicsConfig.GraphicsDevice, null);
            SetupShaders();

            ScreenManager.Game.ResetElapsedTime();

            resetMatrices = true;
        }

        protected override void DeviceReset(object sender, EventArgs e)
        {
            base.DeviceReset(sender, e);

            resetMatrices = true;
        }

        private void SetupShaders()
        {
            SetupShaderMatices();

            blockEffect.VertexColorEnabled = true;
            blockEffect.LightingEnabled = false;
            blockEffect.TextureEnabled = GlobalConfig.DrawMethod == DrawStyle.Textured;
            blockEffect.Texture = tiledGamepieceTexture.BaseTexture;
            
            frozenEffect.VertexColorEnabled = true;
            frozenEffect.LightingEnabled = false;
            frozenEffect.TextureEnabled = GlobalConfig.DrawMethod == DrawStyle.Textured;
            frozenEffect.Texture = tiledFrozenGamepieceTexture.BaseTexture;

            outlineEffect.VertexColorEnabled = true;
            outlineEffect.LightingEnabled = false;
        }

        private void SetupShaderMatices()
        {
            blockEffect.View = GraphicsConfig.View;
            blockEffect.Projection = GraphicsConfig.Projection;
            blockEffect.World = GraphicsConfig.World;

            frozenEffect.View = GraphicsConfig.View;
            frozenEffect.Projection = GraphicsConfig.Projection;
            frozenEffect.World = GraphicsConfig.World;
            
            outlineEffect.View = GraphicsConfig.View;
            outlineEffect.Projection = GraphicsConfig.Projection;
            outlineEffect.World = GraphicsConfig.World;

            resetMatrices = false;
        }

        private void LoadUnfreezePrompts()
        {
            string[] freezePrompts;
            if (ScreenManager.Input.GamePadConnected)
            {
                freezePrompts = FreezePromptGamepad;
            }
            else
            {
                freezePrompts = FreezePromptKeyboard;
            }
            foreach (string prompt in freezePrompts)
            {
                hints.AddHint(prompt);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            updateTimer.Begin();
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //HACK: this keeps the profile build from blowing up
            if (elapsedTime <= .001f)
            {
                elapsedTime = .001f;
            }

            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                if (gameWorld.NumberBodies > 75 && PlayerCanUnfreeze && !freezePromptShown && gameConfig.blockType == BlockType.Freeze)
                {
                    freezePromptShown = true;
                    LoadUnfreezePrompts();
                }

                gameWorld.Update(elapsedTime);

                //update drop timer
                if (currentPiece != null && currentPiece.HasEverCollided)
                {
                    dropTimer = Math.Min(dropTimer, dropAfterCollideInterval);
                    dropIndicator.DepleteRatio = Math.Max(dropIndicator.DepleteRatio, (1f / dropTimer) * dropIndicator.FilledAmount);
                }
                dropTimer -= elapsedTime;

                timeSinceSpawn += elapsedTime;
                if (CheckForPieceSpawn(elapsedTime))
                {
                    dropTimer = forceDropInterval;

                    if (dropIndicator != null)
                    {
                        dropIndicator.DepleteRatio = 1f / dropTimer;
                        dropIndicator.FilledAmount = 1f;
                    }

                    SpawnPiece();
                    timeSinceSpawn = 0f;
                }

                backgroundBlocks.Update(elapsedTime);

                if (currentPiece != null)
                {
                    currentPiece.InFailLocation = !InsideGameArea(currentPiece);
                    /*if (!InsideGameArea(currentPiece))
                    {
                        currentPiece.InFailLocation = true;
                    }*/
                    currentPiece.RemainingLifeTime = dropTimer;
                }

                for (int i = 0; i < gamePieces.Count; i++)
                {
                    gamePieces[i].Update(elapsedTime);

                    if (gamePieces[i].Blocks.Count == 0)
                    {
                        gamePieces.Remove(gamePieces[i]);
                        i--;
                    }
                }

                if (nextPiece != null)
                {
                    dropIndicator.CanForceDrop = CanForceDrop;
                }

                //if we're in a game over condition and haven't triggered an exit yet
                if (CheckForGameOver() && !IsExiting)
                {
                    string restartMessage = "placeholder";
                    if (CheckForWin())
                    {
                        restartMessage = "You Win! Play Again?";
                    }
                    else if (CheckForFail())
                    {
                        restartMessage = "Game Over! Play Again?";
                    }

                    RestartPrompt(restartMessage);
                    TryToSaveScore();
                }

            }
            if (!normalExit && IsExiting)
            {
                normalExit = true;
                TryToSaveScore();
            }
            updateTime = (float)updateTimer.End(false);
        }

        private void TryToSaveScore()
        {
            if (HighScoreTable.SupportedGameType(gameConfig.gameDifficulty)&&
                gameConfig.scored)
            {
                if (CanSaveScore())
                {
                    SaveScore();
                }
                else if (!saveAttempted)
                {
                    SignInPrompt("Sign in to save your score?");
                }
            }
            saveAttempted = true;
        }

        protected virtual bool CanSaveScore()
        {
#if XBOX360
            if (ControllingPlayer != null &&
                PlayerManager.IsControllerSignedIn(ControllingPlayer.Value))
            {
                return true;
            }

            return false;
#else
            return true;
#endif
        }

        protected bool CheckForGameOver()
        {
            return CheckForFail() || CheckForWin();
        }

        protected virtual bool CheckForFail()
        {
            foreach (GamePiece piece in gamePieces)
            {
                if (piece != currentPiece && !InsideGameArea(piece)
                    && highestFillPiece >= ground.FailFillHeight)
                {
                    return true;
                }
            }

            return false;
        }

        protected bool InsideGameArea(GamePiece piece)
        {
            Vector2 pieceCenter = piece.GamePieceCenter();

            if ((pieceCenter.Y >= ground.FailTop ||
                pieceCenter.Y <= ground.FailBottom ||
                pieceCenter.X <= ground.FailLeft ||
                pieceCenter.X >= ground.FailRight))
            {
                return false;
            }
            return true;
        }

        protected virtual bool CheckForWin()
        {
            return false;
        }

        protected void SignInPrompt(string message)
        {
            MessageBoxScreen signInMessageBox = ScreenManager.GetMessageBox(message);

            signInMessageBox.Accepted += SignInPlayerAndSaveScore;

            ScreenManager.AddScreen(signInMessageBox, null);
        }

        protected void SignInPlayerAndSaveScore(object sender, PlayerIndexEventArgs e)
        {
            PlayerManager.ShowSignIn(SaveScore);
        }

        protected void RestartPrompt(string message)
        {
            MessageBoxScreen restartMessageBox = ScreenManager.GetMessageBox(message);

            restartMessageBox.Accepted += ConfirmRestart;
            restartMessageBox.Cancelled += LoadMenu;

            ScreenManager.AddScreen(restartMessageBox, null);
        }

        private void SaveScore()
        {
            if (scoring != null)
            {
                if (!scoreAdded)
                {
                    AddScore();
                }
#if XBOX360
                if (CanSaveScore())
                {
                    SaveLoad.Trigger(RequestStorageAndSaveScore, null);
                }
#else
                SaveLoad.Trigger(HighScoreTable.Save, null);
#endif
            }
        }

        private void RequestStorageAndSaveScore()
        {
            StorageDeviceManager.RequestStorageDevice(HighScoreTable.Save);
        }

        private void AddScore()
        {
            scoreAdded = true;
            if (PlayerManager.IsControllerSignedIn(ControllingPlayer.Value))
            {
                AddToHighScoreTable();
            }
        }

        protected void AddToHighScoreTable()
        {
            if (PlayerManager.IsControllerSignedIn(ControllingPlayer.Value))
            {
                playerName = PlayerManager.GetPlayer(ControllingPlayer.Value).Gamertag;
                HighScoreTable.Add(playerName, gameConfig.gameType, gameConfig.gameDifficulty, scoring.Score, scoring.PlayTime, scoring.Remaining);
            }
        }

        protected void ConfirmRestart(object sender, PlayerIndexEventArgs e)
        {
            BackgroundScreen background = ScreenManager.GetScreens()[0] as BackgroundScreen;

            if (background == null)
            {
                throw new NullReferenceException("null background screen, unpossible!");
            }

            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                                new BackgroundScreen(background.BackGroundName),
                                BlockGameFactory.CreateGame(gameConfig));
        }

        protected virtual void LoadMenu(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                               new BackgroundScreen("Menu Backgrounds/Blank"),
                               new SinglePlayerModeScreen());
        }

        protected virtual void SpawnPiece()
        {
            GameBlock checkBlock;
            for (int i = 0; i < gameWorld.NumberBodies; i++)
            {
                checkBlock = gameWorld.getBody(i) as GameBlock;
                if (checkBlock != null && !checkBlock.IsStatic)
                {
                    if (currentPiece != null &&
                        !currentPiece.Blocks.Contains(checkBlock))
                    {
                        highestFillPiece = Math.Max(highestFillPiece, checkBlock.DerivedPosition.Y);
                    }
                }
            }

            if (nextPiece == null)
            {
                nextPiece = GamePieceFactory.CreateGamePiece(spawnLocation, tiledGamepieceTexture, gameConfig, new RandomColorGenerator());
                nextPiece.PieceNumber = gamePieceCounter;
                gamePieceCounter++;
            }
            if (currentPiece != null)
            {
                currentPiece.IsControlledPiece = false;
            }
            currentPiece = nextPiece;
            currentPiece.IsControlledPiece = true;

            EventManager.Trigger(this, "PieceCreate");

            gamePieces.Add(currentPiece);
            foreach (GameBlock theBlock in currentPiece.Blocks)
            {
                theBlock.GroupNumber = groupCounter;
                gameWorld.addBody(theBlock);
            }
            groupCounter++;

            nextPiece = GamePieceFactory.CreateGamePiece(spawnLocation, tiledGamepieceTexture, gameConfig, new RandomColorGenerator());
            nextPiece.PieceNumber = gamePieceCounter;
            gamePieceCounter++;
            nextPieceDisplay.NextPiece = nextPiece;
        }

        protected virtual bool CheckForPieceSpawn(float elapsedTime)
        {
            bool needsSpawn = false;

            if (dropTimer <= 0f || forceSpawn)
            {
                needsSpawn = true;
            }

            return needsSpawn;
        }
        protected void SetControllingPlayer(object sender, PlayerIndexEventArgs e)
        {
            ControllingPlayer = e.PlayerIndex;
            RumbleComponent.Player = ControllingPlayer.Value;
        }

        public override void HandleInput(InputState input)
        {
            if (ControllingPlayer != null &&
                input.GamePadDisconnected(ControllingPlayer.Value))
            {
                ControllerDisconnectedMessageBox message = new ControllerDisconnectedMessageBox();
                message.Accepted += SetControllingPlayer;
                ScreenManager.AddScreen(message, null);
            }

            // Look up inputs for the active player profile.
            int playerIndex;

            if (ControllingPlayer != null)
            {
                playerIndex = (int)ControllingPlayer.Value;
            }
            else
            {
                playerIndex = 1;
            }

            KeyboardState newKeyState = input.CurrentKeyboardStates[playerIndex];
            GamePadState newPadState = input.CurrentGamePadStates[playerIndex];

            PlayerIndex meh;
            if (IsUnfreezeGame && PlayerCanUnfreeze)
            {
                if ((input.IsNewKeyPress(Keys.Space, ControllingPlayer, out meh) || 
                    input.IsNewButtonPress(Buttons.A, ControllingPlayer, out meh)))
                {
                    EventManager.Trigger(this, "Unfreeze", true);
                }
            }

            forceSpawn = false;
            if (input.IsNewKeyPress(Keys.P, ControllingPlayer, out meh) ||
                input.IsNewButtonPress(Buttons.X, ControllingPlayer, out meh))
            {
                if (CanForceDrop)
                {
                    forceSpawn = true;
                }
            }

            if (input.IsPauseGame(null))
            {
                PauseMenuScreen pauseScreen = new PauseMenuScreen();
                ScreenManager.AddScreen(pauseScreen, ControllingPlayer);
            }

            if (currentPiece != null)
            {
                currentPiece.HandleInput(newPadState, newKeyState);
            }
        }

        private void unfreezeCallback(object sender, string theEvent, params object[] args)
        {
            foreach (GamePiece piece in gamePieces)
            {
                foreach (GameBlock block in piece.Blocks)
                {
                    FreezingGameBlock freeze = block as FreezingGameBlock;
                    if (freeze != null)
                    {
                        freeze.UnFreezeFor(gameConfig.unFreezeTime);
                    }
                }
            }

            Message message = new Message();
            message.text = "Unfreeze!";
            message.color = Color.White;
            message.location = new Vector2(
                GraphicsConfig.GraphicsDevice.Viewport.Width / 2,
                GraphicsConfig.GraphicsDevice.Viewport.Height / 2);

            EventManager.Trigger(this, "Message", message);
        }

        private void blockGameAccumulator(float elapsedTime)
        {
            Vector2 gravity = new Vector2(0f, GlobalConfig.EnumCoefficient(gameConfig.gravity));
            gravity.Y *= gravityScalar;

            foreach (GamePiece piece in gamePieces)
            {
                piece.ApplyForce(gravity);
                piece.GamePieceAccumulator(elapsedTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            if (resetMatrices)
            {
                SetupShaderMatices();
            }

            ScreenManager.GraphicsDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
            ScreenManager.GraphicsDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;

#if DEBUG
            switch (GlobalConfig.DrawMethod)
            {
                case DrawStyle.Debug:
                    gameWorld.debugDrawAllBodies(ScreenManager.GraphicsDevice, blockEffect, false);
                    break;
                default:
#endif
                    if (gameConfig.gameDifficulty != GameDifficulty.SplashScreen)
                    {
                        backgroundBlocks.Draw(drawBatch);
                    }

                    if (currentPiece != null)
                    {
                        drawBatch.Begin(PrimitiveType.LineList, outlineEffect);
                        //draw colored outline of the blocks here
                        foreach (GameBlock block in currentPiece.Blocks)
                        {
                            BuildOutlineVerts(block);
                            drawBatch.Draw(outlineVerts);
                        }
                        drawBatch.End();
                    }

                    drawBatch.Begin(PrimitiveType.TriangleList, blockEffect);
                    for (int i = gamePieces.Count - 1; i >= 0; i--)
                    {
                        drawBatch.Draw(gamePieces[i].DrawVerts);
                    }
                    drawBatch.End();

                    drawBatch.Begin(PrimitiveType.TriangleList, frozenEffect);
                    if (gameConfig.blockType == BlockType.Freeze)
                    {
                        for (int i = gamePieces.Count - 1; i >= 0; i--)
                        {
                            drawBatch.Draw(gamePieces[i].FrozenVerts);
                        }
                    }
                    drawBatch.End();

                    if (drawGround)
                    {
                        ground.Draw(drawBatch);
                    }
#if DEBUG
                    break;
            }
#endif

            if (GlobalConfig.ShowPerfData)
            {
                string fpsString = PerfString();

                spriteBatch.Begin();
                spriteBatch.DrawString(font, fpsString, location + Vector2.One, Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0);
                spriteBatch.DrawString(font, fpsString, location, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0);
                spriteBatch.End();
            }

            base.Draw(gameTime);
        }

        protected virtual string PerfString()
        {
            return string.Format("gameplay: {0} bodies, {1} iter, {2:0.000} update\nbackground: {3} bodies, {4} iter, {5:0.000} upda\nupdate loop time: {6:0.000}", 
                gameWorld.NumberBodies, gameWorld.PhysicsSimIter, gameWorld.UpdateTime, 
                backgroundBlocks.BackGroundWorld.NumberBodies, backgroundBlocks.BackGroundWorld.PhysicsSimIter, backgroundBlocks.BackGroundWorld.UpdateTime,
                updateTime);
        }

        private void BuildOutlineVerts(GameBlock block)
        {
            Color color = block.BlockColor;
            outlineVerts[0].Color = color;
            outlineVerts[1].Color = color;
            outlineVerts[2].Color = color;
            outlineVerts[3].Color = color;
            outlineVerts[4].Color = color;
            outlineVerts[5].Color = color;
            outlineVerts[6].Color = color;
            outlineVerts[7].Color = color;

            outlineVerts[0].Position = VectorTools.vec3FromVec2(
                block.getPointMass(0).Position);
            outlineVerts[1].Position = VectorTools.vec3FromVec2(
                block.getPointMass(1).Position);
            outlineVerts[2].Position = VectorTools.vec3FromVec2(
                block.getPointMass(1).Position);
            outlineVerts[3].Position = VectorTools.vec3FromVec2(
                block.getPointMass(2).Position);
            outlineVerts[4].Position = VectorTools.vec3FromVec2(
                block.getPointMass(2).Position);
            outlineVerts[5].Position = VectorTools.vec3FromVec2(
                block.getPointMass(3).Position);
            outlineVerts[6].Position = VectorTools.vec3FromVec2(
                block.getPointMass(3).Position);
            outlineVerts[7].Position = VectorTools.vec3FromVec2(
                block.getPointMass(0).Position);
        }

        public override void Dispose()
        {
            UnRegisterEvents();
            gameWorld.Dispose();
            if (backgroundBlocks != null)
            {
                backgroundBlocks.Dispose();
            }
            base.Dispose();
        }
    }
}