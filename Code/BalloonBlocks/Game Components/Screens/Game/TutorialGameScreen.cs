﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Physics;
using System.Diagnostics;
#endregion

namespace BalloonBlocks
{
    public class TutorialModeScreen : BlockGameScreen
    {
        enum TutotialState
        {
            Initial,
            MovePiece,
            RotatePiece,
            TouchColors,
            Unfreeze,
            PlayGame,
        }

        string movePromptKeyboard = "Use the W, S, A, D keys to move the game piece.";
        string movePromptGamepad = "Use the Left[LTHUMB] to move the game piece.";

        string rotatePromptKeyboard = "Use the Left and Right arrow keys to rotate the piece.";
        string rotatePromptGamepad = "Use the [LT] [RT] or the Right[RTHUMB] to rotate the piece.";

        string touchColorsPrompt = "Touch 3 or more blocks of the same color together to make them pop.";

        string freezePromptKeyboard = "Press Spacebar to unfreeze all the frozen blocks.";
        string freezePromptGamepad = "Press [A] to unfreeze all the frozen blocks.";

        TutotialState state;
        bool blocksAreFreezing;
        bool showColorPulse;
        bool spawningNormally;
        bool moveShown;
        bool rotateShown;
        bool touchShown;
        bool touchBlocksSpawned;
        bool unfreezeShown;

        float initialRotation;
        float waitTimer;

        protected Vector2 initialSpawn = new Vector2(0, 0);

        public TutorialModeScreen(GameConfig config, GameGround groundBodies,
            bool drawGround)
            : base(config, groundBodies, drawGround)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
            blocksAreFreezing = false;
            showColorPulse = false;
            spawningNormally = false;
            gameConfig.gravity = Gravity.None;
            state = TutotialState.Initial;
        }

        protected override bool CanSaveScore()
        {
            return false;
        }

        protected override bool CheckForPieceSpawn(float elapsedTime)
        {
            if (spawningNormally)
            {
                return base.CheckForPieceSpawn(elapsedTime);
            }
            else
            {
                return false;
            }
        }

        void SpawnPieceInLocation(Vector2 location)
        {
            if (nextPiece == null)
            {
                nextPiece = GamePieceFactory.CreateGamePiece(location, tiledGamepieceTexture, gameConfig, new RandomColorGenerator());
                nextPiece.PieceNumber = gamePieceCounter;
                gamePieceCounter++;
            }
            if (currentPiece != null)
            {
                currentPiece.IsControlledPiece = false;
            }
            currentPiece = nextPiece;
            currentPiece.IsControlledPiece = true;
            currentPiece.ShowColorPulse = showColorPulse;
            currentPiece.SetControlForces(
                GlobalConfig.EnumCoefficient(Gravity.Normal));

            EventManager.Trigger(this, "PieceCreate");

            gamePieces.Add(currentPiece);
            foreach (GameBlock theBlock in currentPiece.Blocks)
            {
                theBlock.FreezeAble = blocksAreFreezing;
                theBlock.GroupNumber = groupCounter;
                gameWorld.addBody(theBlock);
            }
            groupCounter++;

            nextPiece = GamePieceFactory.CreateGamePiece(location, tiledGamepieceTexture, gameConfig, new RandomColorGenerator());
            nextPiece.PieceNumber = gamePieceCounter;
            gamePieceCounter++;
            nextPieceDisplay.NextPiece = nextPiece;
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                switch (state)
                {
                    case TutotialState.Initial:
                        if (currentPiece == null)
                        {
                            SpawnPieceInLocation(initialSpawn);
                        }
                        currentPiece.ControlMode = ControlMode.Unassisted;
                        state++;
                        break;
                    case TutotialState.MovePiece:
                        if (!moveShown)
                        {
                            ShowMovePrompt();
                            moveShown = true;
                        }
                        Vector2 currentPieceLocation = currentPiece.GamePieceCenter();
                        Vector2 diff = currentPieceLocation - initialSpawn;
                        float length = diff.Length();
                        if (length >= 4f)
                        {
                            state++;
                            hints.FadeOutAll();
                        }
                        break;
                    case TutotialState.RotatePiece:
                        if (!rotateShown)
                        {
                            ShowRotatePrompt();
                            rotateShown = true;
                            initialRotation = currentPiece.GamePieceRotation();
                        }
                        float currentRotation = currentPiece.GamePieceRotation();
                        if (currentRotation <= initialRotation - MathHelper.PiOver2 ||
                            currentRotation >= initialRotation + MathHelper.PiOver2)
                        {
                            state++;
                            hints.FadeOutAll();
                        }
                        break;
                    case TutotialState.TouchColors:
                        if (!touchShown)
                        {
                            ShowTouchPrompt();
                            touchShown = true;
                            TurnOnGravity();
                            StartFreezingBlocks();
                        }
                        waitTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                        if (waitTimer > 5f && !touchBlocksSpawned)
                        {
                            touchBlocksSpawned = true;
                            spawningNormally = true;
                            
                            SpawnPieceInLocation(new Vector2(-6f, -6f));
                            SpawnPieceInLocation(new Vector2(0f, -6f));
                            SpawnPieceInLocation(new Vector2(6f, -6f));

                            SpawnPieceInLocation(new Vector2(-6f, 2f));
                            SpawnPieceInLocation(new Vector2(0f, 2f));
                            SpawnPieceInLocation(new Vector2(6f, 2f));
                            state++;
                        }
                        break;
                    case TutotialState.Unfreeze:
                        if (!unfreezeShown)
                        {
                            ShowUnfreezePrompt();
                            unfreezeShown = true;
                        }
                        break;
                    case TutotialState.PlayGame:
                        break;
                    default:
                        break;
                }
            }
        }

        void StartFreezingBlocks()
        {
            blocksAreFreezing = true;

            GameBlock block;
            for (int i = 0; i < gameWorld.NumberBodies; i++)
            {
                block = gameWorld.getBody(i) as GameBlock;
                if (block != null)
                {
                    block.FreezeAble = true;
                }
            }
        }

        void ShowMovePrompt()
        {
            string movePrompt;
            if (ScreenManager.Input.GamePadConnected)
            {
                movePrompt = movePromptGamepad;
            }
            else
            {
                movePrompt = movePromptKeyboard;
            }
            hints.AddHint(movePrompt, false);
        }

        void ShowRotatePrompt()
        {
            string rotatePrompt;
            if (ScreenManager.Input.GamePadConnected)
            {
                rotatePrompt = rotatePromptGamepad;
            }
            else
            {
                rotatePrompt = rotatePromptKeyboard;
            }
            hints.AddHint(rotatePrompt, false);
        }

        private void ShowTouchPrompt()
        {
            hints.AddHint(touchColorsPrompt, true);
        }

        private void ShowUnfreezePrompt()
        {
            string unfreezePrompt;
            if (ScreenManager.Input.GamePadConnected)
            {
                unfreezePrompt = freezePromptGamepad;
            }
            else
            {
                unfreezePrompt = freezePromptKeyboard;
            }
            hints.AddHint(unfreezePrompt, true);
        }

        private void TurnOnGravity()
        {
            gameConfig.gravity = Gravity.Normal;
        }

        protected override void LoadMenu(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex,
                               new BackgroundScreen("Menu Backgrounds/Main"),
                               new MainMenuScreen());
        }
    }
}