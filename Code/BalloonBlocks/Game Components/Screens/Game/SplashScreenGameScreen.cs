﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Utilities;
#endregion

namespace BalloonBlocks
{
    class SplashScreenGameScreen : BlockGameScreen
    {
        public SplashScreenGameScreen(GameConfig config, GameGround groundBodies
            ):base(config, groundBodies, false)
        {
            spawnLocation.X -= GlobalConfig.EnumCoefficient(config.blockSize);
        }

        protected override void SetupPlugins()
        {
        }

        protected override bool CheckForFail()
        {
            return false;
        }

        protected override bool CheckForPieceSpawn(float elapsedTime)
        {
            bool needsSpawn = false;

            if (gamePieces.Count == 0)
            {
                needsSpawn = true;
            }

            return needsSpawn;
        }

        protected override void SpawnPiece()
        {
            GamePiece newBody = GamePieceFactory.CreateGamePiece(spawnLocation, tiledGamepieceTexture, gameConfig, new RandomColorGenerator(), false);

            gamePieces.Add(newBody);
            foreach (GameBlock theBlock in newBody.Blocks)
            {
                theBlock.GroupNumber = groupCounter;
                gameWorld.addBody(theBlock);
            }
        }

        protected override string PerfString()
        {
            return "";
        }
    }
}
