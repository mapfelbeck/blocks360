﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Threading;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class LoadTestScreen : BlockGameScreen
    {
        public LoadTestScreen(GameConfig config, GameGround groundBodies, bool drawGround)
            : base(config, groundBodies, drawGround) { }

        protected override bool CheckForPieceSpawn(float elapsedTime)
        {
            return forceSpawn;
        }

        public override void HandleInput(InputState input)
        {
            base.HandleInput(input);

            PlayerIndex meh;

            if (input.IsNewKeyPress(Keys.P, ControllingPlayer, out meh) ||
                input.IsNewButtonPress(Buttons.X, ControllingPlayer, out meh))
            {
                    forceSpawn = true;
            }

            if (input.IsNewKeyPress(Keys.Left, ControllingPlayer, out meh) ||
                input.IsNewButtonPress(Buttons.LeftShoulder, ControllingPlayer, out meh))
            {
                if (gameWorld.PhysicsSimIter > 1)
                {
                    gameWorld.PhysicsSimIter = gameWorld.PhysicsSimIter - 1;
                }
            }
            else if (input.IsNewKeyPress(Keys.Right, ControllingPlayer, out meh) ||
                input.IsNewButtonPress(Buttons.RightShoulder, ControllingPlayer, out meh))
            {
                gameWorld.PhysicsSimIter = gameWorld.PhysicsSimIter + 1;
            }
        }

        protected override void SpawnPiece()
        {
            GamePiece newBody;

            ColorGenerator colorGen = new RandomColorGenerator();

            newBody = GamePieceFactory.CreateGamePiece(spawnLocation, tiledGamepieceTexture, gameConfig, colorGen);

            gamePieces.Add(newBody);
            foreach (GameBlock theBlock in newBody.Blocks)
            {
                theBlock.GroupNumber = groupCounter;
                theBlock.Poppable = false;
                gameWorld.addBody(theBlock);
            }
            groupCounter++;
        }

        protected override bool CheckForFail()
        {
            return false;
        }

        public override void LoadContent()
        {
            base.LoadContent();

            gameWorld.PhysicsSimIter = 1;
            gameWorld.AutoTuneIter = false;
        }
    }
}