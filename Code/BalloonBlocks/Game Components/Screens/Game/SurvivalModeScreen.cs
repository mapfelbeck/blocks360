﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Physics;
#endregion

namespace BalloonBlocks
{
    public class SurvivalModeScreen : BlockGameScreen
    {
        float timeLeft;

        public SurvivalModeScreen(GameConfig config, GameGround groundBodies,
            bool drawGround)
            : base(config, groundBodies, drawGround)
        {
            switch (config.gameDifficulty)
            {
                case GameDifficulty.Easy:
                    timeLeft = 3f * 60f;
                    break;
                case GameDifficulty.Medium:
                    timeLeft = 6f * 60f;
                    break;
                case GameDifficulty.Hard:
                    timeLeft = 9f * 60f;
                    break;
                default:
                    break;
            }

            SurvivalScoring survivalScoring = scoring as SurvivalScoring;
            if (survivalScoring != null)
            {
                survivalScoring.TimeRemaining = timeLeft;
            }
        }

        protected override void SetupScoring()
        {
            scoring = new SurvivalScoring(gameConfig.gameDifficulty, timeLeft);
        }

        protected override bool CheckForWin()
        {
            if (timeLeft <= 0)
            {
                return true;
            }

            return false;
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                timeLeft -= elapsedTime;
            }

            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        /*public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                timeLeft -= elapsedTime;
                if (timeLeft <= 0)
                {
                    WinGame();
                }
            }
        }*/
    }
}