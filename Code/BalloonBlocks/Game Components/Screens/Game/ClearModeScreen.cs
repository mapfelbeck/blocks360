﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Physics;
#endregion

namespace BalloonBlocks
{
    public class ClearModeScreen : BlockGameScreen
    {
        int blocksToClear;
        int linesToCreate;
        Vector2 boundsLowerLeft;

        public ClearModeScreen(GameConfig config, GameGround groundBodies,
            bool drawGround)
            : base(config, groundBodies, drawGround)
        {
            switch (config.gameDifficulty)
            {
                case GameDifficulty.Easy:
                    linesToCreate = 1;
                    break;
                case GameDifficulty.Medium:
                    linesToCreate = 1;
                    break;
                case GameDifficulty.Hard:
                    linesToCreate = 2;
                    break;
                default:
                    break;
            }

            blocksToClear = 0;
            boundsLowerLeft = new Vector2(-9f, -10f);
        }

        protected override void SetupScoring()
        {
            scoring = new ClearScoring(gameConfig.gameDifficulty, blocksToClear);
        }

        public override void LoadContent()
        {
            base.LoadContent();
            
            PreLoadBlocks();
        }

        /// <summary>
        /// Load the challenge blocks the player must clear in order 
        /// to win the round
        /// </summary>
        private void PreLoadBlocks()
        {
            GamePiece gamePiece;
            Vector2 location = boundsLowerLeft;
            Vector2 thisLocaton;
            int currColor = RandomMath.Random.Next(GlobalConfig.colors.Length);
            int maxColors = GlobalConfig.colors.Length;
            float blockSize = GlobalConfig.EnumCoefficient(gameConfig.blockSize);

            int linesCreated = 0;
            ColorGenerator colorGen = new IterativeColorGenerator();
            while(linesCreated < linesToCreate)
            {
                thisLocaton = location;
                thisLocaton.X += ((float)RandomMath.Random.NextDouble() * (blockSize * .25f)) - (blockSize * .125f);
                thisLocaton.Y += ((float)RandomMath.Random.NextDouble() * (blockSize * .25f)) - (blockSize * .125f);

                gamePiece = GamePieceFactory.CreateClearPiece(thisLocaton, tiledGamepieceTexture, gameConfig, colorGen);

                gamePieces.Add(gamePiece);
                foreach (GameBlock theBlock in gamePiece.Blocks)
                {
                    theBlock.DeleteCallback += ChallengeBlockCleared;
                    theBlock.GroupNumber = groupCounter;
                    gameWorld.addBody(theBlock);
                    blocksToClear++;
                }

                location.X += blockSize * 1.5f;
                if (location.X >= -boundsLowerLeft.X)
                {
                    location.X = boundsLowerLeft.X;
                    location.Y += blockSize * 1.5f;
                    linesCreated++;
                }

                groupCounter++;
                currColor = (currColor+1)%maxColors;
            }

            ClearScoring score = scoring as ClearScoring;
            if (score != null)
            {
                score.BlockCount = blocksToClear;
            }
        }

        public override void RegisterEvents()
        {
            base.RegisterEvents();
            EventManager.Register(ChallengeBlockClearedCallback, "ChallengeBlockCleared");
        }

        public override void UnRegisterEvents()
        {
            base.UnRegisterEvents();
            EventManager.UnRegister(ChallengeBlockClearedCallback, "ChallengeBlockCleared");
        }

        private void ChallengeBlockCleared(Body body)
        {
            EventManager.Trigger(this, "ChallengeBlockCleared");
        }

        protected override bool CheckForWin()
        {
            if (blocksToClear <= 0)
            {
                return true;
            }

            return false;
        }

        private void ChallengeBlockClearedCallback(object sender, string theEvent, params object[] args)
        {
            --blocksToClear;

            ClearScoring clearScore = scoring as ClearScoring;
            if (clearScore != null)
            {
                clearScore.BlockCount = blocksToClear;
            }
        }
    }
}