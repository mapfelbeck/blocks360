﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Threading;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class PerfTestScreen : BlockGameScreen
    {
#if XBOX360
        int numPieces = 13 * 4;
#else
        int numPieces = 13 * 6;
#endif
        float runTime;
        float runTimeMax = float.PositiveInfinity;

        public PerfTestScreen(GameConfig config, GameGround groundBodies, bool drawGround)
            : base(config, groundBodies, drawGround) { }

        protected override bool CheckForPieceSpawn(float elapsedTime)
        {
            bool spawnThisFrame = false;
            if (gamePieces.Count == 0)
            {
                spawnThisFrame = true;
            }
            return spawnThisFrame;
        }

        protected override void SpawnPiece()
        {
            GamePiece newBody;
            Vector2 spawnPosition = new Vector2(-19.5f, -10f);

            ColorGenerator colorGen = new RandomColorGenerator();

            for (int i = 0; i < numPieces; i++)
            {
                newBody = GamePieceFactory.CreateGamePiece(spawnPosition, tiledGamepieceTexture, gameConfig, colorGen);

                gamePieces.Add(newBody);
                foreach (GameBlock theBlock in newBody.Blocks)
                {
                    theBlock.GroupNumber = groupCounter;
                    gameWorld.addBody(theBlock);
                }
                spawnPosition.X += 3f;
                groupCounter++;

                if (spawnPosition.X >= 19.5f)
                {
                    spawnPosition.X = -19.5f;
                    spawnPosition.Y += 3f;
                }
            }
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            runTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (runTime >= runTimeMax)
            {
                ScreenManager.Game.Exit();
            }
        }

        protected override bool CheckForFail()
        {
            return false;
        }
    }
}