﻿using System;

namespace BalloonBlocks
{
    public static class CustomGameConfig
    {
        //game mode
        public static GameType gameType;

        public static GameSpeed gameSpeed;

        //block type
        public static BlockType blockType;
        public static BlockSize blockSize;

        //game piece shape
        public static ShapeType pieceShape;

        public static void LoadSettings(GameSettings settings)
        {
            CustomGameConfig.gameType = settings.GameMode;
            CustomGameConfig.gameSpeed = settings.Speed;
            CustomGameConfig.pieceShape = settings.PieceType;
            CustomGameConfig.blockSize = settings.BlockScale;
            CustomGameConfig.blockType = settings.BlockType;
        }
    }
}
