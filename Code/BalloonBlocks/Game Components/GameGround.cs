﻿#region Using Statements
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
#endregion

namespace BalloonBlocks
{
    public class GameGround
    {
        List<StaticBodyConfig> staticbodyConfigs;
        public List<StaticBodyConfig> StaticbodyConfigs
        {
            get { return staticbodyConfigs; }
        }

        NineSliceElement background;
        List<Physics.Body> staticBodies;
        public List<Physics.Body> StaticBodies
        {
            get { return staticBodies; }
        }

        float width;
        float height;
        float border;
        Vector2 position;

        float failLeft;
        public float FailLeft
        {
            get { return failLeft; }
        }

        float failRight;
        public float FailRight
        {
            get { return failRight; }
        }

        float failTop;
        public float FailTop
        {
            get { return failTop; }
        }

        float failBottom;
        public float FailBottom
        {
            get { return failBottom; }
        }

        float failFillHeight;
        public float FailFillHeight
        {
            get { return failFillHeight; }
        }
        
        BasicEffect groundEffect;

        public GameGround(float border, float width, float height, Vector2 position)
        {
            this.width = width;
            this.height = height;
            this.border = border;
            this.position = position;

            staticbodyConfigs = new List<StaticBodyConfig>();
            staticbodyConfigs.Add(new StaticBodyConfig(border, border,
                new Vector2(-(border + width) / 2, (border + height) / 2) + position));
            staticbodyConfigs.Add(new StaticBodyConfig(width, border,
                new Vector2(0, (border + height) / 2) + position));
            staticbodyConfigs.Add(new StaticBodyConfig(border, border,
                new Vector2((border + width) / 2, (border + height) / 2) + position));
            
            staticbodyConfigs.Add(new StaticBodyConfig(border, height,
                new Vector2(-(border + width) / 2, 0) + position));
            staticbodyConfigs.Add(new StaticBodyConfig(width, height,
                new Vector2(0, 0) + position));
            staticbodyConfigs.Add(new StaticBodyConfig(border, height,
                new Vector2((border + width) / 2, 0) + position));

            staticbodyConfigs.Add(new StaticBodyConfig(border, border,
                new Vector2(-(border + width) / 2, -(border + height) / 2) + position));
            staticbodyConfigs.Add(new StaticBodyConfig(width, border,
                new Vector2(0, -(border + height) / 2) + position));
            staticbodyConfigs.Add(new StaticBodyConfig(border, border,
                new Vector2((border + width) / 2, -(border + height) / 2) + position));

            staticBodies = new List<Physics.Body>();

            failTop = float.NegativeInfinity;
            failBottom = float.PositiveInfinity;
            failLeft = float.PositiveInfinity;
            failRight = float.NegativeInfinity;
        }

        public void LoadContent(ContentManager content)
        {
            Texture2D rawTexture = content.Load<Texture2D>("Game UI/gameArena");

            float[] hProportions = { border, width, border };
            float[] vProportions = { border, height, border };
            Vector2 upperLeft = upperLeftVert(staticBodies);
            Vector2 lowerRight = lowerRightVert(staticBodies);
            background = new NineSliceElement(rawTexture, hProportions, vProportions, 1, 1, upperLeft, lowerRight);
            background.Color = GameConstants.UIElementColor;
            background.Alpha = GameConstants.UIBackgroundElementTransparency;

            groundEffect = new BasicEffect(GraphicsConfig.GraphicsDevice, null);
            groundEffect.VertexColorEnabled = true;
            groundEffect.LightingEnabled = false;
            groundEffect.TextureEnabled = GlobalConfig.DrawMethod == DrawStyle.Textured;
            groundEffect.Texture = rawTexture;
            groundEffect.View = GraphicsConfig.View;
            groundEffect.Projection = GraphicsConfig.Projection;
            groundEffect.World = GraphicsConfig.World;

            foreach (VertexPositionColorTexture vert in background.Vertexes)
            {
                failTop = MathHelper.Max(failTop, vert.Position.Y);
                failBottom = MathHelper.Min(failBottom, vert.Position.Y);
                failLeft = MathHelper.Min(failLeft, vert.Position.X);
                failRight = MathHelper.Max(failRight, vert.Position.X);
            }
            failFillHeight = MathHelper.Lerp(failBottom, failTop, .8f);
        }

        public void Draw(DrawBatch batch)
        {
            batch.Begin(PrimitiveType.TriangleList, groundEffect);
            batch.Draw(background.Vertexes);
            batch.End();
        }

        public void CreatAndAddStaticBodiesToWorld(Physics.World gameWorld, int groupCounter)
        {
            Physics.ClosedShape groundShape;
            for (int i = 0; i < staticbodyConfigs.Count; i++)
            {
                groundShape = new Physics.ClosedShape(staticbodyConfigs[i].Vertexes);

                Physics.Body physicsBody = new Physics.Body(groundShape, float.PositiveInfinity, staticbodyConfigs[i].Location + position, 0f, Vector2.One, false);
                physicsBody.GroupNumber = groupCounter;
                staticBodies.Add(physicsBody);
                if (i == 1 || i == 4)
                {
                    continue;
                }
                gameWorld.addBody(physicsBody);
            }
        }

        private Vector2 upperLeftVert(List<Physics.Body> bodies)
        {
            Vector2 upperLeft = new Vector2(float.PositiveInfinity, float.NegativeInfinity);
            for (int i = 0; i < bodies.Count; i++)
            {
                for (int j = 0; j < bodies[i].PointMassCount; j++)
                {
                    upperLeft.X = Math.Min(bodies[i].getPointMass(j).Position.X, upperLeft.X);
                    upperLeft.Y = Math.Max(bodies[i].getPointMass(j).Position.Y, upperLeft.Y);
                }
            }
            return upperLeft;
        }

        private Vector2 lowerRightVert(List<Physics.Body> bodies)
        {
            Vector2 lowerRight = new Vector2(float.NegativeInfinity, float.PositiveInfinity);
            for (int i = 0; i < bodies.Count; i++)
            {
                for (int j = 0; j < bodies[i].PointMassCount; j++)
                {
                    lowerRight.X = Math.Max(bodies[i].getPointMass(j).Position.X, lowerRight.X);
                    lowerRight.Y = Math.Min(bodies[i].getPointMass(j).Position.Y, lowerRight.Y);
                }
            }
            return lowerRight;
        }

    }
}