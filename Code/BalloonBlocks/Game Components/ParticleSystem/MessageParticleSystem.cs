﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace BalloonBlocks
{
    public class Message
    {
        public string text;
        public Vector2 location;
        public Color color;
    }

    public class MessageParticleSystem : FontParticleSystem
    {
        public MessageParticleSystem(int howManyEffects)
            : base(howManyEffects)
        {
        }

        private void creatParticlesCallback(object sender, string theEvent, params object[] args)
        {
            Message message = args[0] as Message;
            if (message != null)
            {
                AddParticles(message.location, message.color, message.text, 1);
            }
        }
        public void AddParticles(Vector2 where, Color particleColor, string particleString, int amount)
        {
            // create that many particles, if you can.
            for (int i = 0; i < amount && freeParticles.Count > 0; i++)
            {
                // grab a particle from the freeParticles queue, and Initialize it.
                Particle p = freeParticles.Dequeue();
                InitializeParticle(p, where, particleColor, particleString);
            }
        }

        /// <summary>
        /// Set up the constants that will give this particle system its behavior and
        /// properties.
        /// </summary>
        protected override void InitializeConstants()
        {
            base.InitializeConstants();
            filename = "MessageParticleFont";

            // high initial speed with lots of variance.  make the values closer
            // together to have more consistently circular explosions.
            minInitialSpeed = 50f;
            maxInitialSpeed = 50f;

            minAcceleration = 10;
            maxAcceleration = 10;

            // explosions should be relatively short lived
            minLifetime = 1.5f;
            maxLifetime = 1.5f;

            minScale = 1.5f;
            maxScale = 1.5f;

            minNumParticles = 1;
            maxNumParticles = 3;

            //minRotationSpeed = -MathHelper.PiOver4;
            //maxRotationSpeed = MathHelper.PiOver4;
            minRotationSpeed = -0;
            maxRotationSpeed = 0;

            // additive blending is very good at creating fiery effects.
            //spriteBlendMode = SpriteBlendMode.Additive;
            spriteBlendMode = SpriteBlendMode.AlphaBlend;

            DrawOrder = AdditiveDrawOrder;
        }

        protected void InitializeParticle(Particle p, Vector2 where, Color particleColor, string particleString)
        {
            InitializeParticle(p, where, particleColor);
            p.ParticleString = particleString;
            p.Velocity = -Vector2.UnitY * 100f;
            p.Rotation = 0f;
        }

        protected override void RegisterEvents()
        {
            base.RegisterEvents();
            EventManager.Register(creatParticlesCallback, "Message");
        }

        protected override void UnRegisterEvents()
        {
            base.RegisterEvents();
            EventManager.UnRegister(creatParticlesCallback, "Message");
        }

        public override void Dispose()
        {
            base.Dispose();
            UnRegisterEvents();
        }
    }
}