﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;
#endregion

namespace BalloonBlocks
{
    public class ComboParticleSystem : FontParticleSystem
    {
        public ComboParticleSystem(int howManyEffects)
            : base(howManyEffects)
        {
        }

        private void creatParticlesCallback(object sender, string theEvent, params object[] args)
        {
            Combo combo = args[0] as Combo;
            if (combo != null)
            {
                Vector2 screenCoordinate = GraphicsConfig.WorldToScreenCoordinates(
                    VectorTools.vec3FromVec2(combo.Location));
                
                int numParticles =
                RandomMath.Random.Next(combo.Quantity, combo.Quantity*2);

                AddParticles(screenCoordinate, combo.ComboColor, string.Format("X{0}", combo.Quantity), numParticles);
            }
        }

        public void AddParticles(Vector2 where, Color particleColor, string particleString, int amount)
        {
            // create that many particles, if you can.
            for (int i = 0; i < amount && freeParticles.Count > 0; i++)
            {
                // grab a particle from the freeParticles queue, and Initialize it.
                Particle p = freeParticles.Dequeue();
                InitializeParticle(p, where, particleColor, particleString);
            }
        }

        /// <summary>
        /// Set up the constants that will give this particle system its behavior and
        /// properties.
        /// </summary>
        protected override void InitializeConstants()
        {
            base.InitializeConstants();
            filename = "ComboParticleFont";

            // high initial speed with lots of variance.  make the values closer
            // together to have more consistently circular explosions.
            minInitialSpeed = 60;
            maxInitialSpeed = 140;

            // doesn't matter what these values are set to, acceleration is tweaked in
            // the override of InitializeParticle.
            minAcceleration = 0;
            maxAcceleration = 0;

            minLifetime = .5f;
            maxLifetime = 1.0f;

            minScale = 1.0f;
            maxScale = 2.0f;

            minNumParticles = 3;
            maxNumParticles = 10;

            //minRotationSpeed = -MathHelper.PiOver4;
            //maxRotationSpeed = MathHelper.PiOver4;
            minRotationSpeed = -0;
            maxRotationSpeed = 0;

            // additive blending is very good at creating fiery effects.
            //spriteBlendMode = SpriteBlendMode.Additive;
            spriteBlendMode = SpriteBlendMode.AlphaBlend;

            DrawOrder = AdditiveDrawOrder;
        }

        protected void InitializeParticle(Particle p, Vector2 where, Color particleColor, string particleString)
        {
            InitializeParticle(p, where, particleColor);
            p.ParticleString = particleString;
            p.Rotation = 0f;
        }

        protected override void RegisterEvents()
        {
            base.RegisterEvents();
            EventManager.Register(creatParticlesCallback, "Combo");
        }

        protected override void UnRegisterEvents()
        {
            base.UnRegisterEvents();
            EventManager.UnRegister(creatParticlesCallback, "Combo");
        }

        public override void Dispose()
        {
            base.Dispose();
            UnRegisterEvents();
        }
    }
}