﻿#region Using Statements
using Microsoft.Xna.Framework;
using System.Collections.Generic;
#endregion

namespace BalloonBlocks
{
    public class StaticBodyConfig
    {
        Vector2 location;
        public Vector2 Location
        {
            get { return location; }
        }

        List<Vector2> vertexes;
        public List<Vector2> Vertexes
        {
            get { return vertexes; }
        }

        float width;
        public float Width { get { return width; } }

        float height;
        public float Height { get { return height; } }

        public StaticBodyConfig(float theWidth, float theHeight, Vector2 theLocation)
        {
            location = theLocation;
            vertexes = new List<Vector2>();
            width = theWidth;
            height = theHeight;
            vertexes.Add(new Vector2(0f, 0f));
            vertexes.Add(new Vector2(0f, theHeight));
            vertexes.Add(new Vector2(theWidth, theHeight));
            vertexes.Add(new Vector2(theWidth, 0f));
        }
    }
}