﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace BalloonBlocks
{
    class BackgroundColorGenerator : ColorGenerator
    {
        Color color;

        public BackgroundColorGenerator(Color? theColor, byte alpha)
        {
            if (theColor != null)
            {
                color = theColor.Value;
            }
            else
            {
                color = RandomMath.RandomColor();
            }

            color.A = alpha;
        }

        public override Color NextColor()
        {
            return color;
        }
    }
}
