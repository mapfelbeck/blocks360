﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace BalloonBlocks
{
    abstract public class ColorGenerator
    {
        public ColorGenerator() { }

        abstract public Color NextColor();
    }
}
