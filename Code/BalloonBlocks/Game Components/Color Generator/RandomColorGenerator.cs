﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace BalloonBlocks
{
    class RandomColorGenerator : ColorGenerator
    {
        public override Color NextColor()
        {
            return RandomMath.RandomColor();
        }
    }
}
