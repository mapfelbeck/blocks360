﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace BalloonBlocks
{
    class IterativeColorGenerator : ColorGenerator
    {
        private int currColor;

        public IterativeColorGenerator(int initialIndex)
        {
            currColor = initialIndex % GlobalConfig.colors.Length;
        }

        public IterativeColorGenerator()
            : this
                (RandomMath.Random.Next(GlobalConfig.colors.Length))
        {
        }

        public override Color NextColor()
        {
            currColor = (currColor + 1) % GlobalConfig.colors.Length;
            return GlobalConfig.colors[currColor];
        }
    }
}
