using Microsoft.Xna.Framework.Graphics;

namespace BalloonBlocks
{
    /// <summary>
    /// Constants used throughout the code
    /// </summary>
    public static class GameConstants
    {
        public const string settingsFile = "Balloon Blocks Settings";
        public const string highScoreFile = "Balloon Blocks High Scores";

        public const float MassPerPoint = 1f;

        public const float GravityConstant = -9.8f;

        //control forces
        public const float PushForceXCoefficient = 1.25f;
        public const float PushForceYCoefficient = .75f;
        public const float TorqueForceCoefficient = 1.25f;

        //inflate pressure for game blocks
        public const float GasPressure = 250f;

        //spring constants
        public const float ShapeSpringK = 450f;
        public const float ShapeSpringDamp = 15f;
        public const float EdgeSpringK = 450f;
        public const float EdgeSpringDamp = 15f;
        public const float InternalSpringK = 450f;
        public const float InternalSpringDamp = 15f;
        public const float AttachSpringK = 450f;
        public const float AttachSpringDamp = 15f;

        public const int MinVolume = 0;
        public const int MaxVolume = 100;

        //speed increase constants for speeding up gameplay as it progresses
        public const int CombosToMaxSpeed = 20;
        public const float MaxGravityIncrease = 1.5f;
        public const float MaxFrictionDecrease = .4f;

        //max number of the same color allowed in a game piece
        public static int MaxColorPerPiece = 1;

        //how long a block inflates before popping
        public const float BlockCollideTime = 1f;
        //how long the block pulses from red (out of game area) or 
        //white(in game area) before the next plaock spawns
        public const float BlockSpawnWarningTime = 2f;
        //must wait at least this long between blck spawns
        public const float ForcedSpawnCoolOff = 2f;

        public const float IntroDisplayTime = 6f;
        public const bool IntroSkippable = true;
        
#if XBOX360
        public const bool IntroTimesOut = false;
#else
        public const bool IntroTimesOut = true;
#endif
        public const float ExitDisplayTime = 6f;
        public const bool ExitSkippable = true;
        public const bool ExitTimesOut = true;

        public static Color UIElementColor = Color.White;
        public const byte UIElementTransparency = 128;
        public const byte UIBackgroundElementTransparency = 192;

        //Debug options
#if DEBUG
        public const bool skipIntroScreen = true;
        public const bool skipExitScreen = true;
        public const bool showFPSCounter = true;
        public const bool trackGC = false;
        public const bool showSafeArea = false;
        public const bool showPerfData = false;
#else
        public const bool skipIntroScreen = false;
        public const bool skipExitScreen = false;
        public const bool showFPSCounter = false;
        public const bool trackGC = false;
        public const bool showSafeArea = false;
        public const bool showPerfData = false;
#endif

        public const bool fakeTrialMode = false;

#if WINDOWS
        public const bool multiSampling = false;
#else
        public const bool multiSampling = true;
#endif
    }
}