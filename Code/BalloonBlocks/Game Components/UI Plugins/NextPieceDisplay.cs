﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Utilities;

#endregion

namespace BalloonBlocks
{
    public class NextPieceDisplay:DrawableGamePlugin
    {
        GamePiece nextPiece;
        public GamePiece NextPiece
        {
            get { return nextPiece; }
            set
            {
                nextPiece = value;
                SetupVerts(nextPiece);
            }
        }

        Texture2D pieceTexture;
        NineSliceElement background;

        BasicEffect effect;

        VertexPositionColorTexture[] nextPieceVerts;
        
        Vector2 screenUpperLeft;
        public Vector2 Location
        {
            get { return screenUpperLeft; }
        }
        Vector2 screenLowerRight;
        public Vector2 Size
        {
            get { return screenLowerRight - screenUpperLeft; }
        }
        
        Vector2 worldUpperLeft;
        Vector2 worldLowerRight;
        Vector2 worldCenter;
        float worldLength;

        DrawBatch drawBatch;

        public NextPieceDisplay(ShapeType shape, Vector2 loc, float sz)
        {
            screenUpperLeft = loc;
            screenLowerRight = loc + new Vector2(sz);
            worldUpperLeft = GraphicsConfig.ScreenToWorldCoordinates(screenUpperLeft);
            worldLowerRight = GraphicsConfig.ScreenToWorldCoordinates(screenLowerRight);
            worldCenter = GraphicsConfig.ScreenToWorldCoordinates(screenUpperLeft + new Vector2(sz / 2));

            worldLength = worldLowerRight.X - worldUpperLeft.X;

            nextPieceVerts = new VertexPositionColorTexture[BlocksPerShape(shape) * 6];
        }

        public override void LoadContent(ContentManager content)
        {
            drawBatch = new DrawBatch(GraphicsConfig.GraphicsDevice);
            pieceTexture = content.Load<Texture2D>("block textures/tiled greyscale");
            Texture2D rawBackground = content.Load<Texture2D>("Game UI/GUIElementBackground");

            float[] hProportions = { 1, 4, 1 };
            float[] vProportions = { 1, 4, 1 };
            int screenSize = (int)screenLowerRight.X - (int)screenUpperLeft.X;
            int screenborder = screenSize / 10;
            Rectangle drawRect = new Rectangle((int)screenUpperLeft.X - (int)screenborder, (int)screenUpperLeft.Y - (int)screenborder,
                screenSize + screenborder * 2, screenSize + screenborder * 2);
            background = new NineSliceElement(rawBackground, hProportions, vProportions, drawRect);
            background.HScale = 1f;
            background.VScale = 1f;
            /*
            Vector2 backgroundTemp = worldLowerRight - worldUpperLeft;
            backgroundTemp *= .1f;
            background = new NineSliceElement(rawBackground, hProportions, vProportions, 1, 1, worldUpperLeft - backgroundTemp, worldLowerRight + backgroundTemp);
            */
            background.Color = GameConstants.UIElementColor;
            background.Alpha = GameConstants.UIElementTransparency;

            // visual effect
            effect = new BasicEffect(GraphicsConfig.GraphicsDevice, null);
            effect.VertexColorEnabled = true;
            effect.LightingEnabled = false;
            effect.TextureEnabled = GlobalConfig.DrawMethod == DrawStyle.Textured;
            effect.Texture = pieceTexture;
            effect.View = GraphicsConfig.View;
            effect.Projection = GraphicsConfig.Projection;
            effect.World = GraphicsConfig.World;
        }

        /// <summary>
        /// Move a vertex from relative to one location to another 
        /// and apply a scaling factor
        /// </summary>
        /// <param name="theVertPosition">vertex location in world space</param>
        /// <param name="oldPosition">old relative position</param>
        /// <param name="newPosition">new relative position</param>
        /// <param name="scale">scaling factor</param>
        /// <returns></returns>
        Vector2 moveAndScaleVert(Vector2 theVertPosition, Vector2 oldPosition, Vector2 newPosition, float scale)
        {
            Vector2 relativePosition = theVertPosition - oldPosition;
            return newPosition + relativePosition * scale;
        }

        private void SetupVerts(GamePiece nextPiece)
        {
            Vector2 pieceUpperLeft = upperLeftVert(nextPiece);
            Vector2 pieceLowerRight = lowerRightVert(nextPiece);

            float pieceLength = Math.Max(pieceLowerRight.X - pieceUpperLeft.X,
                                        pieceUpperLeft.Y - pieceLowerRight.Y);

            float scale = Math.Min(worldLength / pieceLength, 1f);
            Vector2 pieceLocation = nextPiece.GamePieceCenter();
            Vector2 adjust = worldCenter - pieceLocation;

            int vertIndex = 0;
            for (int i = 0; i < nextPiece.Blocks.Count; i++)
            {
                nextPieceVerts[vertIndex].Position = VectorTools.vec3FromVec2(
                    moveAndScaleVert(nextPiece.Blocks[i].getPointMass(0).Position, pieceLocation, worldCenter, scale));
                nextPieceVerts[vertIndex + 1].Position = VectorTools.vec3FromVec2(
                    moveAndScaleVert(nextPiece.Blocks[i].getPointMass(1).Position, pieceLocation, worldCenter, scale));
                nextPieceVerts[vertIndex + 2].Position = VectorTools.vec3FromVec2(
                    moveAndScaleVert(nextPiece.Blocks[i].getPointMass(2).Position, pieceLocation, worldCenter, scale));
                nextPieceVerts[vertIndex + 3].Position = VectorTools.vec3FromVec2(
                    moveAndScaleVert(nextPiece.Blocks[i].getPointMass(2).Position, pieceLocation, worldCenter, scale));
                nextPieceVerts[vertIndex + 4].Position = VectorTools.vec3FromVec2(
                    moveAndScaleVert(nextPiece.Blocks[i].getPointMass(3).Position, pieceLocation, worldCenter, scale));
                nextPieceVerts[vertIndex + 5].Position = VectorTools.vec3FromVec2(
                    moveAndScaleVert(nextPiece.Blocks[i].getPointMass(0).Position, pieceLocation, worldCenter, scale));

                nextPieceVerts[vertIndex + 0].Color = nextPiece.Blocks[i].BlockColor;
                nextPieceVerts[vertIndex + 1].Color = nextPiece.Blocks[i].BlockColor;
                nextPieceVerts[vertIndex + 2].Color = nextPiece.Blocks[i].BlockColor;
                nextPieceVerts[vertIndex + 3].Color = nextPiece.Blocks[i].BlockColor;
                nextPieceVerts[vertIndex + 4].Color = nextPiece.Blocks[i].BlockColor;
                nextPieceVerts[vertIndex + 5].Color = nextPiece.Blocks[i].BlockColor;

                nextPieceVerts[vertIndex + 0].TextureCoordinate = nextPiece.Blocks[i].DrawTile.LowerLeft;
                nextPieceVerts[vertIndex + 1].TextureCoordinate = nextPiece.Blocks[i].DrawTile.UpperLeft;
                nextPieceVerts[vertIndex + 2].TextureCoordinate = nextPiece.Blocks[i].DrawTile.UpperRight;
                nextPieceVerts[vertIndex + 3].TextureCoordinate = nextPiece.Blocks[i].DrawTile.UpperRight;
                nextPieceVerts[vertIndex + 4].TextureCoordinate = nextPiece.Blocks[i].DrawTile.LowerRight;
                nextPieceVerts[vertIndex + 5].TextureCoordinate = nextPiece.Blocks[i].DrawTile.LowerLeft;
                
                vertIndex += 6;
            }
        }

        private Vector2 upperLeftVert(GamePiece nextPiece)
        {
            Vector2 upperLeft = new Vector2(float.PositiveInfinity, float.NegativeInfinity);
            for (int i = 0; i < nextPiece.Blocks.Count; i++)
            {
                for (int j = 0; j < nextPiece.Blocks[i].PointMassCount; j++)
                {
                    upperLeft.X = Math.Min(nextPiece.Blocks[i].getPointMass(j).Position.X, upperLeft.X);
                    upperLeft.Y = Math.Max(nextPiece.Blocks[i].getPointMass(j).Position.Y, upperLeft.Y);
                }
            }
            return upperLeft;
        }

        private Vector2 lowerRightVert(GamePiece nextPiece)
        {
            Vector2 lowerRight = new Vector2(float.NegativeInfinity, float.PositiveInfinity);
            for (int i = 0; i < nextPiece.Blocks.Count; i++)
            {
                for (int j = 0; j < nextPiece.Blocks[i].PointMassCount; j++)
                {
                    lowerRight.X = Math.Max(nextPiece.Blocks[i].getPointMass(j).Position.X, lowerRight.X);
                    lowerRight.Y = Math.Min(nextPiece.Blocks[i].getPointMass(j).Position.Y, lowerRight.Y);
                }
            }
            return lowerRight;
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
        }

        public override void Draw()
        {
            effect.Texture = background.BaseTexture;
            drawBatch.Begin(PrimitiveType.TriangleList, effect);
            drawBatch.Draw(background.Vertexes);
            drawBatch.End();

            effect.Texture = pieceTexture;
            drawBatch.Begin(PrimitiveType.TriangleList, effect);
            drawBatch.Draw(nextPieceVerts);
            drawBatch.End();
        }

        private int BlocksPerShape(ShapeType shape)
        {
            int result = 0;
            switch (shape)
            {
                case ShapeType.Single:
                    result = 1;
                    break;
                case ShapeType.Double:
                    result = 2;
                    break;
                case ShapeType.Triomino:
                    result = 3;
                    break;
                case ShapeType.Tetromino:
                    result = 4;
                    break;
                case ShapeType.Pentomino:
                    result = 5;
                    break;
                case ShapeType.Square:
                    result = 4;
                    break;
                default:
                    result = -1;
                    break;
            }
            return result;
        }
    }
}