﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using Utilities;
#endregion

namespace BalloonBlocks
{
    public class UnfreezeDisplay : DrawableGamePlugin
    {
        SpriteBatch spriteBatch;
        Texture2D texture;
        Vector2 rechargeLocation;
        Vector2 rechargeSize;
        byte barAlpha;
        byte buttonAlpha;

        float rechargeTimer;
        float timeToRecharge;

        bool firstChargeDone;
        float timeToFirstCharge;

        bool unFreezeReady;
        public bool UnFreezeReady
        {
            get { return unFreezeReady; }
        }

        Color unfilledColor;
        Color filledColor;

        Rectangle buttonRectangle;
        Texture2D buttonColored;
        Texture2D buttonGray;
        Color buttonColor;

        SpriteFont font;
        string text;
        Vector2 textSize;
        Vector2 textLocation;

        public UnfreezeDisplay(Vector2 location, Vector2 size, float rechargeTime)
        {
            this.rechargeLocation = new Vector2(location.X - (size.X / 2), location.Y - (size.Y / 2));
            this.rechargeSize = size;
            rechargeTimer = 0f;
            barAlpha = 96;
            buttonAlpha = 120;
            //5 minutes to recharge normally
            timeToRecharge = rechargeTime;

            firstChargeDone = false;
            //10 second to charge fully the first time
            timeToFirstCharge = 20f;
        }

        public override void LoadContent(ContentManager content)
        {
            spriteBatch = new SpriteBatch(GraphicsConfig.GraphicsDevice);
            texture = content.Load<Texture2D>("Game UI/ChargeBarH");

            font = content.Load<SpriteFont>("Fonts/ScoringFont");

            unfilledColor = Color.White;
            unfilledColor.A = barAlpha;
            filledColor = Color.Red;
            filledColor.A = barAlpha;

            buttonColor = Color.White;
            buttonColor.A = buttonAlpha;
            buttonColored = content.Load<Texture2D>("Button Textures/ButtonA");
            buttonGray = content.Load<Texture2D>("Button Textures/ButtonAGray");

            int buttonSize = GraphicsConfig.Fullscreen.Height / 20;
            buttonRectangle = new Rectangle((int)(rechargeLocation.X + rechargeSize.X) - buttonSize,
                                            (int)rechargeLocation.Y - buttonSize,
                                            buttonSize, buttonSize);


            SetText();
        }

        private void SetText()
        {
            if (rechargeTimer < timeToRecharge)
            {
                text = "Unfreeze Recharging...";
            }
            else
            {
                text = "Unfreeze Ready!";
            }

            textSize = font.MeasureString(text);
            textLocation = new Vector2(
                (rechargeLocation.X + rechargeSize.X / 2) - textSize.X / 2,
                rechargeLocation.Y - textSize.Y);
        }

        private void unfreezeCallback(object sender, string theEvent, params object[] args)
        {
            bool clearCharge = (bool)args[0];
            if (clearCharge)
            {
                unFreezeReady = false;
                rechargeTimer = 0f;
                SetText();
            }
        }

        private void comboScoreCallback(object sender, string theEvent, params object[] args)
        {
            Combo theCombo = args[0] as Combo;
            rechargeTimer += theCombo.Quantity * (timeToRecharge * .01f);
        }

        private void streakScoreCallback(object sender, string theEvent, params object[] args)
        {
            int theStreak = (int)args[0];
            if (theStreak < 3)
            {
                rechargeTimer += theStreak * (timeToRecharge * .05f);
            }
            else
            {
                if (rechargeTimer >= timeToRecharge)
                {
                    EventManager.Trigger(this, "Unfreeze", false);
                }
                else
                {
                    rechargeTimer = timeToRecharge;
                }
            }
        }

        protected override void RegisterEvents()
        {
            base.RegisterEvents();
            EventManager.Register(comboScoreCallback, "Combo");
            EventManager.Register(streakScoreCallback, "Streak");
            EventManager.Register(unfreezeCallback, "Unfreeze");
        }

        protected override void UnRegisterEvents()
        {
            base.UnRegisterEvents();
            EventManager.UnRegister(comboScoreCallback, "Combo");
            EventManager.UnRegister(streakScoreCallback, "Streak");
            EventManager.UnRegister(unfreezeCallback, "Unfreeze");
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                if (firstChargeDone)
                {
                    rechargeTimer += elapsed;
                }
                else
                {
                    rechargeTimer += elapsed * (timeToRecharge / timeToFirstCharge);
                }
                rechargeTimer = Math.Min(rechargeTimer, timeToRecharge);

                if (rechargeTimer >= timeToRecharge && !unFreezeReady)
                {
                    unFreezeReady = true;
                    firstChargeDone = true;
                    EventManager.Trigger(this, "EnableUnfreeze");
                    SetText();
                }
            }
        }

        public override void Draw()
        {
            Rectangle textureRect;
            Rectangle drawRect;
            float normalizedUnfreee = rechargeTimer / timeToRecharge;
            int textureFillAmount = (int)(normalizedUnfreee * texture.Width);
            int drawFillAmount = (int)(normalizedUnfreee * rechargeSize.X);

            spriteBatch.Begin();
            DrawShadowedString(font, text, textLocation, Color.White);
            if (rechargeTimer >= timeToRecharge)
            {
                drawRect = new Rectangle((int)rechargeLocation.X, (int)rechargeLocation.Y, (int)rechargeSize.X, (int)rechargeSize.Y);
                spriteBatch.Draw(texture, drawRect, null, filledColor);
            }
            else
            {
                drawRect = new Rectangle((int)rechargeLocation.X, (int)rechargeLocation.Y, drawFillAmount, (int)rechargeSize.Y);
                textureRect = new Rectangle(0, 0, textureFillAmount, texture.Height);
                spriteBatch.Draw(texture, drawRect, textureRect, filledColor);

                drawRect = new Rectangle((int)rechargeLocation.X + drawFillAmount, (int)rechargeLocation.Y, (int)rechargeSize.X - drawFillAmount, (int)rechargeSize.Y);
                textureRect = new Rectangle(textureFillAmount, 0, texture.Width - textureFillAmount, texture.Height);
                spriteBatch.Draw(texture, drawRect, textureRect, unfilledColor);
            }
            if (unFreezeReady)
            {
                spriteBatch.Draw(buttonColored, buttonRectangle, buttonColor);
            }
            else
            {
                spriteBatch.Draw(buttonGray, buttonRectangle, buttonColor);
            }
            spriteBatch.End();
        }

        protected void DrawShadowedString(SpriteFont font, string text, Vector2 position, Color color)
        {
            spriteBatch.DrawString(font, text, new Vector2(position.X + 1f, position.Y + 1f), Color.Black);
            spriteBatch.DrawString(font, text, position, color);
        }
    }
}