﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace BalloonBlocks
{
    public class DropIndicator : DrawableGamePlugin
    {
        SpriteBatch spriteBatch;

        byte barAlpha;
        byte buttonAlpha;

        Texture2D dropBarTexture;
        int dropBarTextureWidth;
        int dropBarTextureHeight;
        Point dropBarLocation;
        int dropBarHeight;
        int dropBarWidth;
        Color dropBarTopColor;
        Color dropBarBottomColor;
        float dropBarFilledAmount;
        public float FilledAmount
        {
            get { return dropBarFilledAmount; }
            set { dropBarFilledAmount = value; }
        }
        float dropBarDepleteRatio;
        public float DepleteRatio
        {
            get { return dropBarDepleteRatio; }
            set { dropBarDepleteRatio = value; }
        }

        Texture2D buttonColored;
        Texture2D buttonGray;
        Rectangle buttonRectangle;
        Color buttonColor;

        int buttonSize;

        bool canForceDrop;
        public bool CanForceDrop
        {
            get { return canForceDrop; }
            set { canForceDrop = value; }
        }

        public DropIndicator()
        {
            dropBarFilledAmount = 0f;
        }

        public override void LoadContent(ContentManager content)
        {
            barAlpha = 96;
            buttonAlpha = 120;

            spriteBatch = new SpriteBatch(GraphicsConfig.GraphicsDevice);

            buttonColor = Color.White;
            buttonColor.A = buttonAlpha;
            buttonColored = content.Load<Texture2D>("Button Textures/ButtonX");
            buttonGray = content.Load<Texture2D>("Button Textures/ButtonXGray");
            buttonSize = GraphicsConfig.Fullscreen.Height / 20;
            dropBarTexture = content.Load<Texture2D>("Game UI/ChargeBarV");
            dropBarTextureWidth = dropBarTexture.Width;
            dropBarTextureHeight = dropBarTexture.Height;

            dropBarTopColor = Color.White;
            dropBarTopColor.A = barAlpha;
            dropBarBottomColor = Color.Red;
            dropBarBottomColor.A = barAlpha;
        }

        public void PlaceRelativeTo(Vector2 location, Vector2 size)
        {
            buttonRectangle = new Rectangle((int)(location.X + (size.X * 1.15f)),
                                            (int)location.Y + (int)(size.Y * 1.1f),
                                            buttonSize, buttonSize);
            dropBarLocation = new Point(buttonRectangle.X, (int)(location.Y - size.Y * .1f));
            dropBarHeight = buttonRectangle.Top - dropBarLocation.Y;
            dropBarWidth = dropBarHeight / 6;
            dropBarLocation.X += buttonRectangle.Width / 2 - dropBarWidth / 2;
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                dropBarFilledAmount -= elapsed * dropBarDepleteRatio;
            }
        }

        public override void Draw()
        {
            spriteBatch.Begin();

            Rectangle dropBarTop = new Rectangle(dropBarLocation.X, dropBarLocation.Y,
                dropBarWidth, (int)(dropBarHeight * (1 - dropBarFilledAmount)));
            Rectangle dropBarTopTexture = new Rectangle(0, 0,
                dropBarTextureWidth, (int)(dropBarTextureHeight * (1 - dropBarFilledAmount)));

            Rectangle dropBarBottom = new Rectangle(dropBarLocation.X, dropBarTop.Bottom,
                dropBarWidth, dropBarHeight - dropBarTop.Height);
            Rectangle dropBarBottomTexture = new Rectangle(0, dropBarTopTexture.Height,
                dropBarTextureWidth, dropBarTextureHeight - dropBarTopTexture.Height);

            spriteBatch.Draw(dropBarTexture, dropBarTop, dropBarTopTexture, dropBarTopColor);
            spriteBatch.Draw(dropBarTexture, dropBarBottom, dropBarBottomTexture, dropBarBottomColor);

            if (canForceDrop)
            {
                spriteBatch.Draw(buttonColored, buttonRectangle, buttonColor);
            }
            else
            {
                spriteBatch.Draw(buttonGray, buttonRectangle, buttonColor);
            }
            spriteBatch.End();
        }
    }
}
