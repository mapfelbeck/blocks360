﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using Utilities;
#endregion

namespace BalloonBlocks
{
    public class ClearScoring : ScoringDisplay
    {
        int blockCount;
        public int BlockCount
        {
            get { return blockCount; }
            set { 
                blockCount = value;
                SetClearString();
            }
        }

        public override int Remaining
        {
            get { return blockCount; }
        }

        string blocksToClearString;

        public ClearScoring(GameDifficulty difficulty, int blocksToClear)
            : base(difficulty)
        {
            linesToDrawDefault = 4;
            blockCount = blocksToClear;
            SetClearString();
        }

        private void SetClearString()
        {
            blocksToClearString = string.Format("Remaining: {0}", blockCount);
        }

        protected override Vector2 WidestString()
        {
            return font.MeasureString("Blocks Remaining: 99");
        }

        public override void Draw()
        {
            if (!draw)
            {
                return;
            }
            drawBatch.Begin(PrimitiveType.TriangleList, effect);
            drawBatch.Draw(background.Vertexes);
            drawBatch.End();

            Vector2 position = scoreLocation;
            float fontHeight = font.MeasureString(highScoreString).Y;
            spriteBatch.Begin();

            DrawShadowedString(font, playTimeString, position, baseColor);
            position.Y += fontHeight;

            DrawShadowedString(font, highScoreString,
                position,
                baseColor);

            position.Y += fontHeight;
            DrawShadowedString(font, scoreString, position, new Color(currColorVector));

            if (currentScoreItem != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, currentScoreItem.ScoreString, position, currentScoreItem.Color);
            }

            for (int i = 0; i < Math.Min(scoreList.Count, 2); i++)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, scoreList[i].ScoreString, position, scoreList[i].Color);
            }

            if (comboString != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, comboString, position, comboStringColor);
            }

            if (streakString != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, streakString, position, baseColor);
            }

            position.Y += fontHeight;
            DrawShadowedString(font, blocksToClearString, position, baseColor);

            spriteBatch.End();
        }

    }
}