﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using Utilities;
using System.Globalization;
#endregion

namespace BalloonBlocks
{
    public class ScoringDisplay:DrawableGamePlugin
    {
        float playtime;
        public virtual float PlayTime
        {
            get { return playtime; }
        }

        TimeSpan playTimeSpan;

        //the player's score for this game
        int score;
        public int Score
        {
            get { return score; }
        }

        public virtual int Remaining
        {
            get { return 0; }
        }

        //the record score for this game type
        int highScore;
        //the score counts up rather than accumulating instantly, this
        //is what we're displaying right now
        int displayedScore;

        int scoreAccumulationRate;

        protected int linesToDrawDefault;

        protected string scoreString;
        protected string highScoreString;
        protected string playTimeString;
        protected string comboString;
        protected Color comboStringColor;

        protected string streakString;
        //current streak
        int streakCounter;
        const float streakTimerMax = GameConstants.BlockCollideTime;
        //time since streak accumulated
        float streakTimer;
        //min streak size that we care about
        int streakMessageAmount;
        //largest streak this game
        int highestStreak;

        int seconds;
        int minutes;

        protected Vector2 scoreLocation;

        protected SpriteFont font;

        protected SpriteBatch spriteBatch;

        protected Dictionary<Color, Combo> combos;

        protected List<ScoreItem> scoreList;
        protected ScoreItem currentScoreItem;

        protected Color baseColor;
        protected Vector3 baseColorVector;
        protected Vector3 targetColorVector;
        protected Vector3 currColorVector;

        bool highScoreMessageShown;
        int highestCombo;

        string[] comboMessages = { "{0}X Combo! Wow!",
                                   "{0}X Combo! Great!",
                                   "Great Job! {0}X Combo!"};

        string[] HighestComboMessages = { "Largest Combo Yet! {0}X!",
                                   "New Combo Record! {0}X!",
                                   "New Record! {0}X Combo",
                                   "Best Combo Yet! {0}X!"};

        protected bool draw;

        protected DrawBatch drawBatch;
        protected NineSliceElement background;
        protected BasicEffect effect;

        protected float fontHeight;

        protected int currentComboTotal;

        public ScoringDisplay(GameDifficulty difficulty)
        {
            currentComboTotal = 0;
            linesToDrawDefault = 3;
            score = 0;
            displayedScore = 0;
            if (HighScoreTable.GetScores(GameType.Normal, difficulty) != null)
            {
                highScore = HighScoreTable.GetScores(GameType.Normal, difficulty)[0].Score;
            }
            else
            {
                highScore = 10000;
            }
            if (difficulty != GameDifficulty.SplashScreen)
            {
                draw = true;
            }
            baseColor = new Color(240, 240, 240);
            baseColorVector = baseColor.ToVector3();
            targetColorVector = baseColor.ToVector3();
            currColorVector = baseColor.ToVector3();
            comboStringColor = baseColor;
            currentScoreItem = null;

            streakCounter = 0;
            streakTimer = 0;
            streakMessageAmount = 2;
            highestStreak = 0;

            SetScoreText();
        }

        public override void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("Fonts/ScoringFont");

            spriteBatch = new SpriteBatch(GraphicsConfig.GraphicsDevice);

            Rectangle safeArea = GraphicsConfig.SafeArea;

            float widestStringWidth = WidestString().X;
            scoreLocation = new Vector2();
            scoreLocation.X = (float)safeArea.Left * .25f + (float)safeArea.Right - widestStringWidth;
            scoreLocation.Y = (float)safeArea.Y;

            drawBatch = new DrawBatch(GraphicsConfig.GraphicsDevice);
            Texture2D rawTexture = content.Load<Texture2D>("Game UI/GUIElementBackground");

            // visual effect
            effect = new BasicEffect(GraphicsConfig.GraphicsDevice, null);
            effect.VertexColorEnabled = true;
            effect.LightingEnabled = false;
            effect.TextureEnabled = GlobalConfig.DrawMethod == DrawStyle.Textured;
            effect.Texture = rawTexture;
            effect.View = GraphicsConfig.View;
            effect.Projection = GraphicsConfig.Projection;
            effect.World = GraphicsConfig.World;
            
            float[] hProportions = { 1, 4, 1 };
            float[] vProportions = { 1, 4, 1 };

            Vector2 backgroundUL = new Vector2(scoreLocation.X - 10, scoreLocation.Y - 10);
            Vector2 backgroundLR = new Vector2(scoreLocation.X + widestStringWidth + 5,
                //scoreLocation.Y + (safeArea.Right - scoreLocation.X - 10));
                scoreLocation.Y + widestStringWidth - 15);
            background = new NineSliceElement(rawTexture, hProportions, vProportions, 1, .25f, GraphicsConfig.ScreenToWorldCoordinates(backgroundUL), GraphicsConfig.ScreenToWorldCoordinates(backgroundLR));
            background.Color = GameConstants.UIElementColor;
            background.Alpha = GameConstants.UIElementTransparency;

            combos = new Dictionary<Color, Combo>();
            foreach (Color color in GlobalConfig.colors)
            {
                combos.Add(color, new Combo(0, 0f, color, Vector2.Zero));
            }

            scoreList = new List<ScoreItem>();

            fontHeight = font.MeasureString("H").Y;
        }

        protected virtual Vector2 WidestString()
        {
            return font.MeasureString("High Score: 10000000");
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
                int linesToDraw = linesToDrawDefault;

                playtime += elapsed;
                streakTimer += elapsed;

                playTimeSpan = TimeSpan.FromSeconds((double)playtime);
                if (playTimeSpan.Seconds != seconds || playTimeSpan.Minutes != minutes)
                {
                    seconds = playTimeSpan.Seconds;
                    minutes = playTimeSpan.Minutes;
                    SetScoreText();
                }

                bool comboQueued = false;
                //incriment the age of all the active combos, if a combo reachs the age
                //limit, queue it as score
                foreach (Combo combo in combos.Values)
                {
                    combo.Lifetime += elapsed;
                    if (combo.Quantity >= 3 && combo.Lifetime >= GameConstants.BlockCollideTime)
                    {
                        comboQueued = true;
                        QueueComboScore(combo.ComboColor, combo.Quantity);
                        EventManager.Trigger(this, "Combo", combo);

                        if (combo.Quantity > highestCombo)
                        {
                            Message message = new Message();
                            message.location = GraphicsConfig.WorldToScreenCoordinates(
                                VectorTools.vec3FromVec2(combo.Location));
                            message.color = combo.ComboColor;
                            message.text = string.Format(RandomHighestComboMessage(), combo.Quantity);
                            EventManager.Trigger(this, "Message", message);
                            highestCombo = combo.Quantity;
                        }
                        else if (combo.Quantity > 4)
                        {
                            Message message = new Message();
                            message.location = GraphicsConfig.WorldToScreenCoordinates(
                                VectorTools.vec3FromVec2(combo.Location));
                            message.color = combo.ComboColor;
                            message.text = string.Format(RandomComboMessage(), combo.Quantity);
                            EventManager.Trigger(this, "Message", message);
                        }

                        streakTimer = 0f;
                        streakCounter++;

                        combo.Lifetime = 0f;
                        combo.Quantity = 0;
                        combo.Location = Vector2.Zero;
                    }
                }

                if (comboQueued)
                {
                    int total = 0;
                    foreach (Combo combo in combos.Values)
                    {
                        total += combo.Quantity;
                    }
                    currentComboTotal = total;
                }
                if (currentComboTotal > 3)
                {
                    float newRumbleRate = ((float)(currentComboTotal - 3) / 10f);
                    newRumbleRate = Math.Min(newRumbleRate, 1f);
                    RumbleComponent.SetRumble(newRumbleRate);
                }
                else
                {
                    RumbleComponent.SetRumble(0f);
                }

                if (score == displayedScore && scoreList.Count > 0)
                {
                    currentScoreItem = scoreList[0];
                    score += currentScoreItem.Score;
                    scoreList.Remove(currentScoreItem);
                    scoreAccumulationRate = Math.Max(currentScoreItem.Score / 60, 1);
                    targetColorVector = currentScoreItem.Color.ToVector3();
                }
                else if (displayedScore != score)
                {
                    displayedScore = Math.Min(displayedScore + scoreAccumulationRate, score);
                    if (score > highScore)
                    {
                        if (!highScoreMessageShown)
                        {
                            Message message = new Message();
                            message.text = "New High Score!";
                            message.color = Color.White;
                            message.location = new Vector2(
                                GraphicsConfig.GraphicsDevice.Viewport.Width / 2,
                                GraphicsConfig.GraphicsDevice.Viewport.Height / 2);

                            EventManager.Trigger(this, "Message", message);
                            highScoreMessageShown = true;
                        }
                        highScore = Math.Max(highScore, displayedScore);
                    }
                    SetScoreText();
                }
                else if (score == displayedScore)
                {
                    targetColorVector = baseColorVector;
                    currentScoreItem = null;
                }

                // did we just trigger a streak large enough that we care about it?
                if (streakTimer > streakTimerMax && streakCounter >= streakMessageAmount)
                {
                    Message streakMessage = new Message();
                    streakMessage.text = string.Format("{0}X Combo Streak!!", streakCounter);
                    streakMessage.location = new Vector2(
                        GraphicsConfig.GraphicsDevice.Viewport.Width / 2,
                        GraphicsConfig.GraphicsDevice.Viewport.Height / 3);
                    streakMessage.color = Color.White;
                    EventManager.Trigger(this, "Message", streakMessage);
                    EventManager.Trigger(this, "Streak", streakCounter);
                    highestStreak = Math.Max(streakCounter, highestStreak);
                    QueueStreakScore(streakCounter);
                    SetScoreText();
                    streakCounter = 0;
                }
                else if (streakTimer > streakTimerMax)
                {
                    streakCounter = 0;
                    streakTimer = 0;
                }

                if (currColorVector != targetColorVector)
                {
                    currColorVector = Vector3.Lerp(currColorVector, targetColorVector, elapsed * 2f);
                }

                if (comboString != null)
                {
                    linesToDraw++;
                }
                if (streakString != null)
                {
                    linesToDraw++;
                }
                if (currentScoreItem != null)
                {
                    linesToDraw++;
                }
                if (scoreList.Count > 0)
                {
                    if (scoreList.Count > 2)
                    {
                        linesToDraw += 2;
                    }
                    else
                    {
                        linesToDraw += scoreList.Count;
                    }
                }
                background.VScale = LinesToVScale(linesToDraw);
            }
        }

        private float LinesToVScale(int lines)
        {
            return .25f + ((lines - 3) * .18f);
        }

        private void QueueComboScore(Color color, int quantity)
        {
            scoreList.Add(new ScoreItem(ComboValue(quantity), color));
        }

        private void QueueStreakScore(int quantity)
        {
            scoreList.Add(new ScoreItem(StreakValue(quantity), Color.White));
        }

        /// <summary>
        /// Returns the value of a combo in points
        /// </summary>
        /// <param name="value">combo size</param>
        /// <returns>combo value in points</returns>
        private int ComboValue(int value)
        {
            /*
             * value <= 3:
             * 50 * value
             * value > 3:
             * (approximatly)
             * 150 *(1.33 ^(value-3))
             */
            int result = 0;
            switch (value)
            {
                case 0:
                    result = 0;
                    break;
                case 1:
                    result = 50;
                    break;
                case 2:
                    result = 100;
                    break;
                case 3:
                    result = 150;
                    break;
                case 4:
                    result = 200;
                    break;
                case 5:
                    result = 265;
                    break;
                case 6:
                    result = 350;
                    break;
                case 7:
                    result = 470;
                    break;
                case 8:
                    result = 625;
                    break;
                default:
                    result = (int)(150.0 * Math.Pow(1.33, (double)value - 3.0));
                    break;
            }

            return result;
        }

        private int StreakValue(int value)
        {
            int result = 50 * (int)Math.Pow(2, (double)value - 1);
            return result;
        }

        public override void Draw()
        {
            if (!draw)
            {
                return;
            }
            drawBatch.Begin(PrimitiveType.TriangleList, effect);
            drawBatch.Draw(background.Vertexes);
            drawBatch.End();

            Vector2 position = scoreLocation;
            float fontHeight = font.MeasureString(highScoreString).Y;
            spriteBatch.Begin();

            DrawShadowedString(font, playTimeString, position, baseColor);
            position.Y += fontHeight;

            DrawShadowedString(font, highScoreString,
                position,
                baseColor);

            position.Y += fontHeight;
            DrawShadowedString(font, scoreString, position, new Color(currColorVector));

            if (currentScoreItem != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, currentScoreItem.ScoreString, position, currentScoreItem.Color);    
            }

            for (int i = 0; i < Math.Min(scoreList.Count,2); i++)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, scoreList[i].ScoreString, position, scoreList[i].Color);
            }

            if (comboString != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, comboString, position, comboStringColor);
            }

            if (streakString != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, streakString, position, baseColor);
            }

            spriteBatch.End();
        }

        protected void DrawShadowedString(SpriteFont font, string text, Vector2 position, Color color)
        {
            spriteBatch.DrawString(font, text, new Vector2(position.X + 1f, position.Y + 1f), Color.Black);
            spriteBatch.DrawString(font, text, position, color);
        }

        private void SetScoreText()
        {
            playTimeString = string.Format("Play Time: {0:00}:{1:00}", playTimeSpan.Minutes, playTimeSpan.Seconds);
            highScoreString = string.Format("High Score: {0}", highScore.ToString("#,#", CultureInfo.CurrentCulture));
            scoreString = string.Format("Score: {0}", displayedScore.ToString("#,#", CultureInfo.CurrentCulture));
            if (highestCombo > 0)
            {
                comboString = string.Format("Max Combo: {0}X", highestCombo);
            }
            if (highestStreak > 0)
            {
                streakString = string.Format("Max Streak: {0}", highestStreak);
            }
        }

        private void scoreCallback(object sender, string theEvent, params object[] args)
        {
            GameBlock theBlock = sender as GameBlock;
            if (theBlock != null)
            {
                AccumulateCombo(theBlock);
            }
        }

        private void AccumulateCombo(GameBlock block)
        {
            Combo combo = combos[block.BlockColor];
            if (combo.Quantity == 0)
            {
                combo.Location = block.DerivedPosition;
            }
            else
            {
                combo.Location =
                    ((combo.Location * combo.Quantity) + block.DerivedPosition) / (combo.Quantity+1);
            }
            combo.Quantity++;
            combo.Lifetime = 0f;

            currentComboTotal = Math.Max(combo.Quantity, currentComboTotal);
        }

        protected override void RegisterEvents()
        {
            EventManager.Register(scoreCallback, "BubblePop");
        }

        protected override void UnRegisterEvents()
        {
            EventManager.UnRegister(scoreCallback, "BubblePop");
        }

        private string RandomComboMessage()
        {
            return comboMessages[RandomMath.Random.Next(comboMessages.Length)];
        }

        private string RandomHighestComboMessage()
        {
            return HighestComboMessages[RandomMath.Random.Next(HighestComboMessages.Length)];
        }

        public override void Dispose()
        {
            base.Dispose();
            RumbleComponent.SetRumble(0f);
        }
    }
}