﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BalloonBlocks
{
    public class Combo
    {
        Color comboColor;
        public Color ComboColor
        {
            get { return comboColor; }
        }
        int quantity;
        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        float lifeTime;
        public float Lifetime
        {
            get { return lifeTime; }
            set { lifeTime = value; }
        }
        Vector2 location;
        public Vector2 Location
        {
            get { return location; }
            set { location = value; }
        }
        public Combo(int count, float time, Color color, Vector2 loc)
        {
            quantity = count;
            lifeTime = time;
            comboColor = color;
            location = loc;
        }
    }
}
