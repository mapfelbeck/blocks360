﻿using System;
using Microsoft.Xna.Framework.Graphics;
using System.Globalization;

namespace BalloonBlocks
{
    public class ScoreItem
    {
        int score;
        public int Score
        {
            get { return score; }
        }
        string scoreString;
        public string ScoreString
        {
            get { return scoreString; }
        }
        Color color;
        public Color Color
        {
            get { return color; }
        }

        public ScoreItem(int newScore, Color theColor)
        {
            score = newScore;
            scoreString = string.Format("+{0}", newScore.ToString("#,#",CultureInfo.CurrentCulture));
            color = theColor;
        }
    }
}
