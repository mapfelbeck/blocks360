﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using Utilities;
#endregion

namespace BalloonBlocks
{
    public class SurvivalScoring : ScoringDisplay
    {
        int minutesRemaining;
        int secondsRemaining;
        float timeRemaining;
        public float TimeRemaining
        {
            get { return timeRemaining; }
            set
            {
                timeRemaining = value;
                SetTimeString();
            }
        }
        string timeRemainingString;

        public override float PlayTime
        {
            get { return Math.Max(0f, timeRemaining); }
        }

        public SurvivalScoring(GameDifficulty difficulty, float timeRemaining)
            : base(difficulty)
        {
            linesToDrawDefault = 3;
            this.timeRemaining = timeRemaining;
            minutesRemaining = (int)timeRemaining / 60;
            secondsRemaining = (int)timeRemaining % 60;
            SetTimeString();
        }

        public override void  Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                timeRemaining -= elapsed;

                int minutes = (int)timeRemaining / 60;
                int seconds = (int)timeRemaining % 60;
                if (minutes != minutesRemaining || seconds != secondsRemaining)
                {
                    minutesRemaining = minutes;
                    secondsRemaining = seconds;
                    SetTimeString();
                }
            }
        }

        private void SetTimeString()
        {
            if (minutesRemaining > 0)
            {
                timeRemainingString = string.Format("Time Remaining: {0}:{1:00}", minutesRemaining, secondsRemaining);
            }
            else
            {
                timeRemainingString = string.Format("Time Remaining: {0}", secondsRemaining);
            }
        }

        protected override Vector2 WidestString()
        {
            return font.MeasureString("Time Remaining: 10:00");
        }

        public override void Draw()
        {
            if (!draw)
            {
                return;
            }
            drawBatch.Begin(PrimitiveType.TriangleList, effect);
            drawBatch.Draw(background.Vertexes);
            drawBatch.End();

            Vector2 position = scoreLocation;
            float fontHeight = font.MeasureString(highScoreString).Y;
            spriteBatch.Begin();

            DrawShadowedString(font, timeRemainingString, position, baseColor);
            position.Y += fontHeight;

            DrawShadowedString(font, highScoreString,
                position,
                baseColor);

            position.Y += fontHeight;
            DrawShadowedString(font, scoreString, position, new Color(currColorVector));

            if (currentScoreItem != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, currentScoreItem.ScoreString, position, currentScoreItem.Color);
            }

            for (int i = 0; i < Math.Min(scoreList.Count, 2); i++)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, scoreList[i].ScoreString, position, scoreList[i].Color);
            }

            if (comboString != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, comboString, position, comboStringColor);
            }

            if (streakString != null)
            {
                position.Y += fontHeight;
                DrawShadowedString(font, streakString, position, baseColor);
            }

            spriteBatch.End();
        }

    }
}