﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace BalloonBlocks
{
    public class HintPrompt : DrawableGamePlugin
    {
        enum MessageState
        {
            FadingIn,
            ScrollingUp,
            Waiting,
            FadingOut,
        }

        public class HintMessage
        {
            MessageState state;
            float stateTransition;
            public float StateTransition
            {
                get { return stateTransition; }
                set { stateTransition = value; }
            }
            string text;
            public string Text
            {
                get { return text; }
                set { text = value; }
            }
            Color hintColor;
            public Color Color
            {
                get { return new Color(hintColor.R, hintColor.G, hintColor.B, alphaValue); }
            }
            Color hintShadowColor;
            public Color ShadowColor
            {
                get { return new Color(hintShadowColor.R, hintShadowColor.G, hintShadowColor.B, alphaValue); }
            }
            byte alphaValue;
            float messageFadeInTime;
            float messageScrollTime;
            float messageFadeOutTime;
            public bool Done
            {
                get { return state > MessageState.FadingOut; }
            }
            Vector2 position;
            float scrollDistance;
            bool automaticFade;
            public bool AutomaticFade{
                get{return automaticFade;}
                set{automaticFade = value;}
            }

            public Vector2 Position
            {
                get
                {
                    Vector2 temp = new Vector2();
                    switch (state)
                    {
                        case MessageState.FadingIn:
                            temp = position;
                            break;
                        case MessageState.ScrollingUp:
                            temp = new Vector2(position.X, position.Y - scrollDistance * stateTransition);
                            break;
                        case MessageState.FadingOut:
                            temp = new Vector2(position.X, position.Y - scrollDistance);
                            break;
                        case MessageState.Waiting:
                            temp = new Vector2(position.X, position.Y - scrollDistance);
                            break;
                        default:
                            temp = new Vector2(position.X, position.Y - scrollDistance);
                            break;
                    }
                    return temp;
                }
            }

            public HintMessage(string hintText, Vector2 pos, float distance, bool autoFade)
            {
                position = pos;
                scrollDistance = distance;
                text = hintText;
                stateTransition = 0f;
                state = MessageState.FadingIn;
                hintColor = Color.White;
                hintShadowColor = Color.Black;
                alphaValue = 0;
                messageFadeInTime = .5f;
                messageScrollTime = 4f;
                messageFadeOutTime = 1f;
                automaticFade = autoFade;
            }

            public HintMessage(string hintText, Vector2 pos, float distance)
                :this(hintText,pos,distance,true){}

            public void Update(float elapsedTime)
            {
                switch (state)
                {
                    case MessageState.FadingIn:
                        stateTransition += elapsedTime / messageFadeInTime;
                        alphaValue = (byte)(stateTransition * 255);
                        break;
                    case MessageState.ScrollingUp:
                        stateTransition += elapsedTime / messageScrollTime;
                        break;
                    case MessageState.Waiting:
                        if (automaticFade)
                        {
                            stateTransition += 1f;
                        }
                        break;
                    case MessageState.FadingOut:
                        stateTransition += elapsedTime / messageFadeOutTime;
                        alphaValue = (byte)(255 - stateTransition * 255);
                        break;
                }
                if (stateTransition >= 1f)
                {
                    stateTransition = 0f;
                    state++;
                }
            }
        }

        TextElement glyphText;
        SpriteFont font;
        SpriteBatch spriteBatch;

        List<HintMessage> messages;
        HintMessage currentMessage;
        Vector2 promptLocation;
        Vector2 drawLocation;
        float scrollDistance;

        public HintPrompt(Vector2 hintLocation)
        {
            promptLocation = hintLocation;
            messages = new List<HintMessage>();
            currentMessage = null;
        }

        public override void LoadContent(ContentManager content)
        {
            glyphText = new TextElement("Fonts/HintFont");
            glyphText.LoadContent(content, GraphicsConfig.GraphicsDevice);
            glyphText.ButtonScale = .25f;
            font = content.Load<SpriteFont>("Fonts/HintFont");
            spriteBatch = new SpriteBatch(GraphicsConfig.GraphicsDevice);

            scrollDistance = (float)(GraphicsConfig.GraphicsDevice.Viewport.Height / 6);
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (!otherScreenHasFocus && !coveredByOtherScreen)
            {
                if (currentMessage != null)
                {
                    currentMessage.Update(elapsedTime);
                    if (currentMessage.Done)
                    {
                        currentMessage = null;
                    }
                }
                else if (messages.Count > 0)
                {
                    currentMessage = messages[0];
                    messages.Remove(currentMessage);
                    drawLocation = new Vector2(promptLocation.X - font.MeasureString(currentMessage.Text).X / 2,
                                                promptLocation.Y + scrollDistance);
                }
            }
        }

        public override void Draw()
        {
            if (currentMessage != null && font != null)
            {
                spriteBatch.Begin();
                DrawMessage(font, currentMessage);
                spriteBatch.End();
            }
        }

        private void DrawMessage(SpriteFont font, HintMessage message)
        {
            glyphText.Text = message.Text;
            glyphText.Position = message.Position + Vector2.One;
            glyphText.Color = message.ShadowColor;
            glyphText.Draw(spriteBatch);

            glyphText.Position = message.Position;
            glyphText.Color = message.Color;
            glyphText.Draw(spriteBatch);
            //spriteBatch.DrawString(font, message.Text, message.Position + Vector2.One, message.ShadowColor);
            //spriteBatch.DrawString(font, message.Text, message.Position, message.Color);
        }

        public void AddHint(string hint)
        {
            AddHint(hint, true);
        }

        public void AddHint(string hint, bool autoFade)
        {
            if (hint != null)
            {
                HintMessage newHint = new HintMessage(hint,
                    new Vector2(promptLocation.X - font.MeasureString(hint).X / 2, promptLocation.Y + scrollDistance),
                                            scrollDistance, autoFade);
                messages.Add(newHint);
            }
        }

        public void FadeOutAll()
        {
            if (currentMessage != null)
            {
                currentMessage.AutomaticFade = true;
            }
            foreach (HintMessage hint in messages)
            {
                hint.AutomaticFade = true;
            }
        }
    }
}
