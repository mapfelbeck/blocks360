﻿using System;
namespace BalloonBlocks
{
    public class TrialTracker
    {
        static bool initialized = false;
        static bool[,] gamesPlayedMatrix;

        public static void Initialize()
        {
            initialized = true;

            gamesPlayedMatrix = new bool[1 + (int)GameType.Survival, 1 + (int)GameDifficulty.SplashScreen];

            for (int i = 0; i < (int)GameType.Survival; i++)
            {
                for (int j = 0; j < (int)GameDifficulty.SplashScreen; j++)
                {
                    gamesPlayedMatrix[i, j] = false;
                }
            }
        }

        public static void MarkPlayed(GameType type, GameDifficulty difficulty)
        {
            if (!initialized)
            {
                throw new Exception("Trial tracker not Initialize()'d");
            }

            gamesPlayedMatrix[(int)type, (int)difficulty] = true;
        }

        public static bool IsPlayed(GameType type, GameDifficulty difficulty)
        {
            if (!initialized)
            {
                throw new Exception("Trial tracker not Initialize()'d");
            }

            return gamesPlayedMatrix[(int)type, (int)difficulty];
        }
    }
}