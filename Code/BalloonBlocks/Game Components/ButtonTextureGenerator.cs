﻿using Utilities;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace BalloonBlocks
{
    public class ButtonEntry
    {
        public string text;
        public Color color;

        public ButtonEntry(string text, Color color)
        {
            this.text = text;
            this.color = color;
        }
    }

    class ButtonTextureGenerator
    {
        GraphicsDevice graphicsDevice;  // needed to rendering the base texture
        SpriteBatch spriteBatch;
        Tile[] buttonTiles;             // holds source rectangle and texture coordinates for each button
        public Tile[] ButtonTiles
        {
            get
            {
                if (!textureValid)
                {
                    RenderTexture();
                }
                return buttonTiles;
            }
        }
        Texture2D buttonTexture;        // base texture
        public Texture2D ButtonTexture
        {
            get
            {
                if (!textureValid)
                {
                    RenderTexture();
                }
                return buttonTexture;
            }
        }
        public void InvalidateTexture()
        {
            textureValid = false;
        }

        SpriteBatchNineSliceTexture buttonBase;
        public SpriteBatchNineSliceTexture ButtonBase
        {
            get { return buttonBase; }
            set
            {
                buttonBase = value;
                textureValid = false;
            }
        }

        SpriteFont buttonFont;
        public SpriteFont ButtonFont
        {
            get { return buttonFont; }
            set
            {
                buttonFont = value;
                textureValid = false;
            }
        }

        bool textureValid;
        public bool TextureValid
        {
            get { return textureValid; }
        }

        int horizontalPadding;
        public int HorizontalPadding
        {
            get { return horizontalPadding; }
            set
            {
                horizontalPadding = value;
                textureValid = false;
            }
        }
        public int VerticalPadding
        {
            get { return verticalPadding; }
            set
            {
                verticalPadding = value;
                textureValid = false;
            }
        }
        int verticalPadding;

        List<ButtonEntry> buttonEntries;
        public List<ButtonEntry> ButtonEntries
        {
            get { return buttonEntries; }
        }

        Color[] colors = {  
                    Color.Red,
                    Color.Green,
                    Color.Blue,
                    Color.Yellow,
                    Color.Purple,
                    new Color(255,128,0),//orange
                };

        public ButtonTextureGenerator(GraphicsDevice device, string[] strings, SpriteFont font,
            SpriteBatchNineSliceTexture backingTexture, int horizontalPadding, int verticalPadding)
        {
            graphicsDevice = device;
            spriteBatch = new SpriteBatch(device);

            //buttonStrings = strings;
            buttonFont = font;

            this.horizontalPadding = horizontalPadding;
            this.verticalPadding = verticalPadding;

            buttonBase = backingTexture;

            textureValid = false;

            buttonEntries = new List<ButtonEntry>();
            if (strings != null)
            {
                AddStrings(strings);
            }

            if (buttonEntries.Count > 0)
            {
                RenderTexture();
            }
        }

        public ButtonTextureGenerator(GraphicsDevice device, string[] strings, SpriteFont font, SpriteBatchNineSliceTexture backingTexture)
            : this(device, strings, font, backingTexture, 0, 0) { }

        public void RenderTexture()
        {
            if (buttonEntries.Count <= 0)
            {
                throw new Exception("No button text, unable to render");
            }
            if (buttonTexture != null)
            {
                buttonTexture.Dispose();
            }
            buttonTiles = new Tile[buttonEntries.Count];
            Rectangle[] sourceRectangles = new Rectangle[buttonEntries.Count];

            RenderTarget2D renderTarget = CreateRenderTarget(graphicsDevice, 1, graphicsDevice.PresentationParameters.BackBufferFormat);

            /*DepthStencilBuffer stencilBuffer = CreateDepthStencil(renderTarget,
                    DepthFormat.Depth24Stencil8Single);*/
            DepthStencilBuffer stencilBuffer = CreateDepthStencil(renderTarget,
                    graphicsDevice.PresentationParameters.AutoDepthStencilFormat);
            
            graphicsDevice.SetRenderTarget(0, renderTarget);
            
            DepthStencilBuffer oldStencilBuffer = graphicsDevice.DepthStencilBuffer;
            graphicsDevice.DepthStencilBuffer = stencilBuffer;

            graphicsDevice.Clear(Color.TransparentWhite);

            spriteBatch.Begin(SpriteBlendMode.AlphaBlend);

            int index = 0;
            int colorIndex = 0;

            Vector2 textSize;
            Vector2 textLocation;
            Rectangle drawRect;
            Point drawPoint = new Point(0, 0);

            //foreach (string text in buttonStrings)
            foreach(ButtonEntry entry in buttonEntries)
            {
                textSize = buttonFont.MeasureString(entry.text) + Vector2.One;
                drawRect = new Rectangle(drawPoint.X, drawPoint.Y,
                    (int)textSize.X + 2 * horizontalPadding, (int)textSize.Y + 2 * verticalPadding);
                textLocation = new Vector2(drawPoint.X + horizontalPadding, drawPoint.Y + verticalPadding);

                buttonBase.Draw(spriteBatch, drawRect, colors[colorIndex]);
                DrawShadowedString(entry.text, textLocation, entry.color);

                sourceRectangles[index] = drawRect;

                drawPoint.Y += drawRect.Height;
                index++;
                colorIndex = index % colors.Length;
            }

            spriteBatch.End();

            graphicsDevice.SetRenderTarget(0, null);
            graphicsDevice.DepthStencilBuffer = oldStencilBuffer;

            // now you can use this result  
            buttonTexture = renderTarget.GetTexture();

            for (int i = 0; i < buttonEntries.Count; i++)
            {
                buttonTiles[i] = new Tile(buttonTexture, sourceRectangles[i]);
            }
            
#if WINDOWS
            oldStencilBuffer.Dispose();
            oldStencilBuffer = null;
#endif
            renderTarget.Dispose();
            stencilBuffer.Dispose();
            renderTarget = null;
            stencilBuffer = null;
            
            textureValid = true;
        }

        protected void DrawShadowedString(string text, Vector2 position, Color color)
        {
            spriteBatch.DrawString(buttonFont, text, position + Vector2.One, Color.Black);
            spriteBatch.DrawString(buttonFont, text, position, color);
        }

        public void AddStrings(string[] strings)
        {
            if (strings != null)
            {
                foreach (string text in strings)
                {
                    buttonEntries.Add(new ButtonEntry(text, Color.White));
                }
            }
        }

        private static RenderTarget2D CreateRenderTarget(GraphicsDevice device,
            int numberLevels, SurfaceFormat surface)
        {
            MultiSampleType type =
                device.PresentationParameters.MultiSampleType;

            // If the card can't use the surface format
            if (!GraphicsAdapter.DefaultAdapter.CheckDeviceFormat(
                DeviceType.Hardware,
                GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Format,
                TextureUsage.None,
                QueryUsages.None,
                ResourceType.RenderTarget,
                surface))
            {
                // Fall back to current display format
                surface = device.DisplayMode.Format;
            }
            // Or it can't accept that surface format 
            // with the current AA settings
            else if (!GraphicsAdapter.DefaultAdapter.CheckDeviceMultiSampleType(
                DeviceType.Hardware, surface,
                device.PresentationParameters.IsFullScreen, type))
            {
                // Fall back to no antialiasing
                type = MultiSampleType.None;
            }

            int width, height;
            CheckTextureSize(512, 1024, out width, out height);

            // Create our render target
            return new RenderTarget2D(device,
                width, height, numberLevels, surface,
                type, 0);
        }

        private static bool CheckTextureSize(int width, int height,
            out int newwidth, out int newheight)
        {
            bool retval = false;

            GraphicsDeviceCapabilities Caps;
            Caps = GraphicsAdapter.DefaultAdapter.GetCapabilities(
                DeviceType.Hardware);

            if (Caps.TextureCapabilities.RequiresPower2)
            {
                retval = true;  // Return true to indicate the numbers changed

                // Find the nearest base two log of the current width, 
                // and go up to the next integer                
                double exp = Math.Ceiling(Math.Log(width) / Math.Log(2));
                // and use that as the exponent of the new width
                width = (int)Math.Pow(2, exp);
                // Repeat the process for height
                exp = Math.Ceiling(Math.Log(height) / Math.Log(2));
                height = (int)Math.Pow(2, exp);
            }
            if (Caps.TextureCapabilities.RequiresSquareOnly)
            {
                retval = true;  // Return true to indicate numbers changed
                width = Math.Max(width, height);
                height = width;
            }

            newwidth = Math.Min(Caps.MaxTextureWidth, width);
            newheight = Math.Min(Caps.MaxTextureHeight, height);
            return retval;
        }

        private static DepthStencilBuffer CreateDepthStencil(
            RenderTarget2D target)
        {
            return new DepthStencilBuffer(target.GraphicsDevice, target.Width,
                target.Height, target.GraphicsDevice.DepthStencilBuffer.Format,
                target.MultiSampleType, target.MultiSampleQuality);
        }

        private static DepthStencilBuffer CreateDepthStencil(
            RenderTarget2D target, DepthFormat depth)
        {
            if (GraphicsAdapter.DefaultAdapter.CheckDepthStencilMatch(
                DeviceType.Hardware,
                GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Format,
                target.Format,
                depth))
            {
                return new DepthStencilBuffer(target.GraphicsDevice,
                    target.Width, target.Height, depth,
                    target.MultiSampleType, target.MultiSampleQuality);
            }
            else
                return CreateDepthStencil(target);
        }
    }
}