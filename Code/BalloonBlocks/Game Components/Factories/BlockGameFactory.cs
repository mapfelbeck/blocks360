﻿using Microsoft.Xna.Framework;
using System;

namespace BalloonBlocks
{
    /// <summary>
    /// Holds static Methods that crete and return BlockGame instances
    /// </summary>
    public static class BlockGameFactory
    {
        private static string backgroundTextureName = "block textures/tiled greyscale";
        private static int backgroundTextureSides = 3;

        private static string gameTextureName = "block textures/tiled greyscale";
        private static int gameTextureSides = 3;

        private static GameConfig EasyNormalConfig = new GameConfig(GameType.Normal, ShapeType.Double, BlockSize.Large,
            BlockType.Freeze, true, GameSpeed.Slow, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Easy, 4f, 2f, 7f);

        private static GameConfig MediumNormalConfig = new GameConfig(GameType.Normal, ShapeType.Triomino, BlockSize.Normal,
            BlockType.Freeze, true, GameSpeed.Normal, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Medium, 4f, 2f, 7f);

        private static GameConfig HardNormalConfig = new GameConfig(GameType.Normal, ShapeType.Tetromino, BlockSize.Normal,
            BlockType.Freeze, true, GameSpeed.Fast, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Hard, 2f, 2f, 7f);

        private static GameConfig EasyClearConfig = new GameConfig(GameType.Clear, ShapeType.Double, BlockSize.Large,
            BlockType.Damping, true, GameSpeed.Slow, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Easy, 4f, 2f, 7f);

        private static GameConfig MediumClearConfig = new GameConfig(GameType.Clear, ShapeType.Double, BlockSize.Normal,
            BlockType.Freeze, true, GameSpeed.Normal, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Medium, 4f, 2f, 7f);

        private static GameConfig HardClearConfig = new GameConfig(GameType.Clear, ShapeType.Triomino, BlockSize.Normal,
            BlockType.Freeze, true, GameSpeed.Normal, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Hard, 2f, 2f, 7f);

        private static GameConfig EasySurvivalConfig = new GameConfig(GameType.Survival, ShapeType.Triomino, BlockSize.Large,
            BlockType.Freeze, true, GameSpeed.Fast, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Easy, 4f, 1f, 6f);

        private static GameConfig MediumSurvivalConfig = new GameConfig(GameType.Survival, ShapeType.Triomino, BlockSize.Normal,
            BlockType.Freeze, true, GameSpeed.Fast, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Medium, 3f, 1f, 5f);

        private static GameConfig HardSurvivalConfig = new GameConfig(GameType.Survival, ShapeType.Tetromino, BlockSize.Normal,
            BlockType.Freeze, true, GameSpeed.VeryFast, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Hard, 2f, 1f, 4f);

        private static GameConfig TutorialConfig = new GameConfig(GameType.Normal, ShapeType.Triomino, BlockSize.Normal,
            BlockType.Freeze, false, GameSpeed.Normal, gameTextureName, gameTextureSides, backgroundTextureName, backgroundTextureSides,
            GameDifficulty.Medium, 4f, 2f, 7f);

        public static BlockGameScreen CreateSplashScreenGame()
        {
            GameConfig config = new GameConfig();

            config.blockSize = BlockSize.SplashScreen;
            config.friction = Friction.Normal;
            config.gravity = Gravity.Normal;
            config.blockType = BlockType.Normal;
            config.scored = false;
            config.shape = ShapeType.Square;

            config.blockTextureName = gameTextureName;
            config.blockTextureSize = gameTextureSides;

            config.backgroundTextureName = backgroundTextureName;
            config.backgroundTextureSize = backgroundTextureSides;

            config.gameDifficulty = GameDifficulty.SplashScreen;

            BlockGameScreen screen = new SplashScreenGameScreen(config, buildSplashScreenGround());
            screen.HandlesInput = false;

            return screen;
        }

        public static PerfTestScreen CreatePerfTestGame()
        {
            GameConfig config = new GameConfig();

            config.blockSize = BlockSize.Small;
            config.friction = Friction.Normal;
            config.gravity = Gravity.Normal;
            config.blockType = BlockType.Normal;
            config.scored = false;
            config.shape = ShapeType.Square;
            config.gameDifficulty = GameDifficulty.Custom;

            config.blockTextureName = gameTextureName;
            config.blockTextureSize = gameTextureSides;

            config.backgroundTextureName = backgroundTextureName;
            config.backgroundTextureSize = backgroundTextureSides;

            return new PerfTestScreen(config, buildPerfTestGameGround(), true);
        }

        public static LoadTestScreen CreateLoadTestGame()
        {
            GameConfig config = new GameConfig();

            config.blockSize = BlockSize.Small;
            config.friction = Friction.Normal;
            config.gravity = Gravity.Normal;
            config.blockType = BlockType.Normal;
            config.scored = false;
            config.shape = ShapeType.Square;
            config.gameDifficulty = GameDifficulty.Custom;

            config.blockTextureName = gameTextureName;
            config.blockTextureSize = gameTextureSides;

            config.backgroundTextureName = backgroundTextureName;
            config.backgroundTextureSize = backgroundTextureSides;

            return new LoadTestScreen(config, buildNormalGameGround(), true);
        }

        public static BlockGameScreen CreateGame(ShapeType pieceType, BlockSize blockSize,
            Friction friction, Gravity gravity, BlockType blockMode, bool scored)
        {
            MusicPlayer.PlayRandomTrack();
            GameConfig config = new GameConfig();

            config.blockSize = blockSize;
            config.shape = pieceType;
            config.scored = scored;
            config.blockType = blockMode;
            config.friction = friction;
            config.gravity = gravity;

            config.blockTextureName = gameTextureName;
            config.blockTextureSize = gameTextureSides;

            config.backgroundTextureName = backgroundTextureName;
            config.backgroundTextureSize = backgroundTextureSides;

            config.unFreezeTime = 4f;

            config.gameDifficulty = GameDifficulty.Custom;

            config.dropAfterCollideTime = 2f;
            config.forceDropTime = 7f;

            return new BlockGameScreen(config, buildNormalGameGround(), true);
        }

        public static BlockGameScreen CreateGame(GameType type, GameDifficulty difficulty)
        {
            BlockGameScreen screen = null;

            TrialTracker.MarkPlayed(type, difficulty);

            switch (type)
            {
                case GameType.Normal:
                    screen = CreateNormalGame(difficulty);
                    break;
                case GameType.Clear:
                    screen = CreateClearGame(difficulty);
                    break;
                case GameType.Survival:
                    screen = CreateSurvivalGame(difficulty);
                    break;
                default:
                    break;
            }

            return screen;
        }

        private static BlockGameScreen CreateSurvivalGame(GameDifficulty difficulty)
        {
            BlockGameScreen screen = null;
            switch (difficulty)
            {
                case GameDifficulty.Easy:
                    MusicPlayer.PlayEasyTrack();
                    screen = new SurvivalModeScreen(EasySurvivalConfig, buildNormalGameGround(), true);
                    break;
                case GameDifficulty.Medium:
                    MusicPlayer.PlayMediumTrack();
                    screen = new SurvivalModeScreen(MediumSurvivalConfig, buildNormalGameGround(), true);
                    break;
                case GameDifficulty.Hard:
                    MusicPlayer.PlayHardTrack();
                    screen = new SurvivalModeScreen(HardSurvivalConfig, buildNormalGameGround(), true);
                    break;
                default:
                    break;
            }
            return screen;
        }

        private static BlockGameScreen CreateClearGame(GameDifficulty difficulty)
        {
            BlockGameScreen screen = null;
            switch (difficulty)
            {
                case GameDifficulty.Easy:
                    MusicPlayer.PlayEasyTrack();
                    screen = new ClearModeScreen(EasyClearConfig, buildNormalGameGround(), true);
                    break;
                case GameDifficulty.Medium:
                    MusicPlayer.PlayMediumTrack();
                    screen = new ClearModeScreen(MediumClearConfig, buildNormalGameGround(), true);
                    break;
                case GameDifficulty.Hard:
                    MusicPlayer.PlayHardTrack();
                    screen = new ClearModeScreen(HardClearConfig, buildNormalGameGround(), true);
                    break;
                default:
                    break;
            }
            return screen;
        }

        private static BlockGameScreen CreateNormalGame(GameDifficulty difficulty)
        {
            BlockGameScreen screen = null;
            switch (difficulty)
            {
                case GameDifficulty.Easy:
                    MusicPlayer.PlayEasyTrack();
                    screen = new BlockGameScreen(EasyNormalConfig, buildNormalGameGround(), true);
                    break;
                case GameDifficulty.Medium:
                    MusicPlayer.PlayMediumTrack();
                    screen = new BlockGameScreen(MediumNormalConfig, buildNormalGameGround(), true);
                    break;
                case GameDifficulty.Hard:
                    MusicPlayer.PlayHardTrack();
                    screen = new BlockGameScreen(HardNormalConfig, buildNormalGameGround(), true);
                    break;
                default:
                    break;
            }
            return screen;
        }

        public static BlockGameScreen CreateTutorialGame()
        {
            MusicPlayer.PlayRandomTrack();
            BlockGameScreen screen;

            screen = new TutorialModeScreen(TutorialConfig, buildNormalGameGround(), true);

            return screen;
        }

        public static BlockGameScreen CreateGame(GameConfig config)
        {
            BlockGameScreen screen;
            if (config.gameDifficulty != GameDifficulty.Custom)
            {
                screen = CreateGame(config.gameType, config.gameDifficulty);
            }
            else
            {
                switch (config.gameDifficulty)
                {
                    case GameDifficulty.Easy:
                        MusicPlayer.PlayEasyTrack();
                        break;
                    case GameDifficulty.Medium:
                        MusicPlayer.PlayMediumTrack();
                        break;
                    case GameDifficulty.Hard:
                        MusicPlayer.PlayHardTrack();
                        break;
                    case GameDifficulty.Multiplayer:
                        MusicPlayer.PlayRandomTrack();
                        break;
                    case GameDifficulty.Custom:
                        MusicPlayer.PlayRandomTrack();
                        break;
                    case GameDifficulty.Tutorial:
                        MusicPlayer.PlayRandomTrack();
                        break;
                    case GameDifficulty.SplashScreen:
                        MusicPlayer.PlayTitleTrack();
                        break;
                    default:
                        break;
                }

                switch (config.gameType)
                {
                    case GameType.Normal:
                        screen = new BlockGameScreen(config, buildNormalGameGround(), true);
                        break;
                    case GameType.Clear:
                        screen = new ClearModeScreen(config, buildNormalGameGround(), true);
                        break;
                    case GameType.Survival:
                        screen = new SurvivalModeScreen(config, buildNormalGameGround(), true);
                        break;
                    default:
                        screen = null;
                        break;
                }
            }
            return screen;
        }

        static private GameGround buildNormalGameGround()
        {
            return new GameGround(2f, 20f, 16f, new Vector2(0f, -2f));
        }

        static private GameGround buildPerfTestGameGround()
        {
            return new GameGround(2f, 40f, 18f, new Vector2(0f, -2f));
        }

        static private GameGround buildSplashScreenGround()
        {
            Vector2 location;
            if (GraphicsConfig.IsWideScreen)
            {
                location = new Vector2(0f, -5.5f);
            }
            else
            {
                location = new Vector2(0f,  -6f);
            }

            return new GameGround(2f, 40f, 4f, location);
        }
    }
}