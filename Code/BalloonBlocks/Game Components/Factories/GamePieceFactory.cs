﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

namespace BalloonBlocks
{
    /// <summary>
    /// This class has one visible static member, CreateGamePiece, which assembles
    /// and return a GamePiece
    /// </summary>
    public class GamePieceFactory
    {
        const int numberNormalGameTiles = 8;
        const int clearPieceTextureNumber = 8;

        public static GamePiece CreateGamePiece(Vector2 spawnLocation,
            TiledTexture tiledTexture, GameConfig config, ColorGenerator colorGen)
        {
            return CreateGamePiece(spawnLocation, tiledTexture, config, colorGen, true);
        }

        /// <summary>
        /// Builds a GamePiece
        /// </summary>
        /// <param name="gameWorld">Pyisics sim the piece will live in</param>
        /// <param name="spawnLocation">Game world coordinates where the piece will spawn</param>
        /// <param name="tiledTexture">Texture that the GamePiece draw with</param>
        /// <param name="size">size for the GameBlocks in the GamePiece</param>
        /// <param name="type">GamePiece shape to build</param>
        /// <param name="blockType">type of GameBlocks to put in the piece, normal, disappearing or freezing</param>
        /// <returns></returns>
        public static GamePiece CreateGamePiece(Vector2 spawnLocation, 
            TiledTexture tiledTexture, GameConfig config, ColorGenerator colorGen, bool randomTiles)
        {
            GamePiece newGamePiece;

            float size = GlobalConfig.EnumCoefficient(config.blockSize);
            float gravity = GlobalConfig.EnumCoefficient(config.gravity);

            List<Vector2> blockLocations = BuildGridLocations(config.shape);
            List<Vector2> shapeLocations = new List<Vector2>();
            List<Color> colors = BuildBlockColorList(config.shape, colorGen);
            List<Physics.ClosedShape> shapes = new List<Physics.ClosedShape>();
            List<Physics.ExternalSpring> attachSprings = new List<Physics.ExternalSpring>();
            List<GameBlock> blocks = new List<GameBlock>();

            for (int i = 0; i < blockLocations.Count; i++)
            {
                shapes.Add(BuildShape(blockLocations[i] * size, colors[i]));
                shapeLocations.Add(
                    new Vector2(spawnLocation.X + blockLocations[i].X * size + .5f,
                                spawnLocation.Y + blockLocations[i].Y * size - .5f));
            }

            //build all the blocks
            float rotation = MathHelper.PiOver2;
            if (config.blockType == BlockType.Background)
            {
                rotation = RandomMath.RandomBetween(0f, MathHelper.Pi);
            }
            for (int i = 0; i < shapes.Count; i++)
            {
                GameBlock body = BuildBlock(size, config.blockType, shapeLocations[i], shapes[i], config.gameDifficulty, rotation);

                body.addInternalSpring(0, 2, GameConstants.InternalSpringK, GameConstants.InternalSpringDamp);
                body.addInternalSpring(1, 3, GameConstants.InternalSpringK, GameConstants.InternalSpringDamp);
                body.BlockColor = colors[i];
                blocks.Add(body);
            }

            //attach all the adjacent block with springs
            for (int i = 0; i < blocks.Count; i++)
            {
                for (int j = i; j < blocks.Count; j++)
                {
                    if (blocks[i] != blocks[j])
                    {
                        AttachBlocks(blocks[i], blocks[j], ref attachSprings, size);
                    }
                }
            }

            VertexPositionColorTexture[] vertexes;
            vertexes = new VertexPositionColorTexture[blocks.Count * 6];

            int randomInt = 0;
            if (randomTiles)
            {
                randomInt = RandomMath.Random.Next(tiledTexture.TileCount - 1);
            }
            for (int i = 0; i < blocks.Count; i++)
            {
                blocks[i].DrawTile = tiledTexture[randomInt];

                if (randomTiles)
                {
                    randomInt = RandomMath.Random.Next(tiledTexture.TileCount - 1);
                }
                else
                {
                    randomInt++;
                }
                //randomInt = RandomMath.Random.Next(numberNormalGameTiles);
                //randomInt = RandomMath.Random.Next(tiledTexture.TileCount);


                vertexes[i * 6 + 0].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 1].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 2].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 3].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 4].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 5].Color = blocks[i].NormalColor;

                vertexes[i * 6 + 0].TextureCoordinate = blocks[i].DrawTile.LowerLeft;
                vertexes[i * 6 + 1].TextureCoordinate = blocks[i].DrawTile.UpperLeft;
                vertexes[i * 6 + 2].TextureCoordinate = blocks[i].DrawTile.UpperRight;

                vertexes[i * 6 + 3].TextureCoordinate = blocks[i].DrawTile.UpperRight;
                vertexes[i * 6 + 4].TextureCoordinate = blocks[i].DrawTile.LowerRight;
                vertexes[i * 6 + 5].TextureCoordinate = blocks[i].DrawTile.LowerLeft;
            }

            if (config.blockType != BlockType.Background)
            {
                newGamePiece = new GamePiece(blocks, attachSprings,
                    vertexes, gravity);
            }
            else
            {
                newGamePiece = new BackgroundPiece(blocks, attachSprings,
                    vertexes, gravity);
            }

            newGamePiece.ControlMode = GlobalConfig.ControlMode;
            newGamePiece.AutoDampRate = GlobalConfig.AutoDampNormalized;

            return newGamePiece;
        }

        public static GamePiece CreateClearPiece(Vector2 spawnLocation, 
            TiledTexture tiledTexture, GameConfig config, ColorGenerator colorGen)
        {
            GamePiece newGamePiece;
            float size = GlobalConfig.EnumCoefficient(config.blockSize);

            Color color = colorGen.NextColor();

            List<Vector2> blockLocations = BuildGridLocations(ShapeType.Single);
            List<Vector2> shapeLocations = new List<Vector2>();
            List<Physics.ClosedShape> shapes = new List<Physics.ClosedShape>();
            List<Physics.ExternalSpring> attachSprings = new List<Physics.ExternalSpring>();
            List<GameBlock> blocks = new List<GameBlock>();

            for (int i = 0; i < blockLocations.Count; i++)
            {
                shapes.Add(BuildShape(blockLocations[i] * size, color));
                shapeLocations.Add(
                    new Vector2(spawnLocation.X + blockLocations[i].X * size + .5f,
                                spawnLocation.Y + blockLocations[i].Y * size - .5f));
            }

            //build all the blocks
            for (int i = 0; i < shapes.Count; i++)
            {
                GameBlock body = BuildBlock(size, config.blockType, shapeLocations[i], shapes[i], config.gameDifficulty, MathHelper.PiOver2);

                body.addInternalSpring(0, 2, GameConstants.InternalSpringK, GameConstants.InternalSpringDamp);
                body.addInternalSpring(1, 3, GameConstants.InternalSpringK, GameConstants.InternalSpringDamp);
                body.BlockColor = color;
                blocks.Add(body);
            }

            //attach all the adjacent blocks together with springs
            for (int i = 0; i < blocks.Count; i++)
            {
                for (int j = i; j < blocks.Count; j++)
                {
                    if (blocks[i] != blocks[j])
                    {
                        AttachBlocks(blocks[i], blocks[j], ref attachSprings, size);
                    }
                }
            }

            VertexPositionColorTexture[] vertexes;
            vertexes = new VertexPositionColorTexture[blocks.Count * 6];

            for (int i = 0; i < blocks.Count; i++)
            {
                blocks[i].DrawTile = tiledTexture[clearPieceTextureNumber];

                vertexes[i * 6 + 0].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 1].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 2].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 3].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 4].Color = blocks[i].NormalColor;
                vertexes[i * 6 + 5].Color = blocks[i].NormalColor;

                vertexes[i * 6 + 0].TextureCoordinate = blocks[i].DrawTile.LowerLeft;
                vertexes[i * 6 + 1].TextureCoordinate = blocks[i].DrawTile.UpperLeft;
                vertexes[i * 6 + 2].TextureCoordinate = blocks[i].DrawTile.UpperRight;

                vertexes[i * 6 + 3].TextureCoordinate = blocks[i].DrawTile.UpperRight;
                vertexes[i * 6 + 4].TextureCoordinate = blocks[i].DrawTile.LowerRight;
                vertexes[i * 6 + 5].TextureCoordinate = blocks[i].DrawTile.LowerLeft;
            }

            newGamePiece = new GamePiece(blocks, attachSprings,
                vertexes, GlobalConfig.EnumCoefficient(config.gravity));

            return newGamePiece;
        }

        private static GameBlock BuildBlock(float size, BlockType blockType, Vector2 shapeLocation, Physics.ClosedShape shape, GameDifficulty difficulty, float rotation)
        {
            GameBlock body;
            BlockConfig config = GetBlockConfig(difficulty);
            switch (blockType)
            {
                case BlockType.Background:
                    body = new BackgroundBlock(
                        shape, GameConstants.MassPerPoint,
                        GameConstants.ShapeSpringK, GameConstants.ShapeSpringDamp,
                        GameConstants.EdgeSpringK, GameConstants.EdgeSpringDamp,
                        shapeLocation, rotation, Vector2.One * size, config);
                    return body;
                case BlockType.Normal:
                    body = new GameBlock(
                        shape, GameConstants.MassPerPoint,
                        GameConstants.ShapeSpringK, GameConstants.ShapeSpringDamp,
                        GameConstants.EdgeSpringK, GameConstants.EdgeSpringDamp,
                        shapeLocation, rotation, Vector2.One * size, config);
                    return body;
                case BlockType.Damping:
                    body = new DampingGameBlock(
                        shape, GameConstants.MassPerPoint,
                        GameConstants.ShapeSpringK, GameConstants.ShapeSpringDamp,
                        GameConstants.EdgeSpringK, GameConstants.EdgeSpringDamp,
                        shapeLocation, rotation, Vector2.One * size, config);
                    return body;
                case BlockType.Deflating:
                    body = new DeflatingGameBlock(
                        shape, GameConstants.MassPerPoint,
                        GameConstants.ShapeSpringK, GameConstants.ShapeSpringDamp,
                        GameConstants.EdgeSpringK, GameConstants.EdgeSpringDamp,
                        shapeLocation, rotation, Vector2.One * size, config);
                    return body;
                case BlockType.Freeze:
                    body = new FreezingGameBlock(
                        shape, GameConstants.MassPerPoint,
                        GameConstants.ShapeSpringK, GameConstants.ShapeSpringDamp,
                        GameConstants.EdgeSpringK, GameConstants.EdgeSpringDamp,
                        shapeLocation, rotation, Vector2.One * size, config);
                    return body;
                default:
                    break;
            }
            return null;
        }

        private static BlockConfig GetBlockConfig(GameDifficulty difficulty)
        {
            BlockConfig config = new BlockConfig();

            switch (difficulty)
            {
                case GameDifficulty.Hard:
                    config.dampingRate = 0.15f;
                    config.dampingMax = 0.98f;
                    config.timeTillDamping = 0.5f;
                    config.timeTillDeflate = 5f;
                    config.timeTillFreeze = 2f;
                    config.freezeWaitTimerLength = .5f;
                    config.deflateRate = .25f;
                    config.freezeVelocityThreshhold = .1f;
                    config.freezeDistortionThreshhold = 1f;
                    break;
                default:
                    config.dampingRate = 0.15f;
                    config.dampingMax = 0.98f;
                    config.timeTillDamping = 0.5f;
                    config.timeTillDeflate = 10f;
                    config.timeTillFreeze = 2f;
                    config.freezeWaitTimerLength = 1f;
                    config.deflateRate = .25f;
                    config.freezeVelocityThreshhold = .08f;
                    config.freezeDistortionThreshhold = .8f;
                    break;
            }

            return config;
        }

        private static void AttachBlocks(GameBlock BlockA, GameBlock BlockB, ref List<Physics.ExternalSpring> springs, float size)
        {
            int numberPMA = BlockA.PointMassCount;
            int numberPMB = BlockB.PointMassCount;
            Physics.PointMass pmA;

            /*if the two block are greater than their size distant then they're
             * diagonal to each other and we don't want to connect them*/
            Vector2 blockACenter = BlockA.DerivedPosition;
            Vector2 blockBCenter = BlockB.DerivedPosition;
            if (Vector2.Distance(blockACenter, blockBCenter) > size + .01f)
            {
                return;
            }

            Physics.ExternalSpring spring;
            for (int i = 0; i < numberPMA; i++)
            {
                pmA = BlockA.getPointMass(i);
                for (int j = 0; j < numberPMB; j++)
                {
                    Vector2 dist = pmA.Position - BlockB.getPointMass(j).Position;
                    float absolute = dist.Length();

                    if (absolute < 0.001f)
                    {
                        spring = new Physics.ExternalSpring(BlockA, BlockB, i, j,
                            0f, GameConstants.AttachSpringK, GameConstants.AttachSpringDamp);

                        springs.Add(spring);
                    }
                }
            }
        }

        static private Physics.ClosedShape BuildShape(Vector2 relativePosition, Color color)
        {
            Physics.ClosedShape newShape = new Physics.ClosedShape();

            newShape.begin();
            newShape.addVertex(new Vector2(relativePosition.X, relativePosition.Y));
            newShape.addVertex(new Vector2(relativePosition.X + 1f, relativePosition.Y));
            newShape.addVertex(new Vector2(relativePosition.X + 1f, relativePosition.Y - 1f));
            newShape.addVertex(new Vector2(relativePosition.X, relativePosition.Y - 1f));
            newShape.finish();

            return newShape;
        }

        private static List<Vector2> BuildGridLocations(ShapeType type)
        {
            if (type == ShapeType.Single)
            {
                return BuildGridLocation();
            }
            else if (type == ShapeType.Double)
            {
                return BuildGridLocation(RandomMath.RandomDouble());
            }
            else if (type == ShapeType.Triomino)
            {
                return BuildGridLocation(RandomMath.RandomTriomino());
            }
            else if (type == ShapeType.Tetromino)
            {
                return BuildGidLocation(RandomMath.RandomTetromino());
            }
            else if (type == ShapeType.Pentomino)
            {
                return BuildGridLocation(RandomMath.RandomPentomino());
            }
            else if (type == ShapeType.Square)
            {
                return BuildGidLocation(TetrominoShape.O);
            }

            return null;
        }

        private static List<Vector2> BuildGridLocation(PentominoShape shape)
        {
            List<Vector2> blockLocations = new List<Vector2>();
  
            switch (shape)
            {
                case PentominoShape.F:
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(2, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    break;
                case PentominoShape.ReverseF:
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(2, 1));
                    break;
                case PentominoShape.I:
                    blockLocations.Add(new Vector2(0, 4));
                    blockLocations.Add(new Vector2(0, 3));
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    break;
                case PentominoShape.L:
                    blockLocations.Add(new Vector2(0, 3));
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    blockLocations.Add(new Vector2(1, 0));
                    break;
                case PentominoShape.ReverseL:
                    blockLocations.Add(new Vector2(1, 3));
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(0, 0));
                    break;
                case PentominoShape.N:
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(2, 0));
                    blockLocations.Add(new Vector2(3, 0));
                    break;
                case PentominoShape.ReverseN:
                    blockLocations.Add(new Vector2(0, 0));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(2, 1));
                    blockLocations.Add(new Vector2(3, 1));
                    break;
                case PentominoShape.P:
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    break;
                case PentominoShape.ReverseP:
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    break;
                case PentominoShape.T:
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(2, 2));
                    break;
                case PentominoShape.U:
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(2, 0));
                    blockLocations.Add(new Vector2(2, 1));
                    break;
                case PentominoShape.V:
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(2, 0));
                    break;
                case PentominoShape.W:
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(2, 0));
                    break;
                case PentominoShape.X:
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(2, 1));
                    break;
                case PentominoShape.Y:
                    blockLocations.Add(new Vector2(0, 0));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(2, 0));
                    blockLocations.Add(new Vector2(3, 0));
                    blockLocations.Add(new Vector2(2, 1));
                    break;
                case PentominoShape.ReverseY:
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(2, 1));
                    blockLocations.Add(new Vector2(3, 1));
                    blockLocations.Add(new Vector2(2, 0));
                    break;
                case PentominoShape.Z:
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(2, 0));
                    break;
                case PentominoShape.ReverseZ:
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(0, 0));
                    blockLocations.Add(new Vector2(2, 2));
                    break;
                default:
                    break;
            }

            return blockLocations;
        }

        private static List<Vector2> BuildGidLocation(TetrominoShape shape)
        {
            List<Vector2> blockLocations = new List<Vector2>();

            switch (shape)
            {
                case TetrominoShape.O:
                    blockLocations.Add(new Vector2(0, 2));//
                    blockLocations.Add(new Vector2(1, 2));//
                    blockLocations.Add(new Vector2(0, 1));//
                    blockLocations.Add(new Vector2(1, 1));//
                    break;
                case TetrominoShape.I:
                    blockLocations.Add(new Vector2(0, 3));
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    break;
                case TetrominoShape.L:
                    blockLocations.Add(new Vector2(0, 3));
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(1, 1));
                    break;
                case TetrominoShape.ReverseL:
                    blockLocations.Add(new Vector2(1, 3));
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(0, 1));
                    break;
                case TetrominoShape.ReverseN:
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(2, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(1, 1));
                    break;
                case TetrominoShape.N:
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    blockLocations.Add(new Vector2(2, 1));
                    break;
                case TetrominoShape.T:
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(1, 2));
                    blockLocations.Add(new Vector2(2, 2));
                    blockLocations.Add(new Vector2(1, 1));
                    break;
                default:
                    break;
            }

            return blockLocations;
        }

        private static List<Vector2> BuildGridLocation(TriominoShape shape)
        {
            List<Vector2> blockLocations = new List<Vector2>();

            switch (shape)
            {
                case TriominoShape.I:
                    blockLocations.Add(new Vector2(0, 2));
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    break;
                case TriominoShape.L:
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    blockLocations.Add(new Vector2(1, 0));
                    break;
                default:
                    break;
            }

            return blockLocations;
        }

        private static List<Vector2> BuildGridLocation(DoubleShape shape)
        {
            List<Vector2> blockLocations = new List<Vector2>();

            switch (shape)
            {
                case DoubleShape.Horizontal:
                    blockLocations.Add(new Vector2(1, 0));
                    blockLocations.Add(new Vector2(0, 0));
                    break;
                case DoubleShape.Vertical:
                    blockLocations.Add(new Vector2(0, 1));
                    blockLocations.Add(new Vector2(0, 0));
                    break;
                default:
                    break;
            }

            return blockLocations;
        }

        private static List<Vector2> BuildGridLocation()
        {
            List<Vector2> blockLocations = new List<Vector2>();
            blockLocations.Add(new Vector2(0, 0));
            
            return blockLocations;
        }

        private static List<Color> BuildBlockColorList(ShapeType type, ColorGenerator colorGen)
        {
            if (type == ShapeType.Single)
            {
                return BuildColorList(1, colorGen);
            }
            else if (type == ShapeType.Double)
            {
                return BuildColorList(2, colorGen);
            }
            else if (type == ShapeType.Triomino)
            {
                return BuildColorList(3, colorGen);
            }
            else if (type == ShapeType.Tetromino)
            {
                return BuildColorList(4, colorGen);
            }
            else if (type == ShapeType.Pentomino)
            {
                return BuildColorList(5, colorGen);
            }
            else if (type == ShapeType.Square)
            {
                return BuildColorList(4);
            }

            return null;
        }

        private static List<Color> BuildBlockColorList(ShapeType type)
        {
            if (type == ShapeType.Single)
            {
                return BuildColorList(1);
            }
            else if (type == ShapeType.Double)
            {
                return BuildColorList(2);
            }
            else if (type == ShapeType.Triomino)
            {
                return BuildColorList(3);
            }
            else if (type == ShapeType.Tetromino)
            {
                return BuildColorList(4);
            }
            else if (type == ShapeType.Pentomino)
            {
                return BuildColorList(5);
            }
            else if (type == ShapeType.Square)
            {
                return BuildColorList(4);
            }

            return null;
        }

        private static List<Color> BuildColorList(int numColors, ColorGenerator colorGen)
        {
            List<Color> colors = new List<Color>();
            Dictionary<Color, int> colorsUsed = new Dictionary<Color, int>();

            while (colors.Count < numColors)
            {
                Color tempColor = colorGen.NextColor();

                if (!colorsUsed.ContainsKey(tempColor))
                {
                    colorsUsed.Add(tempColor, 1);
                    colors.Add(tempColor);
                }
                else if (colorsUsed[tempColor] < GameConstants.MaxColorPerPiece)
                {
                    colorsUsed[tempColor]++;
                    colors.Add(tempColor);
                }
            }

            return colors;
        }

        private static List<Color> BuildColorList(int numColors)
        {
            List<Color> colors = new List<Color>();
            Dictionary<Color, int> colorsUsed = new Dictionary<Color, int>();

            while(colors.Count < numColors)
            {
                Color tempColor = RandomMath.RandomColor();
                
                if (!colorsUsed.ContainsKey(tempColor))
                {
                    colorsUsed.Add(tempColor, 1);
                    colors.Add(tempColor);
                }
                else if (colorsUsed[tempColor] < GameConstants.MaxColorPerPiece)
                {
                    colorsUsed[tempColor]++;
                    colors.Add(tempColor);
                }
            }

            return colors;
        }
    }
}