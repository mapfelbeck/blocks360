using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using System;
using Utilities;
using Microsoft.Xna.Framework.Storage;

namespace BalloonBlocks
{
    /// <summary>
    /// Sample showing how to manage different game states, with transitions
    /// between menu screens, a loading screen, the game itself, and a pause
    /// menu. This main game class is extremely simple: all the interesting
    /// stuff happens in the ScreenManager component.
    /// </summary>
    public class BlockGame : Game
    {
        GameSettings settings;

#if XBOX360
        GamerServicesComponent servicesComponent;
#endif

        ScreenManager screenManager;

        FrameRateDisplay FPSCounter;
        SafeAreaOverlay safeAreaOverlay;

        GarbageCollectionTracker gcTracker;

        /// <summary>
        /// The main game constructor.
        /// </summary>
        public BlockGame()
        {
            TrialTracker.Initialize();
        
            Content.RootDirectory = "Content";

#if XBOX360
            servicesComponent = new GamerServicesComponent(this);
            Components.Add(servicesComponent);
#endif

#if DEBUG && XBOX360
            Guide.SimulateTrialMode = GameConstants.fakeTrialMode;
#else
            Guide.SimulateTrialMode = false;
#endif
            //this.TargetElapsedTime = TimeSpan.FromMilliseconds(32);

            settings = new GameSettings();
            GameSettings.SetFileName(GameConstants.settingsFile);

            HighScoreTable.LoadDefaults();
            HighScoreTable.SetFileName(GameConstants.highScoreFile);

#if XBOX360
            PlayerManager.Initialize(this, null);
            StorageDeviceManager.Initialize(this);
#endif
            GraphicsConfig.Initialize(this, settings);

#if PROFILE
            IsFixedTimeStep = false;
#endif
            gcTracker = new GarbageCollectionTracker(this);
            Components.Add(gcTracker);
            gcTracker.Enabled = GameConstants.trackGC;
            
            safeAreaOverlay = new SafeAreaOverlay(this);
            safeAreaOverlay.Visible = GlobalConfig.ShowSafeArea;
            Components.Add(safeAreaOverlay);

            AudioManager.Initialize(this, "Content/Audio/BlockGame.xgs",
                "Content/Audio/BlockGame.xwb", "Content/Audio/BlockGame.xsb");

            MusicPlayer.Initialize(this);

            screenManager = new ScreenManager(this);
            Components.Add(screenManager);

            SaveLoad.Initialize(this);

            FPSCounter = new FrameRateDisplay(this);
            FPSCounter.Visible = GlobalConfig.ShowFPSHUD;
            Components.Add(FPSCounter);
#if XBOX360
            GetStorageDevice();
#endif
        }

        protected void GetStorageDevice()
        {
            StorageDeviceManager.RequestStorageDevice(StorageRetrievedXbox);
        }

        protected void StorageRetrievedXbox(StorageDevice device)
        {
            GameSettings.SetFileName(GameConstants.settingsFile);
            HighScoreTable.SetFileName(GameConstants.highScoreFile);

            if (device != null && device.IsConnected)
            {
                settings = GameSettings.Load(device, GameConstants.settingsFile);
                GlobalConfig.LoadSettings(settings);
                CustomGameConfig.LoadSettings(settings);
                AudioManager.LoadSettings(settings);
                MusicPlayer.LoadSettings(settings);
                GraphicsConfig.ApplySetting(settings);

                HighScoreTable.Load(device, GameConstants.highScoreFile);

                RumbleComponent.RumbleActive = settings.RumbleActive;
            }
        }

        protected void LoadDataWindows()
        {
            GameSettings.SetFileName(GameConstants.settingsFile);
            HighScoreTable.SetFileName(GameConstants.highScoreFile);

            settings = GameSettings.Load();
            GlobalConfig.LoadSettings(settings);
            CustomGameConfig.LoadSettings(settings);
            AudioManager.LoadSettings(settings);
            MusicPlayer.LoadSettings(settings);
            GraphicsConfig.ApplySetting(settings);

            HighScoreTable.Load();
        }

        protected override void Initialize()
        {
            base.Initialize();

            RegisterEvents();

            GlobalConfig.LoadSettings(settings);
            CustomGameConfig.LoadSettings(settings);
            GraphicsConfig.ApplySetting(settings);


            RumbleComponent.Initialize(this);
            // initialize the audio system
            AudioManager.LoadSettings(settings);
            MusicPlayer.LoadSettings(settings);

#if WINDOWS
            LoadDataWindows();
#endif

#if PROFILE
            screenManager.AddScreen(new BackgroundScreen("Gameplay Backgrounds/Easy"), null);
            screenManager.AddScreen(BlockGameFactory.CreatePerfTestGame(), null);
#else
            MusicPlayer.PlayTitleTrack();

            if (!GameConstants.skipIntroScreen)
            {
                screenManager.AddScreen(new IntroScreen(), null);
                screenManager.AddScreen(BlockGameFactory.CreateSplashScreenGame(), null);
            }
            else
            {
                screenManager.AddScreen(new BackgroundScreen("Menu Backgrounds/Main"), null);
                screenManager.AddScreen(new MainMenuScreen(), null);
            }
#endif
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            /*if (safeAreaOverlay.Visible)
            {
                safeAreaOverlay.SafeArea = GraphicsConfig.SafeArea;
            }*/
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);

#if XBOX360
            if (StorageDeviceManager.StorageDeviceReady())
            {
                settings.Save(StorageDeviceManager.GetStorageDevice());
                HighScoreTable.Save(StorageDeviceManager.GetStorageDevice());
                //StorageDeviceManager.RequestStorageDevice(settings.Save);
                //StorageDeviceManager.RequestStorageDevice(HighScoreTable.Save);
            }
#else
            settings.Save();
            HighScoreTable.Save();
#endif
        }

        private void saveSettingsCallback(object sender, string theEvent, params object[] args)
        {
#if WINDOWS
            SaveLoad.Trigger(settings.Save, null);
#else
            SaveLoad.Trigger(RequestStorageAndSaveSettings, null);
#endif
        }

        private void enableSafeAreaCallback(object sender, string theEvent, params object[] args)
        {
            if (args != null)
            {
                safeAreaOverlay.Visible = (bool)args[0];
            }
        }


        private void enableFPSDisplayCallback(object sender, string theEvent, params object[] args)
        {
            if (args != null)
            {
                FPSCounter.Visible = (bool)args[0];
            }
        }

        private void RequestStorageAndSaveSettings()
        {
            StorageDeviceManager.RequestStorageDevice(settings.Save);
        }

        void RegisterEvents()
        {
            EventManager.Register(enableFPSDisplayCallback, "EnableDebugDisplay");
            EventManager.Register(enableSafeAreaCallback, "EnableSafeArea");
            EventManager.Register(saveSettingsCallback, "SaveSettings");
        }
    }

    #region Entry Point
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (BlockGame game = new BlockGame())
            {
                game.Run();
            }
        }
    }
    #endregion
}