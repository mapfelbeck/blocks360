﻿using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Storage;
using Utilities;
using System.Diagnostics;
namespace BalloonBlocks
{
    public class GameSettings
    {
        //custom game settings
        public GameType GameMode = GameDefaults.DefaultGameType;
        public GameSpeed Speed = GameDefaults.DefaultGameSpeed;
        public BlockSize BlockScale = GameDefaults.DefaultBlockScale;
        public ShapeType PieceType = GameDefaults.DefaultPieceType;
        public BlockType BlockType = GameDefaults.DefaultBlockType;

        public ControlMode ControlMode = GameDefaults.DefaultControlMode;

        //global game config
        public DrawStyle DrawStyle = GameDefaults.DefaultDrawStyle;
        public bool SoundEnabled = GameDefaults.DefaultSoundEnabled;
        public bool MusicEnabled = GameDefaults.DefaultMusicEnabled;
        public int SoundVolume = GameDefaults.DefaultSoundVolume;
        public int MusicVolume = GameDefaults.DefaultMusicVolume;
        
        //graphics config
        public GraphicsAspectRatio AspectRatioEnum = GameDefaults.DefaultAspectRatio;
        public bool FullScreen = GameDefaults.DefaultFullScreen;
        public int ResolutionIndex = GameDefaults.DefaultResolution;

        //playtest tweakables
        public bool AutoDampRotation = GameDefaults.AutoDampRotation;
        public int AutoDampRate = GameDefaults.AutoDampRate;

        public bool RumbleActive = GameDefaults.RumbleActive;
        public bool IncreasingSpeed = GameDefaults.IncreasingSpeed;

        //saved file name
        private static string settingsFile;

        internal static void SetFileName(string fileName)
        {
            settingsFile = fileName;
        }

        public void Save()
        {
#if XBOX360
            Save(StorageDeviceManager.GetStorageDevice(), settingsFile);
#endif
        }

        public void Save(StorageDevice device)
        {
            Save(device, settingsFile);
        }

        /// <summary>
        /// Saves the current settings
        /// </summary>
        /// <param name="filename">The filename to save to</param>
        private void Save(StorageDevice device, string filename)
        {
            if (device == null || !device.IsConnected)
            {
                return;
            } 
            
            if (filename != null)
            {
                settingsFile = filename;
            }

            GameMode = CustomGameConfig.gameType;
            Speed = CustomGameConfig.gameSpeed;
            BlockScale = CustomGameConfig.blockSize;
            PieceType = CustomGameConfig.pieceShape;
            BlockType = CustomGameConfig.blockType;

            DrawStyle = GlobalConfig.DrawMethod;
            SoundEnabled = GlobalConfig.SoundEnabled;
            MusicEnabled = GlobalConfig.MusicEnabled;
            SoundVolume = GlobalConfig.SoundVolume;
            MusicVolume = GlobalConfig.MusicVolume;

            ControlMode = GlobalConfig.ControlMode;
            AutoDampRate = GlobalConfig.AutoDampRate;
            IncreasingSpeed = GlobalConfig.IncreasingSpeed;
            RumbleActive = GlobalConfig.RumbleActive;

            AspectRatioEnum = GraphicsConfig.AspectRatioEnum;
            FullScreen = GraphicsConfig.IsFullscreen;
            ResolutionIndex = GraphicsConfig.ResolutionIndex;

            StorageContainer container =
                            device.OpenContainer("Balloon Blocks");
            /*StorageContainer container =
                device.OpenContainer(StorageContainer.TitleLocation);*/

            string fullPath = Path.Combine(container.Path, settingsFile);

            Stream stream;
            try
            {
                stream = File.Create(fullPath);

                XmlSerializer serializer = new XmlSerializer(typeof(GameSettings));
                serializer.Serialize(stream, this);
                stream.Close();
            }
            catch
            {
                Trace.WriteLine("Game settings save failed");
            }
            finally
            {
                if (container != null)
                {
                    container.Dispose();
                }
            }
        }

        internal static GameSettings Load()
        {
            GameSettings settings = new GameSettings();
            //throw new System.NotImplementedException();
            return settings;
        }

        public static void Load(StorageDevice device)
        {
            Load(device, settingsFile);
        }

        internal static GameSettings Load(StorageDevice device, string filename)
        {
            GameSettings settings = new GameSettings();

            if (filename != null)
            {
                settingsFile = filename;
            }

            if (device != null && device.IsConnected)
            {
                StorageContainer container =
                                device.OpenContainer("Balloon Blocks");
                /*StorageContainer container =
                    device.OpenContainer(StorageContainer.TitleLocation);*/

                string fullpath = Path.Combine(container.Path, settingsFile);
                try
                {
                    if (File.Exists(fullpath))
                    {
                        Stream stream = null;
                        try
                        {
                            stream = File.OpenRead(fullpath);
                            XmlSerializer serializer = new XmlSerializer(typeof(GameSettings));
                            settings = (GameSettings)serializer.Deserialize(stream);
                        }
                        catch
                        {
                            settings = new GameSettings();
                        }
                        finally
                        {
                            if (stream != null)
                            {
                                stream.Close();
                            }
                        }
                    }
                    else
                    {
                        settings = new GameSettings();
                    }
                }
                catch
                {
                    Trace.WriteLine("Game settings load failed");
                }
                finally
                {
                    if (container != null)
                    {
                        container.Dispose();
                    }
                }
            }

            return settings;
        }
    }
}