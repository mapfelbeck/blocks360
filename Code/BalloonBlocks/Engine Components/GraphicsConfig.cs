﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

namespace BalloonBlocks
{
    public enum GraphicsAspectRatio
    {
        WideScreen,
        Standard,
    }

    public class GraphicsResolution
    {
        private int height;
        public int Height
        {
            get { return height; }
        }

        private int width;
        public int Width
        {
            get { return width; }
        }

        public override string ToString()
        {
            return String.Format("{0}X{1}", width, height);
        }

        public override int GetHashCode()
        {
            return width*height;
        }

        public override bool Equals(object other)
        {
            return (this == (GraphicsResolution)other);
        }

        public static bool operator ==(GraphicsResolution res1, GraphicsResolution res2)
        {
            // Need to call through ReferenceEquals, to prevent recursion
            if (object.ReferenceEquals(res1, null))
                return object.ReferenceEquals(res2, null);

            if (res1.GetType() != res2.GetType())
                return false;

            if (res1.height != res2.height)
                return false;

            return res1.width == res2.width;
        }

        public static bool operator !=(GraphicsResolution res1, GraphicsResolution res2)
        {
            return !(res1 == res2);
        }

        public GraphicsResolution(int resWidth, int resHeight)
        {
            height = resHeight;
            width = resWidth;
        }
    }

    /// <summary>
    /// An interface for changing graphics options
    /// </summary>
    public class GraphicsConfig
    {
        Dictionary<GraphicsAspectRatio, List<GraphicsResolution>> resolutions;

        GraphicsAspectRatio wantedAspectRatioEnum;
        GraphicsAspectRatio aspectRatioEnum;
        public static GraphicsAspectRatio AspectRatioEnum
        {
            get { return Instance.aspectRatioEnum; }
            set { Instance.wantedAspectRatioEnum = value; }
        }

        float aspectRatioFloat;
        public static float AspectRatio
        {
            get { return Instance.aspectRatioFloat; }
        }

        public static bool IsWideScreen
        {
            get { return Instance.aspectRatioEnum == GraphicsAspectRatio.WideScreen; }
        }

        public static GraphicsResolution Resolution
        {
            get { return Instance.resolutions[Instance.aspectRatioEnum][Instance.currentResolutionIndex]; }
        }

        int currentResolutionIndex;
        int wantedResolutionIndex;
        public static int ResolutionIndex
        {
            get { return Instance.currentResolutionIndex; }
            set { Instance.wantedResolutionIndex = value; }
        }

        Matrix view;
        public static Matrix View
        {
            get { return Instance.view; }
        }

        Matrix projection;
        public static Matrix Projection
        {
            get { return Instance.projection; }
        }

        Matrix world;
        public static Matrix World
        {
            get { return Instance.world; }
        }

        bool wantFullscreen;
        bool isFullscreen;
        public static bool IsFullscreen
        {
            get { return Instance.isFullscreen; }
            set { Instance.wantFullscreen = value; }
        }

        private static GraphicsConfig graphicsConfig = null;
        private static GraphicsConfig Instance
        {
            get
            {
                if (graphicsConfig == null)
                {
                    graphicsConfig = new GraphicsConfig();
                }
                return graphicsConfig;
            }
        }

        private GraphicsDeviceManager graphicsDeviceManager;
        public static GraphicsDevice GraphicsDevice
        {
            get { return Instance.graphicsDeviceManager.GraphicsDevice; }
        }
        public static GraphicsDeviceManager DeviceManager
        {
            get { return Instance.graphicsDeviceManager; }
        }

        Rectangle fullscreen;
        public static Rectangle Fullscreen
        {
            get { return Instance.fullscreen; }
        }

        Rectangle safeArea;
        public static Rectangle SafeArea
        {
            get { return Instance.safeArea; }
        }

        public static List<GraphicsResolution> GetResolutions(GraphicsAspectRatio ratio)
        {
            return Instance.resolutions[ratio];
        }

        public static List<GraphicsResolution> GetResolutions()
        {
            return Instance.resolutions[Instance.aspectRatioEnum];
        }

        private GraphicsConfig()
        {
            throw new Exception("Graphics config must be Initialized");
        }

        private GraphicsConfig(Game game, GameSettings settings)
        {
            graphicsDeviceManager = new GraphicsDeviceManager(game);
            graphicsDeviceManager.DeviceReset += DeviceReset;

            resolutions = new Dictionary<GraphicsAspectRatio, List<GraphicsResolution>>();
            
            resolutions.Add(GraphicsAspectRatio.Standard, new List<GraphicsResolution>());
            resolutions.Add(GraphicsAspectRatio.WideScreen, new List<GraphicsResolution>());

#if WINDOWS
            resolutions[GraphicsAspectRatio.Standard].Add(new GraphicsResolution(640, 480));
            resolutions[GraphicsAspectRatio.Standard].Add(new GraphicsResolution(800, 600));
            resolutions[GraphicsAspectRatio.Standard].Add(new GraphicsResolution(1024, 768));
            resolutions[GraphicsAspectRatio.Standard].Add(new GraphicsResolution(1280, 960));
            resolutions[GraphicsAspectRatio.Standard].Add(new GraphicsResolution(1600, 1200));
#else
            resolutions[GraphicsAspectRatio.Standard].Add(new GraphicsResolution(1024, 768));
#endif

#if WINDOWS
            resolutions[GraphicsAspectRatio.WideScreen].Add(new GraphicsResolution(853, 480));
            resolutions[GraphicsAspectRatio.WideScreen].Add(new GraphicsResolution(1280, 720));
            resolutions[GraphicsAspectRatio.WideScreen].Add(new GraphicsResolution(1920, 1080));
#else
            resolutions[GraphicsAspectRatio.WideScreen].Add(new GraphicsResolution(1280, 720));
#endif

            aspectRatioEnum = settings.AspectRatioEnum;
            currentResolutionIndex = settings.ResolutionIndex;
            isFullscreen = settings.FullScreen;

#if PROFILE
            graphicsDeviceManager.SynchronizeWithVerticalRetrace = false;
#endif
            if (GameConstants.multiSampling)
            {
                InitializeMultiSampling();
            }
        }

        public static void Initialize(Game game, GameSettings settings)
        {
            graphicsConfig = new GraphicsConfig(game, settings);

            ApplyChanges();
        }

        internal static void ApplySetting(GameSettings settings)
        {
            AspectRatioEnum = settings.AspectRatioEnum;
            ResolutionIndex = settings.ResolutionIndex;
            IsFullscreen = settings.FullScreen;
            //Instance.aspectRatioEnum = settings.AspectRatioEnum;
            //Instance.currentResolutionIndex = settings.ResolutionIndex;
            //Instance.isFullscreen = settings.FullScreen;

            ApplyChanges();
        }

        public static void ApplyChanges()
        {
            if (Instance.isFullscreen == Instance.wantFullscreen &&
                Instance.currentResolutionIndex == Instance.wantedResolutionIndex &&
                Instance.aspectRatioEnum == Instance.wantedAspectRatioEnum)
            {
                return;
            }
            else
            {
                Instance.isFullscreen = Instance.wantFullscreen;
                Instance.currentResolutionIndex = Instance.wantedResolutionIndex;
                Instance.aspectRatioEnum = Instance.wantedAspectRatioEnum;
            }

            Instance.graphicsDeviceManager.PreferredBackBufferWidth = Resolution.Width;
            Instance.graphicsDeviceManager.PreferredBackBufferHeight = Resolution.Height;
            Instance.graphicsDeviceManager.IsFullScreen = Instance.isFullscreen;

            Instance.graphicsDeviceManager.ApplyChanges();

            Instance.view = Matrix.CreateLookAt(new Vector3(0, 0, 10), Vector3.Zero, Vector3.Up);
            Instance.world = Matrix.Identity;

            float standardScale = 11f;
            float widescreenScale = 3.375f;

            switch (Instance.aspectRatioEnum)
            {
                case GraphicsAspectRatio.Standard:
                    Instance.aspectRatioFloat = 4f / 3f;
                    Instance.projection = Matrix.CreateOrthographic(
                        4f * standardScale, 3f * standardScale, -10f, 10f);
                    break;
                case GraphicsAspectRatio.WideScreen:
                    Instance.aspectRatioFloat = 16f / 9f;
                    Instance.projection = Matrix.CreateOrthographic(
                        16f * widescreenScale, 9f * widescreenScale, -10f, 10f);
                    break;
            }

            Viewport viewport = Instance.graphicsDeviceManager.GraphicsDevice.Viewport;
            Instance.safeArea = viewport.TitleSafeArea;
            Instance.fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);

#if WINDOWS
            Instance.safeArea.X = viewport.Width / 10;
            Instance.safeArea.Y = viewport.Height / 10;
            Instance.safeArea.Width = (8 * viewport.Width) / 10;
            Instance.safeArea.Height = (8 * viewport.Height) / 10;
#endif
        }

        public static Vector2 WorldToScreenCoordinates(Vector2 worldCoordinate)
        {
            Vector3 worldCoordinateV3 = new Vector3(worldCoordinate, 0);
            Vector3 where = Instance.graphicsDeviceManager.GraphicsDevice.Viewport.Project(
                worldCoordinateV3, Projection, View, World);

            return new Vector2(where.X, where.Y);
        }

        public static Vector2 WorldToScreenCoordinates(Vector3 worldCoordinate)
        {
            Vector3 where = Instance.graphicsDeviceManager.GraphicsDevice.Viewport.Project(
                worldCoordinate, Projection, View, World);

            return new Vector2(where.X, where.Y);
        }

        public static Vector3 ScreenToWorldCoordinates(Vector3 screenCoordinate)
        {
            Vector3 where = Instance.graphicsDeviceManager.GraphicsDevice.Viewport.Unproject(
                screenCoordinate, Projection, View, World);

            return where;
        }

        public static Vector2 ScreenToWorldCoordinates(Vector2 screenCoordinate)
        {
            Vector3 where = Instance.graphicsDeviceManager.GraphicsDevice.Viewport.Unproject(
                VectorTools.vec3FromVec2(screenCoordinate), Projection, View, World);

            return new Vector2(where.X, where.Y);
        }

        internal static void SwitchAspectRatio()
        {
            Instance.aspectRatioEnum++;

            if (Instance.aspectRatioEnum > GraphicsAspectRatio.Standard)
            {
                Instance.aspectRatioEnum = GraphicsAspectRatio.WideScreen;
            }

            Instance.currentResolutionIndex = 0;
        }

        internal static void ResolutionUp()
        {
            Instance.currentResolutionIndex++;
            Instance.currentResolutionIndex =
                Instance.currentResolutionIndex % Instance.resolutions[Instance.aspectRatioEnum].Count;
        }

        internal static void ResolutionDown()
        {
            Instance.currentResolutionIndex--;
            if (Instance.currentResolutionIndex < 0)
            {
                Instance.currentResolutionIndex =
                    Instance.resolutions[Instance.aspectRatioEnum].Count - 1;
            }
        }

        internal static void SwitchFullScreen()
        {
            Instance.isFullscreen = !Instance.isFullscreen;
        }

        private void DeviceReset(object sender, EventArgs args)
        {
            GraphicsDeviceManager device = sender as GraphicsDeviceManager;

            GraphicsResolution attemptedResolution = new GraphicsResolution(
                device.PreferredBackBufferWidth, device.PreferredBackBufferHeight);
            GraphicsResolution actualResolution = new GraphicsResolution(
                device.GraphicsDevice.PresentationParameters.BackBufferWidth, device.GraphicsDevice.PresentationParameters.BackBufferHeight);

            aspectRatioFloat = (float)device.GraphicsDevice.PresentationParameters.BackBufferWidth / device.GraphicsDevice.PresentationParameters.BackBufferHeight;

            if (attemptedResolution != actualResolution)
            {
                //FIXME: need to throw a popup for bad resolution switch 
                //and that confirms a good one
                Console.WriteLine("Bad resolution switch!");
            }
        }

        protected void InitializeMultiSampling()
        {
            graphicsDeviceManager.PreferMultiSampling = true;      // Enables anti-aliasing
            graphicsDeviceManager.PreparingDeviceSettings +=
            new EventHandler<PreparingDeviceSettingsEventArgs>(graphics_PreparingDeviceSettings);
        }

        void graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
#if XBOX
            e.GraphicsDeviceInformation.PresentationParameters.MultiSampleQuality = 0;
            e.GraphicsDeviceInformation.PresentationParameters.MultiSampleType =
                MultiSampleType.FourSamples;
            return;
#elif WINDOWS
            int quality = 0;
            GraphicsAdapter adapter = e.GraphicsDeviceInformation.Adapter;
            SurfaceFormat format = adapter.CurrentDisplayMode.Format;
            // Check for 4xAA
            if (adapter.CheckDeviceMultiSampleType(DeviceType.Hardware, format,
                false, MultiSampleType.FourSamples, out quality))
            {
                // even if a greater quality is returned, we only want quality 0
                e.GraphicsDeviceInformation.PresentationParameters.MultiSampleQuality = 0;
                e.GraphicsDeviceInformation.PresentationParameters.MultiSampleType =
                    MultiSampleType.FourSamples;
            }
            // Check for 2xAA
            else if (adapter.CheckDeviceMultiSampleType(DeviceType.Hardware, format,
                false, MultiSampleType.TwoSamples, out quality))
            {
                // even if a greater quality is returned, we only want quality 0
                e.GraphicsDeviceInformation.PresentationParameters.MultiSampleQuality = 0;
                e.GraphicsDeviceInformation.PresentationParameters.MultiSampleType =
                    MultiSampleType.TwoSamples;
            }
#endif
        }
    }
}