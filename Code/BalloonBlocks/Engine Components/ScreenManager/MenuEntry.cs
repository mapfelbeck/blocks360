#region File Description
//-----------------------------------------------------------------------------
// MenuEntry.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Utilities;
using System.Collections.Generic;
using System.Diagnostics;
#endregion

namespace BalloonBlocks
{
    public enum MenuEntryAlignment
    {
        Left,
        Center,
    };

    public enum MenuEntryType
    {
        Normal,
        Back,
        Option
    };

    /// <summary>
    /// Helper class represents a single entry in a MenuScreen. By default this
    /// just draws the entry text string, but it can be customized to display menu
    /// entries in different ways. This also provides an event that will be raised
    /// when the menu entry is selected.
    /// </summary>
    class MenuEntry
    {
        /// <summary>
        /// The text rendered for this entry.
        /// </summary>
        protected string text;

        protected string selectCue;

        protected MenuEntryAlignment alignment;

        /// <summary>
        /// Tracks a fading selection effect on the entry.
        /// </summary>
        /// <remarks>
        /// The entries transition out of the selection effect when they are deselected.
        /// </remarks>
        protected float selectionFade;

        protected float scale;

        /// <summary>
        /// Gets or sets the text of this menu entry.
        /// </summary>
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        string normalDetailText;
        public string NormalDetailText
        {
            get { return normalDetailText; }
            set { normalDetailText = value; }
        }

        string trialDetailText;
        public string TrialDetailText
        {
            get { return trialDetailText; }
            set { trialDetailText = value; }
        }

        public string DetailText
        {
            get
            {
#if XBOX360
                if (IsTrial && trialDetailText != null)
                {
                    return trialDetailText;
                }
                else
                {
#endif
                    return normalDetailText;
#if XBOX360
                }
#endif
            }
        }

        bool isTrial;
        public bool IsTrial{
            get{return isTrial;}
            set{isTrial = value;
                SetColors();
            }
        }

        protected float repeatRate;
        public float RepeatRate
        {
            get { return repeatRate; }
            set { repeatRate = value; }
        }

        protected bool holdToRepeat;
        public bool HoldToRepeat
        {
            get { return holdToRepeat; }
            set { holdToRepeat = value; }
        }

        protected Color NonTrialNormalColor = Color.White;
        protected Color NonTrialSelectedColor = Color.Yellow;

        protected Color TrialNormalColor = Color.LightGray;
        protected Color TrialSelectedColor = Color.DarkGray;

        protected Color normalColor;
        protected Color selectedColor;

        protected bool currentlySelected;
        public bool CurrentlySelected
        {
            get { return currentlySelected; }
        }
        public Color TextColor
        {
            get
            {
                if (currentlySelected)
                {
                    return selectedColor;
                }
                else
                {
                    return normalColor;
                }
            }
        }

        protected virtual void SetColors()
        {
#if XBOX360
            if (!isTrial)
            {
#endif
                normalColor = NonTrialNormalColor;
                selectedColor = NonTrialSelectedColor;
#if XBOX360
            }
            else
            {
                normalColor = TrialNormalColor;
                selectedColor = TrialSelectedColor;
            }
#endif
        } 

        /// <summary>
        /// Event raised when the menu entry is selected.
        /// </summary>
        public event EventHandler<PlayerIndexEventArgs> Selected;
        public event EventHandler<PlayerIndexEventArgs> IncrementEntry;
        public event EventHandler<PlayerIndexEventArgs> DecrementEntry;

        /// <summary>
        /// Method for raising the Selected event.
        /// </summary>
        protected internal virtual void OnSelectEntry(PlayerIndex playerIndex)
        {
#if XBOX360
            if (IsTrial)
            {
                EventManager.Trigger(this, selectCue);
                UpSellSelected(this, new PlayerIndexEventArgs(playerIndex));
            }
            else if (Selected != null)
            {
#endif
                EventManager.Trigger(this, selectCue);
                Selected(this, new PlayerIndexEventArgs(playerIndex));
#if XBOX360
            }
#endif
        }

        PlayerIndex index;
        /// <summary>
        /// Event handler for when the Easy Game menu entry is selected.
        /// </summary>
        void UpSellSelected(object sender, PlayerIndexEventArgs e)
        {
            index = e.PlayerIndex;
            if (PlayerManager.PlayerCanBuyContent(index) &&
                !Guide.IsVisible)
            {
                SellStuff();
            }
            else if(!PlayerManager.PlayerCanBuyContent(index))
            {
                PlayerManager.ShowSignIn(SellStuff);
            }
        }

        protected void SellStuff()
        {
            if (PlayerManager.PlayerCanBuyContent(index) &&
                !Guide.IsVisible)
            {
                Guide.ShowMarketplace(index);
            }
        }

        protected internal virtual void OnIncrementEntry(PlayerIndex playerIndex)
        {
            if (IncrementEntry != null)
            {
                EventManager.Trigger(this, selectCue);
                IncrementEntry(this, new PlayerIndexEventArgs(playerIndex));
            }
        }

        protected internal virtual void OnDecrementEntry(PlayerIndex playerIndex)
        {
            if (DecrementEntry != null)
            {
                EventManager.Trigger(this, selectCue);
                DecrementEntry(this, new PlayerIndexEventArgs(playerIndex));
            }
        }

        public MenuEntry(string text, bool isTrialEntry) :
            this(text, MenuEntryAlignment.Left, isTrialEntry, MenuEntryType.Normal) { }

        public MenuEntry(string text) :
            this(text, MenuEntryAlignment.Left, false, MenuEntryType.Normal) { }

        public MenuEntry(string text, MenuEntryAlignment align, MenuEntryType type) :
            this(text, align, false, type) { }

        public MenuEntry(string text, MenuEntryAlignment align) :
            this(text, align, false, MenuEntryType.Normal) { }

        public MenuEntry(string text, MenuEntryAlignment align, bool isTrialEntry) :
            this(text, align, isTrialEntry, MenuEntryType.Normal)
        { }

        public MenuEntry(string text, MenuEntryAlignment align, bool isTrialEntry, MenuEntryType type)
        {
            this.text = text;
            alignment = align;
            scale = .9f;
            isTrial = isTrialEntry;
            SetColors();

            switch (type)
            {
                case MenuEntryType.Normal:
                    selectCue = "MenuAdvance";
                    break;
                case MenuEntryType.Back:
                    selectCue = "MenuBack";
                    break;
                case MenuEntryType.Option:
                    selectCue = "SliderMove";
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Updates the menu entry.
        /// </summary>
        public virtual void Update(MenuScreen screen, bool isSelected,
                                                      GameTime gameTime)
        {
            // When the menu selection changes, entries gradually fade between
            // their selected and deselected appearance, rather than instantly
            // popping to the new state.
            float fadeSpeed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            scale = Math.Min(1f, scale + fadeSpeed * .5f);

            currentlySelected = isSelected;

            if (isSelected)
                selectionFade = Math.Min(selectionFade + fadeSpeed*4, 1);
            else
                selectionFade = Math.Max(selectionFade - fadeSpeed*3, 0);
            
#if XBOX360
            if (IsTrial)
            {
                IsTrial = Guide.IsTrialMode;
            }
#endif
        }


        /// <summary>
        /// Draws the menu entry. This can be overridden to customize the appearance.
        /// </summary>
        public virtual void Draw(MenuScreen screen, Vector2 position, Vector2 offset,
                                 bool isSelected, GameTime gameTime)
        {
            Color drawColor;
            Color shadowColor = Color.Black;
            if (isSelected)
            {
                drawColor = selectedColor;
            }
            else
            {
                drawColor = normalColor;
            }

            // Draw text, centered on the middle of each line.
            ScreenManager screenManager = screen.ScreenManager;
            SpriteBatch spriteBatch = screenManager.SpriteBatch;
            SpriteFont font = screenManager.Font;
            Vector2 textSize = font.MeasureString(text);
            float drawScale = scale + 0.1f * selectionFade;

            switch (alignment)
            {
                case MenuEntryAlignment.Center:
                    Vector2 transform = new Vector2();
                    transform.X = ((GraphicsConfig.GraphicsDevice.Viewport.Width) / 2);
                    transform.X -= (textSize.X / 2)*drawScale;
                    position.X += transform.X - position.X;
                    break;
                case MenuEntryAlignment.Left:
                    position.X += GraphicsConfig.SafeArea.X * 1.25f;
                    break;
                default:
                    break;
            }
            position += offset;
            
            // Pulsate the size of the selected menu entry.
            //double time = gameTime.TotalGameTime.TotalSeconds;

            // Modify the alpha to fade text out during transitions.
            drawColor = new Color(drawColor.R, drawColor.G, drawColor.B, screen.TransitionAlpha);
            shadowColor.A = (byte)(Math.Pow((double)screen.TransitionAlpha / (double)255, 2) * 255);

            Vector2 origin = new Vector2(0, font.LineSpacing / 2);
            
            spriteBatch.DrawString(font, text, new Vector2(position.X - 1f, position.Y - 1f), shadowColor, 0,
                                   origin, drawScale, SpriteEffects.None, 0);
            spriteBatch.DrawString(font, text, position, drawColor, 0,
                                   origin, drawScale, SpriteEffects.None, 0);
        }

        /// <summary>
        /// Queries how much space this menu entry requires.
        /// </summary>
        public virtual int GetHeight(MenuScreen screen)
        {
            return screen.ScreenManager.Font.LineSpacing;
        }
    }
}
