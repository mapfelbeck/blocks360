﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using System;
using System.Diagnostics;

namespace BalloonBlocks
{
    /// <summary>
    /// Component that manages audio playback for all cues.
    /// </summary>
    public class MusicPlayer : DrawableGameComponent
    {
        ContentManager content;
        private static MusicPlayer musicPlayer;

        private bool musicEnabled;
        private float desiredMusicVolume;
        private float currentMusicVolume;
        private static float volumeChangePerSecond = .25f;

        Song[] musicTracks;
        Song currentTrack;

        string[] musicTrackNames = { "Audio/Music/Title/Tech Talk", 
                                     "Audio/Music/Easy/Highlight Reel",
                                     "Audio/Music/Easy/Pilot Error",
                                     "Audio/Music/Medium/Aitech",
                                     "Audio/Music/Medium/Variation on Egmont",
                                     "Audio/Music/Hard/Chipper",
                                     "Audio/Music/Hard/Eighties Action"};

        private MusicPlayer(Game game)
            : base(game)
        {
            content = new ContentManager(game.Services);
            content.RootDirectory = "Content";

            musicTracks = new Song[musicTrackNames.Length];
            currentTrack = null;
            try
            {
                musicEnabled = GlobalConfig.MusicEnabled;

                if (!musicEnabled)
                {
                    MediaPlayer.Stop();
                }
                desiredMusicVolume = (float)GlobalConfig.MusicVolume / 100f;

                currentMusicVolume = 0f;
                MediaPlayer.Volume = currentMusicVolume;

                MediaPlayer.ActiveSongChanged += new EventHandler(activeSongChangedCallback);
            }
            catch (NoAudioHardwareException)
            {
                SetMusicEnabled(false);
            }
        }

        void activeSongChangedCallback(object sender, EventArgs e)
        {
            if (musicEnabled && MediaPlayer.GameHasControl && MediaPlayer.State == MediaState.Stopped)
            {
                PlayRandomTrack();
            }
        }

        public static void Initialize(Game game)
        {
            musicPlayer = new MusicPlayer(game);

            if (game != null)
            {
                game.Components.Add(musicPlayer);
                musicPlayer.RegisterEvents();
            }
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            if (musicTracks != null)
            {
                for (int i = 0; i < musicTrackNames.Length; i++)
                {
                    musicTracks[i] = content.Load<Song>(musicTrackNames[i]);
                }
            }
        }

        internal static void SetMusicEnabled(bool musicEnabled)
        {
            bool musicWasEnabled = musicPlayer.musicEnabled;
            musicPlayer.musicEnabled = musicEnabled;

            if (musicWasEnabled && !musicPlayer.musicEnabled)
            {
                musicPlayer.currentTrack = null;
                MediaPlayer.Stop();
            }
            else if (musicPlayer.musicEnabled)
            {
                PlayTitleTrack();
            }
        }

        internal static void SetMusicVolume(int volume)
        {
            musicPlayer.desiredMusicVolume = (float)volume / 100f;
        }

        public static void PlayTrack(Song track)
        {
            if (musicPlayer.musicEnabled && MediaPlayer.GameHasControl)
            {
                if (track != musicPlayer.currentTrack)
                {
                    musicPlayer.currentTrack = track;
                    MediaPlayer.Play(track);
                }
            }
        }

        /// <summary>
        /// Update the audio manager, particularly the engine.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(musicEnabled && (currentMusicVolume != desiredMusicVolume)){
                float volumeDifference = desiredMusicVolume - currentMusicVolume;
                float maxVolumeDelta = elapsed * volumeChangePerSecond;

                currentMusicVolume = MathHelper.Clamp(desiredMusicVolume, currentMusicVolume - maxVolumeDelta, currentMusicVolume + maxVolumeDelta);
                MediaPlayer.Volume = currentMusicVolume;
            }

            base.Update(gameTime);
        }

        public static void PlayTitleTrack()
        {
            PlayTrack(musicPlayer.musicTracks[0]);
        }

        public static void PlayEasyTrack()
        {
            PlayTrack(musicPlayer.musicTracks[RandomMath.Random.Next(1,3)]);
        }

        public static void PlayMediumTrack()
        {
            PlayTrack(musicPlayer.musicTracks[RandomMath.Random.Next(3, 5)]);
        }

        public static void PlayHardTrack()
        {
            PlayTrack(musicPlayer.musicTracks[RandomMath.Random.Next(5, 7)]);
        }

        public static void PlayRandomTrack()
        {
            PlayTrack(musicPlayer.RandomTrack());
        }

        private Song RandomTrack()
        {
            int trackNumber = RandomMath.Random.Next(musicTracks.Length);
            return musicTracks[trackNumber];
        }

        /// <summary>
        /// Clean up the component when it is disposing.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                }
            }
            finally
            {
                UnregisterEvents();
                base.Dispose(disposing);
            }
        }

        protected void RegisterEvents()
        {
        }

        protected void UnregisterEvents()
        {
        }

        internal static void LoadSettings(GameSettings settings)
        {
            SetMusicEnabled(settings.MusicEnabled);
            SetMusicVolume(settings.MusicVolume);
        }
    }
}