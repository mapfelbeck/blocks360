﻿#region Using Statements
using System.Collections.Generic;
using System;

#endregion

namespace BalloonBlocks
{
    public delegate void EventCallBack(object sender, string theEvent, params object[] args);

    public class EventManager
    {
        /// <summary>
        /// The EventManager object used for all of the EventManager calls.
        /// </summary>
        private static EventManager eventManager = null;
        static EventManager Instance
        {
            get
            {
                if (eventManager == null)
                {
                    eventManager = new EventManager();
                }
                return eventManager;
            }
        }

        private Dictionary<string, EventCallBack> callbackTable;
        
        protected EventManager()
        {
            callbackTable = new Dictionary<string, EventCallBack>();
        }

        public static void Register(EventCallBack callback, string theEvent)
        {
            Instance.Add(callback, theEvent);
        }

        public static void UnRegister(EventCallBack callback, string theEvent)
        {
            Instance.Remove(callback, theEvent);
        }

        public static void Trigger(object sender, string theEvent, params object[] args)
        {
            Instance.ActivateEvent(sender, theEvent, args);
        }

        private void Add(EventCallBack callback, string theEvent)
        {
            if (!callbackTable.ContainsKey(theEvent))
            {
                callbackTable.Add(theEvent, callback);
            }
            else
            {
                callbackTable[theEvent] += callback;
            }
        }

        private void Remove(EventCallBack callback, string theEvent)
        {
            if (callbackTable.ContainsKey(theEvent))
            {
                callbackTable[theEvent] -= callback;
                if (callbackTable[theEvent] == null)
                {
                    callbackTable.Remove(theEvent);
                }
            }
        }

        private void ActivateEvent(object sender, string theEvent, params object[] args)
        {
            if (callbackTable.ContainsKey(theEvent))
            {
                callbackTable[theEvent](sender, theEvent, args);
            }
        }
    }
}