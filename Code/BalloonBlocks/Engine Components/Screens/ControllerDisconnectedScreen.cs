﻿using System;
using Microsoft.Xna.Framework;

namespace BalloonBlocks
{
    class ControllerDisconnectedMessageBox : MessageBoxScreen
    {
        const string disconnectedMessage =
            "Controller disconnected,[NL]please reconnect the controller or[NL]pick up a different one and[NL]then press a button.";
        
        public ControllerDisconnectedMessageBox()
            : base(disconnectedMessage, false)
        { }

        public override void HandleInput(InputState input)
        {
            PlayerIndex playerIndex;

            if (input.IsAnyButtonPressed(ControllingPlayer, out playerIndex))
            {
                Accept(playerIndex);
                ExitScreen();
            }
        }
    }
}