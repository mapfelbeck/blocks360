#region File Description
//-----------------------------------------------------------------------------
// PauseMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Utilities;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// The pause menu comes up over the top of the game,
    /// giving the player options to resume or quit.
    /// </summary>
    class PauseMenuScreen : PressureBodyMenuScreen
    {
        MenuEntry purchaseMenuEntry;

        /// <summary>
        /// Constructor.
        /// </summary>
        public PauseMenuScreen()
            : base("Paused")
        {
            // Flag that there is no need for the game to transition
            // off when the pause menu is on top of it.
            IsPopup = true;

            // Create our menu entries.
            MenuEntry resumeGameMenuEntry = CreateMenuEntry("Resume Game", MenuEntryAlignment.Center);
            purchaseMenuEntry = CreateMenuEntry("Buy Balloon Blocks!", MenuEntryAlignment.Center);
            MenuEntry quitGameMenuEntry = CreateMenuEntry("Quit Game", MenuEntryAlignment.Center);
            
            // Hook up menu event handlers.
            resumeGameMenuEntry.Selected += OnCancel;
            purchaseMenuEntry.Selected += PurchaseMeuEntrySelected;
            quitGameMenuEntry.Selected += QuitGameMenuEntrySelected;

            // Add entries to the menu.
#if XBOX360
            if (Guide.IsTrialMode)
            {
                AddMenuEntry(purchaseMenuEntry);
            }
#endif
            AddMenuEntry(resumeGameMenuEntry);
            AddMenuEntry(quitGameMenuEntry);
        }

        public override void LoadContent()
        {
            menuEntrySpacingAdjust = -6f;
            base.LoadContent();
        }

        /// <summary>
        /// Event handler for when the Quit Game menu entry is selected.
        /// </summary>
        void QuitGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            const string message = "Quit this game?";

            MessageBoxScreen confirmQuitMessageBox = ScreenManager.GetMessageBox(message);

            confirmQuitMessageBox.Accepted += ConfirmQuitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmQuitMessageBox, ControllingPlayer);
        }

        void PurchaseMeuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ControllingPlayer = e.PlayerIndex;
            if (PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value) &&
                !Guide.IsVisible)
            {
                SellStuff();
            }
            else if (!PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value))
            {
                PlayerManager.ShowSignIn(SellStuff);
            }
        }

        protected void SellStuff()
        {
            if (PlayerManager.PlayerCanBuyContent(ControllingPlayer.Value) &&
                !Guide.IsVisible)
            {
                Guide.ShowMarketplace(ControllingPlayer.Value);
            }
        }

        /// <summary>
        /// Event handler for when the user selects ok on the "are you sure
        /// you want to quit" message box. This uses the loading screen to
        /// transition from the game back to the main menu screen.
        /// </summary>
        void ConfirmQuitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, null, 
                new BackgroundScreen("Menu Backgrounds/Blank"),
                new SinglePlayerModeScreen());
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (!Guide.IsTrialMode && MenuEntries.Contains(purchaseMenuEntry))
            {
                RemoveMenuEntry(purchaseMenuEntry);
            }
        }

        /// <summary>
        /// Draws the pause menu screen. This darkens down the gameplay screen
        /// that is underneath us, and then chains to the base MenuScreen.Draw.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            base.Draw(gameTime);
        }
    }
}
