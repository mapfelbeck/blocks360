﻿using System;

namespace BalloonBlocks
{
    class KeyBoardMessageBoxScreen : MessageBoxScreen
    {
        public KeyBoardMessageBoxScreen(string message)
            : this(message, true)
        { }

        public KeyBoardMessageBoxScreen(string message, bool includeUsageText)
            : base(message, "Enter: Ok", "Esc: Cancel")
        {
        }
    }
}
