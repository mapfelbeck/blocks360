#region File Description
//-----------------------------------------------------------------------------
// MenuScreen.cs
//
// XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// Base class for screens that contain a menu of options. The user can
    /// move up and down to select an entry, or cancel to back out of the screen.
    /// </summary>
    abstract class MenuScreen : GameScreen
    {
        List<MenuEntry> menuEntries = new List<MenuEntry>();
        /// <summary>
        /// Gets the list of menu entries, so derived classes can add
        /// or change the menu contents.
        /// </summary>
        protected IList<MenuEntry> MenuEntries
        {
            get { return menuEntries; }
        }

        int selectedEntry = 0;
        string menuTitle;
        public string MenuTitle
        {
            get { return menuTitle; }
        }

        string menuSubTitle;
        public string MenuSubTitle
        {
            get { return menuSubTitle; }
            set { menuSubTitle = value; }
        }

        bool menuTextChanged;
        protected bool MenuTextChanged
        {
            get { return menuTextChanged; }
            set { menuTextChanged = value; }
        }

        protected string backCue;

        protected Vector2 titlePositionAdjust;
        protected Vector2 subTitlePositionAdjust;
        protected Vector2 menuPositionAdjust;
        protected Vector2 detailPositionAdjust;
        protected float menuEntrySpacingAdjust;

        protected TransitionDirection titleTransition;
        public TransitionDirection TitleTransition
        {
            get { return titleTransition; }
            set { titleTransition = value; }
        }

        protected TransitionDirection menuEntryTransition;
        public TransitionDirection MenuEntryTransition
        {
            get { return menuEntryTransition; }
            set { menuEntryTransition = value; }
        }

        protected TransitionDirection detailTransition;
        public TransitionDirection DetailTransition
        {
            get { return detailTransition; }
            set { detailTransition = value; }
        }

        float buttonHoldTime;

        /// <summary>
        /// Constructor.
        /// </summary>
        public MenuScreen(string menuTitle)
            : base()
        {
            this.menuTitle = menuTitle;

            menuSubTitle = "";

            backCue = "MenuBack";

            titlePositionAdjust = Vector2.Zero;
            subTitlePositionAdjust = Vector2.Zero;
            menuPositionAdjust = Vector2.Zero;
            detailPositionAdjust = Vector2.Zero;
            menuEntrySpacingAdjust = 0f;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            titleTransition = TransitionDirection.Top;
            menuEntryTransition = TransitionDirection.None;
            detailTransition = TransitionDirection.Bottom;

            menuTextChanged = true;
        }

        protected virtual void SetMenuEntryText()
        {
            menuTextChanged = false;
        }

        /// <summary>
        /// Responds to user input, changing the selected entry and accepting
        /// or cancelling the menu.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            // Move to the previous menu entry?
            if (input.IsMenuUp(ControllingPlayer))
            {
                EventManager.Trigger(this, "MenuScroll");
                selectedEntry--;

                if (selectedEntry < 0)
                    selectedEntry = menuEntries.Count - 1;
            }

            // Move to the next menu entry?
            if (input.IsMenuDown(ControllingPlayer))
            {
                EventManager.Trigger(this, "MenuScroll");
                selectedEntry++;

                if (selectedEntry >= menuEntries.Count)
                    selectedEntry = 0;
            }

            // Accept or cancel the menu? We pass in our ControllingPlayer, which may
            // either be null (to accept input from any player) or a specific index.
            // If we pass a null controlling player, the InputState helper returns to
            // us which player actually provided the input. We pass that through to
            // OnSelectEntry and OnCancel, so they can tell which player triggered them.
            PlayerIndex playerIndex;

            if (input.IsMenuSelect(ControllingPlayer, out playerIndex))
            {
                OnSelectEntry(selectedEntry, playerIndex);
            }
            else if (input.IsMenuCancel(ControllingPlayer, out playerIndex))
            {
                EventManager.Trigger(this, backCue);
                OnCancel(playerIndex);
            }
            else if (input.IsMenuIncrement(ControllingPlayer, out playerIndex))
            {
                OnIncrementEntry(selectedEntry, playerIndex);
            }
            else if (input.IsMenuDecrement(ControllingPlayer, out playerIndex))
            {
                OnDecrementEntry(selectedEntry, playerIndex);
            }
            else if (menuEntries[selectedEntry].HoldToRepeat)
            {
                if (input.IsMenuSelectHeld(ControllingPlayer, out playerIndex))
                {
                    buttonHoldTime += 1f / 60f;
                    if (buttonHoldTime > menuEntries[selectedEntry].RepeatRate)
                    {
                        buttonHoldTime = 0f;
                        OnSelectEntry(selectedEntry, playerIndex);
                    }
                }else if (input.IsMenuIncrementHeld(ControllingPlayer, out playerIndex))
                {
                    buttonHoldTime += 1f / 60f;
                    if (buttonHoldTime > menuEntries[selectedEntry].RepeatRate)
                    {
                        buttonHoldTime = 0f;
                        OnIncrementEntry(selectedEntry, playerIndex);
                    }
                }
                else if (input.IsMenuDecrementHeld(ControllingPlayer, out playerIndex))
                {
                    buttonHoldTime += 1f / 60f;
                    if (buttonHoldTime > menuEntries[selectedEntry].RepeatRate)
                    {
                        buttonHoldTime = 0f;
                        OnDecrementEntry(selectedEntry, playerIndex);
                    }
                }
                else
                {
                    buttonHoldTime = 0f;
                }
            }
        }


        /// <summary>
        /// Handler for when the user has chosen a menu entry.
        /// </summary>
        protected virtual void OnSelectEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[selectedEntry].OnSelectEntry(playerIndex);
        }

        protected virtual void OnIncrementEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[selectedEntry].OnIncrementEntry(playerIndex);
        }

        protected virtual void OnDecrementEntry(int entryIndex, PlayerIndex playerIndex)
        {
            menuEntries[selectedEntry].OnDecrementEntry(playerIndex);
        }


        /// <summary>
        /// Handler for when the user has cancelled the menu.
        /// </summary>
        protected virtual void OnCancel(PlayerIndex playerIndex)
        {
            //EventManager.Trigger(this, "MenuBack");
            ExitScreen();
        }


        /// <summary>
        /// Helper overload makes it easy to use OnCancel as a MenuEntry event handler.
        /// </summary>
        protected void OnCancel(object sender, PlayerIndexEventArgs e)
        {
            OnCancel(e.PlayerIndex);
        }

        protected MenuEntry CreateMenuEntry(string text)
        {
            return CreateMenuEntry(text, MenuEntryAlignment.Left, false, MenuEntryType.Normal);
        }

        protected MenuEntry CreateMenuEntry(string text, MenuEntryAlignment align)
        {
            return CreateMenuEntry(text, align, false, MenuEntryType.Normal);
        }

        protected MenuEntry CreateMenuEntry(string text, bool isTrial)
        {
            return CreateMenuEntry(text, MenuEntryAlignment.Left, isTrial, MenuEntryType.Normal);
        }

        protected MenuEntry CreateMenuEntry(string text, MenuEntryAlignment align, bool isTrial)
        {
            return CreateMenuEntry(text, MenuEntryAlignment.Left, isTrial, MenuEntryType.Normal);
        }

        protected virtual MenuEntry CreateMenuEntry(string text, MenuEntryAlignment align, bool isTrial, MenuEntryType type)
        {
            MenuEntry entry = new MenuEntry(text, align, isTrial, type);
            return entry;
        }

        protected MenuEntry CreateBackMenuEntry(string text)
        {
            return CreateBackMenuEntry(text, MenuEntryAlignment.Left);
        }

        protected MenuEntry CreateBackMenuEntry(string text, MenuEntryAlignment align)
        {
            return CreateMenuEntry(text, align, false, MenuEntryType.Back);
        }

        protected MenuEntry CreateOptionMenuEntry(string text)
        {
            return CreateOptionMenuEntry(text, MenuEntryAlignment.Left);
        }

        protected MenuEntry CreateOptionMenuEntry(string text, MenuEntryAlignment align)
        {
            return CreateMenuEntry(text, align, false, MenuEntryType.Option);
        }

        protected virtual void AddMenuEntry(MenuEntry entry)
        {
            MenuEntries.Add(entry);
        }

        protected virtual void RemoveMenuEntry(MenuEntry entry)
        {
            if (menuEntries.Contains(entry))
            {
                menuEntries.Remove(entry);
            }
        }

        /// <summary>
        /// Updates the menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (menuTextChanged)
            {
                SetMenuEntryText();
            }

            // Update each nested MenuEntry object.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                bool isSelected = IsActive && (i == selectedEntry);

                menuEntries[i].Update(this, isSelected, gameTime);
            }
        }


        /// <summary>
        /// Draws the menu.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            spriteBatch.Begin();

            // Draw the menu title.
            Vector2 titlePosition = TitlePosition();
            Vector2 titleOrigin = font.MeasureString(menuTitle) / 2;
            //Color titleColor = new Color(192, 192, 192, TransitionAlpha);
            Color titleColor = new Color(255, 255, 255, TransitionAlpha);
            float titleScale = 1.25f;

            //titlePosition.Y -= transitionOffset * 100;
            Vector2 titleTransform = DeriveTransition(transitionOffset, 100f, titleTransition);

            DrawShadowedString(spriteBatch, font, menuTitle, titlePosition + titleTransform, titleColor, 0,
                                   titleOrigin, titleScale, SpriteEffects.None, 0);

            Vector2 subTitlePosition = SubTitlePosition();
            Vector2 subTitleOrigin = font.MeasureString(menuSubTitle) / 2;
            float subTitleScale = .75f;

            DrawShadowedString(spriteBatch, font, menuSubTitle, subTitlePosition + titleTransform, titleColor, 0,
                                   subTitleOrigin, subTitleScale, SpriteEffects.None, 0);
            
            //setup menu positions
            //Y position only, X depends on the menu entry alignment
            Vector2 menuPosition = Vector2.Zero;
            menuPosition = MenuPosition();

            Vector2 menuPositionOffset;
            if (ScreenState == ScreenState.TransitionOn)
            {
                menuPositionOffset = DeriveTransition(-transitionOffset, 256, menuEntryTransition);
            }
            else
            {
                menuPositionOffset = DeriveTransition(transitionOffset, 512, menuEntryTransition);
            }
            
            DrawMenuEntries(gameTime, ref menuPosition, ref menuPositionOffset);

            //draw detail text if it exists
            if (menuEntries[selectedEntry].DetailText != null)
            {
                //Vector2 detailTextPosition = new Vector2(GraphicsConfig.GraphicsDevice.Viewport.Width / 2, menuPosition.Y) + detailPositionAdjust;
                Vector2 detailTextPosition = DetailTextPosition();
                Vector2 detailTextOrigin = font.MeasureString(menuEntries[selectedEntry].DetailText) / 2;

                Color detailTextColor = new Color(255, 255, 255, TransitionAlpha);
                float detailTextScale = .75f;

                detailTextPosition += DeriveTransition(transitionOffset, 100f, detailTransition);
                //detailTextPosition.Y += transitionOffset * 100;

                DrawShadowedString(spriteBatch, font, menuEntries[selectedEntry].DetailText,
                    detailTextPosition, detailTextColor, 0, detailTextOrigin,
                    detailTextScale, SpriteEffects.None, 0);
            }

            spriteBatch.End();
        }

        protected virtual Vector2 SubTitlePosition()
        {
            Vector2 titlePosition = TitlePosition();
            Vector2 subTitlePosition = titlePosition;
            subTitlePosition.Y += ScreenManager.Font.MeasureString(menuTitle).Y;
            subTitlePosition.Y += subTitlePositionAdjust.Y;
            return subTitlePosition;
        }

        protected virtual Vector2 TitlePosition()
        {
            return new Vector2(GraphicsConfig.GraphicsDevice.Viewport.Width / 2, GraphicsConfig.GraphicsDevice.Viewport.Height / 8) + titlePositionAdjust;
        }

        protected virtual Vector2 MenuPosition()
        {
            Vector2 position = Vector2.Zero;
            position.Y += SubTitlePosition().Y;
            position.Y += ScreenManager.Font.MeasureString(menuSubTitle).Y;
            position.Y += menuPositionAdjust.Y;
            return position;
        }

        protected virtual Vector2 DetailTextPosition()
        {
            Vector2 position = new Vector2(GraphicsConfig.GraphicsDevice.Viewport.Width / 2, 0);
            position.Y += MenuPosition().Y;
            position.Y += MenuHeight();
            position += detailPositionAdjust;
            return position;
        }

        protected virtual float MenuHeight()
        {
            float height = 0f;
            if (menuEntries.Count > 0)
            {
                height = (menuEntries[0].GetHeight(this) + menuEntrySpacingAdjust) * menuEntries.Count;
            }
            return height;
        }

        protected virtual void DrawMenuEntries(GameTime gameTime, ref Vector2 menuPosition, ref Vector2 menuPositionOffset)
        {
            Vector2 drawPosition = menuPosition;
            // Draw each menu entry in turn.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                bool isSelected = IsActive && (i == selectedEntry);

                menuEntry.Draw(this, drawPosition, menuPositionOffset, isSelected, gameTime);

                drawPosition.Y += (menuEntry.GetHeight(this) + menuEntrySpacingAdjust);
            }
        }

        private Vector2 DeriveTransition(float transitionOffset, float scalar, TransitionDirection direction)
        {
            Vector2 position = new Vector2(0);

            switch (direction)
            {
                case TransitionDirection.Left:
                    position.X += transitionOffset * scalar;
                    break;
                case TransitionDirection.Right:
                    position.X -= transitionOffset * scalar;
                    break;
                case TransitionDirection.Top:
                    position.Y -= transitionOffset * scalar;
                    break;
                case TransitionDirection.Bottom:
                    position.Y += transitionOffset * scalar;
                    break;
                default:
                    break;
            }

            return position;
        }

        protected void DrawShadowedString(SpriteBatch spriteBatch, SpriteFont font, string text, Vector2 position, Color color)
        {
            DrawShadowedString(spriteBatch, font, text, position, color, 0f,
                Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }

        protected void DrawShadowedString(SpriteBatch spriteBatch, SpriteFont font, string text, Vector2 position, Color color, float rotation,
            Vector2 origin, float scale, SpriteEffects effects, float layerDepth)
        {
            Color shadowColor = Color.Black;
            shadowColor.A = color.A;

            spriteBatch.DrawString(font, text, position + Vector2.One, shadowColor, 
                rotation, origin, scale, effects, layerDepth);
            spriteBatch.DrawString(font, text, position, color,
                rotation, origin, scale, effects, layerDepth);
        }
    }
}
