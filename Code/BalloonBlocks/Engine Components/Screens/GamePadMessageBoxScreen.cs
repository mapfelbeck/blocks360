﻿using System;

namespace BalloonBlocks
{
    class GamePadMessageBoxScreen : MessageBoxScreen
    {
        public GamePadMessageBoxScreen(string message)
            : this(message, true)
        { }

        public GamePadMessageBoxScreen(string message, bool includeUsageText)
            : base(message, "[A] Ok", "[B] Cancel")
        {
        }
    }
}
