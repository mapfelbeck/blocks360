#region File Description
//-----------------------------------------------------------------------------
// RandomMath.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace BalloonBlocks
{
    /// <summary>
    /// Static methods to assist with random-number generation.
    /// </summary>
    static public class RandomMath
    {
        /// <summary>
        /// The Random object used for all of the random calls.
        /// </summary>
        private static Random random = new Random();
        public static Random Random
        {
            get { return random; }
        }

        /// <summary>
        /// Generate a random floating-point value between the minimum and 
        /// maximum values provided.
        /// </summary>
        /// <remarks>This is similar to the Random.Next method, substituting singles
        /// for integers.</remarks>
        /// <param name="minimum">The minimum value.</param>
        /// <param name="maximum">The maximum value.</param>
        /// <returns>A random floating-point value between the minimum and maximum v
        /// alues provided.</returns>
        public static float RandomBetween(float minimum, float maximum)
        {
            return minimum + (float)random.NextDouble() * (maximum - minimum);
        }

        public static double RandomNormal(double mean, double stdDev)
        {
            double r1 = random.NextDouble();
            double r2 = random.NextDouble();

            return mean + (stdDev * (Math.Sqrt(-2 * Math.Log(r1)) * Math.Cos(6.28 * r2)));
        }

        public static DoubleShape RandomDouble()
        {
            return (DoubleShape)
                random.Next(1);
        }

        public static TriominoShape RandomTriomino()
        {
            return (TriominoShape)
                random.Next(2);
        }

        public static TetrominoShape RandomTetromino()
        {
            return (TetrominoShape)
                random.Next(7);
        }

        public static PentominoShape RandomPentomino()
        {
            return (PentominoShape)
                random.Next(18);
        }

        public static Color RandomColor()
        {
            return GlobalConfig.colors[
                    RandomMath.Random.Next(GlobalConfig.colors.Length)];
        }
    }
}