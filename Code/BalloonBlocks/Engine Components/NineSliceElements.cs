﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

#endregion

namespace BalloonBlocks
{
    public class NineSliceElement
    {
        NineSliceTexture texture;
        public Texture2D BaseTexture
        {
            get { return texture.BaseTexture; }
        }
        VertexPositionColorTexture[] vertexes;
        public VertexPositionColorTexture[] Vertexes
        {
            get { return vertexes; }
        }

        Rectangle drawRect;
        Rectangle DrawRect
        {
            get { return drawRect; }
        }

        float[] hScale;
        public float HScale
        {
            get { return hScale[1]; }
            set
            {
                hScale[1] = value;
                SetupVertexPositions();
            }
        }
        float[] vScale;
        public float VScale
        {
            get { return vScale[1]; }
            set
            {
                vScale[1] = value;
                SetupVertexPositions();
            }
        }
        Color color;
        public Color Color
        {
            get { return color; }
            set
            {
                color = value;
                SetupVertexColors();
            }
        }
        public byte Alpha
        {
            get { return color.A; }
            set
            {
                color.A = value;
                SetupVertexColors();
            }
        }
        Vector2 upperLeft;
        Vector2 lowerRight;
        float width;
        float height;

        /// <summary>
        /// Cretes a new drawable nine slice texture element
        /// </summary>
        /// <param name="texture">base texture</param>
        /// <param name="hProp">length 3 array of horizontal proportions for the base texture elements</param>
        /// <param name="vProp">length 3 array of vertical proportions for the base texture elements</param>
        /// <param name="hScale">default horizontal scale for this element</param>
        /// <param name="vScale">default vertical scale for this element</param>
        /// <param name="ul">upper left corner of this element</param>
        /// <param name="lr">lower right corner of this element</param>
        public NineSliceElement(Texture2D texture, float[] hProp, float[] vProp, float hScale, float vScale, Vector2 ul, Vector2 lr)
        {
            if (hProp == null || hProp.Length != 3)
            {
                throw new ArgumentException("Horizontal proportions array must be non null and length 3");
            }
            if (vProp == null || vProp.Length != 3)
            {
                throw new ArgumentException("Vertical proportions array must be non null and length 3");
            }
            if (texture == null)
            {
                throw new ArgumentNullException("Base texture must be non-null");
            }

            this.hScale = new float[3];
            this.hScale[0] = 1;
            this.hScale[1] = hScale;
            this.hScale[2] = 1;

            this.vScale = new float[3];
            this.vScale[0] = 1;
            this.vScale[1] = vScale;
            this.vScale[2] = 1;

            this.upperLeft = ul;
            this.lowerRight = lr;
            this.texture = new NineSliceTexture(texture, hProp, vProp);
            this.vertexes = new VertexPositionColorTexture[9 * 6];
            this.width = lr.X - ul.X;
            this.height = lr.Y - ul.Y;
            this.color = Color.White;
            SetupVertexColors();
            SetupVertexTextureCoordinates();
            SetupVertexPositions();
        }
        /// <summary>
        /// Cretes a new drawable nine slice texture element
        /// </summary>
        /// <param name="texture">base texture</param>
        /// <param name="hProp">length 3 array of horizontal proportions for the base texture elements</param>
        /// <param name="vProp">length 3 array of vertical proportions for the base texture elements</param>
        /// <param name="hScale">default horizontal scale for this element</param>
        /// <param name="vScale">default vertical scale for this element</param>
        /// <param name="ul">upper left corner of this element</param>
        /// <param name="lr">lower right corner of this element</param>
        public NineSliceElement(Texture2D texture, float[] hProp, float[] vProp, Rectangle drawRect)
        {
            if (hProp == null || hProp.Length != 3)
            {
                throw new ArgumentException("Horizontal proportions array must be non null and length 3");
            }
            if (vProp == null || vProp.Length != 3)
            {
                throw new ArgumentException("Vertical proportions array must be non null and length 3");
            }
            if (texture == null)
            {
                throw new ArgumentNullException("Base texture must be non-null");
            }

            //proportions: ratio that divides the 9 slice element, 
            //e.g. 1,4,1 : | |    | |

            //scale how much the middle sections are stretched
            //e.g. .25f is 25% of normal

            float totalWidth = (float)drawRect.Width;
            float totalHeight = (float)drawRect.Height;

            float horizontalProportionSum = hProp[0] + hProp[1] + hProp[2];
            float verticalProportionSum = vProp[0] + vProp[1] + vProp[2];

            this.hScale = new float[3];
            this.hScale[0] = 1;
            //this.hScale[1] = hScale;
            this.hScale[1] = hProp[1] / horizontalProportionSum;
            this.hScale[2] = 1;

            this.vScale = new float[3];
            this.vScale[0] = 1;
            //this.vScale[1] = vScale;
            this.vScale[1] = vProp[1] / horizontalProportionSum;
            this.vScale[2] = 1;

            this.drawRect = drawRect;
            this.texture = new NineSliceTexture(texture, hProp, vProp);
            this.vertexes = new VertexPositionColorTexture[9 * 6];
            this.color = Color.White;

            this.upperLeft = GraphicsConfig.ScreenToWorldCoordinates(new Vector2(
                drawRect.X, drawRect.Y));
            this.lowerRight = GraphicsConfig.ScreenToWorldCoordinates(new Vector2(
                drawRect.Right, drawRect.Bottom));

            this.width = lowerRight.X - upperLeft.X;
            this.height = lowerRight.Y - upperLeft.Y;

            this.color = Color.White;

            SetupVertexColors();
            SetupVertexTextureCoordinates();
            SetupVertexPositions();
        }
        
        private void SetupVertexPositions()
        {
            Vector2 upperleftCorner;
            int x;
            int y;
            float h;
            float w;
            for (int i = 0; i < 9; i++)
            {
                upperleftCorner = upperLeft;
                x = i % 3;
                for (int j = 0; j < x; j++)
                {
                    upperleftCorner.X += hScale[j] * (texture[j].UpperRight.X-texture[j].UpperLeft.X) * width;
                }
                
                y = i / 3;
                for (int j = 0; j < y; j++)
                {
                    upperleftCorner.Y += vScale[j] * (texture[j * 3].LowerLeft.Y - texture[j * 3].UpperLeft.Y) * height;
                }
                
                h = vScale[y] * (texture[i].LowerLeft.Y - texture[i].UpperLeft.Y) * height;
                w = hScale[x] * (texture[i].LowerRight.X - texture[i].LowerLeft.X) * width;
                
                vertexes[i * 6 + 0].Position = VectorTools.vec3FromVec2(
                   new Vector2(upperleftCorner.X, upperleftCorner.Y + h));
                vertexes[i * 6 + 1].Position = VectorTools.vec3FromVec2(
                   upperleftCorner);
                vertexes[i * 6 + 2].Position = VectorTools.vec3FromVec2(
                   new Vector2(upperleftCorner.X + w, upperleftCorner.Y + h));

                vertexes[i * 6 + 3].Position = VectorTools.vec3FromVec2(
                   new Vector2(upperleftCorner.X + w, upperleftCorner.Y + h));
                vertexes[i * 6 + 4].Position = VectorTools.vec3FromVec2(
                   upperleftCorner);
                vertexes[i * 6 + 5].Position = VectorTools.vec3FromVec2(
                   new Vector2(upperleftCorner.X + w, upperleftCorner.Y));
            }
        }

        private void SetupVertexTextureCoordinates()
        {
            for (int i = 0; i < 9; i++)
            {
                vertexes[i * 6 + 0].TextureCoordinate = texture[i].LowerLeft;
                vertexes[i * 6 + 1].TextureCoordinate = texture[i].UpperLeft;
                vertexes[i * 6 + 2].TextureCoordinate = texture[i].LowerRight;
                vertexes[i * 6 + 3].TextureCoordinate = texture[i].LowerRight;
                vertexes[i * 6 + 4].TextureCoordinate = texture[i].UpperLeft;
                vertexes[i * 6 + 5].TextureCoordinate = texture[i].UpperRight;
            }
        }

        private void SetupVertexColors()
        {
            for (int i = 0; i < 9; i++)
            {
                vertexes[i * 6 + 0].Color = color;
                vertexes[i * 6 + 1].Color = color;
                vertexes[i * 6 + 2].Color = color;
                vertexes[i * 6 + 3].Color = color;
                vertexes[i * 6 + 4].Color = color;
                vertexes[i * 6 + 5].Color = color;
            }
        }
    }
}