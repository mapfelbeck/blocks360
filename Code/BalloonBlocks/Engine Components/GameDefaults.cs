﻿
namespace BalloonBlocks
{
    /// <summary>
    /// Constants that may be over-written by saved values
    /// </summary>
    public static class GameDefaults
    {
        //game piece type constants
        public const GameType DefaultGameType = GameType.Normal;
        public const GameSpeed DefaultGameSpeed = GameSpeed.Normal;
        public const ShapeType DefaultPieceType = ShapeType.Triomino;
        public const BlockType DefaultBlockType = BlockType.Normal;
        public const BlockSize DefaultBlockScale = BlockSize.Normal;

        public const ControlMode DefaultControlMode = ControlMode.Easy;

        public const DrawStyle DefaultDrawStyle = DrawStyle.Textured;

        public const bool DefaultSoundEnabled = true;
        public const bool DefaultMusicEnabled = true;

        public const bool AutoDampRotation = false;
        public const int AutoDampRate = 50;
        public const bool IncreasingSpeed = true;
        public const bool RumbleActive = true;

#if XBOX360
        public const int DefaultSoundVolume = 75;
#else
        public const int DefaultSoundVolume = 35;
#endif
        public const int DefaultMusicVolume = 30;

        public const bool ColorShimmer = true;

        
#if WINDOWS
        public const GraphicsAspectRatio DefaultAspectRatio = GraphicsAspectRatio.Standard;
        public const bool DefaultFullScreen = false;
        public const int DefaultResolution = 1;
#else
        public const GraphicsAspectRatio DefaultAspectRatio = GraphicsAspectRatio.WideScreen;
        public const bool DefaultFullScreen = true;
        public const int DefaultResolution = 0;
#endif
    }
}