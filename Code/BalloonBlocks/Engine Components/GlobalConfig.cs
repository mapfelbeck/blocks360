﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

namespace BalloonBlocks
{
    /// <summary>
    /// An interface for changing game options
    /// </summary>
    static class GlobalConfig
    {
        public static void SpeedToGravityFriction(GameSpeed speed, out Gravity gravity, out Friction friction)
        {
            switch (speed)
            {
                case GameSpeed.VerySlow:
                    gravity = Gravity.Low;
                    friction = Friction.High;
                    break;
                case GameSpeed.Slow:
                    gravity = Gravity.Low;
                    friction = Friction.Normal;
                    break;
                case GameSpeed.Normal:
                    gravity = Gravity.Normal;
                    friction = Friction.Normal;
                    break;
                case GameSpeed.Fast:
                    gravity = Gravity.High;
                    friction = Friction.Normal;
                    break;
                case GameSpeed.VeryFast:
                    gravity = Gravity.High;
                    friction = Friction.Low;
                    break;
                default:
                    gravity = Gravity.None;
                    friction = Friction.VeryHigh;
                    break;
            }
        }

        public static float EnumCoefficient(Gravity speed)
        {
            float result = 0f;
            
            switch (speed)
            {
                case Gravity.None:
                    result = 0f;
                    break;
                case Gravity.Low:
                    result = GameConstants.GravityConstant * .7f;
                    break;
                case Gravity.Normal:
                    result = GameConstants.GravityConstant;
                    break;
                case Gravity.High:
                    result = GameConstants.GravityConstant * 1.5f;
                    break;
            }

            return result;
        }

        public static float EnumCoefficient(BlockSize blockSize)
        {
            float size = 0f;

            switch (blockSize)
            {
                case BlockSize.Small:
                    size = 1f;
                    break;
                case BlockSize.Normal:
                    size = 1.5f;
                    break;
                case BlockSize.Large:
                    size = 1.75f;
                    break;
                case BlockSize.SplashScreen:
                    size = 2.5f;
                    break;
                case BlockSize.BackgroundSmall:
                    size = 5f;
                    break;
                case BlockSize.BackGroundMedium:
                    size = 5.5f;
                    break;
                case BlockSize.BackgroundLarge:
                    size = 6f;
                    break;
                default:
                    size = .5f;
                    break;
            }

            return size;
        }

        public static float EnumCoefficient(Friction friction)
        {
            float result = 0f;

            switch (friction)
            {
                case Friction.Low:
                    result = .2f;
                    break;
                case Friction.Normal:
                    result = .4f;
                    break;
                case Friction.High:
                    result = .6f;
                    break;
                case Friction.VeryHigh:
                    result = .8f;
                    break;
                default:
                    result = .5f;
                    break;
            }

            return result;
        }

        public static Color[] colors = {  
                    Color.Red,
                    Color.Green,
                    Color.Blue,
                    Color.Yellow,
                    Color.Purple,
                    new Color(255,128,0),//orange
                };

        private static DrawStyle drawStyle;
        public static DrawStyle DrawMethod
        {
            get { return drawStyle; }
        }

        public static void CycleDrawStyle()
        {
            drawStyle++;
            if (drawStyle > DrawStyle.Textured)
            {
#if DEBUG
                drawStyle = DrawStyle.Debug;
#else
                drawStyle = DrawStyle.Flat;
#endif
            }
        }

        private static bool showFPSHUD = GameConstants.showFPSCounter;

        public static bool ShowFPSHUD
        {
            get { return showFPSHUD; }
        }

        public static void CycleShowFPSHUD()
        {
            showFPSHUD = !showFPSHUD;
        }

        private static bool showSafeArea = GameConstants.showSafeArea;
        public static bool ShowSafeArea
        {
            get { return showSafeArea; }
        }

        private static bool showPerfData = GameConstants.showPerfData;
        public static bool ShowPerfData
        {
            get { return showPerfData; }
            set { showPerfData = value; }
        }

        public static void CycleShowSafeArea()
        {
            showSafeArea = !showSafeArea;
        }

        private static bool shimmerDisplayOption = GameDefaults.ColorShimmer;
        public static bool ShimmerDisplayOption
        {
            get { return shimmerDisplayOption; }
        }

        public static void CycleShimmerDisplayOption()
        {
            shimmerDisplayOption = !shimmerDisplayOption;
        }

        private static bool soundEnabled;
        public static bool SoundEnabled
        {
            get { return soundEnabled; }
        }

        public static void CycleSoundEnabled()
        {
            soundEnabled = !soundEnabled;
            AudioManager.SetSoundEnabled(soundEnabled);
        }

        private static int soundVolume;
        public static int SoundVolume
        {
            get { return soundVolume; }
        }

        public static void SoundVolumeUp()
        {
            soundVolume = Math.Min(soundVolume + 1, GameConstants.MaxVolume);
            AudioManager.SetSoundEffectsVolume(soundVolume);
        }
        public static void SoundVolumeDown()
        {
            soundVolume = Math.Max(soundVolume - 1, GameConstants.MinVolume);
            AudioManager.SetSoundEffectsVolume(soundVolume);
        }

        private static int musicVolume;
        public static int MusicVolume
        {
            get { return musicVolume; }
        }

        public static void MusicVolumeUp()
        {
            musicVolume = Math.Min(musicVolume + 1, GameConstants.MaxVolume);
            MusicPlayer.SetMusicVolume(musicVolume);
        }
        public static void MusicVolumeDown()
        {
            musicVolume = Math.Max(musicVolume - 1, GameConstants.MinVolume);
            MusicPlayer.SetMusicVolume(musicVolume);
        }

        private static bool musicEnabled;
        public static bool MusicEnabled
        {
            get { return musicEnabled; }
        }

        public static void CycleMusicEnabled()
        {
            musicEnabled = !musicEnabled;
            MusicPlayer.SetMusicEnabled(musicEnabled);
        }

        private static bool increasingSpeed;
        public static bool IncreasingSpeed
        {
            get { return increasingSpeed; }
            set { increasingSpeed = value; }
        }

        private static bool rumbleActive;
        public static bool RumbleActive
        {
            get { return rumbleActive; }
            set
            {
                rumbleActive = value;
                RumbleComponent.RumbleActive = rumbleActive;
            }
        }

        private static ControlMode controlMode;
        public static ControlMode ControlMode
        {
            get { return controlMode; }
            set { controlMode = value; }
        }
        public static void CycleControlMode()
        {
            controlMode++;
            if (controlMode > ControlMode.Unassisted)
            {
                controlMode = ControlMode.Easy;
            }
        }

        private static int autoDampRate;
        public static int AutoDampRate
        {
            get { return autoDampRate; }
        }

        public static float AutoDampNormalized
        {
            get { return (float)autoDampRate / 100f; }
        }

        public static void AutoDampRateUp()
        {
            autoDampRate = Math.Min(autoDampRate + 1, 100);
        }

        public static void AutoDampRateDown()
        {
            autoDampRate = Math.Max(autoDampRate - 1, 0);
        }
        
        public static void LoadSettings(GameSettings settings){
            musicEnabled = settings.MusicEnabled;
            musicVolume = settings.MusicVolume;
            soundEnabled = settings.SoundEnabled;
            soundVolume = settings.SoundVolume;
            drawStyle = settings.DrawStyle;
            autoDampRate = settings.AutoDampRate;
            increasingSpeed = settings.IncreasingSpeed;
            rumbleActive = settings.RumbleActive;
            controlMode = settings.ControlMode;
        }
    }
}