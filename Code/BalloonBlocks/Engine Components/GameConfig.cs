﻿namespace BalloonBlocks
{
    public enum ControlMode
    {
        Easy,
        Unassisted,
    }

    public enum GameType
    {
        Normal,
        Clear,
        Survival,
    }

    public enum GameSpeed
    {
        VerySlow,
        Slow,
        Normal,
        Fast,
        VeryFast
    }

    public enum GameDifficulty
    {
        Easy,
        Medium,
        Hard,
        Multiplayer,
        Custom,
        Tutorial,
        SplashScreen,
    }

    public enum DrawStyle
    {
        Debug,
        Flat,
        Textured,
    }

    public enum BlockType
    {
        Background,
        Normal,
        Damping,
        Deflating,
        Freeze,
    }

    public enum BlockSize
    {
        Small,
        Normal,
        Large,
        SplashScreen,
        BackgroundSmall,
        BackGroundMedium,
        BackgroundLarge,
    }

    public enum Friction
    {
        Low,
        Normal,
        High,
        VeryHigh,
    }

    public enum Gravity
    {
        None,
        Low,
        Normal,
        High,
    }

    public struct GameConfig
    {
        public GameType gameType;
        public GameDifficulty gameDifficulty;
        public ShapeType shape;
        public BlockSize blockSize;
        public BlockType blockType;
        public bool scored;
        public Friction friction;
        public Gravity gravity;
        public string blockTextureName;
        public int blockTextureSize;
        public string backgroundTextureName;
        public int backgroundTextureSize;
        public float unFreezeTime;
        public float dropAfterCollideTime;
        public float forceDropTime;

        public GameConfig(GameType theType, ShapeType theShape, BlockSize theBlockSize, BlockType theMode,
            bool isScored, Friction theFriction, Gravity theGravity,
            string theBlockTextureName, int theBlockTextureSize, string theBackgroundTextureName, int theBackgroundTextureSize,
            GameDifficulty theDifficulty, float unfreeze, float theDropTime, float theForceDropTime)
        {
            gameType = theType;
            gameDifficulty = theDifficulty;
            shape = theShape;
            blockSize = theBlockSize;
            blockType = theMode;
            scored = isScored;
            friction = theFriction;
            gravity = theGravity;
            blockTextureName = theBlockTextureName;
            blockTextureSize = theBlockTextureSize;
            backgroundTextureName = theBackgroundTextureName;
            backgroundTextureSize = theBackgroundTextureSize;
            unFreezeTime = unfreeze;
            dropAfterCollideTime = theDropTime;
            forceDropTime = theForceDropTime;
        }
        public GameConfig(GameType theType, ShapeType theShape, BlockSize theBlockSize, 
            BlockType theMode, bool isScored, GameSpeed theSpeed, string theBlockTextureName, 
            int theBlockTextureSize, string theBackgroundTextureName, 
            int theBackgroundTextureSize,
            GameDifficulty theDifficulty, float unfreeze, float theDropTime, 
            float theForceDropTime)
        {
            gameType = theType;
            gameDifficulty = theDifficulty;
            shape = theShape;
            blockSize = theBlockSize;
            blockType = theMode;
            scored = isScored;

            Friction theFriction;
            Gravity theGravity;
            GlobalConfig.SpeedToGravityFriction(theSpeed, out theGravity, out theFriction);
            friction = theFriction;
            gravity = theGravity;

            blockTextureName = theBlockTextureName;
            blockTextureSize = theBlockTextureSize;
            backgroundTextureName = theBackgroundTextureName;
            backgroundTextureSize = theBackgroundTextureSize;
            unFreezeTime = unfreeze;
            dropAfterCollideTime = theDropTime;
            forceDropTime = theForceDropTime;
        }
    }
}