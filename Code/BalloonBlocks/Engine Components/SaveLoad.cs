﻿using System;
using Microsoft.Xna.Framework;
using System.Threading;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace BalloonBlocks
{
    public delegate void SaveLoadDelegate();
    public delegate void SaveLoadCallback();

    public class SaveLoad : DrawableGameComponent
    {
        private struct SaveLoadStruct
        {
            public SaveLoadDelegate saveLoad;
            public SaveLoadCallback callback;
            public SaveLoadStruct(SaveLoadDelegate saveLoad, SaveLoadCallback callback)
            {
                this.saveLoad = saveLoad;
                this.callback = callback;
            }
        }
        Texture2D saveIcon;
        float rotation;
        Rectangle drawRectangle;
        SpriteBatch spriteBatch;
        Vector2 origin;
        const float rotateScalar = 3.5f;
        bool active = false;

        bool disposed = false;
        Thread saveLoadThread;

        Queue<SaveLoadStruct> saveLoadQueue;

        private static SaveLoad saveData = null;
        static SaveLoad Instance
        {
            get
            {
                if (saveData == null)
                {
                    throw new Exception("SaveData must be initialized first");
                }
                return saveData;
            }
        }

        private SaveLoad(Game game)
            : base(game)
        {
            saveLoadQueue = new Queue<SaveLoadStruct>();
            GraphicsConfig.DeviceManager.DeviceReset += DeviceReset;
            CreateDrawRectangle();

#if XBOX360
                saveLoadThread = new System.Threading.Thread(new ThreadStart(
                    delegate()
                    {
                        // 1, 3, 4, 5 available on the 360, 0 and 2 are reserved
                        Thread.CurrentThread.SetProcessorAffinity(new int[] { 5 });
                        this.RunQueue();
                    }));
#else
            saveLoadThread = new System.Threading.Thread(new ThreadStart(this.RunQueue));
#endif
            saveLoadThread.Start();
        }

        public static void Initialize(Game game)
        {
            saveData = new SaveLoad(game);
            game.Components.Add(saveData);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        static void DeviceReset(object sender, EventArgs e)
        {
            Instance.CreateDrawRectangle();
        }

        void CreateDrawRectangle()
        {
            int rectangleSize = GraphicsConfig.GraphicsDevice.PresentationParameters.BackBufferWidth / 10;
            drawRectangle = new Rectangle(
                GraphicsConfig.SafeArea.Right - rectangleSize, GraphicsConfig.SafeArea.Top + rectangleSize,
                rectangleSize, rectangleSize);
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(GraphicsConfig.GraphicsDevice);
            ContentManager content = Game.Content;
            saveIcon = content.Load<Texture2D>("Game UI/Save Icon");
            origin = new Vector2(saveIcon.Width / 2, saveIcon.Height / 2);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            rotation += elapsed * rotateScalar;
            if (active)
            {
                CreateDrawRectangle();
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (active)
            {
                spriteBatch.Begin();
                spriteBatch.Draw(saveIcon, drawRectangle, null, Color.White, rotation, origin, SpriteEffects.None, 1);
                spriteBatch.End();
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            disposed = true;
        }

        public static void Trigger(SaveLoadDelegate saveLoad, SaveLoadCallback callback)
        {
            Instance.AddDelegate(saveLoad, callback);
        }

        private void AddDelegate(SaveLoadDelegate saveLoad, SaveLoadCallback callback)
        {
            if (saveLoad == null)
            {
                throw new ArgumentNullException("Null SaveLoad delegate");
            }

            saveLoadQueue.Enqueue(new SaveLoadStruct(saveLoad, callback));
        }

        private void RunQueue()
        {
            while (!disposed)
            {
                if (saveLoadQueue.Count > 0)
                {
                    active = true;
                    SaveLoadStruct current = saveLoadQueue.Dequeue();
                    current.saveLoad();
                    if (current.callback != null)
                    {
                        current.callback();
                    }
                    active = false;
                }
                else
                {
                    Thread.Sleep(5);
                }
            }
        }
    }
}