using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace BalloonBlocks
{
    /// <summary>
    /// Component that manages audio playback for all cues.
    /// </summary>
    public class AudioManager : DrawableGameComponent
    {
        ContentManager content;
        private static AudioManager audioManager;

        private AudioEngine audioEngine;
        private SoundBank soundBank;
        private WaveBank waveBank;

        private AudioCategory soundEffectsCatagory;

        private bool soundEnabled;
        private float soundVolume;

        string[] soundEffectNames = { "BubblePop", 
                                     "PieceCreate",
                                     "BlockHit",
                                     "MenuAdvance",
                                     "MenuBack",
                                     "MenuBadSelect",
                                     "MenuScroll",
                                     "MenuSelect",
                                     "SliderMove",
                                     "BlockHit",
                                     "Combo",
                                     "Message",
                                     "Inflate"};

        /// <summary>
        /// Constructs the manager for audio playback of all cues.
        /// </summary>
        /// <param name="game">The game that this component will be attached to.</param>
        /// <param name="settingsFile">The filename of the XACT settings file.</param>
        /// <param name="waveBankFile">The filename of the XACT wavebank file.</param>
        /// <param name="soundBankFile">The filename of the XACT soundbank file.</param>
        private AudioManager(Game game, string settingsFile, string waveBankFile,
            string soundBankFile)
            : base(game)
        {
            content = new ContentManager(game.Services);
            content.RootDirectory = "Content";

            try
            {
                soundEnabled = GlobalConfig.SoundEnabled;
                soundVolume = (float)GlobalConfig.SoundVolume/100f;

                audioEngine = new AudioEngine(settingsFile);
                waveBank = new WaveBank(audioEngine, waveBankFile);
                soundBank = new SoundBank(audioEngine, soundBankFile);

                soundEffectsCatagory = audioEngine.GetCategory("Sound Effects");
                soundEffectsCatagory.SetVolume(soundVolume);
            }
            catch (NoAudioHardwareException)
            {
                // silently fall back to silence
                audioEngine = null;
                waveBank = null;
                soundBank = null;
            }
        }

        /// <summary>
        /// Initialize the static AudioManager functionality.
        /// </summary>
        /// <param name="game">The game that this component will be attached to.</param>
        /// <param name="settingsFile">The filename of the XACT settings file.</param>
        /// <param name="waveBankFile">The filename of the XACT wavebank file.</param>
        /// <param name="soundBankFile">The filename of the XACT soundbank file.</param>
        public static void Initialize(Game game, string settingsFile,
            string waveBankFile, string soundBankFile)
        {
            audioManager = new AudioManager(game, settingsFile, waveBankFile,
                soundBankFile);

            if (game != null)
            {
                game.Components.Add(audioManager);
                audioManager.RegisterEvents();
            }
        }

        public static void SetSoundEnabled(bool isEnabled)
        {
            audioManager.soundEnabled = isEnabled;
        }

        internal static void SetSoundEffectsVolume(int volume)
        {
            audioManager.soundVolume = (float)volume / 100f;
            audioManager.soundEffectsCatagory.SetVolume(audioManager.soundVolume);
        }

        /// <summary>
        /// Retrieve a cue by name.
        /// </summary>
        /// <param name="cueName">The name of the cue requested.</param>
        /// <returns>The cue corresponding to the name provided.</returns>
        public static Cue GetCue(string cueName)
        {
            if ((audioManager == null) || (audioManager.audioEngine == null) ||
                (audioManager.soundBank == null) || (audioManager.waveBank == null))
            {
                return null;
            }
            return audioManager.soundBank.GetCue(cueName);
        }


        /// <summary>
        /// Plays a cue by name.
        /// </summary>
        /// <param name="cueName">The name of the cue to play.</param>
        public static void PlayCue(string cueName)
        {
            if ((audioManager != null) && (audioManager.audioEngine != null) &&
                (audioManager.soundBank != null) && (audioManager.waveBank != null))
            {
                audioManager.soundBank.PlayCue(cueName);
            }
        }

        /// <summary>
        /// Used to trigger sound effect through the event system
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="theEvent"></param>
        /// <param name="args"></param>
        private void cueCallback(object sender, string theEvent, params object[] args)
        {
            if (soundEnabled)
            {
                PlayCue(theEvent);
            }
        }

        /// <summary>
        /// Update the audio manager, particularly the engine.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // update the audio engine
            if (audioEngine != null && soundEnabled)
            {
                audioEngine.Update();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Clean up the component when it is disposing.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (soundBank != null)
                    {
                        soundBank.Dispose();
                        soundBank = null;
                    }
                    if (waveBank != null)
                    {
                        waveBank.Dispose();
                        waveBank = null;
                    }
                    if (audioEngine != null)
                    {
                        audioEngine.Dispose();
                        audioEngine = null;
                    }
                }
            }
            finally
            {
                UnregisterEvents();
                base.Dispose(disposing);
            }
        }

        protected void RegisterEvents()
        {
            foreach (string effect in soundEffectNames)
            {
                EventManager.Register(cueCallback, effect);
            }
        }

        protected void UnregisterEvents()
        {
            foreach (string effect in soundEffectNames)
            {
                EventManager.UnRegister(cueCallback, effect);
            }
        }

        internal static void LoadSettings(GameSettings settings)
        {
            SetSoundEnabled(settings.SoundEnabled);
            SetSoundEffectsVolume(settings.SoundVolume);
        }
    }
}