﻿#region Using Statements
using System;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace BalloonBlocks
{
    public class DrawBatch
    {
        const int DefaultBufferSize = 1000;
        int positionInBuffer = 0;
        PrimitiveType primitiveType;

        int numVertsPerPrimitive;

        bool hasBegun = false;

        bool isDisposed = false;

        GraphicsDevice device;
        Effect effect;
        VertexDeclaration vertexDeclaration;
        VertexPositionColorTexture[] vertices = new VertexPositionColorTexture[DefaultBufferSize];

        public DrawBatch(GraphicsDevice graphicsDevice)
        {
            if (graphicsDevice == null)
            {
                throw new ArgumentNullException("graphicsDevice");
            }

            device = graphicsDevice;

            vertexDeclaration = new VertexDeclaration(graphicsDevice,
                VertexPositionColorTexture.VertexElements);
        }

        public void Begin(PrimitiveType primitiveType, Effect drawEffect)
        {
            if (hasBegun)
            {
                throw new InvalidOperationException
                    ("End must be called before Begin can be called again.");
            }

            if (drawEffect == null)
            {
                throw new ArgumentNullException("drawEffect");
            }

            if (primitiveType == PrimitiveType.LineStrip ||
                primitiveType == PrimitiveType.TriangleFan ||
                primitiveType == PrimitiveType.TriangleStrip)
            {
                throw new NotSupportedException
                    ("The specified primitiveType is not supported by PrimitiveBatch.");
            }

            effect = drawEffect;

            this.primitiveType = primitiveType;

            // how many verts will each of these primitives require?
            this.numVertsPerPrimitive = NumVertsPerPrimitive(primitiveType);

            // prepare the graphics device for drawing by setting the vertex declaration
            // and telling our basic effect to begin.
            device.VertexDeclaration = vertexDeclaration;
            effect.Begin();
            effect.CurrentTechnique.Passes[0].Begin();

            // flip the error checking boolean. It's now ok to call AddVertex, Flush,
            // and End.
            hasBegun = true;
        }

        public void End()
        {
            if (!hasBegun)
            {
                throw new InvalidOperationException
                    ("Begin must be called before End can be called.");
            }

            // Draw whatever the user wanted us to draw
            Flush();

            // and then tell basic effect that we're done.
            effect.CurrentTechnique.Passes[0].End();
            effect.End();
            hasBegun = false;
        }

        private void Flush()
        {
            if (!hasBegun)
            {
                throw new InvalidOperationException
                    ("Begin must be called before Flush can be called.");
            }

            // no work to do
            if (positionInBuffer == 0)
            {
                return;
            }

            // how many primitives will we draw?
            int primitiveCount = positionInBuffer / numVertsPerPrimitive;

            // submit the draw call to the graphics card
            device.DrawUserPrimitives<VertexPositionColorTexture>(primitiveType, vertices, 0,
                primitiveCount);

            // now that we've drawn, it's ok to reset positionInBuffer back to zero,
            // and write over any vertices that may have been set previously.
            positionInBuffer = 0;
        }

        static private int NumVertsPerPrimitive(PrimitiveType primitive)
        {
            int numVertsPerPrimitive;
            switch (primitive)
            {
                case PrimitiveType.PointList:
                    numVertsPerPrimitive = 1;
                    break;
                case PrimitiveType.LineList:
                    numVertsPerPrimitive = 2;
                    break;
                case PrimitiveType.TriangleList:
                    numVertsPerPrimitive = 3;
                    break;
                default:
                    throw new InvalidOperationException("primitive is not valid");
            }
            return numVertsPerPrimitive;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !isDisposed)
            {
                if (vertexDeclaration != null)
                    vertexDeclaration.Dispose();

                if (effect != null)
                    effect.Dispose();

                isDisposed = true;
            }
        }

        internal void Draw(VertexPositionColorTexture[] newVertices)
        {
            if (newVertices == null)
            {
                return;
            }
            
            if((newVertices.Length % numVertsPerPrimitive)!= 0){
                throw new InvalidOperationException("vertexes array length not a multiple of numVertsPerPrimitive");
            }

            if ((newVertices.Length + positionInBuffer) >= DefaultBufferSize)
            {
                Flush();
            }

            newVertices.CopyTo(vertices, positionInBuffer);
            positionInBuffer += newVertices.Length;
        }
    }
}