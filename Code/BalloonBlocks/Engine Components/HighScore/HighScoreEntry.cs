﻿namespace BalloonBlocks
{
    public class HighScoreEntry
    {
        public int Score;
        public int BlocksRemaining;
        public string Name;
        public float PlayTime;
        
        public HighScoreEntry()
        {
            Score = 0;
            Name = "";
            PlayTime = 0f;
            BlocksRemaining = 0;
        }

        public HighScoreEntry(int theScore, string theName, float time)
        {
            Score = theScore;
            Name = theName;
            PlayTime = time;
            BlocksRemaining = 0;
        }

        public HighScoreEntry(int theScore, string theName, float time, int blocksRemaining)
        {
            Score = theScore;
            Name = theName;
            PlayTime = time;
            BlocksRemaining = blocksRemaining;
        }

        public override int GetHashCode()
        {
            return Score * (int)PlayTime;
        }

        public override bool Equals(object other)
        {
            return (this == (HighScoreEntry)other);
        }

        public static bool operator ==(HighScoreEntry ent1, HighScoreEntry ent2)
        {
            // Need to call through ReferenceEquals, to prevent recursion
            if (object.ReferenceEquals(ent1, null))
                return object.ReferenceEquals(ent2, null);

            if (ent1.GetType() != ent2.GetType())
                return false;

            if (ent1.Score != ent2.Score)
                return false;

            if (ent1.Name != ent2.Name)
                return false;

            return ent1.PlayTime == ent2.PlayTime;
        }

        public static bool operator !=(HighScoreEntry ent1, HighScoreEntry ent2)
        {
            return !(ent1 == ent2);
        }

        public static bool operator >=(HighScoreEntry ent1, HighScoreEntry ent2)
        {
            // Need to call through ReferenceEquals, to prevent recursion
            if (object.ReferenceEquals(ent1, null))
                return object.ReferenceEquals(ent2, null);

            if (ent1.GetType() != ent2.GetType())
                return false;

            if (ent1.Score > ent2.Score)
            {
                return true;
            }
            else if (ent1.Score == ent2.Score)
            {
                return ent1.PlayTime <= ent2.PlayTime;
            }

            return false;
        }

        public static bool operator <=(HighScoreEntry ent1, HighScoreEntry ent2)
        {
            // Need to call through ReferenceEquals, to prevent recursion
            if (object.ReferenceEquals(ent1, null))
                return object.ReferenceEquals(ent2, null);

            if (ent1.GetType() != ent2.GetType())
                return false;

            if (ent1.Score < ent2.Score)
            {
                return true;
            }
            else if (ent1.Score == ent2.Score)
            {
                return ent1.PlayTime >= ent2.PlayTime;
            }

            return false;
        }

        public static bool operator >(HighScoreEntry ent1, HighScoreEntry ent2)
        {
            // Need to call through ReferenceEquals, to prevent recursion
            if (object.ReferenceEquals(ent1, null))
                return object.ReferenceEquals(ent2, null);

            if (ent1.GetType() != ent2.GetType())
                return false;

            if (ent1.Score > ent2.Score)
            {
                return true;
            }
            else if (ent1.Score == ent2.Score)
            {
                return ent1.PlayTime < ent2.PlayTime;
            }

            return false;
        }

        public static bool operator <(HighScoreEntry ent1, HighScoreEntry ent2)
        {
            // Need to call through ReferenceEquals, to prevent recursion
            if (object.ReferenceEquals(ent1, null))
                return object.ReferenceEquals(ent2, null);

            if (ent1.GetType() != ent2.GetType())
                return false;

            if (ent1.Score < ent2.Score)
            {
                return true;
            }
            else if (ent1.Score == ent2.Score)
            {
                return ent1.PlayTime > ent2.PlayTime;
            }

            return false;
        }

        public int NormalCompareTo(HighScoreEntry other)
        {
            if (HighScoreEntry.ReferenceEquals(this, other))
            {
                return 0;
            }

            if (this.Score > other.Score)
            {
                return 1;
            }
            else if (this.Score == other.Score)
            {
                if (this.PlayTime < other.PlayTime)
                {
                    return 1;
                }
                else if (this.PlayTime == other.PlayTime)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }
        public int SurvivalCompareTo(HighScoreEntry other)
        {
            if (HighScoreEntry.ReferenceEquals(this, other))
            {
                return 0;
            }

            if (this.PlayTime < other.PlayTime)
            {
                return 1;
            }
            else if (this.PlayTime < other.PlayTime)
            {
                if (this.Score > other.Score)
                {
                    return 1;
                }
                else if (this.Score == other.Score)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }

        public int ClearCompareTo(HighScoreEntry other)
        {
            if (HighScoreEntry.ReferenceEquals(this, other))
            {
                return 0;
            }

            if (this.BlocksRemaining < other.BlocksRemaining)
            {
                return 1;
            }
            else if (this.BlocksRemaining == other.BlocksRemaining)
            {
                if (this.Score > other.Score)
                {
                    return 1;
                }
                else if (this.Score == other.Score)
                {
                    if (this.PlayTime < other.PlayTime)
                    {
                        return 1;
                    }
                    else if (this.PlayTime == other.PlayTime)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }
    }
}