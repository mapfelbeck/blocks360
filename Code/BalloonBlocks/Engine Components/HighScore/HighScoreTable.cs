﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Storage;
using Utilities;
using System.Diagnostics;
#endregion

namespace BalloonBlocks
{
    public class HighScoreTable
    {
        SerializableDictionary<GameType,
            SerializableDictionary<GameDifficulty, List<HighScoreEntry>>> table;

        bool sorted;
        const string defaultPlayer = "Cog";

        static HighScoreTable instance;
        static HighScoreTable Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HighScoreTable();
                }
                return instance;
            }
        }
        private static string highScoreFile;

        public List<HighScoreEntry> this[GameType type, GameDifficulty difficulty]
        {
            get {
                if (!Instance.sorted)
                {
                    Instance.SortTables();
                }
                if (table.ContainsKey(type) && table[type].ContainsKey(difficulty))
                {
                    return table[type][difficulty];
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<HighScoreEntry> GetScores(GameType type, GameDifficulty difficulty)
        {
            return Instance[type, difficulty];
        }

        private HighScoreTable()
        {
            table = new SerializableDictionary<GameType, SerializableDictionary<GameDifficulty, List<HighScoreEntry>>>();
        }

        public static bool SupportedGameType(GameDifficulty difficulty)
        {
            if (difficulty < GameDifficulty.Easy || difficulty > GameDifficulty.Hard)
            {
                return false;
            }
            return true;
        }

        public static void Add(string PlayerName, GameType type, GameDifficulty difficulty, int points, float playTime, int remaining){
            if (difficulty < GameDifficulty.Easy || difficulty > GameDifficulty.Hard)
            {
                throw new IndexOutOfRangeException("unsupported game type");
            }

            Instance.table[type][difficulty].Add(new HighScoreEntry(points, PlayerName, playTime, remaining));

            Instance.sorted = false;
            Instance.SortTables();
        }

        private void SortTables()
        {
            if (!sorted)
            {
                foreach (List<HighScoreEntry> list in table[GameType.Normal].Values)
                {
                    list.Sort(delegate(HighScoreEntry p1, HighScoreEntry p2) { return p1.NormalCompareTo(p2); });
                    list.Reverse();
                }

                foreach (List<HighScoreEntry> list in table[GameType.Clear].Values)
                {
                    list.Sort(delegate(HighScoreEntry p1, HighScoreEntry p2) { return p1.ClearCompareTo(p2); });
                    list.Reverse();
                }

                foreach (List<HighScoreEntry> list in table[GameType.Survival].Values)
                {
                    list.Sort(delegate(HighScoreEntry p1, HighScoreEntry p2) { return p1.SurvivalCompareTo(p2); });
                    list.Reverse();
                }
            }
            sorted = true;
        }

        public static void Save()
        {
#if XBOX360
            Save(StorageDeviceManager.GetStorageDevice(), highScoreFile);
#endif
        }

        public static void Save(StorageDevice device)
        {
            Save(device, highScoreFile);
        }

        private static void Save(StorageDevice device, string filename)
        {
            if (device == null || !device.IsConnected)
            {
                return;
            }

            if (filename != null)
            {
                highScoreFile = filename;
            }

            StorageContainer container =
                            device.OpenContainer("Balloon Blocks");
            /*StorageContainer container =
                device.OpenContainer(StorageContainer.TitleLocation);*/

            string fullPath = Path.Combine(container.Path, highScoreFile);

            if (!Instance.sorted)
            {
                Instance.SortTables();
            }

            foreach (SerializableDictionary<GameDifficulty, List<HighScoreEntry>> table in Instance.table.Values)
            {
                foreach (List<HighScoreEntry> list in table.Values)
                {
                    while (list.Count > 10)
                    {
                        list.RemoveAt(list.Count - 1);
                    }
                }
            }

            Stream stream;
            try
            {
                stream = File.Create(fullPath);

                XmlSerializer serializer = new XmlSerializer(typeof(
                    SerializableDictionary<GameType,
                    SerializableDictionary<GameDifficulty, List<HighScoreEntry>>>));
                serializer.Serialize(stream, Instance.table);
                stream.Close();
            }
            catch
            {
                Trace.WriteLine("High score save failed");
            }
            finally
            {
                if (container != null)
                {
                    container.Dispose();
                }
            }
        }

        internal static void LoadDefaults()
        {
            Instance.BuildDefaultHighScoreTable();
        }

        internal static void SetFileName(string fileName)
        {
            highScoreFile = fileName;
        }

        internal static void Load(StorageDevice device, string filename)
        {
            if (filename != null)
            {
                highScoreFile = filename;
            }

            if (device != null && device.IsConnected)
            {
                StorageContainer container =
                                device.OpenContainer("Balloon Blocks");
                /*StorageContainer container =
                    device.OpenContainer(StorageContainer.TitleLocation);*/

                string fullpath = Path.Combine(container.Path, highScoreFile);

                try
                {
                    if (File.Exists(fullpath))
                    {
                        Stream stream = null;
                        try
                        {
                            stream = File.OpenRead(fullpath);
                            XmlSerializer serializer = new XmlSerializer(typeof(
                                SerializableDictionary<GameType,
                                    SerializableDictionary<GameDifficulty, List<HighScoreEntry>>>));

                            Instance.table = (SerializableDictionary<GameType,
                                                    SerializableDictionary<GameDifficulty, List<HighScoreEntry>>>)serializer.Deserialize(stream);
                        }
                        catch { }
                        finally
                        {
                            if (stream != null)
                            {
                                stream.Close();
                            }
                        }
                    }
                }
                catch
                {
                    Trace.WriteLine("High score load failed");
                }
                finally
                {
                    if (container != null)
                    {
                        container.Dispose();
                    }
                }
            }
        }

        internal static void Load()
        {
            //throw new NotImplementedException();
        }

        public static void Load(StorageDevice device)
        {
            Load(device, highScoreFile);
        }

        private void BuildDefaultHighScoreTable()
        {
            table.Add(GameType.Normal, new SerializableDictionary<GameDifficulty, List<HighScoreEntry>>());
            table.Add(GameType.Clear, new SerializableDictionary<GameDifficulty, List<HighScoreEntry>>());
            table.Add(GameType.Survival, new SerializableDictionary<GameDifficulty, List<HighScoreEntry>>());
            
            BuildNormalDefaultEntries();
            BuildClearDefaultEntries();
            BuildSurvivalDefaultEntries();

            SortTables();
        }

        private void BuildSurvivalDefaultEntries()
        {
            table[GameType.Survival].Add(GameDifficulty.Easy, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Survival][GameDifficulty.Easy].Add(new HighScoreEntry(350 * i, defaultPlayer, 50f * i));
            }
            table[GameType.Survival].Add(GameDifficulty.Medium, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Survival][GameDifficulty.Medium].Add(new HighScoreEntry(450 * i, defaultPlayer, 60f * i));
            }
            table[GameType.Survival].Add(GameDifficulty.Hard, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Survival][GameDifficulty.Hard].Add(new HighScoreEntry(550 * i, defaultPlayer, 70f * i));
            }
        }

        private void BuildClearDefaultEntries()
        {
            table[GameType.Clear].Add(GameDifficulty.Easy, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Clear][GameDifficulty.Easy].Add(new HighScoreEntry(450 * i, defaultPlayer, 60f * i, 8));
            }
            table[GameType.Clear].Add(GameDifficulty.Medium, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Clear][GameDifficulty.Medium].Add(new HighScoreEntry(550 * i, defaultPlayer, 75f * i, 16));
            }
            table[GameType.Clear].Add(GameDifficulty.Hard, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Clear][GameDifficulty.Hard].Add(new HighScoreEntry(650 * i, defaultPlayer, 90f * i, 24));
            }
        }

        private void BuildNormalDefaultEntries()
        {
            table[GameType.Normal].Add(GameDifficulty.Easy, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Normal][GameDifficulty.Easy].Add(new HighScoreEntry(500 * i, defaultPlayer, 60f * i));
            }
            table[GameType.Normal].Add(GameDifficulty.Medium, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Normal][GameDifficulty.Medium].Add(new HighScoreEntry(400 * i, defaultPlayer, 45f * i));
            }
            table[GameType.Normal].Add(GameDifficulty.Hard, new List<HighScoreEntry>());
            for (int i = 1; i <= 10; i++)
            {
                table[GameType.Normal][GameDifficulty.Hard].Add(new HighScoreEntry(300 * i, defaultPlayer, 30f * i));
            }
        }
    }
}