﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace BalloonBlocks
{
    public class FontParticleSystem : ParticleSystem
    {
        SpriteFont font;

        public FontParticleSystem(int howManyEffects)
            : base(howManyEffects)
        {
        }

        protected override void InitializeConstants()
        {
            filePath = "Fonts";
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);

            font = content.Load<SpriteFont>(filePath + "/" + filename);
        }


        public override void Draw()
        {
            // tell sprite batch to begin, using the spriteBlendMode specified in
            // initializeConstants
            spriteBatch.Begin(spriteBlendMode,
                SpriteSortMode.BackToFront, SaveStateMode.None, transforMatrix);

            foreach (Particle p in particles)
            {
                // skip inactive particles
                if (!p.Active)
                    continue;

                // normalized lifetime is a value from 0 to 1 and represents how far
                // a particle is through its life. 0 means it just started, .5 is half
                // way through, and 1.0 means it's just about to be finished.
                // this value will be used to calculate alpha and scale, to avoid 
                // having particles suddenly appear or disappear.
                float normalizedLifetime = p.TimeSinceStart / p.Lifetime;

                // we want particles to fade in and fade out, so we'll calculate alpha
                // to be (normalizedLifetime) * (1-normalizedLifetime). this way, when
                // normalizedLifetime is 0 or 1, alpha is 0. the maximum value is at
                // normalizedLifetime = .5, and is
                // (normalizedLifetime) * (1-normalizedLifetime)
                // (.5)                 * (1-.5)
                // .25
                // since we want the maximum alpha to be 1, not .25, we'll scale the 
                // entire equation by 4.
                float alpha = 4 * normalizedLifetime * (1 - normalizedLifetime);
                //Color color = new Color(new Vector4(1, 1, 1, alpha));
                Color color = p.ParticleColor;
                color.A = (byte)(alpha * 256);

                // make particles grow as they age. they'll start at 75% of their size,
                // and increase to 100% once they're finished.
                float scale = p.Scale * (.75f + .25f * normalizedLifetime);

                origin = font.MeasureString(p.ParticleString) / 2;

                spriteBatch.DrawString(font, p.ParticleString, p.Position, color, p.Rotation,
                    origin, scale, SpriteEffects.None, 0.0f);
            }

            spriteBatch.End();
        }
    }
}