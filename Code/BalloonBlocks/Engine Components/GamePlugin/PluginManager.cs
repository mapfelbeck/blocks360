﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace BalloonBlocks
{
    class PluginManager
    {
        delegate void DrawDelegate();

        struct DrawOrderEntry
        {
            public DrawDelegate Draw;
            public int Order;
            public DrawOrderEntry(DrawDelegate draw, int order)
            {
                Draw = draw;
                Order = order;
            }
            public int CompareTo(DrawOrderEntry other)
            {
                return Order.CompareTo(other.Order);
            }
        }

        List<DrawOrderEntry> drawablePlugins;
        List<GamePlugin> plugins;

        public PluginManager()
        {
            drawablePlugins = new List<DrawOrderEntry>();
            plugins = new List<GamePlugin>();
        }

        public void Add(GamePlugin plugin)
        {
            plugin.Initialize();
            plugins.Add(plugin);
            if (plugin is DrawableGamePlugin)
            {
                DrawableGamePlugin drawable = plugin as DrawableGamePlugin;
                DrawOrderEntry newEntry = new DrawOrderEntry(drawable.Draw, drawable.DrawOrder);
                drawablePlugins.Add(newEntry);
                drawablePlugins.Sort(delegate(DrawOrderEntry p1, DrawOrderEntry p2) { return p1.CompareTo(p2); });
            }
        }

        public void Remove(GamePlugin plugin)
        {
            plugin.Dispose();
            plugins.Remove(plugin);
            if (plugin is DrawableGamePlugin)
            {
                DrawableGamePlugin drawable = plugin as DrawableGamePlugin;
                DrawOrderEntry entry = new DrawOrderEntry(drawable.Draw, drawable.DrawOrder);
                drawablePlugins.RemoveAll(new System.Predicate<DrawOrderEntry>(delegate(DrawOrderEntry val) { return (val.Draw == entry.Draw); }));  
            }
        }

        public void Initialize()
        {
            foreach (GamePlugin plugin in plugins)
            {
                plugin.Initialize();
            }
        }

        public void LoadContent(ContentManager content)
        {
            foreach (GamePlugin plugin in plugins)
            {
                plugin.LoadContent(content);
            }
        }

        public void UnloadContent()
        {
            foreach (GamePlugin plugin in plugins)
            {
                plugin.UnloadContent();
            }
        }

        public void Update(GameTime gameTime, bool otherScreenHasFocus,
                                              bool coveredByOtherScreen)
        {
            foreach (GamePlugin plugin in plugins)
            {
                plugin.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            }
        }

        public void Draw()
        {
            foreach (DrawOrderEntry entry in drawablePlugins)
            {
                entry.Draw();
            }
        }

        public void Dispose()
        {
            foreach (GamePlugin plugin in plugins)
            {
                plugin.Dispose();
            }
        }
    }
}
