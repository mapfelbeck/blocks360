﻿using System;
using Microsoft.Xna.Framework;

namespace BalloonBlocks
{
    abstract public class DrawableGamePlugin : GamePlugin
    {
        int drawOrder;
        public int DrawOrder
        {
            get { return drawOrder; }
            set
            {
                drawOrder = value;
                if (DrawOrderChanged != null)
                {
                    DrawOrderChanged(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler DrawOrderChanged;

        abstract public void Draw();
    }
}
