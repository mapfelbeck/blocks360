﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace BalloonBlocks
{
    public abstract class GamePlugin : IDisposable
    {
        public GamePlugin()
        {
        }

        virtual public void Initialize()
        {
            RegisterEvents();
        }

        virtual protected void RegisterEvents() { }

        virtual protected void UnRegisterEvents() { }

        abstract public void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen);

        virtual public void LoadContent(ContentManager content) { }

        virtual public void UnloadContent() { }

        virtual public void Dispose()
        {
            UnRegisterEvents();
        }
    }
}