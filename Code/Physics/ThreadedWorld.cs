﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities;
using Microsoft.Xna.Framework;

namespace Physics
{
    class ThreadedWorld : World
    {
        TaskBatch taskBatch;

        /// <summary>
        /// Creates the physics World
        /// </summary>
        public ThreadedWorld(int worldMaterialCount, MaterialPair[,] worldMaterialPairs,
            MaterialPair worldDefaultMaterialPair, float worldPenetrationThreshold,
            Vector2 worldMin, Vector2 worldMax, ColliderBase collider)
            : this(worldMaterialCount, worldMaterialPairs,
            worldDefaultMaterialPair, worldPenetrationThreshold,
            new AABB(worldMin, worldMax), collider)
        {
        }

        /// <summary>
        /// Creates the physics World
        /// </summary>
        public ThreadedWorld(int worldMaterialCount, MaterialPair[,] worldMaterialPairs,
            MaterialPair worldDefaultMaterialPair, float worldPenetrationThreshold,
            AABB worldBounds, ColliderBase collider)
            : base(worldMaterialCount, worldMaterialPairs,
                worldDefaultMaterialPair, worldPenetrationThreshold,
                worldBounds, collider)
        {
            AutoTuneIter = true;
            taskBatch = new TaskBatch();
        }

        protected override void AccumulateAndIntegrate(float iterElapsed)
        {
            taskBatch.Begin();

            int bodiesPerTask = collider.Count / taskBatch.NumberThreads;
            int start = 0;
            int end = start + bodiesPerTask;
            for (int i = 0; i < taskBatch.NumberThreads; i++)
            {
                taskBatch.AddTask(AccumulateAndIntegrateForces, start, end, iterElapsed);

                start += bodiesPerTask;
                end += bodiesPerTask;
            }
            if (start < collider.Count)
            {
                taskBatch.AddTask(AccumulateAndIntegrateForces, start, collider.Count, iterElapsed);
            }

            taskBatch.End();
        }

        public override void Dispose()
        {
            taskBatch.Dispose();
            base.Dispose();
        }
    }
}
