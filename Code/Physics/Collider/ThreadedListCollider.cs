﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Utilities;
using System.Threading;

namespace Physics
{
    class ThreadedListCollider : ColliderBase
    {
        List<Body> bodies;
        int oldBodyCount;
        TaskBatch taskBatch;
        int numThreads;
        int[] workDivision;
        Pool<CollideListTask> taskPool;
        List<Pool<CollideListTask>.Node> activeTasks;

        public ThreadedListCollider(float penetrationThreshold)
            : base(penetrationThreshold)
        {
            bodies = new List<Body>();
            taskBatch = new TaskBatch();
            numThreads = taskBatch.NumberThreads;
            workDivision = new int[numThreads + 1];
            taskPool = new Pool<CollideListTask>(numThreads);
            activeTasks = new List<Pool<CollideListTask>.Node>();
        }

        public override int Count
        {
            get
            {
                return bodies.Count;
            }
        }

        public override IEnumerator GetEnumerator()
        {
            return bodies.GetEnumerator();
        }

        public override Body this[int index]
        {
            get
            {
                return bodies[index];
            }
        }

        public override void Add(Body body)
        {
            bodies.Add(body);
        }

        public override void Remove(Body body)
        {
            bodies.Remove(body);
        }

        public override bool Contains(Body body)
        {
            return bodies.Contains(body);
        }

        public override void Clear()
        {
            taskBatch.Dispose();
            bodies.Clear();
        }

        public override void BuildCollisions(List<BodyCollisionInfo> collisionList)
        {
            Pool<CollideListTask>.Node collideTask;

            taskBatch.Begin();
            if (bodies.Count > 10)
            {
                DivideWork(bodies.Count);

                for (int i = 0; i < numThreads; i++)
                {
                    //collideTask = new CollideListTask(bodies, workDivision[i], workDivision[i + 1], collisionList, PenetrationThreshold);
                    collideTask = taskPool.Get();
                    collideTask.Item.Init(bodies, workDivision[i], workDivision[i + 1], collisionList, PenetrationThreshold);
                    taskBatch.AddTask(collideTask.Item);
                    activeTasks.Add(collideTask);
                }
            }
            else
            {
                //collideTask = new CollideListTask(bodies, 0, bodies.Count, collisionList, PenetrationThreshold);
                collideTask = taskPool.Get();
                collideTask.Item.Init(bodies, 0, bodies.Count, collisionList, PenetrationThreshold);
                taskBatch.AddTask(collideTask.Item);
                activeTasks.Add(collideTask);
            }
            taskBatch.End();

            foreach (Pool<CollideListTask>.Node task in activeTasks)
            {
                task.Item.Clear();
                taskPool.Return(task);
            }
            activeTasks.Clear();
        }

        private void DivideWork(int numberBodies)
        {
            if (numberBodies == oldBodyCount)
            {
                return;
            }

            int totalCollideChecks = TriangularNumber(bodies.Count);
            int idealCollideChecksPerThread = totalCollideChecks / numThreads;
            int collideChecksThisDivision;
            int currentBody = 0;

            workDivision[0] = 0;
            for (int i = 1; i < workDivision.Length; i++)
            {
                collideChecksThisDivision = 0;

                while (collideChecksThisDivision < idealCollideChecksPerThread)
                {
                    currentBody++;
                    collideChecksThisDivision += (numberBodies - (currentBody + 1));
                }
                collideChecksThisDivision = 0;
                workDivision[i] = currentBody;
            }
            workDivision[numThreads] = numberBodies;

            oldBodyCount = numberBodies;
        }

        //(n^2-n)/2
        private int TriangularNumber(int n)
        {
            return (n * n - n) / 2;
        }
    }
}