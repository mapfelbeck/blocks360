﻿using System;
using System.Collections.Generic;
using Utilities;
using Microsoft.Xna.Framework;
using System.Collections;

namespace Physics
{
    public abstract class ColliderBase : IEnumerable
    {
        float penetrationThreshold;
        public float PenetrationThreshold
        {
            get { return penetrationThreshold; }
            set { penetrationThreshold = value; }
        }

        abstract public int Count
        {
            get;
        }
        abstract public Body this[int index]
        {
            get;
        }

        public ColliderBase(float penetrationThreshold)
        {
            this.penetrationThreshold = penetrationThreshold;
        }

        abstract public IEnumerator GetEnumerator();

        abstract public void Add(Body body);

        abstract public void Remove(Body body);

        abstract public bool Contains(Body body);

        abstract public void BuildCollisions(List<BodyCollisionInfo> collisionList);

        abstract public void Clear();
    }
}