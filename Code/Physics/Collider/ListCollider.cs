﻿using System.Collections.Generic;
using Utilities;
using System.Collections;

namespace Physics
{
    class ListCollider : ColliderBase
    {
        List<Body> bodies;

        public ListCollider(float penetrationThreshold)
            : base(penetrationThreshold)
        {
            bodies = new List<Body>();
        }

        public override int Count
        {
            get
            {
                return bodies.Count;
            }
        }

        public override IEnumerator GetEnumerator()
        {
            return bodies.GetEnumerator();
        }

        public override Body this[int index]
        {
            get
            {
                return bodies[index];
            }
        }

        public override void Add(Body body)
        {
            bodies.Add(body);
        }

        public override void Remove(Body body)
        {
            bodies.Remove(body);
        }

        public override bool Contains(Body body)
        {
            return bodies.Contains(body);
        }

        public override void Clear()
        {
            bodies.Clear();
        }

        public override void BuildCollisions(List<BodyCollisionInfo> collisionList)
        {
            // now check for collision.
            // inter-body collision!
            for (int i = 0; i < bodies.Count; i++)
            {
                for (int j = i + 1; j < bodies.Count; j++)
                {
                    /*
                    //these 2 optimization actually hurt performance in the block game
                    // early out - these bodies materials are set NOT to collide
                    if (!materialPairs[bodies[i].Material, bodies[j].Material].Collide)
                        continue;
                    
                    // another early-out - both bodies are static.
                    if ((bodies[i].IsStatic) && (bodies[j].IsStatic))
                        continue;
                    */

                    // broad-phase collision via AABB.
                    AABB boxA = bodies[i].getAABB();
                    AABB boxB = bodies[j].getAABB();

                    // early out

                    if (!boxA.Intersects(ref boxB))
                        continue;

                    Body.bodyCollide(bodies[i], bodies[j], collisionList, PenetrationThreshold);
                    Body.bodyCollide(bodies[j], bodies[i], collisionList, PenetrationThreshold);
                }
            }
        }

    }
}
