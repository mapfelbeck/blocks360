﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace Physics
{
    class CollideListTask : Task
    {
        float penetrationThreshold;

        int start;
        int end;

        List<Body> bodies;
        List<BodyCollisionInfo> newCollisions;
        List<BodyCollisionInfo> collisions;

        public CollideListTask()
        {
            newCollisions = new List<BodyCollisionInfo>();
        }

        public CollideListTask(List<Body> bodies, int start, int end, List<BodyCollisionInfo> collisions, float penetrationThreshold)
            : this()
        {
            Init(bodies, start, end, collisions, penetrationThreshold);
        }

        public void Init(List<Body> bodies, int start, int end, List<BodyCollisionInfo> collisions, float penetrationThreshold)
        {
            this.bodies = bodies;
            this.start = start;
            this.end = end;
            this.collisions = collisions;
            this.penetrationThreshold = penetrationThreshold;
            //taskOver = false;
        }
        //bool taskOver;
        public void Clear()
        {
            /*if (!taskOver)
            {
                throw new Exception("task not over dummy");
            }*/
            this.bodies = null;
            this.start = -1;
            this.end = -1;
            this.collisions = null;
            this.penetrationThreshold = 0;
        }

        public override void TaskRun()
        {
            // now check for collision.
            // inter-body collision!
            for (int i = start; i < end; i++)
            {
                for (int j = i + 1; j < bodies.Count; j++)
                {
                    /*
                    //these 2 optimization actually hurt performance in the block game
                    // early out - these bodies materials are set NOT to collide
                    if (!materialPairs[bodies[i].Material, bodies[j].Material].Collide)
                        continue;
                    
                    // another early-out - both bodies are static.
                    if ((bodies[i].IsStatic) && (bodies[j].IsStatic))
                        continue;
                    */

                    // broad-phase collision via AABB.
                    AABB boxA = bodies[i].getAABB();
                    AABB boxB = bodies[j].getAABB();

                    // early out
                    if (!boxA.Intersects(ref boxB))
                        continue;

                    Body.bodyCollide(bodies[i], bodies[j], newCollisions, penetrationThreshold);
                    Body.bodyCollide(bodies[j], bodies[i], newCollisions, penetrationThreshold);
                }
            }
        }

        public override void TaskComplete()
        {
            if (newCollisions.Count > 0)
            {
                lock (collisions)
                {
                    collisions.AddRange(newCollisions);
                }
                newCollisions.Clear();
            }
            //taskOver = true;
        }
    }
}