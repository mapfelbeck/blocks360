﻿using System.Collections.Generic;
using Utilities;
using System.Collections;
using System;

namespace Physics
{
    class QuadTreeCollider : ColliderBase
    {
        QuadTree<Body> tree;
        List<Body> queryResults;

        public override int Count
        {
            get { return tree.Count; }
        }

        public override Body this[int index]
        {
            get { return tree[index]; }
        }

        public override IEnumerator GetEnumerator()
        {
            return tree.Contents.GetEnumerator();
        }

        public QuadTreeCollider(float penetrationThreshold, AABB bounds, int maxDepth)
            : base(penetrationThreshold)
        {
            tree = new QuadTree<Body>(bounds, maxDepth);
            queryResults = new List<Body>();
        }

        public override void Add(Body body)
        {
            tree.Add(body);
        }

        public override void Remove(Body body)
        {
            tree.Remove(body);
        }

        public override void Clear()
        {
            tree.Clear();
        }

        public override bool Contains(Body body)
        {
            return tree.Contents.Contains(body);
        }

        public override void BuildCollisions(List<BodyCollisionInfo> collisionList)
        {
            tree.BuildContentsCache(false);

            for (int i = 0; i < tree.Count; i++)
            {
                tree.Query(tree[i].AABB, queryResults);
                for (int j = 0; j < queryResults.Count; j++)
                {
                    if (tree[i] != queryResults[j])
                    {
                        Body.bodyCollide(tree[i], queryResults[j], collisionList, PenetrationThreshold);
                    }
                }
                queryResults.Clear();
            }
        }
    }
}