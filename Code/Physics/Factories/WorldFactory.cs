﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
using Utilities;

namespace Physics
{
    /// <summary>
    /// World Builder
    /// </summary>
    public class WorldFactory
    {
        public static World BuildWorld()
        {
            // initialize materials.
            int materialCount = 1;
            MaterialPair[,] materialPairs = new MaterialPair[1, 1];
            MaterialPair defaultMaterialPair = new MaterialPair();
            defaultMaterialPair.Friction = 0.3f;
            defaultMaterialPair.Elasticity = 0.8f;
            defaultMaterialPair.Collide = true;
            defaultMaterialPair.CollisionFilter = new collisionFilter(World.aCollisionFilter);

            materialPairs[0, 0] = defaultMaterialPair;

            Vector2 min = new Vector2(-20.0f, -20.0f);
            Vector2 max = new Vector2(20.0f, 20.0f);

            AABB worldBounds = new AABB(min, max);

            float penetrationThreshold = 0.3f;

            //ColliderBase collider = new ListCollider(penetrationThreshold);
            ColliderBase collider = new QuadTreeCollider(penetrationThreshold, worldBounds, 5);

            World theWorld = new World(materialCount,
                materialPairs, defaultMaterialPair, penetrationThreshold, worldBounds, collider);

            return theWorld;
        }

        public static World BuildThreadedWorld()
        {
            // initialize materials.
            int materialCount = 1;
            MaterialPair[,] materialPairs = new MaterialPair[1, 1];
            MaterialPair defaultMaterialPair = new MaterialPair();
            defaultMaterialPair.Friction = 0.3f;
            defaultMaterialPair.Elasticity = 0.8f;
            defaultMaterialPair.Collide = true;
            defaultMaterialPair.CollisionFilter = new collisionFilter(World.aCollisionFilter);

            materialPairs[0, 0] = defaultMaterialPair;

            Vector2 min = new Vector2(-20.0f, -20.0f);
            Vector2 max = new Vector2(20.0f, 20.0f);

            AABB worldBounds = new AABB(min, max);

            float penetrationThreshold = 0.3f;

            //ColliderBase collider = new ListCollider(penetrationThreshold);
            ColliderBase collider = new ThreadedListCollider(penetrationThreshold);

            World theWorld = new ThreadedWorld(materialCount,
                materialPairs, defaultMaterialPair, penetrationThreshold, worldBounds, collider);

            return theWorld;
        }
    }
}