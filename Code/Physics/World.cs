using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;
using System.Diagnostics;

namespace Physics
{
    /// <summary>
    /// Collision Filter type. return TRUE to allow collision, FALSE to ignore collision.
    /// </summary>
    /// <param name="bodyA">The colliding body</param>
    /// <param name="bodyApm">Point mass that has collided</param>
    /// <param name="bodyB">Body that bodyA collided with</param>
    /// <param name="bodyBpm1">PointMass 1 on the edge that was collided with</param>
    /// <param name="bodyBpm2">PointMass 2 on the edge that was collided with</param>
    /// <param name="hitPt">Location of collision in global space</param>
    /// <param name="normalVel">Velocity along normal of collision.</param>
    /// <returns>TRUE = accept collision, FALSE = ignore collision</returns>
    public delegate bool collisionFilter(Body bodyA, int bodyApm, Body bodyB, int bodyBpm1, int bodyBpm2, Vector2 hitPt, float normalVel);

    public delegate void Accumulator(float elapsedTime);

    /// <summary>
    /// represents data about collision between 2 materials.
    /// </summary>
    public struct MaterialPair
    {
        /// <summary>
        /// Whether these 2 materials should collide with each other or not.
        /// </summary>
        public bool Collide;

        /// <summary>
        /// Amount of "bounce" when collision occurs. value range [0,1]. 0 == no bounce, 1 == 100% bounce
        /// </summary>
        public float Elasticity;

        /// <summary>
        /// Amount of friction.  Value range [0,1].  0 == no friction, 1 == 100% friction, will stop on contact.
        /// </summary>
        public float Friction;

        /// <summary>
        /// Collision filter function.
        /// </summary>
        public collisionFilter CollisionFilter;
    }

    // collision information
    public struct BodyCollisionInfo
    {
        public void Clear() { bodyA = bodyB = null; bodyApm = bodyBpmA = bodyBpmB = -1; hitPt = Vector2.Zero; edgeD = 0f; normal = Vector2.Zero; penetration = 0f; }
        public Body bodyA;
        public int bodyApm;
        public Body bodyB;
        public int bodyBpmA;
        public int bodyBpmB;
        public Vector2 hitPt;
        public float edgeD;
        public Vector2 normal;
        public float penetration;
    };

    /// <summary>
    /// World - this class represents the physics world, and keeps track of all Bodies in it, coordinating integration, collision, etc.
    /// </summary>
    public class World
    {
        /// <summary>
        /// number of materials created.
        /// </summary>
        public int MaterialCount
        {
            get { return materialCount; }
        }

        public int NumberBodies
        {
            get { return collider.Count; }
        }

        //How many iterations do we run per call to Update?
        int physicsSimIter = 1;
        public int PhysicsSimIter
        {
            get { return physicsSimIter; }
            set { physicsSimIter = value; }
        }

        protected bool autoTuneIter;
        public bool AutoTuneIter
        {
            get { return autoTuneIter; }
            set { autoTuneIter = value; }
        }

        Timer updateTimer;
        protected float updateTime;
        public float UpdateTime
        {
            get { return updateTime; }
        }

        protected ColliderBase collider;
        AABB worldLimits;

        float penetrationThreshold;
        int penetrationCount;

        //Used to give each body added to the physics world a unique id
        int bodyCounter;

        // material chart.
        MaterialPair[,] materialPairs;
        MaterialPair defaultMatPair;
        int materialCount;

        float BodyDamping = .5f;

        List<BodyCollisionInfo> collisionList;

        //// debug visualization variables
        VertexDeclaration vertexDecl = null;
        
        public Accumulator externalAccumulator;

        /// <summary>
        /// Creates the physics World
        /// </summary>
        public World(int worldMaterialCount, MaterialPair[,] worldMaterialPairs, 
            MaterialPair worldDefaultMaterialPair, float worldPenetrationThreshold,
            Vector2 worldMin, Vector2 worldMax, ColliderBase collider)
            : this(worldMaterialCount, worldMaterialPairs,
            worldDefaultMaterialPair,worldPenetrationThreshold,
            new AABB(worldMin,worldMax), collider)
        {
        }

        /// <summary>
        /// Creates the physics World
        /// </summary>
        public World(int worldMaterialCount, MaterialPair[,] worldMaterialPairs, 
            MaterialPair worldDefaultMaterialPair, float worldPenetrationThreshold, 
            AABB worldBounds, ColliderBase collider)
        {
            this.collider = collider;
            collisionList = new List<BodyCollisionInfo>();

            bodyCounter = 0;

            // initialize materials.
            materialCount = worldMaterialCount;
            materialPairs = worldMaterialPairs;
            defaultMatPair = worldDefaultMaterialPair;

            setWorldLimits(worldBounds);

            penetrationThreshold = worldPenetrationThreshold;

            updateTimer = new Timer();
        }

        public void setWorldLimits(Vector2 min, Vector2 max)
        {
            worldLimits = new AABB(ref min, ref max);
        }

        public void setWorldLimits(AABB limits)
        {
            worldLimits = limits;
        }

        /// <summary>
        /// Add a new material to the world.  all previous material data is kept intact.
        /// </summary>
        /// <returns>int ID of the newly created material</returns>
        public int addMaterial()
        {
            MaterialPair[,] old = materialPairs;
            materialCount++;

            materialPairs = new MaterialPair[materialCount, materialCount];

            // replace old data.
            for (int i = 0; i < materialCount; i++)
            {
                for (int j = 0; j < materialCount; j++)
                {
                    if ((i < (materialCount-1)) && (j < (materialCount-1)))
                        materialPairs[i, j] = old[i, j];
                    else
                        materialPairs[i, j] = defaultMatPair;
                }
            }

            return materialCount - 1;
        }

        /// <summary>
        /// Enable or Disable collision between 2 materials.
        /// </summary>
        /// <param name="a">material ID A</param>
        /// <param name="b">material ID B</param>
        /// <param name="collide">true = collide, false = ignore collision</param>
        public void setMaterialPairCollide(int a, int b, bool collide)
        {
            if ((a >= 0) && (a < materialCount) && (b >= 0) && (b < materialCount))
            {
                materialPairs[a, b].Collide = collide;
                materialPairs[b, a].Collide = collide;
            }
        }

        /// <summary>
        /// Set the collision response variables for a pair of materials.
        /// </summary>
        /// <param name="a">material ID A</param>
        /// <param name="b">material ID B</param>
        /// <param name="friction">friction.  [0,1] 0 = no friction, 1 = 100% friction</param>
        /// <param name="elasticity">"bounce" [0,1] 0 = no bounce (plastic), 1 = 100% bounce (super ball)</param>
        public void setMaterialPairData(int a, int b, float friction, float elasticity)
        {
            if ((a >= 0) && (a < materialCount) && (b >= 0) && (b < materialCount))
            {
                materialPairs[a, b].Friction = friction;
                materialPairs[a, b].Elasticity = elasticity;

                materialPairs[b, a].Friction = friction;
                materialPairs[b, a].Elasticity = elasticity;
            }
        }

        /// <summary>
        /// Sets a user function to call when 2 bodies of the given materials collide.
        /// </summary>
        /// <param name="a">Material A</param>
        /// <param name="b">Material B</param>
        /// <param name="filter">User fuction (delegate)</param>
        public void setMaterialPairFilterCallback(int a, int b, collisionFilter filter)
        {
            if ((a >= 0) && (a < materialCount) && (b >= 0) && (b < materialCount))
            {
                materialPairs[a, b].CollisionFilter += filter;

                materialPairs[b, a].CollisionFilter += filter;
            }
        }

        /// <summary>
        /// Add a Body to the world.  Bodies do this automatically, you should NOT need to call this.
        /// </summary>
        /// <param name="b">the body to add to the world</param>
        public void addBody(Body b)
        {
            if (!collider.Contains(b))
            {
                b.VelocityDamping = BodyDamping;
                b.BodyNumber = bodyCounter;
                bodyCounter++;
                collider.Add(b);
            }
        }

        /// <summary>
        /// Remove a body from the world.  call this outside of an update to remove the body.
        /// </summary>
        /// <param name="b">the body to remove</param>
        public void removeBody(Body b)
        {
            if (collider.Contains(b))
            {
                collider.Remove(b);
                if (b.DeleteCallback != null)
                {
                    b.DeleteCallback(b);
                }
            }
        }

        /// <summary>
        /// Get a body at a specific index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Body getBody(int index)
        {
            if ((index >= 0) && (index < collider.Count))
                return collider[index];

            return null;
        }

        /// <summary>
        /// Find the closest PointMass in the world to a given point.
        /// </summary>
        /// <param name="pt">global point</param>
        /// <param name="bodyID">index of the body that contains the PointMass</param>
        /// <param name="pmID">index of the PointMass</param>
        public void getClosestPointMass(Vector2 pt, out int bodyID, out int pmID)
        {
            bodyID = -1;
            pmID = -1;

            float closestD = 1000.0f;
            for (int i = 0; i < collider.Count; i++)
            {
                float dist = 0f;
                int pm = collider[i].getClosestPointMass(pt, out dist);
                if (dist < closestD)
                {
                    closestD = dist;
                    bodyID = i;
                    pmID = pm;
                }
            }
        }

        /// <summary>
        /// Given a global point, get a body (if any) that contains this point.
        /// Useful for picking objects with a cursor, etc.
        /// </summary>
        /// <param name="pt">global point</param>
        /// <returns>Body (or null)</returns>
        public Body getBodyContaining(ref Vector2 pt)
        {
            for (int i = 0; i < collider.Count; i++)
            {
                if (collider[i].contains(ref pt))
                    return collider[i];
            }

            return null;
        }

        /// <summary>
        /// Update the world by a specific timestep.
        /// </summary>
        /// <param name="elapsed">elapsed time in seconds</param>
        public virtual void Update(float elapsed)
        {
            updateTimer.Begin();

            int numUpdates = PhysicsSimIter;

            if (AutoTuneIter)
            {
                numUpdates = AutoIter(collider.Count);
                physicsSimIter = numUpdates;
            }

            //Console.WriteLine("iter count: {0}", numUpdates);

            float iterElapsed = elapsed / numUpdates;

            for (int iter = 0; iter < numUpdates; iter++)
            {
                penetrationCount = 0;

                if (externalAccumulator != null)
                {
                    externalAccumulator(iterElapsed);
                }

                for (int i = 0; i < collider.Count; i++)
                {
                    if (collider[i].deleteThis)
                    {
                        removeBody(collider[i]);
                        i--;
                    }
                }

                // first, accumulate all forces acting on PointMasses.
                AccumulateAndIntegrate(iterElapsed);

                collider.BuildCollisions(collisionList);
                // now handle all collisions found during the update at once.
                lock (collisionList)
                {
                    HandleCollisions(collisionList);
                }

                // now dampen velocities.
                for (int i = 0; i < collider.Count; i++)
                    collider[i].dampenVelocity(iterElapsed);
            }

            for (int i = 0; i < collider.Count; i++)
            {
                collider[i].Update(elapsed);
            }

            // now reset body forces
            for (int i = 0; i < collider.Count; i++)
            {
                collider[i].resetExternalForces();
            }
            updateTime = (float)updateTimer.End(false);
        }

        private int AutoIter(int bodyCount)
        {
            int iter = 1;

#if WINDOWS
            if (bodyCount < 40)
            {
                iter = 5;
            }
            else if (bodyCount < 80)
            {
                iter = 4;
            }
            else if (bodyCount < 120)
            {
                iter = 3;
            }
            else if (bodyCount < 160)
            {
                iter = 2;
            }
            else
            {
                iter = 1;
            }
#else
            if (bodyCount < 55)
            {
                iter = 4;
            }
            else if (bodyCount < 75)
            {
                iter = 3;
            }
            else if (bodyCount < 90)
            {
                iter = 2;
            }
            else
            {
                iter = 1;
            }
#endif
            return iter;
        }

        protected virtual void AccumulateAndIntegrate(float iterElapsed)
        {
            AccumulateAndIntegrateForces(0, collider.Count, iterElapsed);
        }

        protected void AccumulateAndIntegrateForces(params object[] args)
        {
            if (args == null || args.Length != 3)
            {
                throw new ArgumentException("Bad AccumulateForces args");
            }

            int start = (int)args[0];
            int end = (int)args[1];
            float iterElapsed = (float)args[2];

            for (int i = start; i < end; i++)
            {
                if (!collider[i].IsStatic)
                {
                    collider[i].derivePositionAndAngle(iterElapsed);
                    collider[i].accumulateExternalForces(iterElapsed);
                    collider[i].accumulateInternalForces(iterElapsed);
                    collider[i].integrate(iterElapsed);
                    collider[i].updateAABB(iterElapsed, false);
                }
            }
        }

        protected void AccumulateForces(int start, int end, float iterElapsed)
        {
            for (int i = start; i < end; i++)
            {
                if (!collider[i].IsStatic)
                {
                    collider[i].derivePositionAndAngle(iterElapsed);
                    collider[i].accumulateExternalForces(iterElapsed);
                    collider[i].accumulateInternalForces(iterElapsed);
                }
            }
        }

        protected void HandleCollisions(List<BodyCollisionInfo> collisions)
        {
            // handle all collisions!
            for (int i = 0; i < collisions.Count; i++)
            {
                BodyCollisionInfo info = collisions[i];

                if (info.bodyA == null || info.bodyB == null)
                {
                    continue;
                }

                if (info.bodyA.CollisionCallback != null)
                {
                    info.bodyA.CollisionCallback(info.bodyB);
                }

                PointMass A = info.bodyA.getPointMass(info.bodyApm);
                PointMass B1 = info.bodyB.getPointMass(info.bodyBpmA);
                PointMass B2 = info.bodyB.getPointMass(info.bodyBpmB);

                // velocity changes as a result of collision.
                Vector2 bVel = new Vector2();
                bVel.X = (B1.Velocity.X + B2.Velocity.X) * 0.5f;
                bVel.Y = (B1.Velocity.Y + B2.Velocity.Y) * 0.5f;

                Vector2 relVel = new Vector2();
                relVel.X = A.Velocity.X - bVel.X;
                relVel.Y = A.Velocity.Y - bVel.Y;

                float relDot;
                Vector2.Dot(ref relVel, ref info.normal, out relDot);

                // collision filter!
                if (!materialPairs[info.bodyA.Material, info.bodyB.Material].CollisionFilter(info.bodyA, info.bodyApm, info.bodyB, info.bodyBpmA, info.bodyBpmB, info.hitPt, relDot))
                    continue;

                if (info.penetration > penetrationThreshold)
                {
                    //Console.WriteLine("penetration above Penetration Threshold!!  penetration={0}  threshold={1} difference={2}",
                    //    info.penetration, mPenetrationThreshold, info.penetration-mPenetrationThreshold);

                    penetrationCount++;
                    continue;
                }

                float b1inf = 1.0f - info.edgeD;
                float b2inf = info.edgeD;

                float b2MassSum = ((float.IsPositiveInfinity(B1.Mass)) || (float.IsPositiveInfinity(B2.Mass))) ? float.PositiveInfinity : (B1.Mass + B2.Mass);

                float massSum = A.Mass + b2MassSum;
                
                float Amove;
                float Bmove;
                if (float.IsPositiveInfinity(A.Mass))
                {
                    Amove = 0f;
                    Bmove = (info.penetration) + 0.001f;
                }
                else if (float.IsPositiveInfinity(b2MassSum))
                {
                    Amove = (info.penetration) + 0.001f;
                    Bmove = 0f;
                }
                else
                {
                    Amove = (info.penetration * (b2MassSum / massSum));
                    Bmove = (info.penetration * (A.Mass / massSum));
                }

                float B1move = Bmove * b1inf;
                float B2move = Bmove * b2inf;

                float AinvMass = (float.IsPositiveInfinity(A.Mass)) ? 0f : 1f / A.Mass;
                float BinvMass = (float.IsPositiveInfinity(b2MassSum)) ? 0f : 1f / b2MassSum;

                float jDenom = AinvMass + BinvMass;
                Vector2 numV = new Vector2();
                float elas = 1f + materialPairs[info.bodyA.Material, info.bodyB.Material].Elasticity;
                numV.X = relVel.X * elas;
                numV.Y = relVel.Y * elas;

                float jNumerator;
                Vector2.Dot(ref numV, ref info.normal, out jNumerator);
                jNumerator = -jNumerator;

                float j = jNumerator / jDenom;

                if (!float.IsPositiveInfinity(A.Mass))
                {
                    A.Position.X += info.normal.X * Amove;
                    A.Position.Y += info.normal.Y * Amove;
                }

                if (!float.IsPositiveInfinity(B1.Mass))
                {
                    B1.Position.X -= info.normal.X * B1move;
                    B1.Position.Y -= info.normal.Y * B1move;
                }

                if (!float.IsPositiveInfinity(B2.Mass))
                {
                    B2.Position.X -= info.normal.X * B2move;
                    B2.Position.Y -= info.normal.Y * B2move;
                }
                
                Vector2 tangent = new Vector2();
                VectorTools.getPerpendicular(ref info.normal, ref tangent);
                float friction = materialPairs[info.bodyA.Material,info.bodyB.Material].Friction;
                float fNumerator;
                Vector2.Dot(ref relVel, ref tangent, out fNumerator);
                fNumerator *= friction;
                float f = fNumerator / jDenom;

                // adjust velocity if relative velocity is moving toward each other.
                if (relDot <= 0.0001f)
                {
                    if (!float.IsPositiveInfinity(A.Mass))
                    {
                        A.Velocity.X += (info.normal.X * (j / A.Mass)) - (tangent.X * (f / A.Mass));
                        A.Velocity.Y += (info.normal.Y * (j / A.Mass)) - (tangent.Y * (f / A.Mass));
                    }

                    if (!float.IsPositiveInfinity(b2MassSum))
                    {
                        B1.Velocity.X -= (info.normal.X * (j / b2MassSum) * b1inf) - (tangent.X * (f / b2MassSum) * b1inf);
                        B1.Velocity.Y -= (info.normal.Y * (j / b2MassSum) * b1inf) - (tangent.Y * (f / b2MassSum) * b1inf);
                    }

                    if (!float.IsPositiveInfinity(b2MassSum))
                    {
                        B2.Velocity.X -= (info.normal.X * (j / b2MassSum) * b2inf) - (tangent.X * (f / b2MassSum) * b2inf);
                        B2.Velocity.Y -= (info.normal.Y * (j / b2MassSum) * b2inf) - (tangent.Y * (f / b2MassSum) * b2inf);
                    }
                }
            }
            collisions.Clear();
        }


        /// <summary>
        /// draw the world extents on-screen.
        /// </summary>
        /// <param name="device">Graphics Device</param>
        /// <param name="effect">An Effect to draw the lines with (should implement vertex color diffuse)</param>
        public void debugDrawMe(GraphicsDevice device, Effect effect)
        {
            if (vertexDecl == null)
            {
                vertexDecl = new VertexDeclaration(device, VertexPositionColor.VertexElements);
            }

            // draw the world limits.
            VertexPositionColor[] limits = new VertexPositionColor[5];

            limits[0].Position = new Vector3(worldLimits.UL.X, worldLimits.LR.Y, 0);
            limits[0].Color = Color.SlateGray;

            limits[1].Position = new Vector3(worldLimits.LR.X, worldLimits.LR.Y, 0);
            limits[1].Color = Color.SlateGray;

            limits[2].Position = new Vector3(worldLimits.LR.X, worldLimits.UL.Y, 0);
            limits[2].Color = Color.SlateGray;

            limits[3].Position = new Vector3(worldLimits.UL.X, worldLimits.UL.Y, 0);
            limits[3].Color = Color.SlateGray;

            limits[4].Position = new Vector3(worldLimits.UL.X, worldLimits.LR.Y, 0);
            limits[4].Color = Color.SlateGray;

            device.VertexDeclaration = vertexDecl;
            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, limits, 0, 4);
                pass.End();
            }
            effect.End();
        }

        /// <summary>
        /// draw the velocities of all PointMasses in the simulation on-screen in an orange/yellow color.
        /// </summary>
        /// <param name="device">GraphicsDevice</param>
        /// <param name="effect">An Effect to draw the lines with</param>
        public void debugDrawPointVelocities(GraphicsDevice device, Effect effect)
        {
            if (vertexDecl == null)
            {
                vertexDecl = new VertexDeclaration(device, VertexPositionColor.VertexElements);
            }

            for (int i = 0; i < collider.Count; i++)
            {
                VertexPositionColor[] vels = new VertexPositionColor[collider[i].PointMassCount * 2];

                for (int pm = 0; pm < collider[i].PointMassCount; pm++)
                {
                    vels[(pm * 2) + 0].Position = VectorTools.vec3FromVec2(collider[i].getPointMass(pm).Position);
                    vels[(pm * 2) + 0].Color = Color.Yellow;
                    vels[(pm * 2) + 1].Position = VectorTools.vec3FromVec2(collider[i].getPointMass(pm).Position + (collider[i].getPointMass(pm).Velocity * 0.25f));
                    vels[(pm * 2) + 1].Color = Color.Orange;
                }

                effect.Begin();
                foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                {
                    pass.Begin();
                    device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vels, 0, collider[i].PointMassCount);
                    pass.End();
                }
                effect.End();
            }
        }

        public static bool defaultCollisionFilter(Body A, int Apm, Body B, int Bpm1, int Bpm2, Vector2 hitPt, float normSpeed)
        {
            return true;
        }
        public static bool aCollisionFilter(Body A, int Apm, Body B, int Bpm1, int Bpm2, Vector2 hitPt, float normSpeed)
        {
            return true;
        }

        /// <summary>
        /// Draw all of the bodies in the world in debug mode, for quick visualization of the entire scene.
        /// </summary>
        /// <param name="device">GraphicsDevice</param>
        /// <param name="effect">An Effect to draw the lines with</param>
        /// <param name="drawAABBs"></param>
        public void debugDrawAllBodies(GraphicsDevice device, Effect effect, bool drawAABBs)
        {
            for (int i = 0; i < collider.Count; i++)
            {
                if (drawAABBs)
                    collider[i].debugDrawAABB(device, effect);

                collider[i].debugDrawMe(device, effect);
            }
        }
        
        /// <summary>
        /// This threshold allows objects to be crushed completely flat without snapping through to the other side of objects.
        /// It should be set to a value that is slightly over half the average depth of an object for best results.  Defaults to 0.5.
        /// </summary>
        public float PenetrationThreshold
        {
            set { penetrationThreshold = value; }
            get { return penetrationThreshold; }
        }

        /// <summary>
        /// How many collisions exceeded the Penetration Threshold last update.  if this is a high number, you can assume that
        /// the simulation has "broken" (one or more objects have penetrated inside each other).
        /// </summary>
        public int PenetrationCount
        {
            get { return penetrationCount; }
        }

        public void SetBodyDampening(float damping)
        {
            BodyDamping = damping;

            foreach(Body body in collider){
                body.VelocityDamping = damping;
            }
        }

        public void Clear()
        {
            collider.Clear();
        }

        public virtual void Dispose()
        {
            collisionList = null;
            collider.Clear();
        }
    }
}
