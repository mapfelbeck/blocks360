using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

namespace Physics
{
    /// <summary>
    /// a subclass of SpringBody, with the added element of pressurized gas inside the body.  The amount
    /// of pressure can be adjusted at will to inflate / deflate the object.  The object will not deflate
    /// much smaller than the original size of the Shape if shape matching is enabled.
    /// </summary>
    public class PressureBody : SpringBody
    {
        private float mVolume;
        private float mGasAmount;
        private Vector2[] mNormalList;
        private float[] mEdgeLengthList;

        /// <summary>
        /// Default constructor, with shape-matching ON.
        /// </summary>
        /// <param name="w">World object to add this body to</param>
        /// <param name="s">ClosedShape for this body</param>
        /// <param name="massPerPoint">mass per PointMass</param>
        /// <param name="gasPressure">amount of gas inside the body</param>
        /// <param name="shapeSpringK">shape-matching spring constant</param>
        /// <param name="shapeSpringDamp">shape-matching spring damping</param>
        /// <param name="edgeSpringK">spring constant for edges</param>
        /// <param name="edgeSpringDamp">spring damping for edges</param>
        /// <param name="pos">global position</param>
        /// <param name="angleInRadians">global angle</param>
        /// <param name="scale">scale</param>
        /// <param name="kinematic">kinematic control boolean</param>
        public PressureBody(ClosedShape s, float massPerPoint, float gasPressure, float shapeSpringK, float shapeSpringDamp, float edgeSpringK, float edgeSpringDamp, Vector2 pos, float angleInRadians, Vector2 scale, bool kinematic)
            : base(s, massPerPoint, shapeSpringK, shapeSpringDamp, edgeSpringK, edgeSpringDamp, pos, angleInRadians, scale, kinematic)
        {
            mGasAmount = gasPressure;
            mNormalList = new Vector2[pointMasses.Count];
            mEdgeLengthList = new float[pointMasses.Count];
        }

        /// <summary>
        /// Amount of gas inside the body.
        /// </summary>
        public float GasPressure
        {
            set { mGasAmount = value; }
            get { return mGasAmount; }
        }

        /// <summary>
        /// Gets the last calculated volume for the body.
        /// </summary>
        public float Volume
        {
            get { return mVolume; }
        }

        public override void accumulateInternalForces(float elapsedTime)
        {
            base.accumulateInternalForces(elapsedTime);

            if (!IsAsleep)
            {
                // internal forces based on pressure equations.  we need 2 loops to do this.  one to find the overall volume of the
                // body, and 1 to apply forces.  we will need the normals for the edges in both loops, so we will cache them and remember them.
                mVolume = 0f;

                for (int i = 0; i < pointMasses.Count; i++)
                {
                    int prev = (i > 0) ? i - 1 : pointMasses.Count - 1;
                    int next = (i < pointMasses.Count - 1) ? i + 1 : 0;

                    // currently we are talking about the edge from i --> j.
                    // first calculate the volume of the body, and cache normals as we go.
                    Vector2 edge1N = new Vector2();
                    edge1N.X = pointMasses[i].Position.X - pointMasses[prev].Position.X;
                    edge1N.Y = pointMasses[i].Position.Y - pointMasses[prev].Position.Y;
                    VectorTools.makePerpendicular(ref edge1N);

                    Vector2 edge2N = new Vector2();
                    edge2N.X = pointMasses[next].Position.X - pointMasses[i].Position.X;
                    edge2N.Y = pointMasses[next].Position.Y - pointMasses[i].Position.Y;
                    VectorTools.makePerpendicular(ref edge2N);

                    Vector2 norm = new Vector2();
                    norm.X = edge1N.X + edge2N.X;
                    norm.Y = edge1N.Y + edge2N.Y;

                    float nL = (float)Math.Sqrt((norm.X * norm.X) + (norm.Y * norm.Y));
                    if (nL > 0.001f)
                    {
                        norm.X /= nL;
                        norm.Y /= nL;
                    }

                    float edgeL = (float)Math.Sqrt((edge2N.X * edge2N.X) + (edge2N.Y * edge2N.Y));

                    // cache normal and edge length
                    mNormalList[i] = norm;
                    mEdgeLengthList[i] = edgeL;

                    float xdist = Math.Abs(pointMasses[i].Position.X - pointMasses[next].Position.X);

                    float volumeProduct = xdist * Math.Abs(norm.X) * edgeL;

                    // add to volume
                    mVolume += 0.5f * volumeProduct;
                }

                // now loop through, adding forces!
                float invVolume = 1f / mVolume;

                for (int i = 0; i < pointMasses.Count; i++)
                {
                    int j = (i < pointMasses.Count - 1) ? i + 1 : 0;

                    float pressureV = (invVolume * mEdgeLengthList[i] * mGasAmount);
                    pointMasses[i].Force.X += mNormalList[i].X * pressureV;
                    pointMasses[i].Force.Y += mNormalList[i].Y * pressureV;

                    pointMasses[j].Force.X += mNormalList[j].X * pressureV;
                    pointMasses[j].Force.Y += mNormalList[j].Y * pressureV;
                }
            }
        }

        public override void debugDrawMe(GraphicsDevice device, Effect effect)
        {
            base.debugDrawMe(device, effect);

            // draw edge normals!
            VertexPositionColor[] normals = new VertexPositionColor[pointMasses.Count*2];

            for (int i = 0; i < pointMasses.Count; i++)
            {
                int prev = (i > 0) ? i - 1 : pointMasses.Count - 1;
                int next = (i < pointMasses.Count - 1) ? i + 1 : 0;

                // currently we are talking about the edge from i --> j.
                // first calculate the volume of the body, and cache normals as we go.
                Vector2 edge1N = VectorTools.getPerpendicular(pointMasses[i].Position - pointMasses[prev].Position);
                edge1N.Normalize();

                Vector2 edge2N = VectorTools.getPerpendicular(pointMasses[next].Position - pointMasses[i].Position);
                edge2N.Normalize();

                Vector2 norm = edge1N + edge2N;
                float nL = norm.Length();
                if (nL > 0.001f)
                    norm.Normalize();

                normals[(i * 2) + 0].Position = VectorTools.vec3FromVec2(pointMasses[i].Position);
                normals[(i * 2) + 0].Color = Color.Yellow;

                normals[(i * 2) + 1].Position = VectorTools.vec3FromVec2(pointMasses[i].Position + norm);
                normals[(i * 2) + 1].Color = Color.Honeydew;
            }

            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, normals, 0, pointMasses.Count);
                pass.End();
            }
            effect.End();
        }
    }
}
