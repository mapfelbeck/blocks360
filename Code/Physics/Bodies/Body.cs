using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

namespace Physics
{
    public delegate void BodyCollisionCallback(Body otherBody);
    public delegate void DeleteBodyCallback(Body body);

    /// <summary>
    /// contains base functionality for all bodies in the JelloPhysics world.  all bodies are
    /// made up of a ClosedShape geometry, and a list of PointMass objects equal to the number of vertices in the
    /// ClosedShape geometry.  The vertices are considered to be connected by lines in order, which creates the collision
    /// volume for this body.  Individual implementations of Body handle forcing the body to keep it's shape through
    /// various methods.
    /// </summary>
    public class Body : IQuadTreeItem<Body>
    {
        UpdateItemDelegate<Body> treePositionUpdate;
        public UpdateItemDelegate<Body> UpdateDelegate
        {
            get { return treePositionUpdate; }
            set { treePositionUpdate = value; }
        }

        DeleteItemDelegate<Body> treeDelete;
        public DeleteItemDelegate<Body> DeleteDelegate
        {
            get { return treeDelete; }
            set { treeDelete = value; }
        }

        public BodyCollisionCallback CollisionCallback;
        public DeleteBodyCallback DeleteCallback;

        protected ClosedShape baseShape;
        protected Vector2[] globalShape;

        public Vector2[] GlobalShape
        {
            get { return globalShape; }
        }
        protected List<PointMass> pointMasses;
        protected Vector2 scale;
        protected Vector2 derivedPos;
        protected Vector2 derivedVel;
        protected float derivedAngle;
        protected float derivedOmega;
        protected float lastAngle;
        protected AABB aabb;
        public AABB AABB
        {
            get { return aabb; }
        }
        protected int material;
        protected bool isStatic;
        protected bool kinematic;
        protected object objectTag;
        protected int bodyNumber;
        public int BodyNumber
        {
            get { return bodyNumber; }
            set { bodyNumber = value; }
        }

        protected int groupNumber;
        public int GroupNumber
        {
            get { return groupNumber; }
            set { groupNumber = value; }
        }

        protected float velocityDamping = 0.999f;

        public bool deleteThis = false;

        //// debug visualization variables
        VertexDeclaration vertexDecl = null;

        bool isAsleep = false;
        public bool IsAsleep
        {
            get { return isAsleep; }
            set { isAsleep = value; }
        }

        /// <summary>
        /// Gets / Sets whether this is a static body.  setting static greatly improves performance on static bodies.
        /// </summary>
        public bool IsStatic
        {
            get { return isStatic; }
            set { isStatic = value; }
        }

        /// <summary>
        /// Sets whether this body is kinematically controlled.  kinematic control requires shape-matching forces to work properly.
        /// </summary>
        public bool IsKinematic
        {
            get { return kinematic; }
            set { kinematic = value; }
        }

        public float VelocityDamping
        {
            get { return velocityDamping; }
            set { velocityDamping = value; }
        }

        public object ObjectTag
        {
            get { return objectTag; }
            set { objectTag = value; }
        }

        /// <summary>
        /// default constructor.
        /// </summary>
        /// <param name="w">world to add this body to (done automatically)</param>
        public Body()
        {
            aabb = new AABB();
            baseShape = null;
            globalShape = null;
            pointMasses = new List<PointMass>();
            scale = Vector2.One;
            isStatic = false;
            kinematic = false;

            material = 0;
        }

        /// <summary>
        /// create a body, and set its shape and position immediately
        /// </summary>
        /// <param name="w">world to add this body to (done automatically)</param>
        /// <param name="shape">closed shape for this body</param>
        /// <param name="massPerPoint">mass for each PointMass to be created</param>
        /// <param name="position">global position of the body</param>
        /// <param name="angleInRadians">global angle of the body</param>
        /// <param name="scale">local scale of the body</param>
        /// <param name="kinematic">whether this body is kinematically controlled</param>
        /// <param name="aabb"></param>
        /// <param name="PointMasses"></param>
        public Body(ClosedShape bodyShape, float massPerPoint, Vector2 position,
            float angleInRadians, Vector2 bodyScale, bool isKinematic)
        {
            aabb = new AABB();
            derivedPos = position;
            derivedAngle = angleInRadians;
            lastAngle = derivedAngle;
            scale = bodyScale;
            material = 0;
            isStatic = float.IsPositiveInfinity(massPerPoint);
            kinematic = isKinematic;

            pointMasses = new List<PointMass>();
            setShape(bodyShape);
            for (int i = 0; i < pointMasses.Count; i++)
                pointMasses[i].Mass = massPerPoint;

            updateAABB(0f, true);
        }

        /// <summary>
        /// create a body, and set its shape and position immediately - with individual masses for each PointMass.
        /// </summary>
        /// <param name="w">world to add this body to (done automatically)</param>
        /// <param name="shape">closed shape for this body</param>
        /// <param name="pointMasses">list of masses for each PointMass</param>
        /// <param name="position">global position of the body</param>
        /// <param name="angleInRadians">global angle of the body</param>
        /// <param name="scale">local scale of the body</param>
        /// <param name="kinematic">whether this body is kinematically controlled.</param>
        public Body(ClosedShape bodyShape, List<float> bodyPointMasses, Vector2 position, float angleInRadians, Vector2 bodyScale, bool isKinematic)
        {
            aabb = new AABB();
            derivedPos = position;
            derivedAngle = angleInRadians;
            lastAngle = derivedAngle;
            scale = bodyScale;
            material = 0;
            isStatic = false;
            kinematic = isKinematic;

            pointMasses = new List<PointMass>();
            setShape(bodyShape);
            for (int i = 0; i < pointMasses.Count; i++)
                pointMasses[i].Mass = bodyPointMasses[i];

            updateAABB(0f, true);
        }

        public virtual void Update(float elapsedTime)
        {
            if (treePositionUpdate != null)
            {
                treePositionUpdate(this);
            }
        }

        /// <summary>
        /// set the shape of this body to a new ClosedShape object.  This function 
        /// will remove any existing PointMass objects, and replace them with new ones IF
        /// the new shape has a different vertex count than the previous one.  In this case
        /// the mass for each newly added point mass will be set zero.  Otherwise the shape is just
        /// updated, not affecting the existing PointMasses.
        /// </summary>
        /// <param name="shape">new closed shape</param>
        public void setShape(ClosedShape shape)
        {
            baseShape = shape;

            if (baseShape.Vertices.Count != pointMasses.Count)
            {
                pointMasses.Clear();
                globalShape = new Vector2[baseShape.Vertices.Count];

                baseShape.transformVertices(ref derivedPos, derivedAngle, ref scale, ref globalShape);

                for (int i = 0; i < baseShape.Vertices.Count; i++)
                    pointMasses.Add(new PointMass(0.0f, globalShape[i]));
            }
        }

        /// <summary>
        /// set the mass for each PointMass in this body.
        /// </summary>
        /// <param name="mass">new mass</param>
        public void setMassAll(float mass)
        {
            for (int i = 0; i < pointMasses.Count; i++)
                pointMasses[i].Mass = mass;

            if (float.IsPositiveInfinity(mass)) { isStatic = true; }
        }

        /// <summary>
        /// set the mass for each PointMass individually.
        /// </summary>
        /// <param name="index">index of the PointMass</param>
        /// <param name="mass">new mass</param>
        public void setMassIndividual(int index, float mass)
        {
            if ((index >= 0) && (index < pointMasses.Count))
                pointMasses[index].Mass = mass;
        }

        /// <summary>
        /// set the mass for all point masses from a list.
        /// </summary>
        /// <param name="masses">list of masses (count MUSE equal PointMasses.Count)</param>
        public void setMassFromList(List<float> masses)
        {
            if (masses.Count == pointMasses.Count)
            {
                for (int i = 0; i < pointMasses.Count; i++)
                    pointMasses[i].Mass = masses[i];
            }
        }

        /// <summary>
        /// Material for this body.  Used for physical interaction and collision notification.
        /// </summary>
        public int Material
        {
            get { return material; }
            set { material = value; }
        }

        /// <summary>
        /// Set the position and angle of the body manually.
        /// </summary>
        /// <param name="pos">global position</param>
        /// <param name="angleInRadians">global angle</param>
        public virtual void setPositionAngle(Vector2 pos, float angleInRadians, Vector2 scale)
        {
            baseShape.transformVertices(ref pos, angleInRadians, ref scale, ref globalShape);
            for (int i = 0; i < pointMasses.Count; i++)
                pointMasses[i].Position = globalShape[i];

            derivedPos = pos;
            derivedAngle = angleInRadians;
        }

        /// <summary>
        /// For moving a body kinematically.  sets the position in global space.  via shape-matching, the
        /// body will eventually move to this location.
        /// </summary>
        /// <param name="pos">position in global space.</param>
        public virtual void setKinematicPosition(ref Vector2 pos)
        {
            derivedPos = pos;
        }

        /// <summary>
        /// For moving a body kinematically.  sets the angle in global space.  via shape-matching, the
        /// body will eventually rotate to this angle.
        /// </summary>
        /// <param name="angleInRadians"></param>
        public virtual void setKinematicAngle(float angleInRadians)
        {
            derivedAngle = angleInRadians;
        }

        /// <summary>
        /// For changing a body kinematically.  via shape matching, the body will eventually
        /// change to the given scale.
        /// </summary>
        /// <param name="scale"></param>
        public virtual void setKinematicScale(ref Vector2 newScale)
        {
            scale = newScale;
        }

        /// <summary>
        /// Derive the global position and angle of this body, based on the average of all the points.
        /// This updates the DerivedPosision, DerivedAngle, and DerivedVelocity properties.
        /// This is called by the World object each Update(), so usually a user does not need to call this.  Instead
        /// you can juse access the DerivedPosition, DerivedAngle, DerivedVelocity, and DerivedOmega properties.
        /// </summary>
        public void derivePositionAndAngle(float elaspsed)
        {
            // no need it this is a static body, or kinematically controlled.
            if (isStatic || kinematic || IsAsleep)
                return;

            // find the geometric center.
            Vector2 center = new Vector2();
            center.X = 0;
            center.Y = 0;

            Vector2 vel = new Vector2();
            vel.X = 0f;
            vel.Y = 0f;

            for (int i = 0; i < pointMasses.Count; i++)
            {
                center.X += pointMasses[i].Position.X;
                center.Y += pointMasses[i].Position.Y;

                vel.X += pointMasses[i].Velocity.X;
                vel.Y += pointMasses[i].Velocity.Y;
            }

            center.X /= pointMasses.Count;
            center.Y /= pointMasses.Count;

            vel.X /= pointMasses.Count;
            vel.Y /= pointMasses.Count;

            derivedPos = center;
            derivedVel = vel;

            // find the average angle of all of the masses.
            float angle = 0;
            int originalSign = 1;
            float originalAngle = 0;
            for (int i = 0; i < pointMasses.Count; i++)
            {
                Vector2 baseNorm = new Vector2();
                baseNorm.X = baseShape.Vertices[i].X;
                baseNorm.Y = baseShape.Vertices[i].Y;
                Vector2.Normalize(ref baseNorm, out baseNorm);

                Vector2 curNorm = new Vector2();
                curNorm.X = pointMasses[i].Position.X - derivedPos.X;
                curNorm.Y = pointMasses[i].Position.Y - derivedPos.Y;
                Vector2.Normalize(ref curNorm, out curNorm);

                float dot;
                Vector2.Dot(ref baseNorm, ref curNorm, out dot);
                if (dot > 1.0f) { dot = 1.0f; }
                if (dot < -1.0f) { dot = -1.0f; }

                float thisAngle = (float)Math.Acos(dot);
                if (!VectorTools.isCCW(ref baseNorm, ref curNorm)) { thisAngle = -thisAngle; }

                if (i == 0)
                {
                    originalSign = (thisAngle >= 0.0f) ? 1 : -1;
                    originalAngle = thisAngle;
                }
                else
                {
                    float diff = (thisAngle - originalAngle);
                    int thisSign = (thisAngle >= 0.0f) ? 1 : -1;

                    if ((Math.Abs(diff) > Math.PI) && (thisSign != originalSign))
                    {
                        thisAngle = (thisSign == -1) ? ((float)Math.PI + ((float)Math.PI + thisAngle)) : (((float)Math.PI - thisAngle) - (float)Math.PI);
                    }
                }

                angle += thisAngle;
            }

            angle /= pointMasses.Count;
            derivedAngle = angle;

            // now calculate the derived Omega, based on change in angle over time.
            float angleChange = (derivedAngle - lastAngle);
            if (Math.Abs(angleChange) >= Math.PI)
            {
                if (angleChange < 0f)
                    angleChange = angleChange + (float)(Math.PI * 2);
                else
                    angleChange = angleChange - (float)(Math.PI * 2);
            }

            derivedOmega = angleChange / elaspsed;

            lastAngle = derivedAngle;
        }

        /// <summary>
        /// Derived position of the body in global space, based on location of all PointMasses.
        /// </summary>
        public Vector2 DerivedPosition
        {
            get { return derivedPos; }
        }

        /// <summary>
        /// Derived global angle of the body in global space, based on location of all PointMasses.
        /// </summary>
        public float DerivedAngle
        {
            get { return derivedAngle; }
        }

        /// <summary>
        /// Derived global velocity of the body in global space, based on velocity of all PointMasses.
        /// </summary>
        public Vector2 DerivedVelocity
        {
            get { return derivedVel; }
        }

        /// <summary>
        /// Derived rotational velocity of the body in global space, based on changes in DerivedAngle.
        /// </summary>
        public float DerivedOmega
        {
            get { return derivedOmega; }
        }

        /// <summary>
        /// this function should add all internal forces to the Force member variable of each PointMass in the body.
        /// these should be forces that try to maintain the shape of the body.
        /// </summary>
        public virtual void accumulateInternalForces(float elapsedTime) { }

        /// <summary>
        /// this function should add all external forces to the Force member variable of each PointMass in the body.
        /// these are external forces acting on the PointMasses, such as gravity, etc.
        /// </summary>
        public virtual void accumulateExternalForces(float elapsedTime) { }

        public virtual void resetExternalForces() { }

        internal void integrate(float elapsed)
        {
            if (isStatic || isAsleep) { return; }

            for (int i = 0; i < pointMasses.Count; i++)
                pointMasses[i].integrateForce(elapsed);
        }

        internal void dampenVelocity(float elapsed)
        {
            if (isStatic || isAsleep) { return; }

            float perFrameFriction = (float)Math.Pow((double)(1 - velocityDamping), (double)elapsed);

            for (int i = 0; i < pointMasses.Count; i++)
            {
                pointMasses[i].Velocity.X *= perFrameFriction;
                pointMasses[i].Velocity.Y *= perFrameFriction;
            }
        }

        /// <summary>
        /// update the AABB for this body, including padding for velocity given a timestep.
        /// This function is called by the World object on Update(), so the user should not need this in most cases.
        /// </summary>
        /// <param name="elapsed">elapsed time in seconds</param>
        public void updateAABB(float elapsed, bool forceUpdate)
        {
            if ((!IsStatic) || (forceUpdate))
            {
                aabb.Clear();
                for (int i = 0; i < pointMasses.Count; i++)
                {
                    Vector2 p = pointMasses[i].Position;
                    aabb.ExpandToInclude(ref p);

                    // expanding for velocity only makes sense for dynamic objects.
                    if (!IsStatic)
                    {
                        p.X += (pointMasses[i].Velocity.X * elapsed);
                        p.Y += (pointMasses[i].Velocity.Y * elapsed);
                        aabb.ExpandToInclude(ref p);
                    }
                }
            }
        }

        public void UpdateAABB()
        {
            updateAABB(1f / 60f, true);
        }

        /// <summary>
        /// get the Axis-aligned bounding box for this body.  used for broad-phase collision checks.
        /// </summary>
        /// <returns>AABB for this body</returns>
        public AABB getAABB()
        {
            return aabb;
        }

        /// <summary>
        /// collision detection.  detect if a global point is inside this body.
        /// </summary>
        /// <param name="pt">point in global space</param>
        /// <returns>true = point is inside body, false = it is not.</returns>
        public bool contains(ref Vector2 pt)
        {
            // basic idea: draw a line from the point to a point known to be outside the body.  count the number of
            // lines in the polygon it intersects.  if that number is odd, we are inside.  if it's even, we are outside.
            // in this implementation we will always use a line that moves off in the positive X direction from the point
            // to simplify things.
            Vector2 endPt = new Vector2();
            endPt.X = aabb.LR.X + 0.1f;
            endPt.Y = pt.Y;

            // line we are testing against goes from pt -> endPt.
            bool inside = false;
            Vector2 edgeSt = pointMasses[0].Position;
            Vector2 edgeEnd = new Vector2();
            int c = pointMasses.Count;
            for (int i = 0; i < c; i++)
            {
                // the current edge is defined as the line from edgeSt -> edgeEnd.
                if (i < (c - 1))
                    edgeEnd = pointMasses[i + 1].Position;
                else
                    edgeEnd = pointMasses[0].Position;

                // perform check now...
                if (((edgeSt.Y <= pt.Y) && (edgeEnd.Y > pt.Y)) || ((edgeSt.Y > pt.Y) && (edgeEnd.Y <= pt.Y)))
                {
                    // this line crosses the test line at some point... does it do so within our test range?
                    float slope = (edgeEnd.X - edgeSt.X) / (edgeEnd.Y - edgeSt.Y);
                    float hitX = edgeSt.X + ((pt.Y - edgeSt.Y) * slope);

                    if ((hitX >= pt.X) && (hitX <= endPt.X))
                        inside = !inside;
                }
                edgeSt = edgeEnd;
            }

            return inside;
        }

        /// <summary>
        /// collision detection - given a global point, find the point on this body that is closest to the global point,
        /// and if it is an edge, information about the edge it resides on.
        /// </summary>
        /// <param name="pt">global point</param>
        /// <param name="hitPt">returned point on the body in global space</param>
        /// <param name="normal">returned normal on the body in global space</param>
        /// <param name="pointA">returned ptA on the edge</param>
        /// <param name="pointB">returned ptB on the edge</param>
        /// <param name="edgeD">scalar distance between ptA and ptB [0,1]</param>
        /// <returns>distance</returns>
        public float getClosestPoint(Vector2 pt, out Vector2 hitPt, out Vector2 normal, out int pointA, out int pointB, out float edgeD)
        {
            hitPt = Vector2.Zero;
            pointA = -1;
            pointB = -1;
            edgeD = 0f;
            normal = Vector2.Zero;

            float closestD = 1000.0f;

            for (int i = 0; i < pointMasses.Count; i++)
            {
                Vector2 tempHit;
                Vector2 tempNorm;
                float tempEdgeD;

                float dist = getClosestPointOnEdge(pt, i, out tempHit, out tempNorm, out tempEdgeD);
                if (dist < closestD)
                {
                    closestD = dist;
                    pointA = i;
                    if (i < (pointMasses.Count - 1))
                        pointB = i + 1;
                    else
                        pointB = 0;
                    edgeD = tempEdgeD;
                    normal = tempNorm;
                    hitPt = tempHit;
                }
            }


            // return.
            return closestD;
        }

        /// <summary>
        /// find the distance from a global point in space, to the closest point on a given edge of the body.
        /// </summary>
        /// <param name="pt">global point</param>
        /// <param name="edgeNum">edge to check against.  0 = edge from pt[0] to pt[1], etc.</param>
        /// <param name="hitPt">returned point on edge in global space</param>
        /// <param name="normal">returned normal on edge in global space</param>
        /// <param name="edgeD">returned distance along edge from ptA to ptB [0,1]</param>
        /// <returns>distance</returns>
        public float getClosestPointOnEdge(Vector2 pt, int edgeNum, out Vector2 hitPt, out Vector2 normal, out float edgeD)
        {
            hitPt = new Vector2();
            hitPt.X = 0f;
            hitPt.Y = 0f;

            normal = new Vector2();
            normal.X = 0f;
            normal.Y = 0f;

            edgeD = 0f;
            float dist = 0f;

            Vector2 ptA = pointMasses[edgeNum].Position;
            Vector2 ptB = new Vector2();

            if (edgeNum < (pointMasses.Count - 1))
                ptB = pointMasses[edgeNum + 1].Position;
            else
                ptB = pointMasses[0].Position;

            Vector2 toP = new Vector2();
            toP.X = pt.X - ptA.X;
            toP.Y = pt.Y - ptA.Y;

            Vector2 E = new Vector2();
            E.X = ptB.X - ptA.X;
            E.Y = ptB.Y - ptA.Y;

            // get the length of the edge, and use that to normalize the vector.
            float edgeLength = (float)Math.Sqrt((E.X * E.X) + (E.Y * E.Y));
            if (edgeLength > 0.00001f)
            {
                E.X /= edgeLength;
                E.Y /= edgeLength;
            }

            // normal
            Vector2 n = new Vector2();
            VectorTools.getPerpendicular(ref E, ref n);

            // calculate the distance!
            float x;
            Vector2.Dot(ref toP, ref E, out x);
            if (x <= 0.0f)
            {
                // x is outside the line segment, distance is from pt to ptA.
                //dist = (pt - ptA).Length();
                Vector2.Distance(ref pt, ref ptA, out dist);
                hitPt = ptA;
                edgeD = 0f;
                normal = n;
            }
            else if (x >= edgeLength)
            {
                // x is outside of the line segment, distance is from pt to ptB.
                //dist = (pt - ptB).Length();
                Vector2.Distance(ref pt, ref ptB, out dist);
                hitPt = ptB;
                edgeD = 1f;
                normal = n;
            }
            else
            {
                // point lies somewhere on the line segment.
                Vector3 toP3 = new Vector3();
                toP3.X = toP.X;
                toP3.Y = toP.Y;

                Vector3 E3 = new Vector3();
                E3.X = E.X;
                E3.Y = E.Y;

                //dist = Math.Abs(Vector3.Cross(toP3, E3).Z);
                Vector3.Cross(ref toP3, ref E3, out E3);
                dist = Math.Abs(E3.Z);
                hitPt.X = ptA.X + (E.X * x);
                hitPt.Y = ptA.Y + (E.Y * x);
                edgeD = x / edgeLength;
                normal = n;
            }

            return dist;
        }

        /// <summary>
        /// find the squared distance from a global point in space, to the closest point on a given edge of the body.
        /// </summary>
        /// <param name="pt">global point</param>
        /// <param name="edgeNum">edge to check against.  0 = edge from pt[0] to pt[1], etc.</param>
        /// <param name="hitPt">returned point on edge in global space</param>
        /// <param name="normal">returned normal on edge in global space</param>
        /// <param name="edgeD">returned distance along edge from ptA to ptB [0,1]</param>
        /// <returns>distance</returns>
        public float getClosestPointOnEdgeSquared(Vector2 pt, int edgeNum, out Vector2 hitPt, out Vector2 normal, out float edgeD)
        {
            hitPt = new Vector2();
            hitPt.X = 0f;
            hitPt.Y = 0f;

            normal = new Vector2();
            normal.X = 0f;
            normal.Y = 0f;

            edgeD = 0f;
            float dist = 0f;

            Vector2 ptA = pointMasses[edgeNum].Position;
            Vector2 ptB = new Vector2();

            if (edgeNum < (pointMasses.Count - 1))
                ptB = pointMasses[edgeNum + 1].Position;
            else
                ptB = pointMasses[0].Position;

            Vector2 toP = new Vector2();
            toP.X = pt.X - ptA.X;
            toP.Y = pt.Y - ptA.Y;

            Vector2 E = new Vector2();
            E.X = ptB.X - ptA.X;
            E.Y = ptB.Y - ptA.Y;

            // get the length of the edge, and use that to normalize the vector.
            float edgeLength = (float)Math.Sqrt((E.X * E.X) + (E.Y * E.Y));
            if (edgeLength > 0.00001f)
            {
                E.X /= edgeLength;
                E.Y /= edgeLength;
            }

            // normal
            Vector2 n = new Vector2();
            VectorTools.getPerpendicular(ref E, ref n);

            // calculate the distance!
            float x;
            Vector2.Dot(ref toP, ref E, out x);
            if (x <= 0.0f)
            {
                // x is outside the line segment, distance is from pt to ptA.
                //dist = (pt - ptA).Length();
                Vector2.DistanceSquared(ref pt, ref ptA, out dist);
                hitPt = ptA;
                edgeD = 0f;
                normal = n;
            }
            else if (x >= edgeLength)
            {
                // x is outside of the line segment, distance is from pt to ptB.
                //dist = (pt - ptB).Length();
                Vector2.DistanceSquared(ref pt, ref ptB, out dist);
                hitPt = ptB;
                edgeD = 1f;
                normal = n;
            }
            else
            {
                // point lies somewhere on the line segment.
                Vector3 toP3 = new Vector3();
                toP3.X = toP.X;
                toP3.Y = toP.Y;

                Vector3 E3 = new Vector3();
                E3.X = E.X;
                E3.Y = E.Y;

                //dist = Math.Abs(Vector3.Cross(toP3, E3).Z);
                Vector3.Cross(ref toP3, ref E3, out E3);
                dist = Math.Abs(E3.Z * E3.Z);
                hitPt.X = ptA.X + (E.X * x);
                hitPt.Y = ptA.Y + (E.Y * x);
                edgeD = x / edgeLength;
                normal = n;
            }

            return dist;
        }


        /// <summary>
        /// Find the closest PointMass in this body, givena global point.
        /// </summary>
        /// <param name="pos">global point</param>
        /// <param name="dist">returned dist</param>
        /// <returns>index of the PointMass</returns>
        public int getClosestPointMass(Vector2 pos, out float dist)
        {
            float closestSQD = 100000.0f;
            int closest = -1;

            for (int i = 0; i < pointMasses.Count; i++)
            {
                float thisD = (pos - pointMasses[i].Position).LengthSquared();
                if (thisD < closestSQD)
                {
                    closestSQD = thisD;
                    closest = i;
                }
            }

            dist = (float)Math.Sqrt(closestSQD);
            return closest;
        }

        /// <summary>
        /// Number of PointMasses in the body
        /// </summary>
        public int PointMassCount
        {
            get { return pointMasses.Count; }
        }

        /// <summary>
        /// Get a specific PointMass from this body.
        /// </summary>
        /// <param name="index">index</param>
        /// <returns>PointMass</returns>
        public PointMass getPointMass(int index)
        {
            return pointMasses[index];
        }

        /// <summary>
        /// Helper function to add a global force acting on this body as a whole.
        /// </summary>
        /// <param name="pt">location of force, in global space</param>
        /// <param name="force">direction and intensity of force, in global space</param>
        public void addGlobalForce(ref Vector2 pt, ref Vector2 force)
        {
            Vector2 R = (derivedPos - pt);

            float torqueF = Vector3.Cross(VectorTools.vec3FromVec2(R), VectorTools.vec3FromVec2(force)).Z;

            for (int i = 0; i < pointMasses.Count; i++)
            {
                Vector2 toPt = (pointMasses[i].Position - derivedPos);
                Vector2 torque = VectorTools.rotateVector(toPt, -(float)(Math.PI) / 2f);

                pointMasses[i].Force += torque * torqueF;

                pointMasses[i].Force += force;
            }
        }

        /// <summary>
        /// This function draws the points to the screen as lines, showing several things:
        /// WHITE - actual PointMasses, connected by lines
        /// GREY - baseshape, at the derived position and angle.
        /// </summary>
        /// <param name="device">graphics device</param>
        /// <param name="effect">effect to use (MUST implement VertexColors)</param>
        public virtual void debugDrawMe(GraphicsDevice device, Effect effect)
        {
            if (vertexDecl == null)
            {
                vertexDecl = new VertexDeclaration(device, VertexPositionColor.VertexElements);
            }


            ////////////////////////////////////////////////////////////////////////////
            // fill the debug verts with derived positions.
            baseShape.transformVertices(ref derivedPos, derivedAngle, ref scale, ref globalShape);

            VertexPositionColor[] debugVerts = new VertexPositionColor[globalShape.Length + 1];
            for (int i = 0; i < globalShape.Length; i++)
            {
                debugVerts[i].Position = VectorTools.vec3FromVec2(globalShape[i]);
                debugVerts[i].Color = Color.Gray;
            }

            debugVerts[debugVerts.Length - 1].Position = VectorTools.vec3FromVec2(globalShape[0]);
            debugVerts[debugVerts.Length - 1].Color = Color.Gray;

            device.VertexDeclaration = vertexDecl;
            device.RenderState.PointSize = 5.0f;

            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, debugVerts, 0, globalShape.Length);
                //device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.PointList, debugVerts, 0, mGlobalShape.Length);
                pass.End();
            }
            effect.End();

            ////////////////////////////////////////////////////////////////////////////
            // fill the debug verts with global positions.
            for (int i = 0; i < pointMasses.Count; i++)
            {
                debugVerts[i].Position = VectorTools.vec3FromVec2(pointMasses[i].Position);
                debugVerts[i].Color = Color.White;
            }

            debugVerts[debugVerts.Length - 1].Position = VectorTools.vec3FromVec2(pointMasses[0].Position);
            debugVerts[debugVerts.Length - 1].Color = Color.White;

            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, debugVerts, 0, pointMasses.Count);
                //device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.PointList, debugVerts, 0, mPointMasses.Count);

                // derived center.
                VertexPositionColor[] center = new VertexPositionColor[1];
                center[0].Position = VectorTools.vec3FromVec2(derivedPos);
                center[0].Color = Color.IndianRed;
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.PointList, center, 0, 1);

                // UP and LEFT vectors.
                VertexPositionColor[] axis = new VertexPositionColor[4];
                axis[0].Position = VectorTools.vec3FromVec2(derivedPos);
                axis[0].Color = Color.OrangeRed;
                axis[1].Position = VectorTools.vec3FromVec2(derivedPos + (VectorTools.rotateVector(Vector2.UnitY, derivedAngle)));
                axis[1].Color = Color.OrangeRed;

                axis[2].Position = VectorTools.vec3FromVec2(derivedPos);
                axis[2].Color = Color.YellowGreen;
                axis[3].Position = VectorTools.vec3FromVec2(derivedPos + (VectorTools.rotateVector(Vector2.UnitX, derivedAngle)));
                axis[3].Color = Color.YellowGreen;

                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, axis, 0, 2);
                pass.End();
            }
            effect.End();


        }

        /// <summary>
        /// Draw the AABB for this body, for debug purposes.
        /// </summary>
        /// <param name="device">graphics device</param>
        /// <param name="effect">effect to use (MUST implement VertexColors)</param>
        public void debugDrawAABB(GraphicsDevice device, Effect effect)
        {
            AABB box = getAABB();

            if (vertexDecl == null)
            {
                vertexDecl = new VertexDeclaration(device, VertexPositionColor.VertexElements);
            }

            // draw the world limits.
            VertexPositionColor[] limits = new VertexPositionColor[5];

            limits[0].Position = new Vector3(box.UL.X, box.LR.Y, 0);
            limits[0].Color = Color.SlateGray;

            limits[1].Position = new Vector3(box.LR.X, box.LR.Y, 0);
            limits[1].Color = Color.SlateGray;

            limits[2].Position = new Vector3(box.LR.X, box.UL.Y, 0);
            limits[2].Color = Color.SlateGray;

            limits[3].Position = new Vector3(box.UL.X, box.UL.Y, 0);
            limits[3].Color = Color.SlateGray;

            limits[4].Position = new Vector3(box.UL.X, box.LR.Y, 0);
            limits[4].Color = Color.SlateGray;

            device.VertexDeclaration = vertexDecl;
            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, limits, 0, 4);
                pass.End();
            }
            effect.End();
        }

        public void drawMe(GraphicsDevice graphicsDevice, BasicEffect mEffect)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public static void bodyCollide(Body bA, Body bB, List<BodyCollisionInfo> infoList, float penetrationThreshold)
        {
            if (bA == null || bB == null)
            {
                throw new Exception("Poowang!");
            }
            int bApmCount = bA.PointMassCount;
            int bBpmCount = bB.PointMassCount;

            AABB boxB = bB.getAABB();

            // check all PointMasses on bodyA for collision against bodyB.  if there is a collision, return detailed info.
            BodyCollisionInfo infoAway = new BodyCollisionInfo();
            BodyCollisionInfo infoSame = new BodyCollisionInfo();
            bool BodyCollisionInfoAwayCreated = false;
            bool BodyCollisionInfoSameCreated = false;
            for (int i = 0; i < bApmCount; i++)
            {
                BodyCollisionInfoAwayCreated = false;
                BodyCollisionInfoSameCreated = false;
                Vector2 pt = bA.getPointMass(i).Position;

                // early out - if this point is outside the bounding box for bodyB, skip it!
                if (!boxB.Contains(ref pt))
                    continue;

                // early out - if this point is not inside bodyB, skip it!
                if (!bB.contains(ref pt))
                    continue;

                int prevPt = (i > 0) ? i - 1 : bApmCount - 1;
                int nextPt = (i < bApmCount - 1) ? i + 1 : 0;

                Vector2 prev = bA.getPointMass(prevPt).Position;
                Vector2 next = bA.getPointMass(nextPt).Position;

                // now get the normal for this point. (NOT A UNIT VECTOR)
                Vector2 fromPrev = new Vector2();
                fromPrev.X = pt.X - prev.X;
                fromPrev.Y = pt.Y - prev.Y;

                Vector2 toNext = new Vector2();
                toNext.X = next.X - pt.X;
                toNext.Y = next.Y - pt.Y;

                Vector2 ptNorm = new Vector2();
                ptNorm.X = fromPrev.X + toNext.X;
                ptNorm.Y = fromPrev.Y + toNext.Y;
                VectorTools.makePerpendicular(ref ptNorm);

                // this point is inside the other body.  now check if the edges on either side intersect with and edges on bodyB.          
                float closestAway = 100000.0f;
                float closestSame = 100000.0f;

                infoAway.Clear();
                infoAway.bodyA = bA;
                infoAway.bodyApm = i;
                infoAway.bodyB = bB;

                infoSame.Clear();
                infoSame.bodyA = bA;
                infoSame.bodyApm = i;
                infoSame.bodyB = bB;

                bool found = false;

                int b1 = 0;
                int b2 = 1;
                for (int j = 0; j < bBpmCount; j++)
                {
                    Vector2 hitPt;
                    Vector2 norm;
                    float edgeD;

                    b1 = j;

                    if (j < bBpmCount - 1)
                        b2 = j + 1;
                    else
                        b2 = 0;

                    /*
                    // quick test of distance to each point on the edge, if both are greater than current mins, we can skip!
                    // FIXME: figure out why this optimization fails in some cases
                    Vector2 pt1 = bB.getPointMass(b1).Position;
                    Vector2 pt2 = bB.getPointMass(b2).Position;

                    float distToA = ((pt1.X - pt.X) * (pt1.X - pt.X)) + ((pt1.Y - pt.Y) * (pt1.Y - pt.Y));
                    float distToB = ((pt2.X - pt.X) * (pt2.X - pt.X)) + ((pt2.Y - pt.Y) * (pt2.Y - pt.Y));

                    
                    if ((distToA > closestAway) && (distToA > closestSame) && (distToB > closestAway) && (distToB > closestSame))
                    {
                        continue;
                    }*/

                    // test against this edge.
                    float dist = bB.getClosestPointOnEdgeSquared(pt, j, out hitPt, out norm, out edgeD);

                    // only perform the check if the normal for this edge is facing AWAY from the point normal.
                    float dot;
                    Vector2.Dot(ref ptNorm, ref norm, out dot);

                    if (dot <= 0f)
                    {
                        if (dist < closestAway)
                        {
                            closestAway = dist;
                            infoAway.bodyBpmA = b1;
                            infoAway.bodyBpmB = b2;
                            infoAway.edgeD = edgeD;
                            infoAway.hitPt = hitPt;
                            infoAway.normal = norm;
                            infoAway.penetration = dist;
                            found = true;
                            BodyCollisionInfoAwayCreated = true;
                        }
                    }
                    else
                    {
                        if (dist < closestSame)
                        {
                            closestSame = dist;
                            infoSame.bodyBpmA = b1;
                            infoSame.bodyBpmB = b2;
                            infoSame.edgeD = edgeD;
                            infoSame.hitPt = hitPt;
                            infoSame.normal = norm;
                            infoSame.penetration = dist;
                            BodyCollisionInfoSameCreated = true;
                        }
                    }
                }

                // we've checked all edges on BodyB.  add the collision info to the stack.
                if ((found) && (closestAway > penetrationThreshold) && (closestSame < closestAway) &&
                    BodyCollisionInfoSameCreated)
                {
                    infoSame.penetration = (float)Math.Sqrt(infoSame.penetration);
                    infoList.Add(infoSame);
                }
                else if (BodyCollisionInfoAwayCreated)
                {
                    infoAway.penetration = (float)Math.Sqrt(infoAway.penetration);
                    infoList.Add(infoAway);
                }
            }
        }
    }
}