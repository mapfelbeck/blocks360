using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Utilities;

namespace Physics
{
    /// <summary>
    /// The simplest type of Body, that tries to maintain its shape through shape-matching (global springs that
    /// try to keep the original shape), and internal springs for support.  Shape matching forces can be
    /// enabled / disabled at will.
    /// </summary>
    public class SpringBody : Body
    {
        protected List<InternalSpring> springs;

        // shape-matching spring constants.
        protected bool shapeMatchingOn = true;
        protected float edgeSpringK;
        protected float edgeSpringDamp;
        protected float shapeSpringK;
        protected float shapeSpringDamp;
                
        //// debug visualization variables
        VertexDeclaration vertexDecl = null;

        /// <summary>
        /// Create a SpringBody with shape matching turned ON.
        /// </summary>
        /// <param name="w"></param>
        /// <param name="shape">ClosedShape shape for this body</param>
        /// <param name="massPerPoint">mass per PointMass.</param>
        /// <param name="shapeSpringK">shape-matching spring constant</param>
        /// <param name="shapeSpringDamp">shape-matching spring damping</param>
        /// <param name="edgeSpringK">spring constant for edges.</param>
        /// <param name="edgeSpringDamp">spring damping for edges</param>
        /// <param name="pos">global position</param>
        /// <param name="angleinRadians">global angle</param>
        /// <param name="scale">scale</param>
        /// <param name="kinematic">kinematic control boolean</param>
        public SpringBody(ClosedShape shape, float massPerPoint, float bodyShapeSpringK, float bodyShapeSpringDamp, float bodyEdgeSpringK, float bodyEdgeSpringDamp, Vector2 pos, float angleinRadians, Vector2 scale, bool kinematic)
            : base(shape, massPerPoint, pos, angleinRadians, scale, kinematic)
        {
            springs = new List<InternalSpring>();

            base.setPositionAngle(pos, angleinRadians, scale);

            shapeMatchingOn = true;
            shapeSpringK = bodyShapeSpringK;
            shapeSpringDamp = bodyShapeSpringDamp;
            edgeSpringK = bodyEdgeSpringK;
            edgeSpringDamp = bodyEdgeSpringDamp;

            // build default springs.
            _buildDefaultSprings();
        }

        /// <summary>
        /// Create a SpringBody with shape matching turned ON.
        /// </summary>
        /// <param name="w"></param>
        /// <param name="shape">ClosedShape shape for this body</param>
        /// <param name="massPerPoint">mass per PointMass.</param>
        /// <param name="shapeSpringK">shape-matching spring constant</param>
        /// <param name="shapeSpringDamp">shape-matching spring damping</param>
        /// <param name="edgeSpringK">spring constant for edges.</param>
        /// <param name="edgeSpringDamp">spring damping for edges</param>
        /// <param name="pos">global position</param>
        /// <param name="angleinRadians">global angle</param>
        /// <param name="scale">scale</param>
        /// <param name="kinematic">kinematic control boolean</param>
        public SpringBody(ClosedShape shape, float massPerPoint, float bodyShapeSpringK, float bodyShapeSpringDamp, float bodyEdgeSpringK, float bodyEdgeSpringDamp, Vector2 pos, float angleinRadians, Vector2 scale, bool kinematic, AABB aabb, List<PointMass> PointMasses)
            : base(shape, massPerPoint, pos, angleinRadians, scale, kinematic)
        {
            springs = new List<InternalSpring>();

            base.setPositionAngle(pos, angleinRadians, scale);

            shapeMatchingOn = true;
            shapeSpringK = bodyShapeSpringK;
            shapeSpringDamp = bodyShapeSpringDamp;
            edgeSpringK = bodyEdgeSpringK;
            edgeSpringDamp = bodyEdgeSpringDamp;

            // build default springs.
            _buildDefaultSprings();
        }

        /// <summary>
        /// Add an internal spring to this body.
        /// </summary>
        /// <param name="pointA">point mass on 1st end of the spring</param>
        /// <param name="pointB">point mass on 2nd end of the spring</param>
        /// <param name="springK">spring constant</param>
        /// <param name="damping">spring damping</param>
        public void addInternalSpring(int pointA, int pointB, float springK, float damping)
        {
            float dist = (pointMasses[pointB].Position - pointMasses[pointA].Position).Length();
            InternalSpring s = new InternalSpring(pointA, pointB, dist, springK, damping);

            springs.Add(s);
        }

        /// <summary>
        /// Clear all springs from the body.
        /// </summary>
        /// <param name="k"></param>
        /// <param name="damp"></param>
        public void clearAllSprings()
        {
            springs.Clear();
            _buildDefaultSprings();
        }

        private void _buildDefaultSprings()
        {
            for (int i = 0; i < pointMasses.Count; i++)
            {
                if (i < (pointMasses.Count - 1))
                    addInternalSpring(i, i + 1, edgeSpringK, edgeSpringDamp);
                else
                    addInternalSpring(i, 0, edgeSpringK, edgeSpringDamp);
            }
        }

        /// <summary>
        /// Set shape-matching on/off.
        /// </summary>
        /// <param name="onoff">boolean</param>
        public void setShapeMatching(bool onoff) { shapeMatchingOn = onoff; }

        /// <summary>
        /// Set shape-matching spring constants.
        /// </summary>
        /// <param name="springK">spring constant</param>
        /// <param name="damping">spring damping</param>
        public void setShapeMatchingConstants(float springK, float damping) { shapeSpringK = springK; shapeSpringDamp = damping; }

        /// <summary>
        /// Change the spring constants for the springs around the shape itself (edge springs)
        /// </summary>
        /// <param name="edgeSpringK">spring constant</param>
        /// <param name="edgeSpringDamp">spring damping</param>
        public void setEdgeSpringConstants(float edgeSpringK, float edgeSpringDamp)
        {
            // we know that the first n springs in the list are the edge springs.
            for (int i = 0; i < pointMasses.Count; i++)
            {
                springs[i].springK = edgeSpringK;
                springs[i].damping = edgeSpringDamp;
            }
        }

        public void setSpringConstants(int springID, float springK, float springDamp)
        {
            // index is for all internal springs, AFTER the default internal springs.
            int index = pointMasses.Count + springID;
            springs[index].springK = springK;
            springs[index].damping = springDamp;
        }

        public float getSpringK(int springID)
        {
            int index = pointMasses.Count + springID;
            return springs[index].springK;
        }

        public float getSpringDamping(int springID)
        {
            int index = pointMasses.Count + springID;
            return springs[index].damping;
        }


        public override void accumulateInternalForces(float elapsedTime)
        {
            base.accumulateInternalForces(elapsedTime);

            if (!IsAsleep)
            {
                // internal spring forces.
                Vector2 force = new Vector2();
                InternalSpring s;
                for (int i = 0; i < springs.Count; i++)
                {
                    s = springs[i];
                    VectorTools.calculateSpringForce(ref pointMasses[s.pointMassA].Position, ref pointMasses[s.pointMassA].Velocity,
                        ref pointMasses[s.pointMassB].Position, ref pointMasses[s.pointMassB].Velocity,
                        s.springD, s.springK, s.damping,
                        ref force);

                    pointMasses[s.pointMassA].Force.X += force.X;
                    pointMasses[s.pointMassA].Force.Y += force.Y;

                    pointMasses[s.pointMassB].Force.X -= force.X;
                    pointMasses[s.pointMassB].Force.Y -= force.Y;
                }

                // shape matching forces.
                if (shapeMatchingOn)
                {
                    baseShape.transformVertices(ref derivedPos, derivedAngle, ref scale, ref globalShape);
                    for (int i = 0; i < pointMasses.Count; i++)
                    {
                        if (shapeSpringK > 0)
                        {
                            if (!kinematic)
                            {
                                VectorTools.calculateSpringForce(ref pointMasses[i].Position, ref pointMasses[i].Velocity,
                                    ref globalShape[i], ref pointMasses[i].Velocity, 0.0f, shapeSpringK, shapeSpringDamp,
                                    ref force);
                            }
                            else
                            {
                                Vector2 kinVel = Vector2.Zero;
                                VectorTools.calculateSpringForce(ref pointMasses[i].Position, ref pointMasses[i].Velocity,
                                    ref globalShape[i], ref kinVel, 0.0f, shapeSpringK, shapeSpringDamp,
                                    ref force);
                            }

                            pointMasses[i].Force.X += force.X;
                            pointMasses[i].Force.Y += force.Y;
                        }
                    }
                }
            }
        }

        public override void debugDrawMe(GraphicsDevice device, Effect effect)
        {
            if (vertexDecl == null)
            {
                vertexDecl = new VertexDeclaration(device, VertexPositionColor.VertexElements);
            }

            // now draw the goal positions.
            VertexPositionColor[] shapeVertices = new VertexPositionColor[pointMasses.Count * 2];
            VertexPositionColor[] springVertices = new VertexPositionColor[springs.Count * 2];

            baseShape.transformVertices(ref derivedPos, derivedAngle, ref scale, ref globalShape);
            for (int i = 0; i < pointMasses.Count; i++)
            {
                shapeVertices[(i * 2) + 0].Position = VectorTools.vec3FromVec2(pointMasses[i].Position);
                shapeVertices[(i * 2) + 0].Color = Color.LawnGreen;

                shapeVertices[(i * 2) + 1].Position = VectorTools.vec3FromVec2(globalShape[i]);
                shapeVertices[(i * 2) + 1].Color = Color.LightSeaGreen;
            }

            for (int i = 0; i < springs.Count; i++)
            {
                springVertices[(i * 2) + 0].Position = VectorTools.vec3FromVec2(pointMasses[springs[i].pointMassA].Position);
                springVertices[(i * 2) + 0].Color = Color.LawnGreen;
                springVertices[(i * 2) + 1].Position = VectorTools.vec3FromVec2(pointMasses[springs[i].pointMassB].Position);
                springVertices[(i * 2) + 1].Color = Color.LightSeaGreen;
            }

            device.VertexDeclaration = vertexDecl;
            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, shapeVertices, 0, pointMasses.Count);
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, springVertices, 0, springs.Count);
                pass.End();
            }
            effect.End();

            base.debugDrawMe(device, effect);
        }

    }
}
