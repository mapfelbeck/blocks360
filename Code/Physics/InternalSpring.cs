
namespace Physics
{
    /// <summary>
    /// Spring that connects 2 PointMasses inside the same Body.
    /// </summary>
    public class InternalSpring
    {
        public InternalSpring()
        {
            pointMassA = pointMassB = 0;
            springD = springK = damping = 0f;
        }

        public InternalSpring(int pmA, int pmB, float d, float k, float damp)
        {
            pointMassA = pmA;
            pointMassB = pmB;
            springD = d;
            springK = k;
            damping = damp;
        }

        /// <summary>
        /// First PointMass the spring is connected to.
        /// </summary>
        public int pointMassA;

        /// <summary>
        /// Second PointMass the spring is connected to.
        /// </summary>
        public int pointMassB;

        /// <summary>
        /// The "rest length" (deisred length) of the spring.  at this length, no force is exerted on the points.
        /// </summary>
        public float springD;

        /// <summary>
        /// spring constant, or "strength" of the spring.
        /// </summary>
        public float springK;

        /// <summary>
        /// coefficient for damping, to reduce overshoot.
        /// </summary>
        public float damping;
    };
}
