
namespace Physics
{
    /// <summary>
    /// Spring that connects 2 PointMasses inside the same Body.
    /// </summary>
    public class ExternalSpring : InternalSpring
    {
        public Physics.SpringBody bodyA;
        public Physics.SpringBody bodyB;

        public ExternalSpring()
        {
            pointMassA = pointMassB = 0;
            springD = springK = damping = 0f;
        }

        public ExternalSpring(Physics.SpringBody bodya,Physics.SpringBody bodyb,
            int pmA, int pmB, float d, float k, float damp):base(pmA, pmB, d, k, damp)
        {
            bodyA = bodya;
            bodyB = bodyb;
        }
    };
}