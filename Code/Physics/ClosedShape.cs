using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Utilities;

namespace Physics
{
    /// <summary>
    /// class that represents a single polygonal closed shape (can be concave)
    /// </summary>
    public class ClosedShape
    {
        ////////////////////////////////////////////////////////////////
        // Vertices that make up this collision geometry.  shape connects vertices in order, closing the last vertex to the first.
        private List<Vector2> localVertices;

        ////////////////////////////////////////////////////////////////
        // default constructor.
        public ClosedShape()
        {
            localVertices = new List<Vector2>();
        }

        // construct from an existing list of vertices.
        public ClosedShape(List<Vector2> verts)
        {
            localVertices = new List<Vector2>(verts);
            finish();
        }

        ////////////////////////////////////////////////////////////////
        // start adding vertices to this collision.  will erase any existing verts.
        public void begin()
        {
            localVertices.Clear();
        }

        ////////////////////////////////////////////////////////////////
        // add a vertex to this collision.
        public int addVertex(Vector2 vert)
        {
            localVertices.Add(vert);
            return localVertices.Count;
        }

        ////////////////////////////////////////////////////////////////
        // finish adding vertices to this collision, and convert them into local space (be default).
        public void finish(bool recenter)
        {
            if (recenter)
            {
                // find the average location of all of the vertices, this is our geometrical center.
                Vector2 center = Vector2.Zero;

                for (int i = 0; i < localVertices.Count; i++)
                    center += localVertices[i];

                center /= localVertices.Count;

                // now subtract this from each element, to get proper "local" coordinates.
                for (int i = 0; i < localVertices.Count; i++)
                    localVertices[i] -= center;
            }
        }

        public void finish()
        {
            finish(true);
        }

        ////////////////////////////////////////////////////////////////
        // access to the vertice list.
        public List<Vector2> Vertices
        {
            get { return localVertices; }
        }

        /// <summary>
        /// Get a new list of vertices, transformed by the given position, angle, and scale.
        /// transformation is applied in the following order:  scale -> rotation -> position.
        /// </summary>
        /// <param name="worldPos">position</param>
        /// <param name="angleInRadians">rotation (in radians)</param>
        /// <param name="localScale">scale</param>
        /// <returns>new list of transformed points.</returns>
        public List<Vector2> transformVertices(Vector2 worldPos, float angleInRadians, Vector2 localScale)
        {
            List<Vector2> ret = new List<Vector2>(localVertices);

            Vector2 v = new Vector2();
            for (int i = 0; i < ret.Count; i++)
            {
                // rotate the point, and then translate.
                v.X = ret[i].X * localScale.X;
                v.Y = ret[i].Y * localScale.Y;
                VectorTools.rotateVector(ref v, angleInRadians, ref v);

                v.X += worldPos.X;
                v.Y += worldPos.Y;
                ret[i] = v;
            }

            return ret;
        }

        /// <summary>
        /// Get a new list of vertices, transformed by the given position, angle, and scale.
        /// transformation is applied in the following order:  scale -> rotation -> position.
        /// </summary>
        /// <param name="worldPos">position</param>
        /// <param name="angleInRadians">rotation (in radians)</param>
        /// <param name="localScale">scale</param>
        /// <param name="outList">new list of transformed points.</param>
        public void transformVertices(ref Vector2 worldPos, float angleInRadians, ref Vector2 localScale, ref Vector2[] outList)
        {
            for (int i = 0; i < localVertices.Count; i++)
            {
                // rotate the point, and then translate.
                Vector2 v = new Vector2();
                v.X = localVertices[i].X * localScale.X;
                v.Y = localVertices[i].Y * localScale.Y;
                VectorTools.rotateVector(ref v, angleInRadians);
                v.X += worldPos.X;
                v.Y += worldPos.Y;
                //outList[i] = VectorTools.rotateVector(mLocalVertices[i] * localScale, angleInRadians);
                outList[i].X = v.X;
                outList[i].Y = v.Y;
            }
        }
    }
}
