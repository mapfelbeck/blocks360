using Microsoft.Xna.Framework;

namespace Physics
{

    /// <summary>
    /// the most important class in JelloPhysics, all bodies in the world are made up of PointMasses, connected to form
    /// shapes.  Each PointMass can have its own mass, allowing for objects with different center-of-gravity.
    /// </summary>
    public class PointMass
    {
        ////////////////////////////////////////////////////////////////
        /// <summary>
        /// Mass of thie PointMass.
        /// </summary>
        public float Mass;

        /// <summary>
        /// Global position of the PointMass.
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// Global velocity of the PointMass.
        /// </summary>
        public Vector2 Velocity;

        /// <summary>
        /// Force accumulation variable.  reset to Zero after each call to integrate().
        /// </summary>
        public Vector2 Force;

        ////////////////////////////////////////////////////////////////
        // CONSTRUCTORS
        public PointMass()
        {
            Mass = 0;
            Position = Velocity = Force = Vector2.Zero;
        }

        public PointMass(float mass, Vector2 pos)
        {
            Mass = mass;
            Position = pos;
            Velocity = Force = Vector2.Zero;
        }

        ////////////////////////////////////////////////////////////////
        /// <summary>
        /// integrate Force >> Velocity >> Position, and reset force to zero.
        /// this is usually called by the World.update() method, the user should not need to call it directly.
        /// </summary>
        /// <param name="elapsed">time elapsed in seconds</param>
        public void integrateForce(float elapsed)
        {
            if (Mass != float.PositiveInfinity)
            {
                float elapMass = elapsed / Mass;

                Velocity.X += (Force.X * elapMass);
                Velocity.Y += (Force.Y * elapMass);

                Position.X += (Velocity.X * elapsed);
                Position.Y += (Velocity.Y * elapsed);
                
                Force.X = 0f;
                Force.Y = 0f;
            }
        }
    }
}
