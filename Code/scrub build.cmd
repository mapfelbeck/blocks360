del /Q /A *.suo

del /Q /S Utilities\*.cachefile
del /Q /S Utilities\*.user
del /Q /S Utilities\bin\*.*
del /Q /S Utilities\obj\*.*
rd /Q /S Utilities\bin
rd /Q /S Utilities\obj

del /Q /S SpriteEffectsPipeline\*.cachefile
del /Q /S SpriteEffectsPipeline\*.user
del /Q /S SpriteEffectsPipeline\bin\*.*
del /Q /S SpriteEffectsPipeline\obj\*.*
rd /Q /S SpriteEffectsPipeline\bin
rd /Q /S SpriteEffectsPipeline\obj

del /Q /S Physics\*.cachefile
del /Q /S Physics\*.user
del /Q /S Physics\bin\*.*
del /Q /S Physics\obj\*.*
rd /Q /S Physics\bin
rd /Q /S Physics\obj

del /Q /S BalloonBlocks\*.cachefile
del /Q /S BalloonBlocks\*.user
del /Q /S BalloonBlocks\bin\*.*
del /Q /S BalloonBlocks\obj\*.*
rd /Q /S BalloonBlocks\bin
rd /Q /S BalloonBlocks\obj

del /Q /S BalloonBlocks\Content\bin\*.*
del /Q /S BalloonBlocks\Content\obj\*.*
rd /Q /S BalloonBlocks\Content\bin
rd /Q /S BalloonBlocks\Content\obj
