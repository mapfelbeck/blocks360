﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Utilities
{
    public enum SubState
    {
        Entry,
        Update,
        Exit,
    }

    public delegate void StateDelegate(GameTime time, SubState subState);

    public abstract class StateMachine<StateType>
    {
        protected Dictionary<StateType, StateDelegate> stateTable;

        protected StateType currentState;
        protected StateDelegate currentDelegate;
        protected StateType desiredState;
        SubState currentSubState;

        public StateMachine()
        {
            stateTable = new Dictionary<StateType, StateDelegate>();
            currentSubState = SubState.Entry;
        }

        public void Update(GameTime time)
        {
            if (currentDelegate == null)
            {
                throw new Exception("Current state delegate not set.");
            }
            currentDelegate(time, currentSubState);

            if (currentState.Equals(desiredState))
            {
                if (currentSubState < SubState.Update)
                {
                    currentSubState++;
                }
            }
            else
            {
                if (currentSubState == SubState.Exit)
                {
                    if (!stateTable.ContainsKey(desiredState))
                    {
                        throw new Exception(string.Format("No delegate function exists for state {0}.", desiredState));
                    }
                    currentState = desiredState;
                    currentDelegate = stateTable[currentState];
                    currentSubState = SubState.Entry;
                }
                else
                {
                    currentSubState++;
                }
            }
        }

        public void SwitchState(StateType newState)
        {
            if (CanTransition(currentState, newState))
            {
                desiredState = newState;
            }
            else
            {
                throw new Exception(string.Format(
                    "Unable to transition from state {0} to {1}", currentState.ToString(), newState.ToString()));
            }
        }

        public virtual bool CanTransition(StateType from, StateType to)
        {
            return true;
        }
    }
}