﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using Utilities;
using Microsoft.Xna.Framework;

namespace Utilities
{
    public class TaskBatch
    {
        bool batchStarted;

        TaskWorker[] taskRunners;

        Thread[] threads;
        int numberThreads;
        public int NumberThreads
        {
            get { return numberThreads; }
        }

        Queue<TaskEntry> taskList;

        ManualResetEvent batchStart;
        List<ManualResetEvent> taskOverEvents;

        public TaskBatch()
            : this(
#if XBOX360
            3
#else
            (int)MathHelper.Max(1, System.Environment.ProcessorCount-1)
#endif
) { }

        public TaskBatch(int nThreads)
        {
            numberThreads = nThreads;
            threads = new Thread[numberThreads];

            taskOverEvents = new List<ManualResetEvent>();

            batchStart = new ManualResetEvent(false);
            taskRunners = new TaskWorker[numberThreads];

            taskList = new Queue<TaskEntry>();

            ResetEvents();

            CreateThreads();
        }

        private void CreateThreads()
        {
            // 1, 3, 4, 5 available on the 360, 0 and 2 are reserved
            int[] HardwareThread = new int[] { 1, 3, 5 };

            for (int i = 0; i < numberThreads; i++)
            {
                taskRunners[i] = new TaskWorker(batchStart, taskList, i);
#if XBOX360
                int hwt = HardwareThread[i % HardwareThread.Length];
                taskRunners[i].SetProcessorAffinity(hwt);
                threads[i] = new System.Threading.Thread(new ThreadStart(taskRunners[i].Run));
#else
                threads[i] = new System.Threading.Thread(new ThreadStart(taskRunners[i].Run));
#endif
                threads[i].Start();
            }
        }

        public void Begin()
        {
            if (batchStarted)
            {
                throw new Exception("Task batch already started");
            }

            ResetEvents();
            batchStarted = true;
            taskCount = 0;
        }

        public void End()
        {
            if (!batchStarted)
            {
                throw new Exception("call TaskBatch.Begin() first");
            }
            //Console.WriteLine("{0} tasks added to task queue, {1} events seen to wait on", taskCount, taskOverEvents.Count);

            batchStart.Set();
            //Console.WriteLine("waiting...");
            /*for (int i = 0; i < taskOverEvents.Count; i++)
            {
                taskOverEvents[i].Item.WaitOne();
            }*/
            for (int i = 0; i < taskOverEvents.Count; i++)
            {
                taskOverEvents[i].WaitOne();
            }
            //Console.WriteLine("done waiting");

            batchStarted = false;
        }

        public void AddTask(TaskDelegate task, params object[] args)
        {
            DelegateTask newTask = new DelegateTask(task, args);
            AddTask(newTask);
        }

        int taskCount;
        public void AddTask(Task task)
        {
            if (!batchStarted)
            {
                throw new Exception("call TaskBatch.Begin() first");
            }
            taskCount++;
            ManualResetEvent theEvent = new ManualResetEvent(false);
            taskOverEvents.Add(theEvent);
            TaskEntry newTask = new TaskEntry(task, theEvent);
            lock (taskList)
            {
                taskList.Enqueue(newTask);
            }
        }

        private void ResetEvents()
        {
            batchStart.Reset();

            /*for (int i = 0; i < taskOverEvents.Count; i++)
            {
                taskOverEvents[i].Item.Reset();
                eventPool.Return(taskOverEvents[i]);
            }*/
            /*taskList.Clear();
            if (taskList.Count > 0)
            {
                Console.WriteLine("wtf?");
            }*/
            taskOverEvents.Clear();
        }

        public void Dispose()
        {
            if (batchStarted)
            {
                throw new Exception("Cannot Dispose() of a running TaskBatch");
            }

            for (int i = 0; i < numberThreads; i++)
            {
                taskRunners[i].Dispose();
            }

            batchStart.Set();
            Thread.Sleep(10);

            batchStart.Close();
        }
    }
}
