﻿#region usings
using System.Threading;
using System;
#endregion

namespace Utilities
{
    public class TaskEvent
    {
        ManualResetEvent taskEvent;

        public TaskEvent()
        {
            taskEvent = new ManualResetEvent(false);
        }

        public void WaitOne()
        {
            taskEvent.WaitOne();
        }

        public bool Set()
        {
            return taskEvent.Set();
        }

        public bool Reset()
        {
            return taskEvent.Reset();
        }

        public void Close()
        {
            taskEvent.Close();
        }
    }
}