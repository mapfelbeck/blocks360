﻿using System;

namespace Utilities
{
    public delegate void TaskDelegate(params object[] args);

    class DelegateTask : Task
    {
        TaskDelegate task;
        object[] args;

        public DelegateTask(TaskDelegate task, params object[] args)
        {
            if (task == null)
            {
                throw new ArgumentNullException("Null task delegate");
            }
            this.task = task;
            this.args = args;
        }

        public override void TaskRun()
        {
            if (task != null)
            {
                task(args);
            }
        }

        public override void TaskComplete()
        {
        }
    }
}
