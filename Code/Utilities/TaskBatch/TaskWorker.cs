using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using System;

namespace Utilities
{
    public class TaskWorker
    {
#if XBOX360
        int affinity;
#endif
        ManualResetEvent batchStartEvent;

        int workerID;

        Queue<TaskEntry> taskList;
        bool disposed;

        public TaskWorker(
            ManualResetEvent batchStartEvent, Queue<TaskEntry> workList, int ID)
        {
            workerID = ID;
            this.batchStartEvent = batchStartEvent;
            this.taskList = workList;
            disposed = false;
        }

        public void Run()
        {
#if XBOX360
            Thread.CurrentThread.SetProcessorAffinity(new int[] { affinity });
#endif
            TaskEntry entry;
            while (!disposed)
            {
                batchStartEvent.WaitOne();

                lock (taskList)
                {
                    if (taskList.Count > 0)
                    {
                        entry = taskList.Dequeue();
                    }
                    else
                    {
                        entry = null;
                    }
                }
                if (entry != null)
                {
                    try
                    {
                        entry.Run();
                    }
                    catch (System.Exception exception)
                    {
                        entry.exception = exception;
                    }

                    try
                    {
                        entry.Complete();
                    }
                    catch (System.Exception exception)
                    {
                        entry.exception = exception;
                    }
                }
            }
        }

        public void Dispose()
        {
            disposed = true;
        }

        
#if XBOX360
        public void SetProcessorAffinity(int hwt)
        {
            affinity = hwt;
        }
#endif
    }
}