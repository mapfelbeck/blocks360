﻿using System;
using System.Threading;

namespace Utilities
{
    public class TaskEntry
    {
        public Task task;
        public Exception exception;
        ManualResetEvent overEvent;

        public TaskEntry(Task task, ManualResetEvent overEvent)
        {
            this.task = task;
            this.overEvent = overEvent;
        }

        public void Run()
        {
            overEvent.Reset();
            task.TaskRun();
        }

        public void Complete()
        {
            if (exception != null)
            {
                throw exception;
            }
            task.TaskComplete();
            overEvent.Set();
        }
    }
}
