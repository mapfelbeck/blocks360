﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities
{
    abstract public class Task
    {
        public Task()
        {
        }

        abstract public void TaskRun();
        abstract public void TaskComplete();
    }
}
