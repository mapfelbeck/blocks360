﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
namespace Utilities
{
    /// <summary>
    /// Contains a texture reference and set of coordinates
    /// </summary>
    public class Tile
    {
        private Texture2D baseTexture = null;
        public Texture2D BaseTexture
        {
            get { return baseTexture; }
        }

        private Vector2 upperLeft;//upper left, i.e. 0,0
        public Vector2 UpperLeft
        {
            get { return upperLeft; }
        }

        private Vector2 upperRight;//upper right, i.e. 1,0
        public Vector2 UpperRight
        {
            get { return upperRight; }
        }

        private Vector2 lowerLeft;//lower left, i.e. 0,1
        public Vector2 LowerLeft
        {
            get { return lowerLeft; }
        }

        private Vector2 lowerRight;//lower right, i.e. 1,1
        public Vector2 LowerRight
        {
            get { return lowerRight; }
        }

        private Rectangle sourceRect;
        public Rectangle SourceRect
        {
            get { return sourceRect; }
        }

        public Tile(Texture2D texture, Vector2 ul, Vector2 ur,
            Vector2 ll, Vector2 lr)
        {
            if (texture == null)
            {
                throw new ArgumentNullException("Texture must be non-null");
            }
            this.baseTexture = texture;
            this.upperLeft = ul;
            this.upperRight = ur;
            this.lowerLeft = ll;
            this.lowerRight = lr;

            SourceRectangleFromTectureCoordinates();
        }

        public Tile(Texture2D texture, Rectangle rectangle)
        {
            if (texture == null)
            {
                throw new ArgumentNullException("Texture must be non-null");
            }
            this.baseTexture = texture;
            this.sourceRect = rectangle;
            
            TextureCoordinatesFromSourceRectangle();
        }

        private void SourceRectangleFromTectureCoordinates()
        {
            int heigth = baseTexture.Height;
            int width = baseTexture.Width;

            sourceRect = new Rectangle((int)(upperLeft.X * width), (int)(upperLeft.Y * width),
                (int)((upperRight.X - upperLeft.X) * width), (int)((lowerLeft.Y - upperLeft.Y) * heigth));
        }

        private void TextureCoordinatesFromSourceRectangle()
        {
            int heigth = baseTexture.Height;
            int width = baseTexture.Width;

            float left = (float)sourceRect.X / (float)width;
            float right = (float)sourceRect.Right / (float)width;
            float top = (float)sourceRect.Y / (float)heigth;
            float bottom = (float)sourceRect.Bottom / (float)heigth;
            upperLeft = new Vector2(left, top);
            upperRight = new Vector2(right, top);
            lowerLeft = new Vector2(left, bottom);
            lowerRight = new Vector2(right, bottom);
        }
    }
}