﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Utilities
{
    /// <summary>
    /// Contains a base Texture and an array of Tiles that hold tecture coordinates
    /// </summary>
    public class TiledTexture
    {
        Texture2D baseTexture;
        public Texture2D BaseTexture
        {
            get { return baseTexture; }
        }

        Tile[] tiles;
        
        int numTiles;
        public int TileCount
        {
            get { return numTiles; }
        }

        int width;
        int height;

        public TiledTexture()
        {
        }

        /// <summary>
        /// Defines a texture and it's tile sub-textures
        /// </summary>
        /// <param name="texture">source texture</param>
        /// <param name="w">number of tiles wide</param>
        /// <param name="h">number of tiles high</param>
        public TiledTexture(Texture2D texture, int tilesWide, int tilesHigh)
        {
            baseTexture = texture;
            numTiles = tilesWide*tilesHigh;
            width = tilesWide;
            height = tilesHigh;

            tiles = new Tile[numTiles];

            float fw = (float)width;
            float fh = (float)height;
            float wDelta = 1f / fw;
            float hDelta = 1f / fh;

            //build the array of uniform size and shape texture tiles
            //tiles are created starting at the upper left and moving
            //across and down
            for (int i = 0; i < numTiles; i++)
            {
                float fi = (float)i;
                Vector2 upperLeft = new Vector2((fi % fw) / fw, (i / tilesWide) / fw);
                tiles[i] = new Tile(
                    baseTexture, upperLeft, new Vector2(upperLeft.X+wDelta,upperLeft.Y),
                                new Vector2(upperLeft.X, upperLeft.Y+hDelta), new Vector2(upperLeft.X+wDelta, upperLeft.Y+hDelta)
                    );
            }
        }

        /// <summary>
        /// Get the Tile that defines the source sprite
        /// on the sheet.
        /// </summary>
        public Tile this[int index]
        {
            get
            {
                return tiles[index];
            }
        }
    }
}