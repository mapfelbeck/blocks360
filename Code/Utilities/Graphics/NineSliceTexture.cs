﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Utilities
{
    public class NineSliceTexture
    {
        Texture2D baseTexture;
        public Texture2D BaseTexture
        {
            get { return baseTexture; }
            set { baseTexture = value; }
        }

        Tile[] tiles;

        float textureHeight;
        public float Height
        {
            get { return textureHeight; }
            set { textureHeight = value;}
        }
        float textureWidth;
        public float Width
        {
            get { return textureWidth; }
            set { textureWidth = value; }
        }

        protected float[] widthsNormalized;
        protected float[] heightsNormalized;

        protected float totalWidthAbsolute;
        protected float totalHeightAbsolute;

        protected float[] widthsAbsolute;
        protected float[] heightsAbsolute;

        public NineSliceTexture(Texture2D texture, float[] widths, float[] heights)
        {
            if (widths == null || widths.Length != 3)
            {
                throw new ArgumentException("Horizontal proportions array must be non null and length 3");
            }
            if (heights == null || heights.Length != 3)
            {
                throw new ArgumentException("Vertical proportions array must be non null and length 3");
            }
            if (texture == null)
            {
                throw new ArgumentNullException("Base texture must be non-null");
            }

            totalHeightAbsolute = heights[0] + heights[1] + heights[2];
            totalWidthAbsolute = widths[0] + widths[1] + widths[2];

            heightsAbsolute = heights;
            widthsAbsolute = widths;

            baseTexture = texture;

            textureHeight = texture.Height;
            textureWidth = texture.Width;

            widthsNormalized = new float[3];
            heightsNormalized = new float[3];

            widthsNormalized[0] = widths[0] / totalWidthAbsolute;
            widthsNormalized[1] = widths[1] / totalWidthAbsolute;
            widthsNormalized[2] = widths[2] / totalWidthAbsolute;
            heightsNormalized[0] = heights[0] / totalHeightAbsolute;
            heightsNormalized[1] = heights[1] / totalHeightAbsolute;
            heightsNormalized[2] = heights[2] / totalHeightAbsolute;

            tiles = new Tile[9];
            
            GenerateTiles();
        }

        public NineSliceTexture(Texture2D texture, float w1, float w2, float w3, float h1, float h2, float h3)
        {
            totalWidthAbsolute = w1 + w2 + w3;
            totalHeightAbsolute = h1 + h2 + h3;

            baseTexture = texture;

            textureHeight = texture.Height;
            textureWidth = texture.Width;

            widthsNormalized = new float[3];
            heightsNormalized = new float[3];

            widthsNormalized[0] = w1 / totalWidthAbsolute;
            widthsNormalized[1] = w2 / totalWidthAbsolute;
            widthsNormalized[2] = w3 / totalWidthAbsolute;
            heightsNormalized[0] = h1 / totalHeightAbsolute;
            heightsNormalized[1] = h2 / totalHeightAbsolute;
            heightsNormalized[2] = h3 / totalHeightAbsolute;
            tiles = new Tile[9];
            GenerateTiles();
        }

        private void GenerateTiles()
        {
            tiles[0] = new Tile(baseTexture, new Vector2(0f, 0f), new Vector2(widthsNormalized[0], 0f),
                                new Vector2(0f, heightsNormalized[0]), new Vector2(widthsNormalized[0], heightsNormalized[0]));
            tiles[1] = new Tile(baseTexture, new Vector2(widthsNormalized[0], 0f), new Vector2(widthsNormalized[0] + widthsNormalized[1], 0f),
                                new Vector2(widthsNormalized[0], heightsNormalized[0]), new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0]));
            tiles[2] = new Tile(baseTexture, new Vector2(widthsNormalized[0] + widthsNormalized[1], 0f), new Vector2(widthsNormalized[0] + widthsNormalized[1] + widthsNormalized[2], 0f),
                                new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0]), new Vector2(widthsNormalized[0] + widthsNormalized[1] + widthsNormalized[2], heightsNormalized[0]));

            tiles[3] = new Tile(baseTexture, new Vector2(0f, heightsNormalized[0]), new Vector2(widthsNormalized[0], heightsNormalized[0]),
                                new Vector2(0f, heightsNormalized[0] + heightsNormalized[1]), new Vector2(widthsNormalized[0], heightsNormalized[0] + heightsNormalized[1]));
            tiles[4] = new Tile(baseTexture, new Vector2(widthsNormalized[0], heightsNormalized[0]), new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0]),
                                new Vector2(widthsNormalized[0], heightsNormalized[0] + heightsNormalized[1]), new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0] + heightsNormalized[1]));
            tiles[5] = new Tile(baseTexture, new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0]), new Vector2(widthsNormalized[0] + widthsNormalized[1] + widthsNormalized[2], heightsNormalized[0]),
                                new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0] + heightsNormalized[1]), new Vector2(widthsNormalized[0] + widthsNormalized[1] + widthsNormalized[2], heightsNormalized[0] + heightsNormalized[1]));

            tiles[6] = new Tile(baseTexture, new Vector2(0f, heightsNormalized[0] + heightsNormalized[1]), new Vector2(widthsNormalized[0], heightsNormalized[0] + heightsNormalized[1]),
                                new Vector2(0f, heightsNormalized[0] + heightsNormalized[1] + heightsNormalized[2]), new Vector2(widthsNormalized[0], heightsNormalized[0] + heightsNormalized[1] + heightsNormalized[2]));
            tiles[7] = new Tile(baseTexture, new Vector2(widthsNormalized[0], heightsNormalized[0] + heightsNormalized[1]), new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0] + heightsNormalized[1]),
                                new Vector2(widthsNormalized[0], heightsNormalized[0] + heightsNormalized[1] + heightsNormalized[2]), new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0] + heightsNormalized[1] + heightsNormalized[2]));
            tiles[8] = new Tile(baseTexture, new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0] + heightsNormalized[1]), new Vector2(widthsNormalized[0] + widthsNormalized[1] + widthsNormalized[2], heightsNormalized[0] + heightsNormalized[1]),
                                new Vector2(widthsNormalized[0] + widthsNormalized[1], heightsNormalized[0] + heightsNormalized[1] + heightsNormalized[2]), new Vector2(widthsNormalized[0] + widthsNormalized[1] + widthsNormalized[2], heightsNormalized[0] + heightsNormalized[1] + heightsNormalized[2]));
        }

        /// <summary>
        /// Get the Tile that defines the source sprite
        /// on the sheet.
        /// </summary>
        public Tile this[int index]
        {
            get
            {
                return tiles[index];
            }
        }
    }
}