﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Utilities
{
    public class SpriteBatchNineSliceTexture : NineSliceTexture
    {

        public SpriteBatchNineSliceTexture(Texture2D texture, float[] widths, float[] heights) :
            base(texture, widths, heights)
        {
        }

        public SpriteBatchNineSliceTexture(Texture2D texture, float w1, float w2, float w3, float h1, float h2, float h3) :
            base(texture, w1, w2, w3, h1, h2, h3)
        {
        }

        public void Draw(SpriteBatch spriteBatch, Rectangle drawRectangle, Color color, float borderScale)
        {
            Rectangle[] destinationRectangels = CreateDestinationRectangles(drawRectangle, borderScale);

            for (int i = 0; i < destinationRectangels.Length; i++)
            {
                spriteBatch.Draw(BaseTexture, destinationRectangels[i], this[i].SourceRect, color);
            }
        }

        public void Draw(SpriteBatch spriteBatch, Rectangle drawRectangle, Color color)
        {
            Draw(spriteBatch, drawRectangle, color, 1f);
        }

        private Rectangle[] CreateDestinationRectangles(Rectangle drawRectangle, float borderScale)
        {
            Rectangle[] destinationRectangles;
            destinationRectangles = new Rectangle[9];
            //float aspectRatio = (float)drawRectangle.Width / (float)drawRectangle.Height;
            float scale = Math.Min((float)drawRectangle.Width / (float)Width,
                (float)drawRectangle.Height / (float)Height);
            int w1 = (int)((widthsAbsolute[0] / totalWidthAbsolute) * Width * scale * borderScale);
            int w3 = (int)((widthsAbsolute[2] / totalWidthAbsolute) * Width * scale * borderScale);

            int w2 = drawRectangle.Width - (int)(w1 + w3);

            int h1 = (int)((heightsAbsolute[0] / totalHeightAbsolute) * Height * scale * borderScale);
            int h3 = (int)((heightsAbsolute[0] / totalHeightAbsolute) * Height * scale * borderScale);

            int h2 = drawRectangle.Height - (int)(h1 + h3);

            destinationRectangles[0] = new Rectangle(drawRectangle.X, drawRectangle.Y,
                w1, h1);
            destinationRectangles[1] = new Rectangle(destinationRectangles[0].Right, drawRectangle.Y,
                w2, h1);
            destinationRectangles[2] = new Rectangle(destinationRectangles[1].Right, drawRectangle.Y,
                w3, h1);

            destinationRectangles[3] = new Rectangle(drawRectangle.X, destinationRectangles[0].Bottom,
                w1, h2);
            destinationRectangles[4] = new Rectangle(destinationRectangles[3].Right, destinationRectangles[1].Bottom,
                w2, h2);
            destinationRectangles[5] = new Rectangle(destinationRectangles[4].Right, destinationRectangles[2].Bottom,
                w3, h2);

            destinationRectangles[6] = new Rectangle(drawRectangle.X, destinationRectangles[3].Bottom,
                w1, h3);
            destinationRectangles[7] = new Rectangle(destinationRectangles[3].Right, destinationRectangles[4].Bottom,
                w2, h3);
            destinationRectangles[8] = new Rectangle(destinationRectangles[4].Right, destinationRectangles[5].Bottom,
                w3, h3);

            return destinationRectangles;
        }
    }
}