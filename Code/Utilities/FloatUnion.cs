﻿using System.Runtime.InteropServices;

namespace Utilities
{
    [StructLayout(LayoutKind.Explicit)]
    public struct FloatUnion
    {
        [FieldOffset(0)]
        public float x;

        [FieldOffset(0)]
        public int n;

        public float approxSqrt()
        {
            n -= 1 << 23;
            n = n >> 1;
            n += 1 << 29;
            return x;
        }
    }

}
