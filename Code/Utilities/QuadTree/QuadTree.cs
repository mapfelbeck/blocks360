﻿#region using statements
using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
#endregion

namespace Utilities
{
    public class QuadTree<T> where T : IQuadTreeItem<T>
    {
        Pool<QuadTreeNode<T>>.Node rootNode;
        public QuadTreeNode<T> Root
        {
            get { return rootNode.Item; }
        }

        Pool<QuadTreeNode<T>> nodePool;

        public int LeafCount
        {
            get
            {
                if (rootNode.Item == null)
                {
                    return 0;
                }
                else
                {
                    return rootNode.Item.LeafCount;
                }
            }
        }

        private List<T> pendingRemovals;

        List<T> contentsCache;
        bool contentsCacheValid;
        public List<T> Contents
        {
            get
            {
                if (rootNode.Item == null)
                {
                    return null;
                }
                else
                {
                    if (!contentsCacheValid)
                    {
                        contentsCache = rootNode.Item.AllContents;
                        contentsCacheValid = true;
                    }
                    return contentsCache;
                }
            }
        }

        public void BuildContentsCache(bool force)
        {
            if (!contentsCacheValid || force)
            {
                contentsCache = rootNode.Item.AllContents;
                contentsCacheValid = true;
            }
        }

        public T this[int index]
        {
            get
            {
                return Contents[index];
            }
        }

        public QuadTree(AABB bounds)
            : this(bounds, 4)
        {
        }

        public QuadTree(AABB bounds, int maxDepth)
        {
            int nodeCount = 0;
            for (int i = 0; i <= maxDepth; i++)
            {
                nodeCount += (int)Math.Pow(4, i);
            }

            nodePool = new Pool<QuadTreeNode<T>>(nodeCount);

            float splitLimit = (float)(Math.Min((double)bounds.Width, (double)bounds.Height) / Math.Pow(2.0, (double)maxDepth));
            //rootNode = GetNode();//new QuadTreeNode<T>(null, bounds, splitLimit, this);
            //rootNode.Item.Initialize(null, bounds.UL, bounds.LR, splitLimit, this);
            rootNode = GetNode(null, bounds.UL, bounds.LR, splitLimit, this);
            contentsCache = new List<T>();
            contentsCacheValid = false;
            pendingRemovals = new List<T>();
        }

        public Pool<QuadTreeNode<T>>.Node GetNode(QuadTreeNode<T> parent, Vector2 min, Vector2 max,
            float splitLimit, QuadTree<T> interfaceTree)
        {
            Pool<QuadTreeNode<T>>.Node node = nodePool.Get();
            node.Item.Initialize(parent, min, max, splitLimit, interfaceTree);
            return node;
        }

        public Pool<QuadTreeNode<T>>.Node GetNode(QuadTreeNode<T> parent, float x, float y, 
            float width, float height, float splitLimit, QuadTree<T> interfaceTree)
        {
            Pool<QuadTreeNode<T>>.Node node = nodePool.Get();
            node.Item.Initialize(parent, x, y, width, height, splitLimit, interfaceTree);
            return node;
        }

        public void ReturnNode(Pool<QuadTreeNode<T>>.Node node)
        {
            node.Item.Clear();
            nodePool.Return(node);
        }

        public int Count
        {
            get {
                if (!contentsCacheValid)
                {
                    contentsCache = rootNode.Item.AllContents;
                    contentsCacheValid = true;
                }
                return contentsCache.Count;
            }
        }

        public bool Collides(T item){
            bool result = false;

            if (!rootNode.Item.Bounds.Contains(item.AABB))
            {
                result = true;
            }
            else if (rootNode.Item.Query(item.AABB).Count > 0)
            {
                result = true;
            }

            return result;
        }

        public virtual void Add(T item)
        {
            rootNode.Item.Add(ref item);
            contentsCacheValid = false;
        }

        public List<T> Query(AABB aabb)
        {
            if (rootNode.Item != null)
            {
                return rootNode.Item.Query(aabb);
            }
            return null;
        }

        public List<T> BroadQuery(AABB aabb)
        {
            if (rootNode.Item != null)
            {
                return rootNode.Item.BroadQuery(aabb);
            }
            return null;
        }

        public void BroadQuery(AABB aabb, List<T> queryResults)
        {
            if (rootNode.Item != null)
            {
                rootNode.Item.BroadQuery(aabb, queryResults);
            }
        }

        public void Query(AABB aabb, List<T> queryResults)
        {
            if (rootNode.Item != null)
            {
                rootNode.Item.Query(aabb, queryResults);
            }
        }

        public void Remove(T item)
        {
            /*if (item.Node != null)
            {
                item.Node.Remove(item);
            }*/
            if (item.DeleteDelegate != null)
            {
                item.DeleteDelegate(item);
            }
            else
            {
                rootNode.Item.Remove(item);
            }
            PruneTree();
            contentsCache.Clear();
            contentsCacheValid = false;
        }

        public void Clear()
        {
            rootNode.Item.DeleteSubNodes();
        }

        public void PruneTree()
        {
            if (rootNode.Item != null)
            {
                rootNode.Item.Prune();
            }
        }

        public void QueuePendingRemoval(T item)
        {
            pendingRemovals.Add(item);
        }

        /// <summary>
        /// Remove all of the "garbage" objects from this collection.
        /// </summary>
        public void ApplyPendingRemovals()
        {
            for (int i = 0; i < pendingRemovals.Count; i++)
            {
                Remove(pendingRemovals[i]);
            }
            pendingRemovals.Clear();
            contentsCacheValid = false;
        }
    }
}