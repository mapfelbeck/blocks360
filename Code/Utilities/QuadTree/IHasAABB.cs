﻿namespace Utilities
{
    /// <summary>
    /// An interface that defines and object with an Axis Aligned Bounding Box
    /// </summary>
    public interface IHasAABB
    {
        AABB AABB { get; }
        //void UpdateAABB();
    }
}