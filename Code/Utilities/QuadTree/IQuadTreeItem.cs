﻿using System.Collections.Generic;
namespace Utilities
{
    /// <summary>
    /// An interface that defines and object with an Axis Aligned Bounding Box
    /// </summary>
    public delegate void UpdateItemDelegate<T>(T item);
    public delegate void DeleteItemDelegate<T>(T item);

    public interface IQuadTreeItem<T> : IHasAABB
    {
        UpdateItemDelegate<T> UpdateDelegate { get; set; }
        DeleteItemDelegate<T> DeleteDelegate { get; set; }
    }
}
