﻿#region using statements
using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
#endregion

namespace Utilities
{
    public class QuadTreeNode<T> where T : IQuadTreeItem<T>
    {
        enum NodeLocation
        {
            Self,
            Parent,
            Child,
        };

        QuadTree<T> interfaceTree;

        AABB bounds;
        public AABB Bounds
        {
            get { return bounds; }
        }
        
        QuadTreeNode<T> parent;
        
        List<Pool<QuadTreeNode<T>>.Node> nodes;
        public List<Pool<QuadTreeNode<T>>.Node> Nodes
        {
            get { return nodes; }
        }

        List<T> contents;
        public List<T> Contents
        {
            get { return contents; }
        }

        public List<T> AllContents
        {
            get
            {
                List<T> result = new List<T>();
                AllContentsHelper(ref result);
                return result;
            }
        }
        private void AllContentsHelper(ref List<T> result)
        {
            result.AddRange(contents);

            foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
            {
                node.Item.AllContentsHelper(ref result);
            }
        }
        public List<T> SubTreeContents
        {
            get
            {
                List<T> result = new List<T>();
                foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
                    node.Item.AllContentsHelper(ref result);
                return result;
            }
        }
        public int Count
        {
            get
            {
                int count = contents.Count;
                count += SubTreeCount;
                return count;
            }
        }

        public int SubTreeCount
        {
            get
            {
                int count = 0;
                foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
                {
                    count += node.Item.Count;
                }
                return count;
            }
        }

        public int LeafCount
        {
            get
            {
                int count = 1;
                foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
                {
                    count += node.Item.LeafCount;
                }
                return count;
            }
        }
        
        public bool IsEmpty
        {
            get { return Count == 0; }
        }

        bool isLeaf;
        public bool IsLeaf
        {
            get { return isLeaf; }
        }

        bool isSplitable;

        public bool IsRoot
        {
            get { return parent == null; }
        }

        float splitLimit;

        public QuadTreeNode()
        {
            bounds = new AABB();
            contents = new List<T>();
            nodes = new List<Pool<QuadTreeNode<T>>.Node>();
        }

        public QuadTreeNode(QuadTreeNode<T> parent, AABB bounds, float splitLimit, QuadTree<T> interfaceTree)
        {
            if (!bounds.Valid)
            {
                throw new InvalidOperationException("Bad bounds AABB");
            }

            this.interfaceTree = interfaceTree;
            this.parent = parent;
            this.bounds = bounds;
            contents = new List<T>();
            nodes = new List<Pool<QuadTreeNode<T>>.Node>();
            isLeaf = true;
            this.splitLimit = splitLimit;
            isSplitable = bounds.Height >= splitLimit && bounds.Width >= splitLimit;
        }

        public void Initialize(QuadTreeNode<T> parent, Vector2 min, Vector2 max, float splitLimit, QuadTree<T> interfaceTree)
        {
            this.interfaceTree = interfaceTree;
            this.parent = parent;
            this.bounds.Set(min, max);
            isLeaf = true;
            this.splitLimit = splitLimit;
            isSplitable = bounds.Height >= splitLimit && bounds.Width >= splitLimit;
        }

        public void Initialize(QuadTreeNode<T> parent, float x, float y,
            float width, float height, float splitLimit, QuadTree<T> interfaceTree)
        {
            this.interfaceTree = interfaceTree;
            this.parent = parent;
            this.bounds.Set(x, y, width, height);
            isLeaf = true;
            this.splitLimit = splitLimit;
            isSplitable = bounds.Height >= splitLimit && bounds.Width >= splitLimit;
        }

        public void Clear()
        {
            bounds.Clear();
            contents.Clear();
            nodes.Clear();
        }

        private bool PushItemDown(ref T item, bool removeFromCurrent)
        {
            if (removeFromCurrent)
            {
                contents.Remove(item);
            }

            if (isLeaf && isSplitable)
            {
                CreateSubNodes();
            }

            foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
            {
                if (node.Item.Bounds.Contains(item.AABB))
                {
                    return node.Item.Add(ref item);
                }
            }

            return false;
        }

        private bool PushItemUp(ref T item, bool removeFromCurrent)
        {
            if (removeFromCurrent)
            {
                contents.Remove(item);
            }

            return parent.Add(ref item);
        }

        public bool Add(ref T item)
        {
            NodeLocation location = ItemLocation(ref item);

            if (location == NodeLocation.Parent && !IsRoot)
            {
                return PushItemUp(ref item, false);
            }
            else if (location == NodeLocation.Child && isSplitable)
            {
                PushItemDown(ref item, false);
            }
            else
            {
                //item.Node = this;
                item.UpdateDelegate = this.UpdatePosition;
                item.DeleteDelegate = this.Remove;
                contents.Add(item);
                if (SubTreeCount == 0)
                {
                    DeleteSubNodes();
                }
                return true;
            }

            return false;
        }

        public void UpdatePosition(T item)
        {
            NodeLocation location = ItemLocation(ref item);

            if (location == NodeLocation.Parent && !IsRoot)
            {
                PushItemUp(ref item, true);
            }
            else if (location == NodeLocation.Child && isSplitable)
            {
                PushItemDown(ref item, true);
            }
        }

        private NodeLocation ItemLocation(ref T item)
        {
            NodeLocation result = NodeLocation.Self;

            float halfWidthLocation = bounds.X + bounds.Width / 2;
            float halfHeightLocation = bounds.Y + bounds.Height / 2;

            if (!bounds.Contains(item.AABB))
            {
                result = NodeLocation.Parent;
            }
            else if (item.AABB.ContainsX(ref halfWidthLocation) || item.AABB.ContainsY(ref halfHeightLocation))
            {
                result = NodeLocation.Self;
            }
            else
            {
                result = NodeLocation.Child;
            }
            return result;
        }

        private void CreateSubNodes()
        {
            if (nodes.Count > 0 || !isSplitable)
            {
                return;
            }
            isLeaf = false;

            float halfWidth = bounds.Width / 2;
            float halfHeight = bounds.Height / 2;

            nodes.Add(interfaceTree.GetNode(this,
                bounds.X, bounds.Y, halfWidth, halfHeight, 
                splitLimit, interfaceTree));
            nodes.Add(interfaceTree.GetNode(this,
                bounds.X + halfWidth, bounds.Y, halfWidth, halfHeight, 
                splitLimit, interfaceTree));

            nodes.Add(interfaceTree.GetNode(this,
                bounds.X, bounds.Y + halfHeight, halfWidth, halfHeight, 
                splitLimit, interfaceTree));
            nodes.Add(interfaceTree.GetNode(this,
                bounds.X + halfWidth, bounds.Y + halfHeight, halfWidth, halfHeight,  
                splitLimit, interfaceTree));
            
        }

        public void DeleteSubNodes()
        {
            foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
            {
                node.Item.DeleteSubNodes();
            }

            foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
            {
                interfaceTree.ReturnNode(node);
            }
            nodes.Clear();
            isLeaf = true;
        }

        public void DeleteData()
        {
            contents.Clear();
        }

        public List<T> Query(AABB queryAABB)
        {
            List<T> result = new List<T>();

            QueryHelper(queryAABB, result);

            return result;
        }

        public List<T> BroadQuery(AABB queryAABB)
        {
            List<T> result = new List<T>();

            BroadQueryHelper(queryAABB, result);

            return result;
        }

        public void Query(AABB queryAABB, List<T> result)
        {
            QueryHelper(queryAABB, result);
        }

        public void BroadQuery(AABB queryAABB, List<T> result)
        {
            BroadQueryHelper(queryAABB, result);
        }

        public void BroadQueryHelper(AABB queryAABB, List<T> result)
        {
            if (queryAABB.Intersects(bounds))
            {
                result.AddRange(contents);

                foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
                {
                    node.Item.BroadQueryHelper(queryAABB, result);
                }
            }
        }

        public void QueryHelper(AABB queryAABB, List<T> result)
        {
            foreach (T item in contents)
            {
                if (queryAABB.Intersects(item.AABB))
                {
                    result.Add(item);
                }
            }

            foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
            {
                // Case 1: search area completely contained by sub-quad
                // if a node completely contains the query area, go down that branch
                // and skip the remaining nodes (break this loop)
                if (node.Item.bounds.Contains(ref queryAABB))
                {
                    node.Item.QueryHelper(queryAABB, result);
                    break;
                }

                // Case 2: Sub-quad completely contained by search area 
                // if the query area completely contains a sub-quad,
                // just add all the contents of that quad and it's children 
                // to the result set. You need to continue the loop to test 
                // the other quads
                if (queryAABB.Contains(ref node.Item.bounds))
                {
                    result.AddRange(node.Item.AllContents);
                    continue;
                }

                // Case 3: search area intersects with sub-quad
                // traverse into this quad, continue the loop to search other
                // quads
                if (node.Item.bounds.Intersects(ref queryAABB))
                {
                    node.Item.QueryHelper(queryAABB, result);
                }
            }
        }

        public void Remove(T item)
        {
            if (!contents.Remove(item))
            {
                foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
                {
                    if (node.Item.bounds.Contains(item.AABB))
                    {
                        node.Item.Remove(item);
                    }
                }
            }

            if (SubTreeCount == 0)
            {
                DeleteSubNodes();
            }
        }

        public void Prune()
        {
            if (Count == 0)
            {
                DeleteSubNodes();
            }
            else
            {
                foreach (Pool<QuadTreeNode<T>>.Node node in nodes)
                {
                    node.Item.Prune();
                }
            }
        }
    }
}