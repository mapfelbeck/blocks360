﻿using System.Diagnostics;

namespace Utilities
{
    public class Timer
    {
        Stopwatch stopwatch;
        string format;
        const string defaultFormat = "{0}";

        public Timer()
        {
            stopwatch = new Stopwatch();
            format = "{0}";
        }

        public void Begin(string format)
        {
            this.format = format;
            stopwatch.Start();
        }

        public void Begin()
        {
            Begin(defaultFormat);
        }

        public double End(bool print)
        {
            double time;
            stopwatch.Stop();
            time = stopwatch.Elapsed.TotalSeconds;
            stopwatch.Reset();

            if (print)
            {
                System.Diagnostics.Trace.WriteLine(string.Format(format, time));
                //Console.WriteLine(format,time);
            }

            return time;
        }
    }
}
