﻿using Utilities;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Utilities
{
    public delegate void DrawDelegate();

    public class TextureRenderer
    {
        int targetHeight;
        int targetWidth;

        GraphicsDevice graphicsDevice;
        
        RenderTarget2D renderTarget;
        DepthStencilBuffer stencilBuffer;

        public TextureRenderer(GraphicsDevice device)
        {
            graphicsDevice = device;
        }

        public void SetDimensions(int height, int width)
        {
            this.targetHeight = height;
            this.targetWidth = width;

            CreateRenderTarget();
        }

        public Texture2D RenderTexture(DrawDelegate drawMethod)
        {
            //setup
            graphicsDevice.SetRenderTarget(0, renderTarget);

            DepthStencilBuffer oldStencilBuffer = graphicsDevice.DepthStencilBuffer;
            graphicsDevice.DepthStencilBuffer = stencilBuffer;

            //draw
            drawMethod();

            //cleanup
            graphicsDevice.SetRenderTarget(0, null);
            graphicsDevice.DepthStencilBuffer = oldStencilBuffer;

            Texture2D texture = renderTarget.GetTexture();

#if WINDOWS
            oldStencilBuffer.Dispose();
            oldStencilBuffer = null;
#endif
            /*renderTarget.Dispose();
            stencilBuffer.Dispose();
            renderTarget = null;
            stencilBuffer = null;*/

            return texture;
        }

        private void CreateRenderTarget()
        {
            //create buffers
            renderTarget =
                CreateRenderTarget(graphicsDevice, 1, graphicsDevice.PresentationParameters.BackBufferFormat);

            stencilBuffer =
                CreateDepthStencil(renderTarget, graphicsDevice.PresentationParameters.AutoDepthStencilFormat);
        }

        private RenderTarget2D CreateRenderTarget(GraphicsDevice device,
            int numberLevels, SurfaceFormat surface)
        {
            MultiSampleType type =
                device.PresentationParameters.MultiSampleType;

            // If the card can't use the surface format
            if (!GraphicsAdapter.DefaultAdapter.CheckDeviceFormat(
                DeviceType.Hardware,
                GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Format,
                TextureUsage.None,
                QueryUsages.None,
                ResourceType.RenderTarget,
                surface))
            {
                // Fall back to current display format
                surface = device.DisplayMode.Format;
            }
            // Or it can't accept that surface format 
            // with the current AA settings
            else if (!GraphicsAdapter.DefaultAdapter.CheckDeviceMultiSampleType(
                DeviceType.Hardware, surface,
                device.PresentationParameters.IsFullScreen, type))
            {
                // Fall back to no antialiasing
                type = MultiSampleType.None;
            }

            int width, height;
            CheckTextureSize(targetWidth, targetHeight, out width, out height);

            // Create our render target
            return new RenderTarget2D(device,
                width, height, numberLevels, surface,
                type, 0);
        }

        private bool CheckTextureSize(int width, int height,
            out int newwidth, out int newheight)
        {
            bool retval = false;

            GraphicsDeviceCapabilities Caps;
            Caps = GraphicsAdapter.DefaultAdapter.GetCapabilities(
                DeviceType.Hardware);

            if (Caps.TextureCapabilities.RequiresPower2)
            {
                retval = true;  // Return true to indicate the numbers changed

                // Find the nearest base two log of the current width, 
                // and go up to the next integer                
                double exp = Math.Ceiling(Math.Log(width) / Math.Log(2));
                // and use that as the exponent of the new width
                width = (int)Math.Pow(2, exp);
                // Repeat the process for height
                exp = Math.Ceiling(Math.Log(height) / Math.Log(2));
                height = (int)Math.Pow(2, exp);
            }
            if (Caps.TextureCapabilities.RequiresSquareOnly)
            {
                retval = true;  // Return true to indicate numbers changed
                width = Math.Max(width, height);
                height = width;
            }

            newwidth = Math.Min(Caps.MaxTextureWidth, width);
            newheight = Math.Min(Caps.MaxTextureHeight, height);
            return retval;
        }

        private DepthStencilBuffer CreateDepthStencil(
            RenderTarget2D target)
        {
            return new DepthStencilBuffer(target.GraphicsDevice, target.Width,
                target.Height, target.GraphicsDevice.DepthStencilBuffer.Format,
                target.MultiSampleType, target.MultiSampleQuality);
        }

        private DepthStencilBuffer CreateDepthStencil(
            RenderTarget2D target, DepthFormat depth)
        {
            if (GraphicsAdapter.DefaultAdapter.CheckDepthStencilMatch(
                DeviceType.Hardware,
                GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Format,
                target.Format,
                depth))
            {
                return new DepthStencilBuffer(target.GraphicsDevice,
                    target.Width, target.Height, depth,
                    target.MultiSampleType, target.MultiSampleQuality);
            }
            else
                return CreateDepthStencil(target);
        }
    }
}