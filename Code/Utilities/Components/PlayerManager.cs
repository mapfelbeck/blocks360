﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;

namespace Utilities
{
    public delegate void SignInCallback();

    internal enum SignInState{
        SignInRequested,
        SigningIn,
        Idle,
    }

    internal class SignInStateMachine : StateMachine<SignInState>
    {
        SignInCallback callback;
        const float initialSignInWaitTime = .5f;

        public SignInStateMachine(SignInCallback signInCallback)
            : base()
        {
            stateTable.Add(SignInState.SignInRequested, SignInRequestedState);
            stateTable.Add(SignInState.SigningIn, SigningInState);
            stateTable.Add(SignInState.Idle, IdleState);

            currentState = SignInState.Idle;
            desiredState = SignInState.Idle;
            currentDelegate = IdleState;

            callback = signInCallback;
        }

        public void SignInRequestedState(GameTime time, SubState subState)
        {
            switch (subState)
            {
                case SubState.Entry:
                    break;
                case SubState.Update:
                    if (!Guide.IsVisible)
                    {
                        SwitchState(SignInState.SigningIn);
                    }
                    break;
                case SubState.Exit:
                    Guide.ShowSignIn(1, false);
                    break;
                default:
                    break;
            }
        }

        public void SigningInState(GameTime time, SubState subState)
        {
            switch (subState)
            {
                case SubState.Entry:
                    break;
                case SubState.Update:
                    if (!Guide.IsVisible)
                    {
                        SwitchState(SignInState.Idle);
                    }
                    break;
                case SubState.Exit:
                    if (callback != null)
                    {
                        callback();
                    }
                    callback = null;
                    break;
                default:
                    break;
            }
        }

        public void IdleState(GameTime time, SubState subState)
        {
            switch (subState)
            {
                case SubState.Entry:
                    break;
                case SubState.Update:
                    break;
                case SubState.Exit:
                    break;
                default:
                    break;
            }
        }

        internal void RequestSignIn(SignInCallback signInCallback)
        {
            callback = signInCallback;
            SwitchState(SignInState.SignInRequested);
        }
    }

    public class PlayerManager : GameComponent
    {
        private static PlayerManager playerManager;
        protected static PlayerManager Instance{
            get{
                if (playerManager != null)
                {
                    return playerManager;
                }
                else
                {
                    throw new NullReferenceException("PlayerComponent must be Initialized");
                }
            }
        }

        private SignInStateMachine signInStateMachine;

        private PlayerManager(Game game, SignInCallback signInCallback)
            : base(game)
        {
            signInStateMachine = new SignInStateMachine(signInCallback);
        }

        public static void Initialize(Game game, SignInCallback signInCallback)
        {
            playerManager = new PlayerManager(game, signInCallback);

            if (game != null)
            {
                game.Components.Add(playerManager);
                playerManager.RegisterEvents();
            }
        }

        private void RegisterEvents()
        {
        }

        private void UnRegisterEvents()
        {
        }

        protected override void Dispose(bool disposing)
        {
            UnRegisterEvents();
            base.Dispose(disposing);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            signInStateMachine.Update(gameTime);
        }

        public static void ShowSignIn(SignInCallback signInCallback)
        {
            Instance.RequestSignIn(signInCallback);
        }

        protected void RequestSignIn(SignInCallback signInCallback)
        {
            signInStateMachine.RequestSignIn(signInCallback);
        }

        public static bool IsControllerSignedIn(PlayerIndex index)
        {
            SignedInGamer gamer = Gamer.SignedInGamers[index];

            return gamer != null;
        }

        public static bool PlayerCanBuyContent(PlayerIndex index)
        {
            SignedInGamer gamer = Gamer.SignedInGamers[index];

            if (gamer != null)
            {
                return gamer.IsSignedInToLive && gamer.Privileges.AllowPurchaseContent;
            }

            return false;
        }

        public static SignedInGamer GetPlayer(PlayerIndex index)
        {
            return Gamer.SignedInGamers[index];
        }
    }
}