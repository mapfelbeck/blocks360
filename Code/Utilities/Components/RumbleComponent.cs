﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace Utilities
{
    public class RumbleComponent : GameComponent
    {
        private static RumbleComponent rumbleComponent;
        static RumbleComponent Instance
        {
            get
            {
                if (rumbleComponent == null)
                {
                    throw new Exception("RumbleComponent must be Initialized first");
                }
                return rumbleComponent;
            }
        }

        public static bool RumbleActive
        {
            get { return Instance.active; }
            set
            {
                Instance.active = value;
                Instance.ResetRumble();
            }
        }

        public static PlayerIndex Player
        {
            get { return Instance.playerIndex; }
            set
            {
                Instance.playerIndex = value;
                Instance.ResetRumble();
            }
        }

        public static float ChangeDelta
        {
            get { return Instance.changeDelta; }
            set { Instance.changeDelta = value; }
        }

        bool active;

        //how many seconds does it take to go from 0f to 1f rumble rate
        float changeDelta;
        PlayerIndex playerIndex;
        
        float currentLeftRumble;
        float desiredLeftRumble;

        float currentRightRumble;
        float desiredRightRumble;

        private RumbleComponent(Game game)
            : base(game)
        {
            changeDelta = 2f;
            active = true;
            playerIndex = PlayerIndex.One;
            ResetRumble();
        }
        
        void ResetRumble()
        {
            foreach (PlayerIndex player in EnumTools.GetValues(typeof(PlayerIndex)))
            {
                GamePad.SetVibration(player, 0, 0);
            }

            desiredLeftRumble = 0f;
            desiredRightRumble = 0f;
            currentLeftRumble = 0f;
            currentRightRumble = 0f;
        }

        public static void SetRumbleDelta(float delta)
        {
            Instance.changeDelta = delta;
        }

        public static void SetPlayerIndex(PlayerIndex index)
        {
            Instance.playerIndex = index;
        }

        public static void SetRumble(float rate)
        {
            SetLeftRumble(rate);
            SetRightRumble(rate);
        }

        public static void SetLeftRumble(float rate)
        {
            Instance.desiredLeftRumble = rate;
        }

        public static void SetRightRumble(float rate)
        {
            Instance.desiredRightRumble = rate;
        }

        public static void Initialize(Game game)
        {
            rumbleComponent = new RumbleComponent(game);

            if (game != null)
            {
                game.Components.Add(rumbleComponent);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (active)
            {
                if (changeDelta > 0f)
                {
                    float maxRumbleChangePossible = elapsed / changeDelta;

                    float rumbleChangeDesired = desiredLeftRumble - currentLeftRumble;
                    float rumbleChangePossible = MathHelper.Clamp(
                        rumbleChangeDesired, -maxRumbleChangePossible, maxRumbleChangePossible);
                    currentLeftRumble = MathHelper.Clamp(currentLeftRumble + rumbleChangePossible, 0f, 1f);

                    rumbleChangeDesired = desiredRightRumble - currentRightRumble;
                    rumbleChangePossible = MathHelper.Clamp(
                        rumbleChangeDesired, -maxRumbleChangePossible, maxRumbleChangePossible);
                    currentRightRumble = MathHelper.Clamp(currentRightRumble + rumbleChangePossible, 0f, 1f);
                }
                else
                {
                    currentLeftRumble = desiredLeftRumble;
                    currentRightRumble = desiredRightRumble;
                }

                GamePad.SetVibration(playerIndex, currentLeftRumble, currentRightRumble);
            }
            else
            {
                GamePad.SetVibration(playerIndex, 0, 0);
            }
        }
    }
}