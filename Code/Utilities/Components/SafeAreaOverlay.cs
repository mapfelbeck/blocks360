#region File Description
//-----------------------------------------------------------------------------
// SafeAreaOverlay.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
#endregion

namespace Utilities
{
    /// <summary>
    /// Reusable component makes it easy to check whether your important
    /// graphics are positioned inside the title safe area, by superimposing
    /// a red border that marks the edges of the safe region.
    /// </summary>
    public class SafeAreaOverlay : DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Texture2D dummyTexture;

        float safeArea;
        public float SafeArea
        {
            get { return safeArea; }
            set { safeArea = value; }
        }

        Color translucentRed;

        // Compute four border rectangles around the edges of the safe area.
        Rectangle leftBorder;
        Rectangle rightBorder;
        Rectangle topBorder;
        Rectangle bottomBorder;

        /// <summary>
        /// Constructor.
        /// </summary>
        public SafeAreaOverlay(Game game)
            : base(game)
        {
            // Choose a high number, so we will draw on top of other components.
            DrawOrder = 1000;
            safeArea = .1f;
        }

        public override void Initialize()
        {
            base.Initialize();
            GraphicsDevice.DeviceReset += DeviceReset;
        }

        private void DeviceReset(object sender, EventArgs e)
        {
            CreateDrawRectangles();
        }
        /// <summary>
        /// Creates the graphics resources needed to draw the overlay.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Create a 1x1 white texture.
            dummyTexture = new Texture2D(GraphicsDevice, 1, 1);
            dummyTexture.SetData(new Color[] { Color.White });

            translucentRed = new Color(Color.Red, 0.5f);
            
            CreateDrawRectangles();
        }

        private void CreateDrawRectangles()
        {
            // Look up the current viewport and safe area dimensions.
            Viewport viewport = GraphicsDevice.Viewport;

            int viewportRight = viewport.X + viewport.Width;
            int viewportBottom = viewport.Y + viewport.Height;


            Rectangle safeAreaRectangle = new Rectangle(
                viewport.X + (int)(viewport.Width * safeArea), viewport.Y + (int)(viewport.Height * safeArea),
                (int)(viewport.Width * (1f - 2f * safeArea)), (int)(viewport.Height * (1f - 2f * safeArea)));

            // Compute four border rectangles around the edges of the safe area.
            leftBorder = new Rectangle(viewport.X,
                                                 viewport.Y,
                                                 safeAreaRectangle.X - viewport.X,
                                                 viewport.Height);

            rightBorder = new Rectangle(safeAreaRectangle.Right,
                                                  viewport.Y,
                                                  viewportRight - safeAreaRectangle.Right,
                                                  viewport.Height);

            topBorder = new Rectangle(safeAreaRectangle.Left,
                                                viewport.Y,
                                                safeAreaRectangle.Width,
                                                safeAreaRectangle.Top - viewport.Y);

            bottomBorder = new Rectangle(safeAreaRectangle.Left,
                                                   safeAreaRectangle.Bottom,
                                                   safeAreaRectangle.Width,
                                                   viewportBottom - safeAreaRectangle.Bottom);
        }
        
        /// <summary>
        /// Draws the title safe area.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(dummyTexture, leftBorder,   translucentRed);
            spriteBatch.Draw(dummyTexture, rightBorder,  translucentRed);
            spriteBatch.Draw(dummyTexture, topBorder,    translucentRed);
            spriteBatch.Draw(dummyTexture, bottomBorder, translucentRed);

            spriteBatch.End();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            GraphicsDevice.DeviceReset -= DeviceReset;
        }
    }
}
