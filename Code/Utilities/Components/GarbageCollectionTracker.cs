﻿using System;
using Microsoft.Xna.Framework;

namespace Utilities
{
    public class GarbageCollectionTracker : GameComponent
    {
        WeakReference gcTracker;
        float gcFreq;
        int gcFrames;

        public GarbageCollectionTracker(Game game)
            : base(game)
        {
            gcTracker = new WeakReference(new object());
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            gcFrames++;

            gcFreq += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (!gcTracker.IsAlive)
            {
                Console.WriteLine("GC: " + gcFreq.ToString() + ", " + gcFrames.ToString() + " frames");
                gcFreq = 0f;
                gcFrames = 0;
                gcTracker = new WeakReference(new object());
            }
        }
    }
}
