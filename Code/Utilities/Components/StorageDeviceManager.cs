﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.Diagnostics;
using System.Collections.Generic;

namespace Utilities
{
    public delegate void GetStorageCallback(StorageDevice device);

    internal enum StorageState
    {
        Idle,
        BeginShowSelector,
        EndShowSelector,
        BeginStoragePrompt,
        EndStoragePrompt,
    }

    internal class StorageStateMachine : StateMachine<StorageState>
    {
        StorageDevice device;
        public StorageDevice Device
        {
            get { return device; }
        }

        public bool StorageDeviceValid
        {
            get { return device != null && device.IsConnected; }
        }

        GetStorageCallback getDeviceCallback;
        IAsyncResult result;

        bool deviceSelectCancelled;

        public StorageStateMachine()
            : base()
        {
            stateTable.Add(StorageState.Idle, IdleState);
            stateTable.Add(StorageState.BeginShowSelector, BeginShowSelectorState);
            stateTable.Add(StorageState.EndShowSelector, EndShowSelectorState);
            stateTable.Add(StorageState.BeginStoragePrompt, BeginStoragePromptState);
            stateTable.Add(StorageState.EndStoragePrompt, EndStoragePromptState);

            currentState = StorageState.Idle;
            desiredState = StorageState.Idle;
            currentDelegate = IdleState;
            getDeviceCallback = null;

            StorageDevice.DeviceChanged += StorageChanged;
        }

        public void IdleState(GameTime time, SubState subState)
        {
            switch (subState)
            {
                case SubState.Entry:
                    result = null;
                    break;
                case SubState.Update:
                    break;
                case SubState.Exit:
                    break;
                default:
                    break;
            }
        }

        public void BeginShowSelectorState(GameTime time, SubState subState)
        {
            switch (subState)
            {
                case SubState.Entry:
                    break;
                case SubState.Update:
                    if (!Guide.IsVisible)
                    {
                        result = Guide.BeginShowStorageDeviceSelector(null, "Select a storage device");
                        SwitchState(StorageState.EndShowSelector);
                    }
                    break;
                case SubState.Exit:
                    break;
                default:
                    break;
            }
        }

        public void EndShowSelectorState(GameTime time, SubState subState)
        {
            switch (subState)
            {
                case SubState.Entry:
                    break;
                case SubState.Update:
                    if (result.IsCompleted)
                    {
                        device = Guide.EndShowStorageDeviceSelector(result);
                        if (device != null)
                        {
                            deviceSelectCancelled = false;
                        }
                        else
                        {
                            deviceSelectCancelled = true;
                        }
                        if (getDeviceCallback != null)
                        {
                            try
                            {
                                getDeviceCallback(device);
                            }
                            catch
                            {
                                Trace.WriteLine("get device callback failed");
                            }
                        }
                        SwitchState(StorageState.Idle);
                    }
                    break;
                case SubState.Exit:
                    getDeviceCallback = null;
                    break;
                default:
                    break;
            }
        }

        public void BeginStoragePromptState(GameTime time, SubState subState)
        {
            string newStorageMessage = "The storage devices available changed. Would you like to select a new storage device? If there's only one avaliable it will be automatically selected for you.";
            List<string> buttons = new List<string>();
            buttons.Add("Yes");
            buttons.Add("No");
            switch (subState)
            {
                case SubState.Entry:
                    break;
                case SubState.Update:
                    if (!Guide.IsVisible)
                    {
                        result = Guide.BeginShowMessageBox("Storage Changed",
                            newStorageMessage,
                            buttons,
                            0,
                            MessageBoxIcon.None,
                            null,
                            null);
                        SwitchState(StorageState.EndStoragePrompt);
                    }
                    break;
                case SubState.Exit:
                    break;
                default:
                    break;
            }
        }

        public void EndStoragePromptState(GameTime time, SubState subState)
        {
            switch (subState)
            {
                case SubState.Entry:
                    break;
                case SubState.Update:
                    if (!Guide.IsVisible)
                    {
                        int? choice = Guide.EndShowMessageBox(result);
                        if (choice.HasValue && choice.Value == 0)
                        {
                            SwitchState(StorageState.BeginShowSelector);
                        }
                        else
                        {
                            deviceSelectCancelled = true;
                            SwitchState(StorageState.Idle);
                        }
                    }
                    break;
                case SubState.Exit:
                    break;
                default:
                    break;
            }
        }

        protected void StorageChanged(object sender, EventArgs e)
        {
            //Trace.WriteLine("storage changed");
            if (!deviceSelectCancelled)
            {
                if (device == null || (device != null && !device.IsConnected))
                {
                    if (currentState == StorageState.Idle)
                    {
                        SwitchState(StorageState.BeginStoragePrompt);
                    }
                }
            }
        }

        internal void RequestStorageDevice(GetStorageCallback callback)
        {
            if (StorageDeviceValid && callback!= null)
            {
                callback(device);
            }
            else
            {
                getDeviceCallback = callback;
                SwitchState(StorageState.BeginShowSelector);
            }
        }
    }

    public class StorageDeviceManager : GameComponent
    {
        private static StorageDeviceManager storageDeviceManager;
        protected static StorageDeviceManager Instance
        {
            get
            {
                if (storageDeviceManager != null)
                {
                    return storageDeviceManager;
                }
                else
                {
                    throw new NullReferenceException("StorageDeviceComponent must be Initialized");
                }
            }
        }

        StorageStateMachine storageStateMachine;

        private StorageDeviceManager(Game game)
            : base(game)
        {
            storageStateMachine = new StorageStateMachine();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            storageStateMachine.Update(gameTime);
        }

        public static void Initialize(Game game)
        {
            storageDeviceManager = new StorageDeviceManager(game);

            if (game != null)
            {
                game.Components.Add(storageDeviceManager);
            }
        }

        public static bool StorageDeviceReady()
        {
            return Instance.storageStateMachine.StorageDeviceValid;
        }

        /// <summary>
        /// Get the storage device immediatly whether it's ready or not
        /// </summary>
        /// <returns></returns>
        public static StorageDevice GetStorageDevice()
        {
            return Instance.storageStateMachine.Device;
        }

        /// <summary>
        /// Get the Storage device asyncronousny
        /// </summary>
        /// <param name="callback"></param>
        public static void RequestStorageDevice(GetStorageCallback callback)
        {
            Instance.storageStateMachine.RequestStorageDevice(callback);
        }
    }
}